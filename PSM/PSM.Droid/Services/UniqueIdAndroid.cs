﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Net.Wifi;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Telephony;
using Android.Views;
using Android.Widget;
using Java.Net;
using Java.Util;
using PSM.Helpers.DeviceIdentifier;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.Droid.Services.UniqueIdAndroid))]
namespace PSM.Droid.Services
{
    public class UniqueIdAndroid : AppCompatActivity, IDevice
    {
        public string GetIdentifier()
        {
            string IMEI = string.Empty;
            //try
            //{
            //    var telephonyManager = (TelephonyManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.TelephonyService);
            //    IMEI = telephonyManager.DeviceId == null ? telephonyManager.GetMeid(0).ToString() : telephonyManager.DeviceId.ToString();
            //}
            //catch (Exception e)
            //{
            //    IMEI = e.ToString();
            //}

            try
            {
                if (Build.VERSION.SdkInt > BuildVersionCodes.P)
                {
                    IMEI = Settings.Secure.GetString(Application.Context.ContentResolver, Settings.Secure.AndroidId).ToString().ToUpper();
                }
                else if (Build.VERSION.SdkInt >= BuildVersionCodes.M && Build.VERSION.SdkInt <= BuildVersionCodes.P)
                {
                    //Mayores a Android 6.0
                    var tm = (TelephonyManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.TelephonyService);
                    int permissionCheck = Convert.ToInt32(ContextCompat.CheckSelfPermission(Application.Context, Android.Manifest.Permission.ReadPhoneState));

                    if (permissionCheck != PermissionChecker.PermissionGranted)
                    {
                        ActivityCompat.RequestPermissions(this, new string[] { Android.Manifest.Permission.ReadPhoneState }, 0);
                        IMEI = "";
                    }
                    else
                    {
                        IMEI = tm.GetImei(0);
                    }
                }
                else if (Build.VERSION.SdkInt < BuildVersionCodes.M)
                {
                    var tm = (TelephonyManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.TelephonyService);
                    IMEI = tm.GetImei(0);
                }
            }
            catch (Exception ex)
            {
                IMEI = ex.ToString();
            }

            return IMEI;
        }

        public string GetPhysicalAddress()
        {
            var result = string.Empty;
            var all = Collections.List(NetworkInterface.NetworkInterfaces);

            foreach (var inter in all)
            {
                var macBytes = (inter as NetworkInterface).GetHardwareAddress();

                if (macBytes == null) continue;

                var sb = new StringBuilder();
                foreach (var b in macBytes)
                {
                    sb.Append((b & 0xFF).ToString("X2") + "-");
                }

                result = sb.ToString().Remove(sb.Length - 1);
            }

            return result;
        }

        public List<Tuple<string, string>> GetInstalledApps()
        {
            var apps = Application.Context.PackageManager.GetInstalledApplications(PackageInfoFlags.MatchAll).ToList();
            var l = apps
                .Where(s => !AppsNames.Any(d => d.StartsWith(s.ClassName ?? "")))
                .Where(s => !AppsNames.Any(d => d.StartsWith(s.ProcessName ?? "")))
                .Select(s => new Tuple<string, string>(s.ClassName, s.ProcessName))
                .ToList();
            return l;
        }

        static List<string> AppsNames = new List<string>
        {
            "com.android.phone",
            "com.lge.theme.superbatterysaving",
            "com.hy.system.fontserver",
            "com.android.LGSetupWizard",
            "com.android.cts.priv.ctsshim",
            "com.google.android.apps.youtube.app.YouTubeApplication",
            "com.google.android.youtube",
            "com.google.android.ext.services",
            "com.microsoft.office.sfb.SfBApp",
            "com.microsoft.office.lync15",
            "com.lge.sizechangable.weather.platform.application.WeatherServiceApplication",
            "com.lge.sizechangable.weather.platform",
            "com.google.android.apps.gsa.binaries.velvet.app.VelvetApplication",
            "com.google.android.googlequicksearchbox",
            "com.lge.theme.black",
            "com.lge.theme.titan",
            "com.lge.theme.white",
            "com.android.providers.calendar",
            "org.telegram.messenger.ApplicationLoader","org.telegram.messenger",
            "com.google.android.apps.docs.editors.kix.configurations.kixwithchangeling.stable.KixWithChangelingStableApplication",
            "com.lge.systemservice.service.LGSystemServerApplication"
,"com.google.android.onetimeinitializer"
,"com.google.android.ext.shared"
,"com.android.wallpapercropper"
,"com.lge.appbox.client.AppBoxApplication"
,"com.qualcomm.qti.autoregistration"
,"com.lge.lgdmsclient"
,"com.android.documentsui"
,"com.android.externalstorage"
,"com.lge.atservice"
,"com.android.htmlviewer"
,"com.lge.app.floating.res"
,".dataservices"
            ,"com.android.companiondevicemanager"
,"com.google.android.apps.docs.editors.sheets"
,"com.lge.sui.widget"
            ,"com.google.android.apps.docs.editors.slides"
,"android.process.media"
            ,"com.google.android.apps.messaging.release.BugleReleaseApplication"
,"com.lge.camera"
,"com.lge.touchcontrol"
,"com.lge.effect"
,"com.lge.eltest"
,"com.lge.LGSetupView"
,"com.lge.homeselector"
, "com.lge.ime.solution.text"
, "com.lge.springcleaning"
, "com.lge.gnsstest"
,"com.lge.fmradio"
,"com.google.android.configupdater"
,"com.android.defcontainer"

        };
    }
}