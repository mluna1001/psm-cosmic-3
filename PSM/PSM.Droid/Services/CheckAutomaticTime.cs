﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PSM.Helpers.Services;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.Droid.Services.CheckAutomaticTime))]
namespace PSM.Droid.Services
{
    public class CheckAutomaticTime : ICheckAutomaticTime
    {
        internal static Context Context;

        public bool IsAutomaticTime()
        {
            var variable = Android.Provider.Settings.Global.GetInt(Context.ContentResolver, Android.Provider.Settings.Global.AutoTime, 0);
            return variable == 1 ? true : false;
        }

        public bool IsAutomaticZone()
        {
            var variable = Android.Provider.Settings.Global.GetInt(Context.ContentResolver, Android.Provider.Settings.Global.AutoTimeZone, 0);
            return variable == 1 ? true : false;
        }
    }
}