﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PSM.Helpers.Services;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.Droid.Services.StorageDevice))]
namespace PSM.Droid.Services
{
    public class StorageDevice : IStorageDevice
    {
        internal static Context Context;
        internal static Intent Intent;

        public string GetFullStorageDevive()
        {
            var available = AvailableInternalMemorySize();
            var total = TotalInternalMemorySize();
            return $"Almacenamiento: { available } de { total }.";
        }

        static string TotalInternalMemorySize()
        {
            var path = Android.OS.Environment.ExternalStorageDirectory.Path;
            StatFs stat = new StatFs(path);
            long blockSize = stat.BlockSizeLong;
            long totalBlocks = stat.BlockCountLong;
            return formatSize(totalBlocks * blockSize);
        }

        static string AvailableInternalMemorySize()
        {
            var path = Android.OS.Environment.ExternalStorageDirectory.Path;
            StatFs stat = new StatFs(path);
            long blockSize = stat.BlockSizeLong;
            long availableBlocks = stat.AvailableBlocksLong;
            return formatSize(availableBlocks * blockSize);
        }

        public static String formatSize(long size)
        {
            String suffix = null;

            if (size >= 1024)
            {
                suffix = "KB";
                size /= 1024;
                if (size >= 1024)
                {
                    suffix = "MB";
                    size /= 1024;
                }
            }

            StringBuilder resultBuffer = new StringBuilder(size.ToString());

            int commaOffset = resultBuffer.Length - 3;
            while (commaOffset > 0)
            {
                resultBuffer.Insert(commaOffset, ',');
                commaOffset -= 3;
            }

            if (suffix != null) resultBuffer.Append(suffix);
            return resultBuffer.ToString();
        }

        public string GetBatteryLevel()
        {
            var intent = new IntentFilter(Intent.ActionBatteryChanged);
            var obj = new PSMConnectionReceiver();
            obj.OnReceive(Context, Intent);
            var res = obj.PercentageStatus;

            return $"Batería del dispositivo: { res }";
        }
    }

    [BroadcastReceiver]
    [IntentFilterAttribute(new[] { Intent.ActionBatteryChanged, Intent.ActionBatteryLow, Intent.ActionBatteryOkay, Intent.ActionPowerConnected, Intent.ActionPowerDisconnected })]
    public class PSMConnectionReceiver : BroadcastReceiver
    {
        public string PercentageStatus { get; set; }

        public override void OnReceive(Context context, Intent intent)
        {
            int deviceStatus = intent.GetIntExtra(BatteryManager.ExtraStatus, -1);
            int level = intent.GetIntExtra(BatteryManager.ExtraLevel, -1);
            int scale = intent.GetIntExtra(BatteryManager.ExtraScale, -1);
            int batteryLevel = (int)(((float)level / (float)scale) * 100.0f);

            if (deviceStatus == (int)BatteryStatus.Charging)
            {
                PercentageStatus = "Status: Cargando en " + batteryLevel + " %";
            }
            if (deviceStatus == (int)BatteryStatus.Discharging)
            {
                PercentageStatus = "Status: Descargando en " + batteryLevel + " %";
            }
            if (deviceStatus == (int)BatteryStatus.Full)
            {
                PercentageStatus = "Status: Batería llena en " + batteryLevel + " %";
            }
            if (deviceStatus == (int)BatteryStatus.Unknown)
            {
                PercentageStatus = "Status: Desconocido en " + batteryLevel + " %";
            }
            if (deviceStatus == (int)BatteryStatus.NotCharging)
            {
                PercentageStatus = "Status: Batería no cargada en " + batteryLevel + " %";
            }
        }
    }
}