﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using PSM.Helpers.Services;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.Droid.Services.FileProcess))]
namespace PSM.Droid.Services
{
    public class FileProcess : IFileProcess
    {
        public JsonSerializer GetTextWriter(object objeto)
        {
            using (TextWriter textWriter = File.CreateText("LocalJsonFile.json"))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(textWriter, objeto);

                return serializer;
            }
        }
    }
}