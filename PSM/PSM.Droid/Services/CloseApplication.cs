﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PSM.Helpers.Services;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.Droid.Services.CloseApplication))]
namespace PSM.Droid.Services
{
    public class CloseApplication : ICloseApplication
    {
        internal static Context Context;

        public void CloseApp()
        {
            var activity = (Activity)Context;
            activity.FinishAffinity();
        }
    }
}