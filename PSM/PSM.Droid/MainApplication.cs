using System;

using Android.App;
using Android.OS;
using Android.Runtime;
using Plugin.CurrentActivity;
using Android.Content;
using Microsoft.AspNet.SignalR.Client;
using ChatApp;
using ChatApp.ORM;
using static ChatApp.ChatClient;
using Android.App.Usage;
using System.Collections.Generic;

namespace PSM.Droid
{
	//You can specify additional application information in this attribute
    [Application]
    public class MainApplication : Application, Application.IActivityLifecycleCallbacks
    {
        public MainApplication(IntPtr handle, JniHandleOwnership transer)
          :base(handle, transer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            RegisterActivityLifecycleCallbacks(this);
            //A great place to initialize Xamarin.Insights and Dependency Services!


            //var datei = (long.MaxValue + DateTime.Parse("2018/07/07").ToBinary()) / 10000;
            //var datef = (long.MaxValue + DateTime.Now.ToBinary()) / 10000;
            //63666829092144
            //List<UsageStats> queryUsageStats = usageStatsManager.queryUsageStats(UsageStatsManager., beginCal.getTimeInMillis(), endCal.getTimeInMillis());

            //GetStatus();

            ////long datei = (long)(DateTime.Parse("2017/07/08") - DateTime.MinValue).TotalMilliseconds;
            ////long datef = (long)(DateTime.Now - DateTime.MinValue).TotalMilliseconds;
            //long datei = DateTime.Parse("2018/07/08").Ticks / TimeSpan.TicksPerMillisecond;
            //long datef = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            

            //UsageStatsManager manager = (UsageStatsManager)GetSystemService(Context.UsageStatsService);
            //var stats = manager.QueryUsageStats(UsageStatsInterval.Best, datei, datef);
            //if (stats == null)
            //{
            //    return;
            //}
            //foreach (UsageStats stat in stats)
            //{
            //    String packageName = stat.PackageName;
                
            //}

        }
        private void GetStatus()
        {
            if (hasPermission())
            {

            }
            else
            {
                StartActivity(new Intent(Android.Provider.Settings.ActionUsageAccessSettings));

                //AppOpsManager appOps = (AppOpsManager)GetSystemService(Context.AppOpsService);
                //var mode = appOps.CheckOpNoThrow(AppOpsManager.OpstrGetUsageStats,
                //        Android.OS.Process.MyUid(), PackageName);
            }
        }
        private bool hasPermission()
        {
            AppOpsManager appOps = (AppOpsManager)GetSystemService(Context.AppOpsService);
            var mode = appOps.CheckOpNoThrow(AppOpsManager.OpstrGetUsageStats, Process.MyUid(), PackageName);
            return mode == AppOpsManager.ModeAllowed;
        }

        public override void OnTerminate()
        {
            base.OnTerminate();
            UnregisterActivityLifecycleCallbacks(this);
        }

        public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
        {
            try
            {
                CrossCurrentActivity.Current.Activity = activity;
            }
            catch (Exception e)
            {

            }
        }

        public void OnActivityDestroyed(Activity activity)
        {
        }

        public void OnActivityPaused(Activity activity)
        {
        }

        public void OnActivityResumed(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
        {
        }

        public void OnActivityStarted(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivityStopped(Activity activity)
        {
        }
    }
}