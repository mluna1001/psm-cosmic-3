﻿using System;
using System.Linq;
using Android;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Plugin.Permissions;
using Android.Content;
using System.Threading.Tasks;
using ChatApp;
using ChatApp.Droid;
using Microsoft.AppCenter.Crashes;
using ZXing.Mobile;
using PSM.Droid.Services;

namespace PSM.Droid
{
    [Activity(Label = "PSM", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        Task<bool> statuspermisos;

        protected override void OnCreate(Bundle bundle)
        {
            statuspermisos = TryGetPermission();
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(bundle);

            Xamarin.Forms.Forms.Init(this, bundle);
            Xamarin.FormsMaps.Init(this, bundle);

            CheckAutomaticTime.Context = this;
            CloseApplication.Context = this;
            StorageDevice.Context = this;
            StorageDevice.Intent = Intent;

            //-->Inicia el  scanner
            MobileBarcodeScanner.Initialize(this.Application);

            Intent intent = new Intent(this, typeof(AppCenterService));
            StartService(intent);

            ChatInit.Context = this;

            ChatConfig.Init = ChatInit.Init;
            ChatConfig.Send = ChatSocket.Send;
            ChatConfig.SendToGroup = ChatSocket.SendToGroup;
            ChatConfig.SetGrupos = ChatInit.SetGrupos;

            if (statuspermisos.Result)
            {
                LoadApplication(new App());
            }
            else if ((int)Build.VERSION.SdkInt < 23)
            {
                LoadApplication(new App());
            }
        }

        async Task<bool> TryGetPermission()
        {
            if ((int)Build.VERSION.SdkInt >= 23)
            {
                return await GetPermissionAsync();
            }
            return false;
        }

        const int RequestLocationId = 0;

        readonly string[] PermissionGroupLocation =
        {
            Manifest.Permission.WriteExternalStorage,
            Manifest.Permission.ReadPhoneState,
            Manifest.Permission.AccessFineLocation,
            Manifest.Permission.Camera,
            Manifest.Permission.Internet,
            Manifest.Permission.ReadExternalStorage,
        };

        async Task<bool> GetPermissionAsync()
        {
            var status = false;
            const string permission = Manifest.Permission.WriteExternalStorage;
            const string ubicationPermission = Manifest.Permission.AccessFineLocation;
            const string cameraPermission = Manifest.Permission.Camera;
            const string readPhoneStatePermission = Manifest.Permission.ReadPhoneState;
            const string internetStatePermission = Manifest.Permission.Internet;

            if ((CheckSelfPermission(permission) == (int)Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(ubicationPermission) == (int)Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(cameraPermission) == (int)Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(readPhoneStatePermission) == (int)Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(internetStatePermission) == (int)Android.Content.PM.Permission.Granted)
               )
            {
                //LoadApplication(new App());
                return true;
            }

            if (ShouldShowRequestPermissionRationale(permission))
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Permisos");
                alert.SetMessage("PSM requiere permisos para continuar");
                alert.SetPositiveButton("Conceder permisos", (senderAlert, args) =>
                {
                    RequestPermissions(PermissionGroupLocation, RequestLocationId);
                    status = true;
                });
                alert.SetNegativeButton("Cancelar", (senderAlert, args) =>
                {
                    Toast.MakeText(this, "Permisos denegados", ToastLength.Short).Show();
                    status = false;
                });

                Dialog dialog = alert.Create();
                dialog.Show();

                return status;
            }
            RequestPermissions(PermissionGroupLocation, RequestLocationId);
            return false;
        }

        public override void OnLowMemory()
        {
            //base.OnLowMemory();
            GC.Collect();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            //PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);


            switch (requestCode)
            {
                case RequestLocationId:
                    if (grantResults[0] == (int)Android.Content.PM.Permission.Granted && grantResults[1] == (int)Android.Content.PM.Permission.Granted)
                    {
                        LoadApplication(new App());
                    }
                    else
                    {
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.SetTitle("Permisos");
                        alert.SetMessage("PSM requiere permisos para continuar, al ser denegados la aplicación se cerrará");
                        alert.SetPositiveButton("Aceptar", (senderAlert, args) =>
                        {
                            CloseApplication close = new CloseApplication();
                            close.CloseApp();
                        });

                        Dialog dialog = alert.Create();
                        dialog.Show();
                    }
                    break;
                default:
                    break;
            }
        }

        protected override void OnDestroy()
        {
            try
            {
                base.OnDestroy();
                var self = this;
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
            }
        }
    }
}

