﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Push;

namespace PSM.Droid
{
    [Service]
    public class AppCenterService : Service
    {
        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            Push.SetSenderId("549203639282");
            Push.PushNotificationReceived += Push_PushNotificationReceived;
            return StartCommandResult.Sticky;
        }

        private void Push_PushNotificationReceived(object sender, PushNotificationReceivedEventArgs e)
        {
            Notification.Builder builder = new Notification.Builder(this)
                .SetContentTitle(e.Title)
                .SetContentText(e.Message)
                .SetSmallIcon(Resource.Drawable.icon);
            Notification notification = builder.Build();
            NotificationManager notificationManager = GetSystemService(Android.Content.Context.NotificationService) as NotificationManager;
            notificationManager.Notify(e.GetHashCode(), notification);
        }
    }
}