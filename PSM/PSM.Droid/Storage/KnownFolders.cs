﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PSM.Droid
{
	public class KnownFolders
	{

		public static StorageFolder Documents {
			get{
				return GetFolder (Folder.Documents);
			}
		}

		public static StorageFolder Downloads {
			get{
				return GetFolder (Folder.Downloads);
			}
		}

		public static StorageFolder Music {
			get{
				return GetFolder (Folder.Music);
			}
		}

		public static StorageFolder Pictures {
			get{
				return GetFolder (Folder.Pictures);
			}
		}

		public static StorageFolder CameraRoll{
			get{
				return GetFolder (Folder.CameraRoll);
			}
		}

		public static StorageFolder Movies {
			get{
				return GetFolder (Folder.Movie);
			}
		}

        public static StorageFolder Notifications
        {
            get
            {
                return GetFolder(Folder.Notifications);
            }
        }

        public static StorageFolder Podcasts
        {
            get
            {
                return GetFolder(Folder.Podcasts);
            }
        }

        public static StorageFolder Ringtones
        {
            get
            {
                return GetFolder(Folder.Ringtones);
            }
        }

        public static StorageFolder Alarms
        {
            get
            {
                return GetFolder(Folder.Alarms);
            }
        }

        public static StorageFolder Data
        {
            get
            {
                return GetFolder(Folder.Data);
            }
        }

        private static StorageFolder GetFolderByDirectory(string directory)
        {
            string path = Path.Combine(directory);
            StorageFolder storagefolder = new StorageFolder();
            string[] element = path.Split('/');
            string foldername = element[element.Length - 1];
            if (Directory.Exists(path))
            {
                storagefolder.FullPath = path;
                storagefolder.Name = foldername;
            }
            else
            {
                throw new DirectoryNotFoundException("No existe el directorio " + foldername);
            }
            return storagefolder;
        }

        private static StorageFolder GetFolderByDirectory(Java.IO.File directory)
        {
            return GetFolderByDirectory(directory.AbsolutePath);
        }

		public static StorageFolder InternalStorage{
			get{
				return GetFolderByDirectory(Android.OS.Environment.ExternalStorageDirectory);
            }
		}
        
        public static List<StorageFolder> SDCard
        {
            get
            {
                var storage = StorageFolder.GetFolderFromPath("/storage/");
                var folders = storage.GetFolders();
                return folders.Where(e => !e.Name.Equals("emulated")).ToList();
            }
        }

        public static StorageFolder System{
			get{
                return GetFolderByDirectory(Android.OS.Environment.RootDirectory);
            }
		}

		private static StorageFolder GetFolder(Folder folder = Folder.Downloads){
            switch (folder)
            {
                case Folder.Downloads:
                    var downloads = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryDownloads);
                    return StorageFolder.GetFolderFromPath(downloads);

                case Folder.Movie:
                    var movie = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryMovies);
                    return StorageFolder.GetFolderFromPath(movie);

                case Folder.Music:
                    var music = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryMusic);
                    return StorageFolder.GetFolderFromPath(music);

                case Folder.Pictures:
                    var pictures = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryPictures);
                    return StorageFolder.GetFolderFromPath(pictures);

                case Folder.CameraRoll:
                    var cameraroll = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryDcim);
                    return StorageFolder.GetFolderFromPath(cameraroll);

                case Folder.Documents:
                    var documents = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryDocuments);
                    return StorageFolder.GetFolderFromPath(documents);

                case Folder.Notifications:
                    var notifications = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryNotifications);
                    return StorageFolder.GetFolderFromPath(notifications);

                case Folder.Alarms:
                    var alarms = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryAlarms);
                    return StorageFolder.GetFolderFromPath(alarms);

                case Folder.Podcasts:
                    var podcasts = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryPodcasts);
                    return StorageFolder.GetFolderFromPath(podcasts);

                case Folder.Ringtones:
                    var ringtones = Path.Combine(InternalStorage.FullPath, Android.OS.Environment.DirectoryRingtones);
                    return StorageFolder.GetFolderFromPath(ringtones);

                case Folder.Data:
                    return GetFolderByDirectory(Android.OS.Environment.DataDirectory);

                default:
                    return KnownFolders.InternalStorage;
            }
		}
	}

	public enum Folder{
		Downloads, Music, Pictures, Documents, Movie, CameraRoll, Notifications, Alarms, Podcasts, Ringtones,
        Data
    }
}

