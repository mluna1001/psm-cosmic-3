﻿using System;

namespace PSM.Droid
{
	public class PathResolver
	{
		public PathResolver()
		{
		}

		public static string GetPathFor(StorageFolder folder, string filename)
		{
			StorageFile file = null;
			try
			{
				file = folder.GetFile(filename);
			}
			finally { }

			if (file != null)
			{
				return file.FullPath;
			}
			return string.Empty;
		}

        /*
		public static string GetPathFor(string foldername, string filename)
		{
			StorageFolder folder = null;
			StorageFile file = null;
			try
			{
				folder = KnownFolders.SDCard.CreateFolder(foldername, CreationCollisionOption.OpenIfExists);
				file = folder.GetFile(filename);
			}
			catch
			{

			}

			if (file != null)
			{
				return file.FullPath;
			}
			return string.Empty;
		}
        */
	}
}
