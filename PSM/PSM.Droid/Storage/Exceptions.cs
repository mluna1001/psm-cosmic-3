﻿using System;
namespace PSM.Droid
{
	public class FileExistsException : Exception
	{
		public FileExistsException(string message) : base(message)
		{
			
		}
	}
}
