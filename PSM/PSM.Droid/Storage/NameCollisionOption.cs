﻿using System;

namespace PSM.Droid
{
	public enum NameCollisionOption
	{
		GenerateUniqueName = 0,
		ReplaceExisting = 1,
		FailIfExists = 2
	}
}

