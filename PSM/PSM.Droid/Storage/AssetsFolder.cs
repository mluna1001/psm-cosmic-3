using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;

namespace PSM.Droid
{
    public class AssetFile : StorageFile
    {
        private Stream FileOpen { get; set; }

        public AssetFile(string filename, Stream stream)
        {
            this.Name = filename;
            string[] fileandformat = filename.Split('.');
            if (fileandformat.Length > 1)
            {
                this.DisplayName = fileandformat[0];
                this.DisplayType = fileandformat[1];
            }
            this.FullPath = "AndroidApp/Assets";
            this.DateCreated = DateTime.Now;
            this.Attributes = FileAttributes.Archive;
            this.FileOpen = stream;
        }

        public override StorageFile CopyTo(StorageFolder folderdestination)
        {
            return this.CopyTo(folderdestination, this.Name, NameCollisionOption.FailIfExists);
        }

        public override StorageFile CopyTo(StorageFolder folderdestination, string filenamewithextension)
        {
            return this.CopyTo(folderdestination, filenamewithextension, NameCollisionOption.FailIfExists);
        }

        public override StorageFile CopyTo(StorageFolder folderdestination, string filenamewithextension, NameCollisionOption option)
        {
            string outfilepath = Path.Combine(folderdestination.FullPath, filenamewithextension);
            if (option == NameCollisionOption.FailIfExists)
            {
                if (File.Exists(outfilepath))
                {
                    throw new FileExistsException("El archivo que tratas de copiar ya existe");
                }
            }
            else
            {
                if (option == NameCollisionOption.GenerateUniqueName)
                {
                    outfilepath = Path.Combine(folderdestination.FullPath, UniqueString() + filenamewithextension);
                }
                else
                {
                    try
                    {
                        File.Delete(outfilepath);
                    }
                    catch
                    {

                    }
                }
            }

            FileStream stream = File.OpenWrite(outfilepath);
            FileOpen.CopyTo(stream);
            this.FullPath = outfilepath;
            FileOpen.Close();
            stream.Close();
            if (File.Exists(outfilepath))
            {
                return StorageFile.GetFileFromPath(outfilepath);
            }
            else
            {
                throw new FileNotFoundException("No fue posible copiar el archivo");
            }
        }

        private string UniqueString()
        {
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            GuidString = GuidString.Replace("=", "");
            GuidString = GuidString.Replace("+", "");
            return GuidString;
        }

    }

    public class AssetsFolder
    {
        Context _context;

        public AssetsFolder(Context context)
        {
            _context = context;
        }
        
        public List<AssetFile> GetFiles()
        {
            List<AssetFile> files = new List<AssetFile>();
            string[] assets = _context.Assets.List("");
            foreach (var asset in assets)
            {
                AssetFile file = GetFile(asset);
                files.Add(file);
            }
            return files;
        }

        public AssetFile GetFile(string filename)
        {
            Stream stream = _context.Assets.Open(filename, Android.Content.Res.Access.Unknown);
            AssetFile file = new AssetFile(filename, stream);
            return file;
        }
    }
}