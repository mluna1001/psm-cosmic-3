﻿using Android.Content;
using System;
using System.IO;

namespace PSM.Droid
{
	public class StorageFile
	{

		public string FullPath { 
			get;
			set;
		}

		public string Name {
			get;
			set;
		}

		public string DisplayName {
			get;
			set;
		}

		public string DisplayType {
			get;
			set;
		}

		public DateTime DateCreated {
			get;
			set;
		}

		public FileAttributes Attributes {
			get;
			set;
		}

        public string ParentDirectory
        {
            get
            {
                string parentdirecty = "";
                var pathsplit = FullPath.Split('/');
                string[] elements = new string[pathsplit.Length - 1];
                for (int i = 0; i < pathsplit.Length - 1; i++)
                {
                    elements[i] = pathsplit[i];
                }
                parentdirecty = "/" + System.IO.Path.Combine(elements);
                return parentdirecty;
            }
        }

		public StorageFile ()
		{
			
		}

		public virtual StorageFile CopyTo(StorageFolder folderdestination){
			return CopyTo (folderdestination, Name);
		}

		public virtual StorageFile CopyTo(StorageFolder folderdestination, string filenamewithextension){
			return CopyTo (folderdestination, filenamewithextension, NameCollisionOption.FailIfExists);
		}

		public virtual StorageFile CopyTo(StorageFolder folderdestination, string filenamewithextension, NameCollisionOption option){
            string outfilepath = Path.Combine(folderdestination.FullPath, filenamewithextension);
            if (Exists()) {
				if (option == NameCollisionOption.FailIfExists) {
					if (File.Exists (outfilepath)) {
						throw new Exception ("El archivo que tratas de copiar ya existe");
					}
				} else {
					if (option == NameCollisionOption.GenerateUniqueName) {
						outfilepath = Path.Combine (folderdestination.FullPath, UniqueString() + filenamewithextension);
					} else {
						File.Delete (outfilepath);
					}
				}
			}
            File.Copy(FullPath, outfilepath);
            if (File.Exists(outfilepath))
            {
                return StorageFile.GetFileFromPath(outfilepath);
            }
            else
            {
                throw new FileNotFoundException("No fue posible copiar el archivo");
            }
		}

		public bool Delete(){
			if (Exists()) {
				File.Delete (FullPath);
                return true;
			}
            return false;
		}

		public static StorageFile GetFileFromPath(string path){
            try
            {
                string[] element = path.Split('/');
                string filenameext = element[element.Length - 1];
                string[] elementfile = filenameext.Split('.');
                string filename = "";
                string ext = "";
                if (elementfile.Length > 2)
                {
                    filename = "";
                    for (int i = 0; i < elementfile.Length - 1; i++)
                    {
                        filename += elementfile[i];
                    }
                    ext = elementfile[elementfile.Length - 1];
                }
                else
                {
                    filename = elementfile[0];
                    ext = elementfile[elementfile.Length - 1];
                }
                var filedatetime = File.GetCreationTime(path);
                var fileAttr = File.GetAttributes(path);
                return new StorageFile
                {
                    Name = filenameext,
                    DisplayName = filename,
                    DisplayType = ext,
                    DateCreated = filedatetime,
                    Attributes = fileAttr,
                    FullPath = path
                };
            }
            catch
            {
                return null;
            }
		}
        
        public Stream Open(FileAccess mode, Context context){
			if (Exists ()) {
                if(Android.OS.Build.VERSION.SdkInt < Android.OS.BuildVersionCodes.M)
                {
                    if (mode == FileAccess.ReadWrite)
                    {
                        return File.Open(FullPath, FileMode.Open, FileAccess.ReadWrite);
                    }
                    else if (mode == FileAccess.Read)
                    {
                        return File.Open(FullPath, FileMode.Open, FileAccess.Read);
                    }
                    else
                    {
                        return File.Open(FullPath, FileMode.Open, FileAccess.Write);
                    }
                } 
                else
                {
                    if (context.CheckSelfPermission(Android.Manifest.Permission.WriteExternalStorage) == Android.Content.PM.Permission.Granted)
                    {
                        if (mode == FileAccess.ReadWrite)
                        {
                            return File.Open(FullPath, FileMode.Open, FileAccess.ReadWrite);
                        }
                        else if (mode == FileAccess.Read)
                        {
                            return File.Open(FullPath, FileMode.Open, FileAccess.Read);
                        }
                        else
                        {
                            return File.Open(FullPath, FileMode.Open, FileAccess.Write);
                        }
                    }
                    else
                    {
                        throw new Exception("Android no permite el abrir stream's de los archivos de la sd card...");
                    }
                }
			}else {
				throw new FileNotExistsException("El archivo que tratas de copiar no existe, path: " + FullPath + ", name: " + Name);
			}
		}

		public virtual bool Exists(){
			if (File.Exists (FullPath)) {
				return true;
			} else {
				throw new FileNotExistsException("El archivo que tratas de copiar no existe, path: " + FullPath + ", name: " + Name);
			}
		}

		private string UniqueString(){
			Guid g = Guid.NewGuid ();
			string GuidString = Convert.ToBase64String (g.ToByteArray ());
			GuidString = GuidString.Replace ("=", "");
			GuidString = GuidString.Replace ("+", "");
			return GuidString;
		}
	}


    public class FileNotExistsException : Exception
    {
        public FileNotExistsException(string message) : base(message) { }
    }
}

