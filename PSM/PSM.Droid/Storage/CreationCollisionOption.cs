﻿using System;

namespace PSM.Droid
{
	public enum CreationCollisionOption
	{
		GenerateUniqueName = 0,
		ReplaceExisting = 1,
		FailIfExists = 2,
		OpenIfExists = 3
	}
}

