﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PSM.Helpers.ReadFile;
using PSM.ORM;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.Droid.Controls.ReadFile))]
namespace PSM.Droid.Controls
{
    public class ReadFile : IReadFile
    {
        RespaldoBDUsuarioDto IReadFile.ReadFile(string nombreArchivo)
        {
            var result = new RespaldoBDUsuarioDto();

            var path = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.Path, "psmultimate.db3");

            result.BasedeDatos = File.ReadAllBytes(path);
            result.Nombre = "psmultimateBack";

            return result;
        }
        
    }
}
