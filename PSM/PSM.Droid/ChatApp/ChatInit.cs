﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ChatApp.Droid;
using ChatApp;
using ChatApp.ORM;
using Xamarin.Forms;
using static ChatApp.ChatClient;
using PSM.ORM;

namespace ChatApp
{
    public class ChatInit
    {

        public static Userprofile CurrentUser { get; set; }
        public static ChatDataBase DB { get; set; }
        public static List<ChatBubbleView> UserChats { get; set; }
        public static ChatClient Client { get; set; }
        public static Userprofile UserSelectedForChat { get; set; }
        public string IdChat { get; set; }
        public static Context Context { get; set; }
        public static List<ORM.ChatGrupo> Grupos { get; set; }

        public static void SetGrupos(List<ORM.ChatGrupo> grupos)
        {
            Grupos = grupos;
        }

        public static void Init(Userprofile userprofile = null, List<ORM.ChatGrupo> grupos = null)
        {
            // iniciamos la base de datos
            if (DB == null) DB = ChatDataBase.Instance;
            // Inuiciamos el cliente del chat
            if (Client == null) Client = new ChatClient();
            // si el usuario es null cargamos al usuario local
            if (userprofile == null)
            {
                LoadLocalUser();
            }
            else
            {
                CurrentUser = userprofile;
            }

            if (grupos != null) Grupos = grupos;

            if (CurrentUser != null)
            {
                GetChats();
                ChatsPendientes();
                if (!ChatService.IsRunning)
                {
                    Intent chat = new Intent(Context, typeof(ChatService));
                    Context.StartService(chat);
                }
            }
        }

        private static async void GetChats()
        {
            // obtenemos los chats
            var usersresult = await Client.GetUsers(CurrentUser.Alias);
            if (usersresult != null)
            {
                // insertamos los usuarios en la base de datos
                DB.InsertUsers(usersresult.GetUsers());
                // cargamos la lista
                UserChats = usersresult.GetUserAsChats();
            }
        }

        public static void LoadLocalUser()
        {
            // nos conectamos a la base de datos local para obtener al usuario actual
            var connection = new PSM.Droid.DataBase.Connection();
            // obtenemos la base de datos local
            var psmdb = connection.GetDataBase();
            // buscamos al usuario local
            var currentuser = ((PSMDataBase)psmdb.Objeto).Usuario.FirstOrDefault();
            // si el usuario existe ....
            if (currentuser != null)
            {
                // asignamos al usuario a una variable global
                CurrentUser = new Userprofile
                {
                    Alias = currentuser.Alias,
                    IdUsuario = currentuser.IdUsuario,
                    Descripcion = "Usuario",
                    Nombre = currentuser.Alias
                };
            }
            else
            {
                CurrentUser = null;
            }
        }

        private static async void ChatsPendientes()
        {
            /*
            Task.Run(async () =>
            { */
            // creamos el cliente del chat
            ChatClient client = new ChatClient();
            // descargamos los chats pendientes
            var chats = await client.ChatsPendientes(CurrentUser.IdUsuario);
            // verificamos que los chats sean diferentes de null
            if (chats != null)
            {
                // iteramos los chats como un valor de la base de datos
                foreach (var item in chats.GetAsChat())
                {
                    try
                    {
                        // verificamos que el chat sea de un miembro del proyecto
                        var userfound = ChatInit.DB.GetUser(item.IdUsuarioOrigen.ToString());
                        // el chat es de un miembro del proyecto
                        if (userfound != null)
                        {
                            // si el usuario de destino soy yo, entonces ....
                            if (item.IdUsuarioDestino == CurrentUser.IdUsuario)
                            {
                                // insertamos el chat en la base de datos
                                DB.InsertChat(item);
                                // notificamos de recivido
                                var receivedchat = await client.NotifyReceived(item.IdChat.ToString());
                                // si se recivio entonces mandamos la notificación
                                if (receivedchat != null)
                                {
                                    // si se actualizo mandamos la notificación
                                    if (receivedchat.Updated)
                                    {
                                        // modificamos el elemento diciendo que si se ha recivido
                                        item.Entregado = true;
                                        // actualizamos en la base de datos
                                        DB.UpdateChat(item);
                                        // enviamos la notificación
                                        SendNotification(item.Mensaje, item.IdChat.ToString(), item.IdUsuarioDestino.ToString(), userfound, Context);
                                    }
                                }
                            }
                            else
                            {
                                // el chat no es para mi
                            }
                        }
                        else
                        {
                            // el chat no es de un usuario del proyecto
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    }
                }
            }
            // });
        }

        public static void SendNotificationGroup(ORM.ChatMensaje chatmensaje)
        {
            // Instantiate the builder and set notification elements:
            Notification.Builder builder = new Notification.Builder(Context)
                .SetContentTitle("Tienes un nuevo mensaje")
                .SetContentText(chatmensaje.Mensaje)
                .SetSmallIcon(PSM.Droid.Resource.Drawable.icon);
            // Build the notification:
            Notification notification = builder.Build();
            // Get the notification manager:
            NotificationManager notificationManager = Context.GetSystemService(Context.NotificationService) as NotificationManager;
            // Publish the notification:
            const int notificationId = 1;
            notificationManager.Notify(chatmensaje.IdChatGrupo + "|" + notificationId, notificationId, notification);
        }

        public static void SendNotification(string message, string idchat, string localidchat, UserChat userfound, Context context)
        {
            // Instantiate the builder and set notification elements:
            Notification.Builder builder = new Notification.Builder(context)
                .SetContentTitle(userfound.Nombre)
                .SetContentText(message)
                .SetSmallIcon(PSM.Droid.Resource.Drawable.icon);
            // Build the notification:
            Notification notification = builder.Build();
            // Get the notification manager:
            NotificationManager notificationManager = context.GetSystemService(Context.NotificationService) as NotificationManager;
            // Publish the notification:
            const int notificationId = 0;
            notificationManager.Notify(idchat + "|" + localidchat, notificationId, notification);
        }
    }
}