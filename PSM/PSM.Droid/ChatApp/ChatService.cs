﻿using System;
using System.Linq;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using PSM.Droid;
using ChatApp.ORM;

namespace ChatApp.Droid
{
	
	[Service]
	public class ChatService : Service
	{
        public static bool IsRunning { get; set; }

        /// <summary>
        /// Este método se ejecuta al crearse un servicio [Ciclo de vida de un servicio]
        /// </summary>
        public override void OnCreate()
        {
            base.OnCreate();
            // El servicio esta corriendo
            IsRunning = true;
            // quitamos los posibles evetos asignados a websocketreceiver
            ChatSocket.WebSocketReceiver -= WebSocketReceiver;
            // agregamos un evento
            ChatSocket.WebSocketReceiver += WebSocketReceiver;
            // iniciamos la conexión con el chat
            ChatSocket.StartHubConnection();
            // iniciamos al usuario
            ChatInit.LoadLocalUser();
        }
        
        /// <summary>
        /// Este método es lanzado cuando el evento Receiver es activado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		void WebSocketReceiver(object sender, WebSocketArgs e)
		{
			if (ChatSocket.AppState == VisibilityState.Foreground)
			{
				return;
			}

			switch (e.Action)
			{
				case Actions.ReceiveMessage:
					ReceiveMessage(e.Data);
					break;


                case Actions.ReceiveMessageFromGroup:
                    ReceiveMessageFromGroup(e.Data);
                    break;
			}
		}

        private void ReceiveMessageFromGroup(dynamic data)
        {
            var idchatmensaje = 0;
            var idgrupo = 0;
            var idproyecto = 0;
            var idusuario = 0;
            var message = "";
            var now = DateTime.Now;

            if(data != null)
            {
                // idchatmessage = arg1, idgrupo = arg2, idproyecto = arg3, idusuario = arg4, message = arg5, now = arg6
                int.TryParse(data.idchatmessage, out idchatmensaje);
                int.TryParse(data.idgrupo, out idgrupo);
                int.TryParse(data.idproyecto, out idproyecto);
                int.TryParse(data.idusuario, out idusuario);
                message = data.message;
                DateTime.TryParse(data.now, out now);
            }

            var chatmensaje = new ChatMensaje
            {
                Mensaje = message,
                Fecha = DateTime.Now,
                IdChatGrupo = idgrupo,
                IdProyecto = idproyecto,
                IdUsuario = idusuario,
                IdChatMensaje = idchatmensaje
            };

            if (ChatInit.Grupos != null)
            {
                // el mensaje es del grupo
                if(ChatInit.Grupos.Any(e => e.IdChatGrupo == idgrupo))
                {
                    // notificamos mensaje de grupo
                    if (ChatConfig.Visibility == VisibilityState.Foreground) ChatConfig.Vibrate();
                    
                    if (ChatConfig.InConversationGroup)
                    {
                        if (ChatConfig.ConversationGroup != null)
                        {
                            if (ChatConfig.ConversationGroup.IdChatGrupo == idgrupo)
                            {
                                // el usuario que envio el mensaje es el mismo de la conversación
                                ChatConfig.OnGroupMessageReceive(chatmensaje);
                            }
                            else
                            {
                                ChatInit.SendNotificationGroup(chatmensaje);
                                // ChatInit.SendNotification(message, idchat, localidchat, userfound, this);
                            }
                        }
                        else
                        {
                            // el usuario no esta en la página de la conversación
                            // enviamos una notificación al celular
                            // ChatInit.SendNotification(message, idchat, localidchat, userfound, this);
                            ChatInit.SendNotificationGroup(chatmensaje);
                        }
                    }
                    else
                    {
                        // el usuario no esta en una conversación
                        // ChatInit.SendNotification(message, idchat, localidchat, userfound, this);
                        ChatInit.SendNotificationGroup(chatmensaje);
                    }
                }
            }

        }

        /// <summary>
        /// Recibe el mensaje desde el servidor
        /// </summary>
        /// <param name="data"></param>
        private async void ReceiveMessage(dynamic data)
		{
            // variables del mensaje
			var username = "";
			var message = "";
			var idchat = "";
			var localidchat = "";

            // convertimos los datos del mensaje en variables
			if (data != null)
			{
				username = data.username;
				message = data.message;
				idchat = data.idchat;
				localidchat = data.localidchat;
			}

            // si el usuario no esta asignado
            if (ChatInit.CurrentUser == null)
            {
                // asignamos al usuario local
                ChatInit.LoadLocalUser();
            }

            // si el usuario sigue siendo null detenemos la ejecucion
            if (ChatInit.CurrentUser == null) return;

            // obtnemos la base de datos
            ChatDataBase db = ChatDataBase.Instance;

            // verificamos si el mensaje es para mi
            if (int.Parse(localidchat) == ChatInit.CurrentUser.IdUsuario)
            {
                // buscamos al usuario que envio el mensaje
                var userfound = db.GetUser(username);
                // ¿el usuario es un miembro del proyecto?
                if (userfound != null) // si existe el usuario; entonces significa que es un miembro del proyecto
                {
                    // si el chat ya existe en la base local, quiere decir que ya se ha recibido
                    if (db.Chat.Exists(e => e.IdChat == int.Parse(idchat))) return;
                    // insertamos el chat recibido
                    db.InsertChat(new Chat { IdChat = int.Parse(idchat), IdUsuarioDestino = int.Parse(localidchat), IdUsuarioOrigen = userfound.IdUsuario, Entregado = true, Fecha = DateTime.Now, Mensaje = message });

                    if (ChatConfig.Visibility == VisibilityState.Foreground) ChatConfig.Vibrate();
                    
                    if (ChatConfig.InConversation)
                    {
                        if (ChatConfig.Conversation != null)
                        {
                            if (ChatConfig.Conversation.IdUsuario == userfound.IdUsuario)
                            {
                                // el usuario que envio el mensaje es el mismo de la conversación
                            }
                            else
                            {
                                ChatInit.SendNotification(message, idchat, localidchat, userfound, this);
                            }
                        }
                        else
                        {
                            // el usuario no esta en la página de la conversación
                            // enviamos una notificación al celular
                            ChatInit.SendNotification(message, idchat, localidchat, userfound, this);
                        }
                    }
                    else
                    {
                        // el usuario no esta en una conversación
                        ChatInit.SendNotification(message, idchat, localidchat, userfound, this);
                    }
                    // notificamos al servidor que se ha recivido el mensaje
                    ChatClient client = new ChatClient();
                    var response = await client.NotifyReceived(idchat);
                    if (response != null)
                    {
                        if (response.Updated)
                        {
                            // el mensaje fue recivido y se a actualizado la base de datos
                        }
                        else
                        {
                            // no se pudo actualizar el mensaje
                        }
                    }
                    else
                    {
                        // no se pudo actualizar el mensaje
                    }
                }
                else
                {
                    // el usuario que envio el mensaje no es de este proyecto
                }
            }
            else if (ChatConfig.ChatOfUser.HasValue && ChatConfig.ChatOfUser.Value == int.Parse(localidchat))
            {
                // buscamos el chat en la base de datos local
                var chatindb = db.Chat.FirstOrDefault(c => c.IdUsuarioDestino == int.Parse(localidchat) && c.IdUsuarioOrigen == int.Parse(username) && c.Mensaje == message && c.IdChat == 0);
                // si el chat existe entonces actualizamos su id
                if (chatindb != null)
                {
                    // actualizamos el id del chat
                    chatindb.IdChat = int.Parse(idchat);
                    // guardamos los cambios
                    db.Chat.SaveChanges();
                }
            }
            else
            {

            }
        }

        /// <summary>
        /// Este mét
        /// </summary>
        /// <param name="intent"></param>
        /// <returns></returns>
		public override IBinder OnBind(Intent intent)
		{
			return null;
		}

        /// <summary>
        /// Inicia un proceso en segundo plano, para que se ejecute todo el tiempo...
        /// </summary>
        /// <param name="intent"></param>
        /// <param name="flags"></param>
        /// <param name="startId"></param>
        /// <returns></returns>
		public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
		{
            // revisamos que el mainlooper sea null para iniciar un looper
			if (MainLooper == null)
			{
                // preparamos el mainlooper
				Looper.Prepare();
			}
			// mantenemos la conexion a tope
			Thread t = new Thread((obj) =>
			{
				while (true)
				{
					Thread.Sleep(TimeSpan.FromSeconds(10));
                    ChatSocket.VerifyHubConnection();
				}
			});
			t.Start();
			return StartCommandResult.Sticky;
		}
	}}
