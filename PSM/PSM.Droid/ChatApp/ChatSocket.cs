using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using System.Threading.Tasks;
using ChatApp.ORM;
using static ChatApp.ChatClient;
using Microsoft.AspNet.SignalR.Client;

namespace ChatApp.Droid
{
    public class ChatSocket
    {

        public static void Send(ORM.Chat chat)
        {
            VerifyHubConnection();
            if (HubConnection == null)
            {
                StartHubConnection();
            }

            if (HubProxy != null)
            {
                // obtnemos la base de datos
                ChatDataBase db = ChatDataBase.Instance;
                // si el chat ya existe en la base local, quiere decir que ya se ha recibido
                // if (db.Chat.Exists(e => e.IdChat == int.Parse(idchat))) return;
                // insertamos el chat a enviar
                db.InsertChat(chat);
                // enviamos el chat al usuario
                HubProxy.Invoke("Send", chat.Mensaje, chat.IdUsuarioOrigen, chat.IdUsuarioDestino);
            }
        }
        

        public static void SendToGroup(ChatMensaje chatmensaje)
        {
            VerifyHubConnection();
            if(HubConnection != null)
            {
                StartHubConnection();
            }

            if(HubProxy != null)
            {
                HubProxy.Invoke("SendToGroup", chatmensaje.IdChatGrupo, chatmensaje.IdProyecto, chatmensaje.IdUsuario, chatmensaje.Mensaje);
            }
        }

        public static HubConnection HubConnection
        {
            get; set;
        }

        public static IHubProxy HubProxy { get; set; }

        private static VisibilityState _appstate = VisibilityState.Background;
        public static VisibilityState AppState
        {
            get
            {
                return _appstate;
            }

            set
            {
                _appstate = value;
            }
        }
        
        public static void StartHubConnection()
        {
            if (HubConnection != null)
            {
            }
            else
            {
                HubConnection = new HubConnection($"{ChatConfig.BaseUrl}");
                HubProxy = HubConnection.CreateHubProxy("ChatHub");
                HubProxy.On<string, string, string, string>("ReceiveMessage", ReceiveMessage);
                HubProxy.On<string, string, string, string, string, string>("ReceiveMessageFromGroup", ReceiveMessageFromGroup);
                try
                {
                    HubConnection.Start();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
        }

        private static void ReceiveMessageFromGroup(string arg1, string arg2, string arg3, string arg4, string arg5, string arg6)
        {
            OnWebSocketReceiver(new { idchatmessage = arg1, idgrupo = arg2, idproyecto = arg3, idusuario = arg4, message = arg5, now = arg6 }, Actions.ReceiveMessageFromGroup);
        }

        public static void VerifyHubConnection()
        {
            if (HubConnection.State == ConnectionState.Disconnected)
            {
                try
                {
                    HubConnection.Stop();
                    HubConnection.Start();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public static event EventHandler<WebSocketArgs> WebSocketReceiver;

        public static void OnWebSocketReceiver(dynamic receivedata, Actions actions)
        {
            if (WebSocketReceiver != null)
            {
                WebSocketReceiver(null, new WebSocketArgs { Data = receivedata, Action = actions });
            }
        }
        
        // 
        // idchat 		[Chat en database]
        // localidchat  [Id del chatlocal]
        // username 	[Nombre de quien esta enviando el mensaje]
        // message		[Mensaje]
        public static void ReceiveMessage(string idchat, string localidchat, string username, string message)
        {
            OnWebSocketReceiver(new { idchat, localidchat, username, message }, Actions.ReceiveMessage);
        }
    }
}