using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PSM.ORM;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(PSM.Droid.LiteConnection.Background))]
namespace PSM.Droid.LiteConnection
{
    
    public class Background : IBackground
    {
        public ORM.Background GetDataBase()
        {
            var internalpath = Android.OS.Environment.ExternalStorageDirectory.Path;
            var path = Path.Combine(internalpath, "androidchecksys.db3");
            return new ORM.Background(path);
        }
    }
}