using SQLite;
using System;
using System.IO;
using Xamarin.Forms;
using PSM.Droid.DataBase;
using PSM.ORM;
using DevelopersAzteca.Storage.SQLite;

[assembly: Dependency(typeof(Connection))]
namespace PSM.Droid.DataBase
{
    public class Connection : IPSMDataBase
    {
        public CosmicResponse GetDataBase()
        {
            var fileName = Keys.DataBaseName;
            var internalpath = Android.OS.Environment.ExternalStorageDirectory.Path;
            //var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(internalpath, fileName);
            CosmicResponse cosmicResponse = new CosmicResponse
            {
                Status = false,
                Message = "Existe"
            };
            
            if (File.Exists(path))
            {
                var psmdatabase = new PSMDataBase(path);
                cosmicResponse.Objeto = psmdatabase;
                cosmicResponse.Status = true;
            }
            else
            {
                var psmdatabase = new PSMDataBase(path);
                cosmicResponse.Objeto = psmdatabase;
            }
            
            return cosmicResponse;
        }
    }
}