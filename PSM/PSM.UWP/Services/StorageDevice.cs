﻿using PSM.Helpers.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.UWP.Services.StorageDevice))]
namespace PSM.UWP.Services
{
    public class StorageDevice : IStorageDevice
    {
        public string GetBatteryLevel()
        {
            return "No hay batería en Windows";
        }

        public string GetFullStorageDevive()
        {
            return "Windows no envia almacenamiento";
        }
    }
}
