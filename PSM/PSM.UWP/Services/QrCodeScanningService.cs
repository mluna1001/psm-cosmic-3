﻿using PSM.Helpers.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZXing.Mobile;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.UWP.Services.QrCodeScanningService))]
namespace PSM.UWP.Services
{
    public class QrCodeScanningService : IQrCodeScanningService
    {
        public Task<string> ScanAsync()
        {
            var optionsDefault = new MobileBarcodeScanningOptions();
            var optionsCustom = new MobileBarcodeScanningOptions()
            {
                //UseFrontCameraIfAvailable = true,
                //Check diferents formats in http://barcode.tec-it.com/en
                // PossibleFormats = new List<ZXing.BarcodeFormat> {  ZXing.BarcodeFormat.CODE_128 }
            };
            var scanner = new MobileBarcodeScanner()
            {
                TopText = "Acerca la cámara al elemento",
                BottomText = "Toca la pantalla para enfocar"
            };

            var scanResults = scanner.Scan(optionsCustom);

            //Fix by Ale 2017-07-06
            //return (scanResults != null) ? scanResults.Text : string.Empty;
            var t = new Task<string>(() => "7501058619211");
            t.Start();
            return t;
        }
    }
}
