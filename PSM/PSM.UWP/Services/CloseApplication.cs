﻿using PSM.Helpers.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.UWP.Services.CloseApplication))]
namespace PSM.UWP.Services
{
    public class CloseApplication : ICloseApplication
    {
        public void CloseApp()
        {
            
        }
    }
}
