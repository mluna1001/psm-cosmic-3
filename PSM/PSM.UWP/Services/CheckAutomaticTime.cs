﻿using PSM.Helpers.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.UWP.Services.CheckAutomaticTime))]
namespace PSM.UWP.Services
{
    public class CheckAutomaticTime : ICheckAutomaticTime
    {
        public bool IsAutomaticTime()
        {
            return true;
        }

        public bool IsAutomaticZone()
        {
            return false;
        }
    }
}
