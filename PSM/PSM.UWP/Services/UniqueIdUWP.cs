﻿using PSM.Helpers.DeviceIdentifier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.UWP.Services.UniqueIdUWP))]
namespace PSM.UWP.Services
{
    class UniqueIdUWP : IDevice
    {
        public string GetIdentifier()
        {
            return "UWP_PSM_2019";
        }

        public List<Tuple<string, string>> GetInstalledApps()
        {
            return new List<Tuple<string, string>>();
        }
    }
}
