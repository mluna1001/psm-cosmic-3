
using System;
using System.Collections.Generic;
using ChatApp.ORM;
using static ChatApp.ChatClient;
using Microsoft.AspNet.SignalR.Client;

namespace ChatApp.iOS
{

    public class ChatSocket
    {
        private static ChatDataBase _db { get; set; }

        public static Userprofile CurrentUser { get; set; }
        public static List<ChatBubbleView> UserChats { get; set; }

        public static void InitChat(Userprofile userprofile)
        {
            _db = ChatDataBase.Instance;
            CurrentUser = userprofile;
            LoadPage();
            ChatsPendientes();
            // Intent chat = new Intent(Forms.Context, typeof(ChatService));
            // Forms.Context.StartService(chat);
        }

        public static void Send(ORM.Chat chat)
        {
            VerifyHubConnection();
            if (HubConnection == null)
            {
                StartHubConnection();
            }

            if (HubProxy != null)
            {
                // obtnemos la base de datos
                ChatDataBase db = ChatDataBase.Instance;
                // si el chat ya existe en la base local, quiere decir que ya se ha recibido
                // if (db.Chat.Exists(e => e.IdChat == int.Parse(idchat))) return;
                // insertamos el chat a enviar
                db.InsertChat(chat);
                // enviamos el chat al usuario
                HubProxy.Invoke("Send", chat.Mensaje, chat.IdUsuarioOrigen, chat.IdUsuarioDestino);
            }
        }

        private static UsersResult usersresult { get; set; }

        private static async void LoadPage()
        {
            ChatClient client = new ChatClient();
            usersresult = await client.GetUsers(CurrentUser.Alias);
            if (usersresult != null)
            {
                // insertamos los usuarios en la base de datos
                _db.InsertUsers(usersresult.GetUsers());
                // cargamos la lista
                UserChats = usersresult.GetUserAsChats();
            }
        }

        private static async void ChatsPendientes()
        {
            // creamos el cliente del chat
            ChatClient client = new ChatClient();
            // descargamos los chats pendientes
            var chats = await client.ChatsPendientes(CurrentUser.IdUsuario);
            // verificamos que los chats sean diferentes de null
            if (chats != null)
            {
                // iteramos los chats como un valor de la base de datos
                foreach (var item in chats.GetAsChat())
                {
                    try
                    {
                        // verificamos que el chat sea de un miembro del proyecto
                        var userfound = _db.GetUser(item.IdUsuarioOrigen.ToString());
                        // el chat es de un miembro del proyecto
                        if (userfound != null)
                        {
                            // si el usuario de destino soy yo, entonces ....
                            if (item.IdUsuarioDestino == CurrentUser.IdUsuario)
                            {
                                // insertamos el chat en la base de datos
                                _db.InsertChat(item);
                                // notificamos de recivido
                                var receivedchat = await client.NotifyReceived(item.IdChat.ToString());
                                // si se recivio entonces mandamos la notificación
                                if (receivedchat != null)
                                {
                                    // si se actualizo mandamos la notificación
                                    if (receivedchat.Updated)
                                    {
                                        // modificamos el elemento diciendo que si se ha recivido
                                        item.Entregado = true;
                                        // actualizamos en la base de datos
                                        _db.UpdateChat(item);
                                        // enviamos la notificación
                                        SendNotification(item.Mensaje, item.IdChat.ToString(), item.IdUsuarioDestino.ToString(), userfound);
                                    }
                                }
                            }
                            else
                            {
                                // el chat no es para mi
                            }
                        }
                        else
                        {
                            // el chat no es de un usuario del proyecto
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    }
                }
            }
        }
        
        public static Userprofile UserSelectedForChat { get; set; }

        public string IdChat
        {
            get;
            set;
        }

        public static HubConnection HubConnection
        {
            get; set;
        }

        public static IHubProxy HubProxy { get; set; }

        private static VisibilityState _appstate = VisibilityState.Background;
        public static VisibilityState AppState
        {
            get
            {
                return _appstate;
            }

            set
            {
                _appstate = value;
            }
        }

        public static void StartHubConnection()
        {
            if (HubConnection != null)
            {

            }
            else
            {
                HubConnection = new HubConnection($"{ChatConfig.BaseUrl}");
                HubProxy = HubConnection.CreateHubProxy("ChatHub");
                HubProxy.On<string, string, string, string>("ReceiveMessage", ReceiveMessage);
                try
                {
                    HubConnection.Start();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
        }

        public static void VerifyHubConnection()
        {
            if (HubConnection.State == ConnectionState.Disconnected)
            {
                try
                {
                    HubConnection.Stop();
                    HubConnection.Start();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public static event EventHandler<WebSocketArgs> WebSocketReceiver;

        public static void OnWebSocketReceiver(dynamic receivedata, Actions actions)
        {
            if (WebSocketReceiver != null)
            {
                WebSocketReceiver(null, new WebSocketArgs { Data = receivedata, Action = actions });
            }
        }

        public static void SendNotification(string message, string idchat, string localidchat, UserChat userfound)
        {
            /*
            // Instantiate the builder and set notification elements:
            Notification.Builder builder = new Notification.Builder(context)
                // .SetContentIntent(pendingIntent)
                .SetContentTitle(userfound.Nombre)
                .SetContentText(message)
                .SetDefaults(NotificationDefaults.All)
                .SetSmallIcon(Android.Resource.Drawable.IcNotificationOverlay);
            // Build the notification:
            Notification notification = builder.Build();
            // Get the notification manager:
            NotificationManager notificationManager = context.GetSystemService(Context.NotificationService) as NotificationManager;
            // Publish the notification:
            const int notificationId = 0;
            notificationManager.Notify(idchat + "|" + localidchat, notificationId, notification);
            */
        }

        // 
        // idchat 		[Chat en database]
        // localidchat  [Id del chatlocal]
        // username 	[Nombre de quien esta enviando el mensaje]
        // message		[Mensaje]
        public static void ReceiveMessage(string idchat, string localidchat, string username, string message)
        {
            OnWebSocketReceiver(new { idchat, localidchat, username, message }, Actions.ReceiveMessage);
        }
    }
}