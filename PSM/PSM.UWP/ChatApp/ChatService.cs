﻿/*
using System;
using System.Linq;
using System.Threading;
using PSM.Droid;
using ChatApp.ORM;

namespace ChatApp.Droid
{
	
	[Service]
	public class ChatService : Service
	{
        /// <summary>
        /// Este método se ejecuta al crearse un servicio [Ciclo de vida de un servicio]
        /// </summary>
        public override void OnCreate()
        {
            base.OnCreate();
            // quitamos los posibles evetos asignados a websocketreceiver
            ChatSocket.WebSocketReceiver -= WebSocketReceiver;
            // agregamos un evento
            ChatSocket.WebSocketReceiver += WebSocketReceiver;
            // iniciamos la conexión con el chat
            ChatSocket.StartHubConnection();
            // iniciamos al usuario
            // InitializeLocalUser();
        }

        /// <summary>
        /// Este método es lanzado cuando el evento Receiver es activado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		void WebSocketReceiver(object sender, WebSocketArgs e)
		{
			if (ChatSocket.AppState == VisibilityState.Foreground)
			{
				return;
			}

			switch (e.Action)
			{
				case Actions.ReceiveMessage:
					ReceiveMessage(e.Data);
					break;
			}
		}

        /// <summary>
        /// Recibe el mensaje desde el servidor
        /// </summary>
        /// <param name="data"></param>
		private async void ReceiveMessage(dynamic data)
		{
            // variables del mensaje
			var username = "";
			var message = "";
			var idchat = "";
			var localidchat = "";

            // convertimos los datos del mensaje en variables
			if (data != null)
			{
				username = data.username;
				message = data.message;
				idchat = data.idchat;
				localidchat = data.localidchat;
			}

            // si el usuario no esta asignado
            if (ChatSocket.CurrentUser == null)
            {
                // asignamos al usuario local
                InitializeLocalUser();
            }

            // si el usuario sigue siendo null detenemos la ejecucion
            if (ChatSocket.CurrentUser == null) return;

            // verificamos si el mensaje es para mi
            if (int.Parse(localidchat) == ChatSocket.CurrentUser.IdUsuario)
            {
                // obtnemos la base de datos
                ChatDataBase db = ChatDataBase.Instance;
                // buscamos al usuario que envio el mensaje
                var userfound = db.GetUser(username);
                // ¿el usuario es un miembro del proyecto?
                if (userfound != null) // si existe el usuario; entonces significa que es un miembro del proyecto
                {
                    // si el chat ya existe en la base local, quiere decir que ya se ha recibido
                    if (db.Chat.Exists(e => e.IdChat == int.Parse(idchat))) return;
                    // insertamos el chat recibido
                    db.InsertChat(new Chat { IdChat = int.Parse(idchat), IdUsuarioDestino = int.Parse(localidchat), IdUsuarioOrigen = userfound.IdUsuario, Entregado = true, Fecha = DateTime.Now, Mensaje = message });

                    if (ChatHub.Visibility == VisibilityState.Foreground) ChatHub.Vibrate();


                    if (ChatHub.InConversation)
                    {
                        if (ChatHub.Conversation != null)
                        {
                            if (ChatHub.Conversation.IdUsuario == userfound.IdUsuario)
                            {
                                // el usuario que envio el mensaje es el mismo de la conversación
                            }
                            else
                            {
                                ChatSocket.SendNotification(message, idchat, localidchat, userfound, this);
                            }
                        }
                        else
                        {
                            // el usuario no esta en la página de la conversación
                            // enviamos una notificación al celular
                            ChatSocket.SendNotification(message, idchat, localidchat, userfound, this);
                        }
                    }
                    else
                    {
                        // el usuario no esta en una conversación
                        ChatSocket.SendNotification(message, idchat, localidchat, userfound, this);
                    }
                    // notificamos al servidor que se ha recivido el mensaje
                    ChatClient client = new ChatClient();
                    var response = await client.NotifyReceived(idchat);
                    if (response != null)
                    {
                        if (response.Updated)
                        {
                            // el mensaje fue recivido y se a actualizado la base de datos
                        }
                        else
                        {
                            // no se pudo actualizar el mensaje
                        }
                    }
                    else
                    {
                        // no se pudo actualizar el mensaje
                    }
                }
                else
                {
                    // el usuario que envio el mensaje no es de este proyecto
                }
            }
            else if (ChatHub.ChatOfUser.HasValue && ChatHub.ChatOfUser.Value == int.Parse(localidchat))
            {
                
                // obtnemos la base de datos
                ChatDataBase db = ChatDataBase.Instance;
                // buscamos el chat en la base de datos local
                var chatindb = db.Chat.FirstOrDefault(c => c.IdUsuarioDestino == int.Parse(localidchat) && c.IdUsuarioOrigen == int.Parse(username) && c.Mensaje == message && c.IdChat == 0);
                // si el chat existe entonces actualizamos su id
                if (chatindb != null)
                {
                    // actualizamos el id del chat
                    chatindb.IdChat = int.Parse(idchat);
                    // guardamos los cambios
                    db.Chat.SaveChanges();
                }
                // si el chat ya existe en la base local, quiere decir que ya se ha recibido
                // if (db.Chat.Exists(e => e.IdChat == int.Parse(idchat))) return;
                // insertamos el chat recibido
                //db.InsertChat(new Chat { IdChat = int.Parse(idchat), IdUsuarioDestino = ChatHub.ChatOfUser.Value, IdUsuarioOrigen = int.Parse(username), Entregado = true, Fecha = DateTime.Now, Mensaje = message });
            }
        }

        /// <summary>
        /// Este mét
        /// </summary>
        /// <param name="intent"></param>
        /// <returns></returns>
		public override IBinder OnBind(Intent intent)
		{
			return null;
		}

        /// <summary>
        /// Inicia un proceso en segundo plano, para que se ejecute todo el tiempo...
        /// </summary>
        /// <param name="intent"></param>
        /// <param name="flags"></param>
        /// <param name="startId"></param>
        /// <returns></returns>
		public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
		{
            // revisamos que el mainlooper sea null para iniciar un looper
			if (MainLooper == null)
			{
                // preparamos el mainlooper
				Looper.Prepare();
			}
			// mantenemos la conexion a tope
			Thread t = new Thread((obj) =>
			{
				while (true)
				{
					Thread.Sleep(TimeSpan.FromSeconds(10));
                    ChatSocket.VerifyHubConnection();
				}
			});
			t.Start();
			return StartCommandResult.Sticky;
		}
	}}
}

*/