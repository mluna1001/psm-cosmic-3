﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Xamarin.Forms;
using System.Net;
using Xamarin.Forms.Platform.UWP;
using Windows.Storage;
using PSM.Controls;

[assembly: ExportRenderer(typeof(PSM.Controls.PdfElement), typeof(PSM.UWP.Controls.PdfElement))]
namespace PSM.UWP.Controls
{
    public class PdfElement : ViewRenderer<PSM.Controls.PdfElement, PSM.UWP.Controls.PdfView>
    {
        protected override async void OnElementChanged(ElementChangedEventArgs<PSM.Controls.PdfElement> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                var customWebView = Element as PSM.Controls.PdfElement;
                var filesplit = customWebView.Uri.Split('\\');
                var filename = filesplit[filesplit.Length - 1];
                var file = await PCLStorage.FileSystem.Current.LocalStorage.GetFileAsync(filename);

                StorageFile pdfnewfile = null;
                try
                {
                    pdfnewfile = await KnownFolders.SavedPictures.GetFileAsync(filename);
                }
                catch
                {
                }

                if (pdfnewfile == null)
                {
                    pdfnewfile = await KnownFolders.SavedPictures.CreateFileAsync(filename);
                    using (var streamreader = await file.OpenAsync(PCLStorage.FileAccess.Read))
                    {
                        var streamwriter = await pdfnewfile.OpenStreamForWriteAsync();
                        streamreader.CopyTo(streamwriter);
                        streamwriter.Dispose();
                        streamreader.Dispose();
                    }
                }
                SetNativeControl(new PdfView { FileName = filename, File = pdfnewfile, Folder = KnownFolders.SavedPictures, AutoLoad = true, Source = new Uri(pdfnewfile.Path, UriKind.RelativeOrAbsolute) });
            }
        }
    }
}
