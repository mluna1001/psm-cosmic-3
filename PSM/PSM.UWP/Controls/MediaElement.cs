﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSM.Controls;
using Xamarin.Forms.Platform.UWP;
using UWPControls = Windows.UI.Xaml.Controls;
using System.IO;
using Windows.Storage;
using PCLStorage;
using System.Net.Http;

[assembly: ExportRenderer(typeof(PSM.Controls.MediaElement), typeof(PSM.UWP.Controls.MediaElement))]
namespace PSM.UWP.Controls
{
    public class MediaElement : ViewRenderer<PSM.Controls.MediaElement, Windows.UI.Xaml.Controls.MediaElement>
    {

        protected override async void OnElementChanged(ElementChangedEventArgs<PSM.Controls.MediaElement> e)
        {
            base.OnElementChanged(e);
            var element = Element as PSM.Controls.MediaElement;
            if (element != null && e.OldElement == null)
            {
                var file = await SaveFile(element);
                var native = CreateNativeControl(element, file);
                SetNativeControl(native);
            }
        }

        private async Task<StorageFile> SaveFile(PSM.Controls.MediaElement element)
        {
            StorageFile videofile = null;
            IFile file = null;
            if (element.Source.IsFile)
            {
                var path = element.Source.FilePath;
                var pathsplit = path.Split('/');
                var filename = pathsplit[pathsplit.Length - 1];
                try
                {
                    file = await PCLStorage.FileSystem.Current.LocalStorage.GetFileAsync(filename);
                }
                catch
                {

                }

                if (file != null)
                {
                    try
                    {
                        videofile = await KnownFolders.VideosLibrary.GetFileAsync(file.Name);
                    }
                    catch
                    {
                        videofile = null;
                    }

                    videofile = await WriteVideoFile(videofile, file, filename);
                }
                else
                {
                    file = await IFileFromUri(new Uri(path, UriKind.RelativeOrAbsolute));
                    videofile = await WriteVideoFile(videofile, file, filename);
                }
            }
            else if (element.Source.IsUri)
            {
                var path = element.Source.Uri.AbsoluteUri;
                var filename = element.Source.Uri.AbsolutePath.Replace("/", string.Empty);
                file = await IFileFromUri(element.Source.Uri);
                videofile = await WriteVideoFile(videofile, file, filename);
            }
            else
            {
                if (element.Source.Stream != null)
                {
                    try
                    {
                        videofile = await KnownFolders.VideosLibrary.GetFileAsync(element.Source.TempFileName);
                    }
                    catch (Exception ex)
                    {

                    }

                    if (videofile == null)
                    {
                        videofile = await KnownFolders.VideosLibrary.CreateFileAsync(element.Source.TempFileName, Windows.Storage.CreationCollisionOption.OpenIfExists);
                        await element.Source.Stream.CopyToAsync(await videofile.OpenStreamForWriteAsync());
                    }
                }
            }

            return videofile;
        }

        private async Task<StorageFile> WriteVideoFile(StorageFile video, IFile file, string filename)
        {
            StorageFile videofile = video;

            if (videofile == null && file != null)
            {
                videofile = await KnownFolders.VideosLibrary.CreateFileAsync(filename, Windows.Storage.CreationCollisionOption.OpenIfExists);
                using (var streamreader = await file.OpenAsync(PCLStorage.FileAccess.Read))
                {
                    var streamwriter = await videofile.OpenStreamForWriteAsync();
                    streamreader.CopyTo(streamwriter);
                    streamwriter.Dispose();
                    streamreader.Dispose();
                }
            }
            else
            {
                if (videofile == null)
                {
                    throw new Exception("No fue posible obtener el archivo");
                }
            }

            return videofile;
        }

        private async Task<IFile> IFileFromUri(Uri uri)
        {
            var path = uri.AbsoluteUri;
            var filename = uri.AbsolutePath.Replace("/", string.Empty);

            IFile file = null;

            if (path.Contains("http"))
            {
                HttpClient client = new HttpClient();
                var data = await client.GetByteArrayAsync(path);
                if (await FileSystem.Current.LocalStorage.CheckExistsAsync(filename) == ExistenceCheckResult.NotFound)
                {
                    try
                    {
                        file = await PCLStorage.FileSystem.Current.LocalStorage.CreateFileAsync(filename, PCLStorage.CreationCollisionOption.OpenIfExists);
                        using (var stream = new MemoryStream(data))
                        {
                            await stream.CopyToAsync(await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite));
                        }
                    }
                    catch (Exception ex)
                    {
                        file = null;
                    }
                }
                else
                {
                    file = await FileSystem.Current.LocalStorage.GetFileAsync(filename);
                }
            }
            else
            {
                if (System.IO.File.Exists(path))
                {
                    if (await FileSystem.Current.LocalStorage.CheckExistsAsync(filename) == ExistenceCheckResult.NotFound)
                    {
                        try
                        {
                            file = await PCLStorage.FileSystem.Current.LocalStorage.CreateFileAsync(filename, PCLStorage.CreationCollisionOption.OpenIfExists);
                            using (var tempfile = File.Open(path, FileMode.Open))
                            {
                                await tempfile.CopyToAsync(await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite));
                            }
                        }
                        catch
                        {
                            file = null;
                        }
                    }
                    else
                    {
                        file = await FileSystem.Current.LocalStorage.GetFileAsync(filename);
                    }
                }
                else
                {
                    throw new Exception("No podemos obtener el archivo");
                }
            }
            return file;
        }

        UWPControls.MediaElement _player { get; set; }
        private UWPControls.MediaElement CreateNativeControl(PSM.Controls.MediaElement element, StorageFile file)
        {
            element.Start += Element_Start;
            element.End += Element_End;
            var _player = new UWPControls.MediaElement();
            if (element.Source.IsFile)
            {
                _player.Source = new Uri(element.Source.FilePath);
            }
            else if(element.Source.IsUri)
            {
                _player.Source = element.Source.Uri;
            }
            else
            {
                _player.SetSource(element.Source.Stream.AsRandomAccessStream(), element.MimeType ?? string.Empty);
            }
            _player.Visibility = element.IsVisible ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
            _player.TransportControls = new Windows.UI.Xaml.Controls.MediaTransportControls
            {
                IsFullWindowButtonVisible = true,
                IsVolumeButtonVisible = true,
                IsStopButtonVisible = true,
                IsSeekBarVisible = true,
                IsStopEnabled = true,
                IsSeekEnabled = true,
                IsVolumeEnabled = true,
                IsFullWindowEnabled = true
            };
            _player.TransportControls.IsHitTestVisible = true;
            _player.AutoPlay = element.AutoPlay;
            _player.DownloadProgressChanged += (s, a) =>
            {
                element.OnDownloadProgressChanged(_player.DownloadProgress);
            };
            _player.MediaOpened += (s, a) =>
            {
                element.OnMediaOpened(EventArgs.Empty);
            };
            _player.MediaFailed += (s, a) =>
            {
                element.OnMediaFailed(EventArgs.Empty);
            };
            _player.MediaEnded += (s, a) =>
            {
                element.OnMediaEnded(EventArgs.Empty);
            };
            return _player;
        }

        private void Element_End(object sender, EventArgs e)
        {
            if (_player != null)
            {
                _player.Stop();
            }
        }

        private void Element_Start(object sender, EventArgs e)
        {
            if (_player != null)
            {
                _player.Play();
            }
        }

    }
}
