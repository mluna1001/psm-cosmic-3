﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Data.Pdf;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace PSM.UWP.Controls
{
    public sealed partial class PdfView : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Uri Source
        {
            get { return (Uri)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(Uri), typeof(PdfView),
                new PropertyMetadata(null, OnSourceChanged));

        public bool IsZoomEnabled
        {
            get { return (bool)GetValue(IsZoomEnabledProperty); }
            set { SetValue(IsZoomEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsZoomEnabledProperty =
            DependencyProperty.Register("IsZoomEnabled", typeof(bool), typeof(PdfView),
                new PropertyMetadata(true, OnIsZoomEnabledChanged));

        internal ZoomMode ZoomMode
        {
            get { return IsZoomEnabled ? ZoomMode.Enabled : ZoomMode.Disabled; }
        }

        public bool AutoLoad { get; set; }

        internal ObservableCollection<BitmapImage> PdfPages
        {
            get;
            set;
        } = new ObservableCollection<BitmapImage>();

        public StorageFolder Folder { get; set; }
        public StorageFile File { get; set; }
        public string FileName { get; set; }

        public PdfView()
        {
            this.Background = new SolidColorBrush(Colors.DarkGray);
            this.InitializeComponent();
        }

        private static void OnIsZoomEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((PdfView)d).OnIsZoomEnabledChanged();
        }

        private static void OnSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((PdfView)d).OnSourceChanged();
        }

        private void OnIsZoomEnabledChanged()
        {
            OnPropertyChanged(nameof(ZoomMode));
        }

        private void OnSourceChanged()
        {
            if (AutoLoad)
            {
                LoadAsync();
            }
        }

        public async Task LoadAsync()
        {
            if (Source == null)
            {
                PdfPages.Clear();
            }
            else
            {
                if (Source.IsFile || !Source.IsWebUri())
                {
                    await LoadFromLocalAsync();
                }
                else if (Source.IsWebUri())
                {
                    await LoadFromRemoteAsync();
                }
                else
                {
                    throw new ArgumentException($"Source '{Source.ToString()}' could not be recognized!");
                }
            }
        }

        private async Task LoadFromRemoteAsync()
        {
            HttpClient client = new HttpClient();
            var stream = await
                client.GetStreamAsync(Source);
            var memStream = new MemoryStream();
            await stream.CopyToAsync(memStream);
            memStream.Position = 0;
            PdfDocument doc = await PdfDocument.LoadFromStreamAsync(memStream.AsRandomAccessStream());

            Load(doc);
        }

        private async Task LoadFromLocalAsync()
        {
            if (File == null)
            {
                if (Folder != null && FileName != null)
                {
                    File = await Folder.GetFileAsync(FileName);
                }
                else
                {
                    File = await StorageFile.GetFileFromApplicationUriAsync(Source);
                }
            }

            PdfDocument doc = await PdfDocument.LoadFromFileAsync(File);
            Load(doc);
        }

        private async void Load(PdfDocument pdfDoc)
        {
            PdfPages.Clear();

            for (uint i = 0; i < pdfDoc.PageCount; i++)
            {
                BitmapImage image = new BitmapImage();

                var page = pdfDoc.GetPage(i);

                using (InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream())
                {
                    await page.RenderToStreamAsync(stream);
                    await image.SetSourceAsync(stream);
                }

                PdfPages.Add(image);
            }
        }

        public void OnPropertyChanged([CallerMemberName]string property = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
