﻿using PSM.Helpers.ReadFile;
using PSM.ORM;
using System;
using System.IO;
using System.Runtime.InteropServices;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(PSM.UWP.Controls.ReadFile))]
namespace PSM.UWP.Controls
{
    public class ReadFile : IReadFile
    {
        RespaldoBDUsuarioDto IReadFile.ReadFile(string nombreArchivo)
        {
            var result = new RespaldoBDUsuarioDto();

            var folder = ApplicationData.Current.LocalFolder.Path;
            var path = Path.Combine(folder, "psmultimate.db3");

            result.BasedeDatos = File.ReadAllBytes(path);
            result.Nombre = "psmultimateBack";

            return result;
        }
    }
}
