﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.UWP.DeviceSensors
{
    public class DeviceFamily
    {

        public static Target Target
        {
            get
            {
                var str = Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamily;
                if (str == "Windows.Desktop")
                {
                    return Target.Desktop;
                }
                else if (str == "Windows.Mobile")
                {
                    return Target.Mobile;
                }
                else
                {
                    return Target.Desktop;
                }
            }
        }

    }

    public enum Target
    {
        Mobile, Desktop, Tablet, Xbox, HoloLens
    }
}
