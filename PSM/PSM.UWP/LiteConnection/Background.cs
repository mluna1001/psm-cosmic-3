﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(PSM.UWP.LiteConnection.Background))]
namespace PSM.UWP.LiteConnection
{
    public class Background : IBackground
    {
        public ORM.Background GetDataBase()
        {
            return new ORM.Background(Path.Combine(ApplicationData.Current.LocalFolder.Path, "windowssyscheck.db3"));
        }
    }
}