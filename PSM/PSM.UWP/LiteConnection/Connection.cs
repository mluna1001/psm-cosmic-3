﻿using DevelopersAzteca.Storage.SQLite;
using PSM.ORM;
using PSM.UWP.LiteConnection;
using System.IO;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(PSM.UWP.LiteConnection.Connection))]
namespace PSM.UWP.LiteConnection
{
    public class Connection : IPSMDataBase
    {
        public CosmicResponse GetDataBase()
        {
            var path = Path.Combine(ApplicationData.Current.LocalFolder.Path, Keys.DataBaseName);
            System.Diagnostics.Debug.WriteLine(path);
            CosmicResponse cosmicResponse = new CosmicResponse
            {
                Status = false,
                Message = "Existe"
            };
            if (File.Exists(path))
            {
                var psmdatabase = new PSMDataBase(path);
                cosmicResponse.Objeto = psmdatabase;
                cosmicResponse.Status = true;
            }
            else
            {
                var psmdatabase = new PSMDataBase(path);
                cosmicResponse.Objeto = psmdatabase;
            }

            return cosmicResponse;
        }
    }
}
