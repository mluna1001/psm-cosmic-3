﻿using PSM.Helpers;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{

    public partial class HistoricoCalificacionPage : ContentPage
    {
        int classId = 0;

        List<QualificationModel> _rutas;
        public HistoricoCalificacionPage()
        {
            InitializeComponent();
            // asignamos a la currengpage como esta pagina
            App.CurrentPage = this;
            InitSection();
        }

        private async void InitSection()
        {
            _rutas = GetInformacion();
            // si la sección no tiene preguntas, regresamos a la pagina anterior
            if (_rutas.Count == 0)
            {
                await DisplayAlert("PSM 360", "No hay calificaciones registradas", "Aceptar");
                await Navigation.PopAsync();
                return;
            }
            await SetQuali(_rutas);
        }

        public List<QualificationModel> GetInformacion()
        {
            var rutas = App.DB.Qualification.Distinct().ToList();
            rutas = rutas.GroupBy(c => c.IdTienda, (key, c) => c.FirstOrDefault()).ToList();

            List<QualificationModel> qml = new List<QualificationModel>();

            foreach (var item in rutas)
            {
                QualificationModel qm = new QualificationModel()
                {
                    IdRuta = item.IdRuta,
                    IdTienda = item.IdTienda,
                    Fecha = item.Fecha,
                    NombreTienda = item.NombreTienda,
                    Calificacion = item.Calificacion,
                    Nivel = 1
                };
                qml.Add(qm);
            }
            return qml;
        }

        private async Task SetQuali(List<QualificationModel> rutas)
        {
            ListShopQuali.Children.Clear();
            string hc = "";
            var questions = new List<QualificationModel>();

            var question = rutas.Where(s => s.Nivel == 4).ToList();
            if (question != null)
            {
                questions.AddRange(question);
            }

            var personDataTemplate = new DataTemplate(() =>
            {
                var grid = new Grid();
                var preguntaLabel = new Label { FontAttributes = FontAttributes.Bold, TextColor = Color.Gray };
                var calificacionLabel = new Label { FontAttributes = FontAttributes.Bold, TextColor = Color.Gray };
                var respuestaLabel = new Label { FontAttributes = FontAttributes.Bold, TextColor = Color.Gray };
                preguntaLabel.SetBinding(Label.TextProperty, "Pregunta");
                calificacionLabel.SetBinding(Label.TextProperty, "Quali");
                respuestaLabel.SetBinding(Label.TextProperty, "Respuesta");
                grid.Children.Add(preguntaLabel);
                grid.Children.Add(respuestaLabel, 1, 0);
                grid.Children.Add(calificacionLabel, 2, 0);
                return new ViewCell { View = grid };
            });


            ListView listView = new ListView()
            {
                ItemTemplate = personDataTemplate,
                ItemsSource = questions,
                SeparatorColor = Color.Accent,
                SeparatorVisibility = SeparatorVisibility.Default,
                RowHeight = 50,
                Margin = new Thickness(0, 20, 0, 0)
            };
            listView.ClassId = hc;


            foreach (var item in rutas)
            {
                if (item.Nivel == 1)
                {
                    Button btnShop = new Button
                    {
                        Text = item.NombreTienda
                    };
                    // asignamos el id unico a la vista
                    btnShop.ClassId = item.GetHashCode().ToString();
                    // asignamos el evento de click al boton
                    btnShop.Clicked += OnTiendaDetailClick;
                    btnShop.BackgroundColor = Color.FromHex("#BDBDBD");
                    btnShop.TextColor = Color.Black;

                    if (classId == int.Parse(btnShop.ClassId))
                    {
                        btnShop.BackgroundColor = Color.FromHex("#CED8F6");
                    }

                    ListShopQuali.Children.Add(btnShop);
                }
                else if (item.Nivel == 2)
                {

                    Button btnShop = new Button
                    {
                        Text = item.Fecha.ToString("dd/MM/yy") + " - " + "Puntuacion: " + item.Calificacion.ToString()
                    };
                    // asignamos el id unico a la vista
                    btnShop.ClassId = item.GetHashCode().ToString();
                    // asignamos el evento de click al boton
                    btnShop.Clicked += OnTiendaDetailSectionClick;
                    btnShop.BackgroundColor = Color.FromHex("#D8D8D8");
                    btnShop.TextColor = Color.Black;

                    if (classId == int.Parse(btnShop.ClassId))
                    {
                        btnShop.BackgroundColor = Color.FromHex("#CED8F6");
                    }
                    //ListShopQuali.Children.Add(label);
                    ListShopQuali.Children.Add(btnShop);
                }
                else if (item.Nivel == 3)
                {
                    string Total = "";
                    if (item.Total != 0)
                    {
                        Total = item.Seccion + " - " + "Puntuacion: " + item.Quali.ToString() + " / " + item.Total.ToString();
                    }
                    else
                    {
                        Total = item.Seccion + " - " + "Puntuacion: " + item.Quali.ToString();
                    }

                    Button btnShop = new Button
                    {
                        Text = Total
                    };
                    // asignamos el id unico a la vista
                    btnShop.ClassId = item.GetHashCode().ToString();
                    // asignamos el evento de click al boton
                    btnShop.Clicked += OnTiendaDetailQuestionClick;
                    btnShop.BackgroundColor = Color.FromHex("#F2F2F2");
                    btnShop.TextColor = Color.Black;

                    if (classId == int.Parse(btnShop.ClassId))
                    {
                        btnShop.BackgroundColor = Color.FromHex("#CED8F6");
                    }

                    ListShopQuali.Children.Add(btnShop);
                }
                else
                {
                    ListShopQuali.Children.Add(listView);
                }
            }
        }



        private async void OnTiendaDetailClick(object sender, EventArgs e)
        {
            var btnShop = sender as Button;
            btnShop.BackgroundColor = Color.Accent;
            classId = int.Parse(btnShop.ClassId);
            if (!string.IsNullOrEmpty(btnShop.ClassId))
            {
                int hashcode = int.Parse(btnShop.ClassId);
                var qualificationModel = _rutas.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (qualificationModel == null) return;

                _rutas = _rutas.Where(s => s.Nivel == 1).ToList();
                var indexofmodel = _rutas.IndexOf(qualificationModel);

                if (indexofmodel > -1)
                {
                    var qs = App.DB.Qualification.Where(s => s.IdTienda == qualificationModel.IdTienda);
                    var qst = qs.To<List<QualificationModel>>();


                    foreach (var item in qst)
                    {
                        item.Nivel = 2;
                    }

                    _rutas.InsertRange(indexofmodel + 1, qst);
                    await SetQuali(_rutas);
                }
            }
        }

        private async void OnTiendaDetailSectionClick(object sender, EventArgs e)
        {
            var btnShop = sender as Button;
            btnShop.BackgroundColor = Color.Accent;
            classId = int.Parse(btnShop.ClassId);
            if (!string.IsNullOrEmpty(btnShop.ClassId))
            {
                int hashcode = int.Parse(btnShop.ClassId);
                var qualificationModel = _rutas.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (qualificationModel == null) return;

                _rutas = _rutas.Where(s => s.Nivel == 1 || s.Nivel == 2).ToList();
                var indexofmodel = _rutas.IndexOf(qualificationModel);

                if (indexofmodel > -1)
                {
                    var qs = App.DB.QualiSection.Where(s => s.IdRuta == qualificationModel.IdRuta);
                    var qst = qs.To<List<QualificationModel>>();


                    foreach (var item in qst)
                    {
                        item.Nivel = 3;
                    }

                    _rutas.InsertRange(indexofmodel + 1, qst);
                    await SetQuali(_rutas);
                }
            }
        }

        private async void OnTiendaDetailQuestionClick(object sender, EventArgs e)
        {
            var btnShop = sender as Button;
            btnShop.BackgroundColor = Color.Accent;
            classId = int.Parse(btnShop.ClassId);

            if (!string.IsNullOrEmpty(btnShop.ClassId))
            {
                int hashcode = int.Parse(btnShop.ClassId);
                var qualificationModel = _rutas.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (qualificationModel == null) return;

                _rutas = _rutas.Where(s => s.Nivel == 1 || s.Nivel == 2 || s.Nivel == 3).ToList();
                var indexofmodel = _rutas.IndexOf(qualificationModel);

                if (indexofmodel > -1)
                {
                    var qs = App.DB.QualiPregunta.Where(s => s.IdRuta == qualificationModel.IdRuta && s.IdSeccion == qualificationModel.IdSeccion);
                    var qst = qs.To<List<QualificationModel>>();


                    foreach (var item in qst)
                    {
                        item.Nivel = 4;
                    }

                    _rutas.InsertRange(indexofmodel + 1, qst);
                    await SetQuali(_rutas);
                }
            }
        }
    }
}