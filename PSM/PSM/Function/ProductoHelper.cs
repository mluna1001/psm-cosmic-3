﻿using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Function
{
    public static class ProductoHelper
    {

        public static List<ProductoModel> GetProductos()
        {
            List<ProductoModel> productos = new List<ProductoModel>();
            var productospadre = new List<Producto>();
            if (App.CurrentSection == null)
            {
                throw new NullReferenceException("App.CurrentSection no puede ser null");
            }
            int cadena = App.DB.InfoTienda.FirstOrDefault(s => s.IdTienda == App.CurrentRoute.IdTienda).IdCadena;
            var productocadena = App.DB.ProductoCadena.Where(e => e.IdSeccion == App.CurrentSection.IdSeccion && e.IdCadena == cadena);
            var infotienda = App.DB.InfoTienda.FirstOrDefault(s => s.IdTienda == App.CurrentRoute.IdTienda);
            var clasificaciontienda = infotienda.Clasificacion;
            var idCodigoTienda = infotienda.IdCodigoTienda;

            if (!productocadena.Any())
            {
                productospadre = App.DB.Producto.Where(e => e.Vigencia == true && e.IdSeccion == App.CurrentSection.IdSeccion && e.IdProducto == e.IdParent).OrderBy(s => s.Orden).ToList();
            }
            else
            {
                var c = App.DB.Producto.Where(e => e.Vigencia == true && e.IdSeccion == App.CurrentSection.IdSeccion && e.IdProducto == e.IdParent).OrderBy(s => s.Orden).ToList();
                productospadre = (from pc in productocadena.ToList()
                         join cad in c on pc.IdProducto equals cad.IdProducto
                         select cad).ToList();
            }

            if (!string.IsNullOrEmpty(clasificaciontienda))
            {
                var prodClass = App.DB.ProductoClasificacion.Where(x => x.IdCodigoTienda == idCodigoTienda && x.Clasificacion.Equals(clasificaciontienda)).ToList();

                productospadre = (from pp in productospadre
                                  join pc in prodClass
                                  on pp.IdProducto equals pc.IdProducto
                                  select pp).ToList();
            }

            foreach (var producto in productospadre)
            {
                var productomodel = new ProductoModel
                {
                    IdSeccion = App.CurrentSection.IdSeccion,
                    ProductoPadre = producto
                };
                PreguntasHijas(productomodel);
                productos.Add(productomodel);
            }
            return productos;
        }

        private static void PreguntasHijas(ProductoModel padre)
        {
            int cadena = App.DB.InfoTienda.FirstOrDefault(s => s.IdTienda == App.CurrentRoute.IdTienda).IdCadena;
            var productocadena = App.DB.ProductoCadena.Where(e => e.IdSeccion == App.CurrentSection.IdSeccion && e.IdCadena == cadena);
            var infotienda = App.DB.InfoTienda.FirstOrDefault(s => s.IdTienda == App.CurrentRoute.IdTienda);
            var clasificaciontienda = infotienda.Clasificacion;
            var idCodigoTienda = infotienda.IdCodigoTienda;

            List<Producto> hijos = new List<Producto>();

            if (!productocadena.Any())
            {
                hijos = App.DB.Producto.Where(e => e.Vigencia == true && e.IdSeccion == App.CurrentSection.IdSeccion && e.IdProducto != padre.ProductoPadre.IdProducto && e.IdParent == padre.ProductoPadre.IdProducto).OrderBy(s => s.Orden).ToList();
            }
            else
            {
                var c = App.DB.Producto.Where(e => e.Vigencia == true && e.IdSeccion == App.CurrentSection.IdSeccion && e.IdProducto != padre.ProductoPadre.IdProducto && e.IdParent == padre.ProductoPadre.IdProducto).OrderBy(s => s.Orden).ToList();
                hijos = (from pc in productocadena.ToList()
                                  join cad in c on pc.IdProducto equals cad.IdProducto
                                  select cad).ToList();
            }

            if (!string.IsNullOrEmpty(clasificaciontienda))
            {
                var prodClass = App.DB.ProductoClasificacion.Where(x => x.IdCodigoTienda == idCodigoTienda && x.Clasificacion.Equals(clasificaciontienda)).ToList();

                hijos = (from pp in hijos
                                  join pc in prodClass
                                  on pp.IdProducto equals pc.IdProducto
                                  select pp).ToList();
            }

            foreach (var producto in hijos)
            {
                var model = new ProductoModel
                {
                    IdSeccion = App.CurrentSection.IdSeccion,
                    ProductoPadre = producto
                };
                PreguntasHijas(model);
                padre.ProductosHijos.Add(model);
            }
        }

    }
}
