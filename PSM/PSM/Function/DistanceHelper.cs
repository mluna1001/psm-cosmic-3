﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Function
{
    public class DistanceHelper
    {
        public static double Radianes(double grados)
        {
            return grados * Math.PI / 180;
        }

        public static double Grados(double radianes)
        {
            return radianes * 180 / Math.PI;
        }
    }
}
