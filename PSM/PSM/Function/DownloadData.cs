﻿using PSM.ORM;
using System;
using System.Threading.Tasks;
using System.Linq;
using DevelopersAzteca.Storage.SQLite;

namespace PSM.Function
{
    public class DownloadData
    {

        public static async Task<bool> BajaTodo(/*Page page*/)
        {
            bool rutasuccess, preguntasuccess = false, catalogosuccess = false;

            rutasuccess = await BajarRuta();
            if (rutasuccess) preguntasuccess = await BajarPreguntas();
            if (preguntasuccess) catalogosuccess = await BajarCatalogos();

            if (rutasuccess && preguntasuccess && catalogosuccess)
                return true;

            return false;
        }

        private static async Task<bool> BajarCatalogos(/*Page page*/)
        {
            bool status = false;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "BajarCatalogos", "DownloadData", "Inicia bajar catalogos/todo");

            bool companias = false;
            bool productos = false;
            bool ubicaciones = false;

            var chatgrupos = await App.API.ChatEndPoint.Grupos(App.CurrentUser.IdProyecto, App.CurrentUser.IdUsuario);
            if (chatgrupos != null && chatgrupos.Count > 0)
            {
                foreach (var chatgrupo in chatgrupos)
                {
                    var chatitem = App.DB.ChatGrupo.FirstOrDefault(e => e.IdChatGrupo == chatgrupo.IdChatGrupo);
                    if (chatitem == null)
                    {
                        App.DB.Add(chatgrupo);
                    }
                    else
                    {
                        chatitem.Baneado = chatgrupo.Baneado;
                        App.DB.Entry(chatitem).State = DataBase.EntryState.Modify;
                        App.DB.SaveChanges();
                    }
                    App.DB.SaveChanges();
                }
            }
            bool isconnected = false;
            var catalogoresponse = await App.API.CatalogoEndPoint.UserCatalogs(App.CurrentUser.IdUsuario, async () =>
            {
                isconnected = false;
                //if (ShowAlert)
                //{
                //    await page.DisplayAlert("PSM 360", "No tienes conexion a internet, intenta mas tarde", "Aceptar");
                //    Bitacora.Insert("Descargando catalogos", "No tienes conexion a internet, intenta mas tarde");
                //}
            });
            if (catalogoresponse != null && catalogoresponse.Success)
            {
                if (catalogoresponse.Companias != null)
                {
                    foreach (var item in catalogoresponse.Companias)
                    {
                        if (!App.DB.Compania.Exists(e => e.IdCompania == item.IdCompania))
                        {
                            App.DB.Add(item);
                        }
                    }
                    companias = App.DB.SaveChanges();
                }

                if (catalogoresponse.Productos != null)
                {
                    foreach (var item in catalogoresponse.Productos)
                    {
                        if (!App.DB.Producto.Exists(e => e.IdProducto == item.IdProducto))
                        {
                            App.DB.Add(item);
                        }
                    }
                    productos = App.DB.SaveChanges();
                }

                if (catalogoresponse.Ubicaciones != null)
                {
                    foreach (var item in catalogoresponse.Ubicaciones)
                    {
                        if (!App.DB.Ubicacion.Exists(e => e.IdUbicacion == item.IdUbicacion))
                        {
                            App.DB.Add(item);
                        }
                    }
                    ubicaciones = App.DB.SaveChanges();
                }

                if (catalogoresponse.Abordajes != null)
                {
                    foreach (var item in catalogoresponse.Abordajes)
                    {
                        if (!App.DB.CatalogoSeccionMenu.Exists(e => e.IdSeccionMenu == item.IdSeccionMenu))
                        {
                            App.DB.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }

                if (catalogoresponse.Archivos != null)
                {
                    foreach (var item in catalogoresponse.Archivos)
                    {
                        if (!App.DB.Multimedia.Exists(e => e.IdMultimedia == item.IdMultimedia))
                        {
                            App.DB.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }

                if (catalogoresponse.ProductoCadena != null)
                {
                    foreach (var item in catalogoresponse.ProductoCadena)
                    {
                        if (!App.DB.ProductoCadena.Exists(e => e.IdProductoCadena == item.IdProductoCadena))
                        {
                            App.DB.Add(item);
                        }
                    }
                }

                //if (ShowAlert)
                //{
                //    await page.DisplayAlert("PSM 360", "Se han descargado los catalogos", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarCatalogos", "DownloadData", "Se han descargado los catalogos");
                //}
                status = true;
            }
            else
            {
                //if (isconnected) if (ShowAlert)
                //    {
                //        await page.DisplayAlert("PSM 360", "No fue posible descargar las tiendas, intenta mas tarde", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarCatalogos", "DownloadData", "No fue posible descargar los catalogos, intenta mas tarde");
                //}
            }

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "EndDownload", "DownloadData", "Termina descarga de catalogos/todo");
            return status;
        }

        private static async Task<bool> BajarPreguntas()
        {
            bool status = false;
            StartDownload("Descargando preguntas");

            bool catalogoseccion = false;
            bool preguntas = false;
            bool respuestas = false;
            bool secciones = false;

            bool isconnected = true;
            var sectionresponse = await App.API.SectionEndPoint.UserSections(App.CurrentUser.IdProyecto, async () =>
            {
                isconnected = false;
                //if (ShowAlert)
                //{
                //    await DisplayAlert("PSM 360", "No tienes conexion a internet, intenta mas tarde", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "BajarPreguntas", "DownloadData", "No tienes conexion a internet, intenta mas tarde");
                //}
            });


            if (sectionresponse != null)
            {
                if (sectionresponse.ClasificacionSeccion != null)
                {
                    foreach (var item in sectionresponse.ClasificacionSeccion)
                    {
                        if (!App.DB.CatalogoSeccion.Exists(e => e.IdCatalogoSeccion == item.IdCatalogoSeccion))
                        {
                            App.DB.CatalogoSeccion.Add(item);
                        }
                    }
                    catalogoseccion = App.DB.SaveChanges();
                }

                if (sectionresponse.Preguntas != null)
                {
                    foreach (var pregunta in sectionresponse.Preguntas)
                    {
                        if (!App.DB.Pregunta.Exists(e => e.IdPregunta == pregunta.IdPregunta))
                        {
                            App.DB.Add(pregunta);
                        }
                    }
                    preguntas = App.DB.SaveChanges();
                }

                if (sectionresponse.Secciones != null)
                {
                    foreach (var section in sectionresponse.Secciones)
                    {
                        if (!App.DB.Seccion.Exists(e => e.IdSeccion == section.IdSeccion))
                        {
                            App.DB.Add(section);
                        }
                    }
                    secciones = App.DB.SaveChanges();
                }

                if (sectionresponse.Respuestas != null)
                {
                    foreach (var respuesta in sectionresponse.Respuestas)
                    {
                        if (!App.DB.Respuesta.Exists(e => e.IdRespuesta == respuesta.IdRespuesta))
                        {
                            App.DB.Add(respuesta);
                        }
                    }
                    respuestas = App.DB.SaveChanges();
                }

                //if (ShowAlert)
                //{
                //    await DisplayAlert("PSM 360", "Se han descargado las preguntas", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarPreguntas", "DownloadData", "Se han descargado las preguntas");
                //}
                status = true;
            }
            else
            {
                //if (isconnected)
                //    if (ShowAlert)
                //    {
                //        await DisplayAlert("PSM 360", "No fue posible descargar las tiendas, intenta mas tarde", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarPreguntas", "DownloadData", "No fue posible descargar las preguntas, intenta mas tarde");
                //}
            }

            EndDownload();
            return status;
        }

        private static async Task<bool> BajarRuta()
        {
            bool status = false;
            StartDownload("Descargando ruta");
            bool isconnected = true;
            var routeresponse = await App.API.RouteEndPoint.UserRoutes(App.CurrentUser.IdUsuario, async () =>
            {
                isconnected = false;
                //if (ShowAlert)
                //{
                //    await DisplayAlert("PSM 360", "No tienes conexión a internet, intenta mas tarde", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "BajarRuta", "DownloadData", "No tienes conexión a internet, intenta mas tarde");
                //}
            });
            if (routeresponse != null)
            {
                if (routeresponse.Rutas != null && routeresponse.Rutas.Count > 0)
                {
                    foreach (var route in routeresponse.Rutas)
                    {
                        if (!App.DB.Ruta.Exists(e => e.IdRuta == route.IdRuta))
                        {
                            App.DB.Add(route);
                        }
                    }
                    App.DB.SaveChanges();
                }

                if (routeresponse.Informacion != null && routeresponse.Informacion.Count > 0)
                {
                    foreach (var infotienda in routeresponse.Informacion)
                    {
                        if (!App.DB.InfoTienda.Exists(e => e.IdInfoTienda == infotienda.IdInfoTienda))
                        {
                            App.DB.Add(infotienda);
                        }
                    }
                    App.DB.SaveChanges();
                }

                if (routeresponse.Informacion != null && routeresponse.Informacion.Count > 0)
                {
                    foreach (var infotienda in routeresponse.Informacion)
                    {
                        if (!App.DB.InfoTienda.Exists(e => e.IdInfoTienda == infotienda.IdInfoTienda))
                        {
                            App.DB.Add(infotienda);
                        }
                    }
                    App.DB.SaveChanges();
                }

                if (routeresponse.Indicadores != null && routeresponse.Indicadores.Count > 0)
                {
                    App.DB.Execute("DELETE FROM Indicador");
                    App.DB.Execute("VACUUM");
                    App.DB.SaveChanges();

                    foreach (var indicador in routeresponse.Indicadores)
                    {
                        App.DB.Add(indicador);
                    }
                    App.DB.SaveChanges();
                }

                //if (ShowAlert)
                //{
                //    await DisplayAlert("PSM 360", "Se ha descargado tu ruta", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarRuta", "DownloadData", "Se ha descargado tu ruta");
                //}
                status = true;
            }
            else
            {
                if (isconnected)
                {
                    //if (ShowAlert)
                    //{
                    //    await DisplayAlert("PSM 360", "Ocurrio un error al descargar ruta, intenta mas tarde", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarRuta", "DownloadData", "Ocurrio un error al descargar ruta, intenta mas tarde");
                    //}
                }
            }
            EndDownload();
            return status;
        }

        private static void StartDownload(string message)
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IniciaDescargaInformacion, "StartDownload", "DownloadPage", message);
        }
        private static void EndDownload()
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TerminaDescargaInformacion, "EndDownload", "DownloadPage");
        }
    }
}