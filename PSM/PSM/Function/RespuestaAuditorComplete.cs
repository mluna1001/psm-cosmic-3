﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Function
{
    public class RespuestaAuditorComplete
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
        public List<RespuestaAuditor> Respuestas { get; set; }
        public bool Edit { get; internal set; }
    }
}
