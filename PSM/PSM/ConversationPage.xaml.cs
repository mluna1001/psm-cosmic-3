﻿using ChatApp;
using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConversationPage : ContentPage
    {

        private ChatUserItem _item { get; set; }

        public ConversationPage(ChatUserItem item)
        {
            InitializeComponent();
            _item = item;
            App.CurrentPage = this;
            BindingContext = _item;
            InitList(_item);
            ChatConfig.ChatOfUser = _item.IdUsuario;
            ChatConfig.Conversation = _item;
            ChatConfig.InConversation = true;
            ChatDataBase.Instance.ChatInserted += Instance_ChatInserted;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "ConversationPage");
        }

        private void Instance_ChatInserted(object sender, ChatApp.ORM.Chat e)
        {
            ChatBubbleView bubble = null;
            if (e.IdUsuarioOrigen == _item.IdUsuario)
            {
                bubble = new ChatBubbleView(ChatBubbleViewType.He)
                {
                    User = _item.Name,
                    Message = e.Mensaje,
                    SendDate = e.Fecha.ToString("hh:mm:ss tt")
                };
            }
            else if (e.IdUsuarioOrigen == App.CurrentUser.IdUsuario)
            {
                bubble = new ChatBubbleView(ChatBubbleViewType.Me)
                {
                    User = App.CurrentUser.Alias,
                    Message = e.Mensaje,
                    SendDate = e.Fecha.ToString("hh:mm:ss tt")
                };
            }

            if (bubble != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var bubblerender = bubble.Render();
                    LastBubbleRender = bubblerender;
                    StackChats.Children.Add(bubblerender);
                });
                ScrollToLastBubbleRender();
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.MensajeRegistrado, "MensajeRegistrado", "ConversationPage", e.Mensaje);
            }
        }

        private ChatBubbleView LastBubbleRender { get; set; }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ScrollToLastBubbleRender();
        }

        public void ScrollToLastBubbleRender()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (LastBubbleRender != null) await ScrollChats.ScrollToAsync(LastBubbleRender, ScrollToPosition.End, true);
            });
        }

        private void InitList(ChatUserItem item)
        {
            if (item.Chats != null)
            {
                var lastindex = item.Chats.Count - 1;
                for (var i = 0; i < item.Chats.Count; i++)
                {
                    var chat = item.Chats[i];
                    ChatBubbleView bubble;
                    // mensaje recibido
                    if (chat.IdUsuarioDestino == App.CurrentUser.IdUsuario)
                    {
                        bubble = new ChatBubbleView(ChatBubbleViewType.He)
                        {
                            User = item.Name
                        };
                    }
                    else // mensaje enviado
                    {
                        bubble = new ChatBubbleView(ChatBubbleViewType.Me)
                        {
                            User = App.CurrentUser.Alias
                        };
                    }

                    bubble.Message = chat.Mensaje;
                    bubble.SendDate = chat.Fecha.ToString("yyyy-MM-dd hh:mm:ss");
                    var bubblerender = bubble.Render();
                    StackChats.Children.Add(bubblerender);
                    if (lastindex == i)
                    {
                        LastBubbleRender = bubblerender;
                    }
                }
            }
        }

        private void BtnEnviar_Clicked(object sender, EventArgs e)
        {
            var message = BoxMessage.Text ?? "";
            if (!string.IsNullOrEmpty(message))
            {
                // enviamos el mensaje
                ChatConfig.Send?.Invoke(new ChatApp.ORM.Chat
                {
                    Fecha = DateTime.Now,
                    Entregado = false,
                    IdUsuarioDestino = _item.IdUsuario,
                    IdUsuarioOrigen = App.CurrentUser.IdUsuario,
                    Mensaje = message,
                    IdChat = 0
                });
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "BtnEnviar_Clicked", "ConversationPage", message);
                BoxMessage.Text = "";
            }
        }
    }
}
