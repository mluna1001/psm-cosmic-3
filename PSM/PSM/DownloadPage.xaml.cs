﻿using DevelopersAzteca.Storage.SQLite;
using Microsoft.AppCenter.Crashes;
using PSM.Function;
using PSM.Helpers;
using PSM.ORM;
using System;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PSM
{
    public partial class DownloadPage : ContentPage
    {
        public bool ShowAlert { get; private set; }

        public DownloadPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            ProgressDownloadIndicator.IsVisible = false;

            var ruta = new TapGestureRecognizer { Command = new AsyncCommand(BtnBajarRuta_Click) };
            var preguntas = new TapGestureRecognizer { Command = new AsyncCommand(BtnBajarPreguntas_Click) };
            var catalogos = new TapGestureRecognizer { Command = new AsyncCommand(BtnBajarCatalogos_Click) };
            var all = new TapGestureRecognizer { Command = new AsyncCommand(BtnBajarTodo_Click) };

            BtnBajarTodo.GestureRecognizers.Add(all);

            BtnBajarRuta.GestureRecognizers.Add(ruta);
            BtnBajarPreguntas.GestureRecognizers.Add(preguntas);
            BtnBajarCatalogos.GestureRecognizers.Add(catalogos);

            LabelRuta.GestureRecognizers.Add(ruta);
            LabelPreguntas.GestureRecognizers.Add(preguntas);
            LabelCatalogos.GestureRecognizers.Add(catalogos);

            if (App.DB.Pregunta.ToList().Count > 0)
            {
                StackAllOptions.IsVisible = true;
                StackSingleOption.IsVisible = false;
            }
            else
            {
                StackAllOptions.IsVisible = false;
                StackSingleOption.IsVisible = true;
            }

            ShowAlert = true;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "DownloadPage");
        }

        private async Task BtnBajarTodo_Click()
        {

            Device.BeginInvokeOnMainThread(() =>
            {
                StackSingleOption.IsVisible = false;
            });
            ShowAlert = false;
            bool rutasuccess = false;
            bool preguntasuccess = false;
            bool catalogosuccess = false;
            bool qualificationsucces = false;
            rutasuccess = await BtnBajarRuta_Click();
            if (rutasuccess) preguntasuccess = await BtnBajarPreguntas_Click();
            if (preguntasuccess) catalogosuccess = await BtnBajarCatalogos_Click();

            var r = App.DB.Ruta;
            if (r.Any())
            {
                bool be;
                bool sendRate = bool.TryParse(r.FirstOrDefault().SendQual.ToString(), out be);
                if (sendRate)
                {
                    if (be)
                    {
                        if (catalogosuccess) qualificationsucces = await BajarCalificacion();
                    }
                }

            }


            ShowAlert = true;
            if ((rutasuccess && preguntasuccess && catalogosuccess) || qualificationsucces)
            {
                await DisplayAlert("PSM 360", "Se ha descagado toda la información", "Aceptar");
            }
            Device.BeginInvokeOnMainThread(() =>
                    {
                        StackAllOptions.IsVisible = false;
                    });

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "BtnBajarTodo_Click", "DownloadPage", "Se ha hecho click en bajar toda la información");
        }

        private async Task<bool> BtnBajarRuta_Click()
        {
            ValidateDevice.Validate("SaveSection_Clicked", "QuestionPage");
            bool status = false;
            StartDownload("Descargando ruta");
            bool isconnected = true;
            var routeresponse = await App.API.RouteEndPoint.UserRoutes(App.CurrentUser.IdUsuario, async () =>
            {
                isconnected = false;
                if (ShowAlert)
                {
                    await DisplayAlert("PSM 360", "No tienes conexión a internet, intenta mas tarde", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "Descargando ruta", "No tienes conexión a internet, intenta mas tarde");
                }
            });
            if (routeresponse != null)
            {

                if (routeresponse.Rutas != null && routeresponse.Rutas.Count > 0)
                {
                    foreach (var route in routeresponse.Rutas)
                    {
                        if (!App.DB.Ruta.Exists(e => e.IdRuta == route.IdRuta))
                        {
                            App.DB.Add(route);
                        }
                    }
                    App.DB.SaveChanges();
                }

                if (routeresponse.Informacion != null && routeresponse.Informacion.Count > 0)
                {
                    App.DB.Execute("delete from InfoTienda");

                    foreach (var infotienda in routeresponse.Informacion)
                    {
                        App.DB.Add(infotienda);
                    }
                    App.DB.SaveChanges();
                }

                if (routeresponse.Tiempos != null)
                {
                    App.DB.Execute("DELETE FROM Tiempo");
                    foreach (var item in routeresponse.Tiempos)
                    {
                        App.DB.Add(item);
                    }
                    App.DB.SaveChanges();
                }

                if (routeresponse.CatalogoIndicador != null && routeresponse.CatalogoIndicador.Count > 0)
                {
                    App.DB.Execute("delete from CatalogoIndicador");

                    foreach (var catalogoindicador in routeresponse.CatalogoIndicador)
                    {
                        App.DB.Add(catalogoindicador);
                    }
                    App.DB.SaveChanges();
                }

                if (routeresponse.Indicadores != null && routeresponse.Indicadores.Count > 0)
                {
                    App.DB.Execute("delete from Indicador");

                    foreach (var indicador in routeresponse.Indicadores)
                    {
                        App.DB.Add(indicador);
                    }
                    App.DB.SaveChanges();
                }

                if (routeresponse.ReporteIndicador != null && routeresponse.ReporteIndicador.Count > 0)
                {
                    App.DB.Execute("DELETE FROM ReporteIndicador");

                    foreach (var reporte in routeresponse.ReporteIndicador)
                    {
                        App.DB.Add(reporte);
                    }
                    App.DB.SaveChanges();
                }

                if (ShowAlert)
                {
                    await DisplayAlert("PSM 360", "Se ha descargado tu ruta", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BtnBajarRuta_Click", "DownloadPage", "Se ha descargado tu ruta");
                }

                var nearbyrouteresponse = await App.API.NearbyRouteStore.NearbyRouteStores(App.CurrentUser.IdUsuario, async () =>
                {
                    isconnected = false;
                    if (ShowAlert)
                    {
                        await DisplayAlert("PSM 360", "No tienes conexión a internet, intenta mas tarde", "Aceptar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "Descargando tiendas cercanas a ruta", "No tienes conexión a internet, intenta mas tarde");
                    }
                });

                if (nearbyrouteresponse.Tiendas != null && nearbyrouteresponse.Tiendas.Count > 0)
                {
                    App.DB.Execute("delete from TiendasCercanasRuta");

                    foreach (var item in nearbyrouteresponse.Tiendas)
                    {
                        App.DB.TiendasCercanasRuta.Add(item);
                    }
                    App.DB.SaveChanges();
                }

                status = true;
            }
            else
            {
                if (isconnected)
                {
                    if (ShowAlert)
                    {
                        await DisplayAlert("PSM 360", "Ocurrio un error al descargar ruta, intenta mas tarde", "Aceptar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BtnBajarRuta_Click", "DownloadPage", "Ocurrio un error al descargar ruta, intenta mas tarde");
                    }
                }
            }
            EndDownload();
            return status;
        }

        private void StartDownload(string message)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                GridButtons.IsVisible = false;
                ProgressDownloadIndicator.IsVisible = true;
                ProgressDownloadIndicator.IsRunning = true;
                ProgressDownloadText.Text = message;
            });

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IniciaDescargaInformacion, "StartDownload", "DownloadPage", message);
        }

        private void EndDownload()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                GridButtons.IsVisible = true;
                ProgressDownloadIndicator.IsVisible = false;
                ProgressDownloadIndicator.IsRunning = false;
                ProgressDownloadText.Text = "";
            });
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TerminaDescargaInformacion, "EndDownload", "DownloadPage");
        }

        private async Task<bool> BtnBajarPreguntas_Click()
        {
            ValidateDevice.Validate("SaveSection_Clicked", "QuestionPage");
            bool status = false;
            StartDownload("Descargando preguntas");

            bool catalogoseccion = false;
            bool preguntas = false;
            bool respuestas = false;
            bool secciones = false;

            bool isconnected = true;
            var sectionresponse = await App.API.SectionEndPoint.UserSections(App.CurrentUser.IdProyecto, async () =>
            {
                isconnected = false;
                if (ShowAlert)
                {
                    await DisplayAlert("PSM 360", "No tienes conexion a internet, intenta mas tarde", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "BtnBajarPreguntas_Click", "DownloadPage", "No tienes conexion a internet, intenta mas tarde");
                }
            });


            if (sectionresponse != null)
            {
                if (sectionresponse.ClasificacionSeccion != null)
                {
                    foreach (var item in sectionresponse.ClasificacionSeccion)
                    {
                        if (!App.DB.CatalogoSeccion.Exists(e => e.IdCatalogoSeccion == item.IdCatalogoSeccion))
                        {
                            App.DB.CatalogoSeccion.Add(item);
                        }
                    }
                    catalogoseccion = App.DB.SaveChanges();
                }

                if (sectionresponse.Preguntas != null)
                {
                    foreach (var pregunta in sectionresponse.Preguntas)
                    {
                        if (!App.DB.Pregunta.Exists(e => e.IdPregunta == pregunta.IdPregunta))
                        {
                            App.DB.Add(pregunta);
                        }
                    }
                    preguntas = App.DB.SaveChanges();
                }

                if (sectionresponse.Secciones != null)
                {
                    foreach (var section in sectionresponse.Secciones)
                    {
                        if (!App.DB.Seccion.Exists(e => e.IdSeccion == section.IdSeccion))
                        {
                            App.DB.Add(section);
                        }
                    }
                    secciones = App.DB.SaveChanges();
                }

                if (sectionresponse.Respuestas != null)
                {
                    foreach (var respuesta in sectionresponse.Respuestas)
                    {
                        if (!App.DB.Respuesta.Exists(e => e.IdRespuesta == respuesta.IdRespuesta))
                        {
                            App.DB.Add(respuesta);
                        }
                    }
                    respuestas = App.DB.SaveChanges();
                }

                if (sectionresponse.SeccionCadena != null)
                {
                    foreach (var seccionCadena in sectionresponse.SeccionCadena)
                    {
                        if (!App.DB.SeccionCadena.Exists(e => e.IdSeccionCadena == seccionCadena.IdSeccionCadena))
                        {
                            App.DB.Add(seccionCadena);
                        }
                    }
                }

                if (sectionresponse.SeccionRegion != null)
                {
                    foreach (var seccionRegion in sectionresponse.SeccionRegion)
                    {
                        if (!App.DB.SeccionRegion.Exists(e => e.IdSeccionRegion == seccionRegion.IdSeccionRegion))
                        {
                            App.DB.Add(seccionRegion);
                        }
                    }
                }

                if (ShowAlert)
                {
                    await DisplayAlert("PSM 360", "Se han descargado las preguntas", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BtnBajarPreguntas_Click", "DownloadPage", "Se han descargado las preguntas");
                }
                status = true;
            }
            else
            {
                if (isconnected)
                    if (ShowAlert)
                    {
                        await DisplayAlert("PSM 360", "No fue posible descargar las tiendas, intenta mas tarde", "Aceptar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BtnBajarPreguntas_Click", "DownloadPage", "No fue posible descargar las preguntas, intenta mas tarde");
                    }
            }

            EndDownload();
            return status;
        }

        private async Task<bool> BtnBajarCatalogos_Click()
        {
            ValidateDevice.Validate("SaveSection_Clicked", "QuestionPage");
            bool status = false;
            StartDownload("Descargando catalogos");

            bool companias = false;
            bool productos = false;
            bool ubicaciones = false;


            var chatgrupos = await App.API.ChatEndPoint.Grupos(App.CurrentUser.IdProyecto, App.CurrentUser.IdUsuario);
            if (chatgrupos != null && chatgrupos.Count > 0)
            {
                foreach (var chatgrupo in chatgrupos)
                {
                    var chatitem = App.DB.ChatGrupo.FirstOrDefault(e => e.IdChatGrupo == chatgrupo.IdChatGrupo);
                    if (chatitem == null)
                    {
                        App.DB.Add(chatgrupo);
                    }
                    else
                    {
                        chatitem.Baneado = chatgrupo.Baneado;
                        App.DB.Entry(chatitem).State = DataBase.EntryState.Modify;
                        App.DB.SaveChanges();
                    }
                    App.DB.SaveChanges();
                }
            }
            bool isconnected = false;
            var catalogoresponse = await App.API.CatalogoEndPoint.UserCatalogs(App.CurrentUser.IdUsuario, async () =>
            {
                isconnected = false;
                if (ShowAlert)
                {
                    await DisplayAlert("PSM 360", "No tienes conexion a internet, intenta mas tarde", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "BtnBajarCatalogos_Click", "DownloadPage", "No tienes conexion a internet, intenta mas tarde");
                }
            });
            if (catalogoresponse != null && catalogoresponse.Success)
            {
                if (catalogoresponse.Companias != null)
                {
                    foreach (var item in catalogoresponse.Companias)
                    {
                        if (!App.DB.Compania.Exists(e => e.IdCompania == item.IdCompania))
                        {
                            App.DB.Add(item);
                        }
                    }
                    companias = App.DB.SaveChanges();
                }

                if (catalogoresponse.Productos != null)
                {
                    foreach (var item in catalogoresponse.Productos)
                    {
                        if (!App.DB.Producto.Exists(e => e.IdProducto == item.IdProducto))
                        {
                            App.DB.Add(item);
                        }
                    }
                    productos = App.DB.SaveChanges();
                }

                if (catalogoresponse.Ubicaciones != null)
                {
                    foreach (var item in catalogoresponse.Ubicaciones)
                    {
                        if (!App.DB.Ubicacion.Exists(e => e.IdUbicacion == item.IdUbicacion))
                        {
                            App.DB.Add(item);
                        }
                    }
                    ubicaciones = App.DB.SaveChanges();
                }

                if (catalogoresponse.Abordajes != null)
                {
                    foreach (var item in catalogoresponse.Abordajes)
                    {
                        if (!App.DB.CatalogoSeccionMenu.Exists(e => e.IdSeccionMenu == item.IdSeccionMenu))
                        {
                            App.DB.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }

                if (catalogoresponse.Archivos != null)
                {
                    foreach (var item in catalogoresponse.Archivos)
                    {
                        if (!App.DB.Multimedia.Exists(e => e.IdMultimedia == item.IdMultimedia))
                        {
                            App.DB.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }

                if (catalogoresponse.ProductoCadena != null)
                {
                    foreach (var item in catalogoresponse.ProductoCadena)
                    {
                        if (!App.DB.ProductoCadena.Exists(e => e.IdProductoCadena == item.IdProductoCadena))
                        {
                            App.DB.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }

                if (catalogoresponse.Tiempos != null)
                {
                    App.DB.Execute("DELETE FROM Tiempo");
                    foreach (var item in catalogoresponse.Tiempos)
                    {
                        App.DB.Add(item);
                    }
                    App.DB.SaveChanges();
                }

                if (catalogoresponse.ProductoClasificacion != null)
                {
                    App.DB.Execute("DELETE FROM ProductoClasificacion");
                    foreach (var item in catalogoresponse.ProductoClasificacion)
                    {
                        App.DB.Add(item);
                    }
                    App.DB.SaveChanges();
                }

                if (ShowAlert)
                {
                    await DisplayAlert("PSM 360", "Se han descargado los catalogos", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BtnBajarCatalogos_Click", "DownloadPage", "Se han descargado los catalogos");
                }
                status = true;
            }
            else
            {
                if (isconnected) if (ShowAlert)
                    {
                        await DisplayAlert("PSM 360", "No fue posible descargar las tiendas, intenta mas tarde", "Aceptar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BtnBajarCatalogos_Click", "DownloadPage", "No fue posible descargar los catalogos, intenta mas tarde");
                    }
            }

            EndDownload();
            return status;
        }

        protected override bool OnBackButtonPressed()
        {
            var navigationpage = new NavigationPage(new MainPage());
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            HomePage.Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
            return true;
        }

        public async Task<bool> BajarCalificacion()
        {
            bool status = false;
            bool isconnected = true;
            var qualiResponse = await App.API.QualiEndPoint.QualiRoute(App.CurrentUser.IdUsuario, async () =>
            {
                isconnected = false;
                if (ShowAlert)
                {
                    await DisplayAlert("PSM 360", "No tienes conexión a internet, intenta mas tarde", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "BajarCalificacion", "DownloadPage", "No tienes conexión a internet, intenta mas tarde");
                }
            });
            if (qualiResponse != null)
            {
                if (qualiResponse.Qualification != null && qualiResponse.Qualification.Count > 0)
                {
                    foreach (var item in qualiResponse.Qualification)
                    {
                        if (!App.DB.Qualification.Exists(a => a.IdRuta == item.IdRuta))
                        {
                            App.DB.Qualification.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }
                if (qualiResponse.QualiSections != null && qualiResponse.QualiSections.Count > 0)
                {
                    foreach (var item in qualiResponse.QualiSections)
                    {
                        if (!App.DB.QualiSection.Exists(a => a.IdRuta == item.IdRuta))
                        {
                            App.DB.QualiSection.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }
                if (qualiResponse.QualiCuestion != null && qualiResponse.QualiCuestion.Count > 0)
                {
                    App.DB.Execute("DELETE FROM QualiPregunta");
                    foreach (var item in qualiResponse.QualiCuestion)
                    {
                        //if (!App.DB.QualiPregunta.Exists(a => a.IdRuta == item.IdRuta))
                        //{
                        App.DB.QualiPregunta.Add(item);
                        //}
                    }
                    App.DB.SaveChanges();
                }

                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarCalificacion", "DownloadPage", "Se han descargado las calificaciones");

                status = true;
            }
            else
            {
                if (isconnected)
                {
                    if (ShowAlert)
                    {
                        await DisplayAlert("PSM 360", "Ocurrio un error al descargar informacion, intenta mas tarde", "Aceptar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarCalificacion", "DownloadPage", "Ocurrio un error al descargar calificacion, intenta mas tarde");
                    }
                }
            }
            EndDownload();
            return status;
        }
    }
}
