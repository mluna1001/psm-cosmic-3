﻿using Xamarin.Forms.Maps;

namespace PSM
{
    public class CustomPin : Pin
    {
        public Pin Pin { get; set; }

        public string Id { get; set; }

        public string Url { get; set; }
    }
}
