﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.Controls
{
    public class MediaElement : View
    {
        public static readonly BindableProperty SourceProperty =
            BindableProperty.Create("Source", typeof(VideoSource), typeof(MediaElement), null);

        public VideoSource Source
        {
            get { return (VideoSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public static readonly BindableProperty AutoPlayProperty =
            BindableProperty.Create("AutoPlay", typeof(bool), typeof(MediaElement), false);
        
        public bool AutoPlay { get; set; }

        public static readonly BindableProperty MimeTypeProperty =
            BindableProperty.Create("MimeType", typeof(string), typeof(MediaElement), "");

        public event EventHandler<EventArgs> Start;
        public void Play()
        {
            Start?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler<EventArgs> End;
        public void Stop()
        {
            End?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// El tipo de archivo multimedia, si se desconoce, se puede pasar una cadena vacia
        /// </summary>
        public string MimeType { get; set; }

        private string _extension { get; set; }
        public string Extension
        {
            get
            {
                return _extension;
            }

            set
            {
                _extension = value;
                if (!string.IsNullOrEmpty(_extension))
                {
                    if (_extension.Contains("."))
                    {
                        _extension = _extension.Replace(".", string.Empty);
                    }

                    switch (_extension)
                    {
                        case "mpeg":
                            MimeType = "video/mpeg";
                            break;

                        case "mpg":
                            MimeType = "video/mpeg";
                            break;

                        case "mp4":
                            MimeType = "video/mpeg";
                            break;

                        default:
                            throw new NotSupportedException($"Format {_extension} not supported");
                    }
                }
            }
        }

        public event EventHandler<double> DownloadProgressChanged;

        public void OnDownloadProgressChanged(double percent)
        {
            DownloadProgressChanged?.Invoke(this, percent);
        }

        public event EventHandler<EventArgs> MediaOpened;

        public void OnMediaOpened(EventArgs empty)
        {
            MediaOpened?.Invoke(this, empty);
        }

        public event EventHandler<EventArgs> MediaFailed;

        public void OnMediaFailed(EventArgs empty)
        {
            MediaFailed?.Invoke(this, empty);
        }

        public event EventHandler<EventArgs> MediaEnded;

        public void OnMediaEnded(EventArgs empty)
        {
            MediaEnded?.Invoke(this, empty);
        }
    }
}
