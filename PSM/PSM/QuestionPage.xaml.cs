﻿using PSM;
using PSM.DeviceSensors;
using PSM.Function;
using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using PSM.Controls;
using Microsoft.AppCenter.Crashes;
using PSM.Helpers.Subcontrols;
using PSM.Helpers;

namespace PSM
{
    public partial class QuestionPage : ContentPage
    {
        //public List<PreguntaModel> _models;

        private List<PreguntaModel> __models;
        Button btnsave { get; set; }
        public List<PreguntaModel> _models
        {
            get { return __models; }
            set { __models = value; }
        }

        public bool EditAnswers { get; set; }
        public StackLayout Root { get; set; }

        public QuestionPage()
        {
            InitializeComponent();
            // asignamos a la currengpage como esta pagina
            App.CurrentPage = this;
            // no es posible editar las respuestas
            EditAnswers = false;
            // Inicializamos la seccion
            Root = RootLayout;
            InitSection();
        }

        protected override bool OnBackButtonPressed()
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalioDeSeccionSinGuardar, "OnBackButtonPressed", "QuestionPage", $"Salió de la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} sin guardar respuestas");
            Navigation.PopAsync();
            return true;
        }

        /// <summary>
        /// Evento lanzado al guardar una seccion
        /// </summary>
        /// <param name="sender">Objeto que envio el evento</param>
        /// <param name="e">Parametros</param>
        private void SaveSection_Clicked(object sender, EventArgs e)
        {
            btnsave = sender as Button;
            btnsave.IsEnabled = false;
            // quitamos al evento el metodo questionpage_on...
            OnRespuestaAuditorComplete -= QuestionPage_OnRespuestaAuditorComplete;
            // asignamos el metodo al evento
            OnRespuestaAuditorComplete += QuestionPage_OnRespuestaAuditorComplete;
            // Validamos que tenga la fecha/hora automática
            ValidateDevice.Validate("SaveSection_Clicked", "QuestionPage");
            // procesamos las respuestas para poder guardarlas
            RespuestaAuditorProcess();
        }

        private async void QuestionPage_OnRespuestaAuditorComplete(object sender, RespuestaAuditorComplete e)
        {
            // se ha completado la seccion?
            if (!e.IsSuccess)
            {
                // no se ha completado, entonces mandamos un mensaje de error
                await DisplayAlert("PSM 360", e.Message, "Aceptar");
            }
            else
            {
                // obtebemos la fecha en que se termino la seccion
                var dateofregister = DateTime.Now;

                if (e.Edit)
                {
                    App.DB.Execute("Delete from RespuestaAuditor Where IdSeccion =" + App.CurrentSection.IdSeccion + " and IdRuta =" + App.CurrentRoute.IdRuta);
                    foreach (var respuesta in e.Respuestas)
                    {
                        respuesta.Fecha = dateofregister;
                        App.DB.RespuestaAuditor.Add(respuesta);
                    }

                    App.DB.SaveChanges();
                }
                else
                {
                    foreach (var respuesta in e.Respuestas)
                    {
                        respuesta.Fecha = dateofregister;
                        App.DB.RespuestaAuditor.Add(respuesta);
                    }

                    App.DB.SaveChanges();
                }

                try
                {
                    // guardamos los cambios de la base de datos
                    //App.DB.SaveChanges();
                    // mandamos confirmación de que se ha guardado las respuestas
                    await DisplayAlert("PSM 360", "Se han guardado las respuestas", "Aceptar");
                    // Guardamos en bitácora que ha guardado la sección
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.GuardoRespuestas, "QuestionPage_OnRespuestaAuditorComplete", "QuestionPage", $"Se guardaron las respuestas de la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} ");
                    // regresamos a la página anterior
                    await Navigation.PopAsync();
                    // si el catalogo es una inicidencia entonces procesamos la ruta
                    if (App.CurrentSection.IdCatalogoSeccion == 11)
                    {
                        // lanzamos el evento IncidenciaComplete, para cacharlo en cualquier parte del código en la que se haya implementado
                        OnIncidenciaCompleta();
                    }
                }
                catch (Exception a)
                {
                    // no se pudo guardar los datos en la base de datos...
                    await DisplayAlert("PSM 360", "Ocurrio un error al guardar las respuestas, intenta más tarde", "Aceptar");
                    // regresamos a la ventana anterior
                    await Navigation.PopAsync();
                }
            }
        }

        /// <summary>
        /// EVento para saber cuando una incidencia es completada
        /// </summary>
        public event EventHandler<EventArgs> InicidenciaCompleta;

        private void OnIncidenciaCompleta()
        {
            InicidenciaCompleta?.Invoke(this, EventArgs.Empty);
        }

        #region GetModel
        /// <summary>
        /// Método que procesa todas las respuestas generadas en la pagina
        /// </summary>
        public void RespuestaAuditorProcess()
        {

            //int? location = App.CurrentLocation.IdUbicacion;
            int IdRuta = App.CurrentRoute.IdRuta;
            int IdSeccion = App.CurrentSection.IdSeccion;
            var respAudit = App.DB.RespuestaAuditor.Where(s => s.IdRuta == IdRuta && s.IdSeccion == IdSeccion && s.Sincronizado == false);
            // Creamos una lista de respuestas pre asignada
            List<RespuestaAuditor> respuestas = new List<RespuestaAuditor>();
            // valor que indica si se ha completado o no la seccion
            bool notcomplete = false;
            // iteramos los modelos
            foreach (var model in _models)
            {
                // si el  modelo es de tipo campo de texto
                if (model.PreguntaPadre.IdControl == 1 || model.PreguntaPadre.IdControl == 4 || model.PreguntaPadre.IdControl == 5 || model.PreguntaPadre.IdControl == 7 || model.PreguntaPadre.IdControl == 3)
                {
                    // verificamos si se valida o no la pregunta
                    if (model.PreguntaPadre.SeValida.HasValue && model.PreguntaPadre.SeValida.Value)
                    {
                        // se valida la pregunta

                        // revisamos si la respuesta de la pregunta es un string vacio o null, lo que quiere decir que no hay respuesta
                        if (string.IsNullOrEmpty(model.Respuesta))
                        {
                            // lanzamos el evento de que se ha terminado de procesar respuestasauditor pero con un estado false
                            OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                            {
                                IsSuccess = false,
                                Message = "Ingresa una respuesta en la pregunta " + model.PreguntaPadre.Descripcion
                            });
                            // la seccion no se ha completado
                            notcomplete = true;
                            btnsave.IsEnabled = true;
                            // rompemos el loop
                            break;
                        }
                    }

                    if (string.IsNullOrEmpty(model.Respuesta))
                    {
                        continue;
                    }
                    else
                    {
                        if ((model.PreguntaPadre.IdControl == 5 || model.PreguntaPadre.IdControl == 7) &&
                            (model.Respuesta.Trim().Equals(".") || model.Respuesta.Trim().Equals("-") || model.Respuesta.Trim().Equals(",")))
                        {
                            if ((model.Respuesta.Trim().Equals(".")) || (model.Respuesta.Trim().Equals(",")) || (model.Respuesta.Trim().Equals("-")))
                            {
                                // lanzamos el evento de que se ha terminado de procesar respuestasauditor pero con un estado false
                                OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                                {
                                    IsSuccess = false,
                                    Message = "Ingresa una cantidad en la pregunta " + model.PreguntaPadre.Descripcion
                                });
                                // la seccion no se ha completado
                                notcomplete = true;
                                btnsave.IsEnabled = true;
                                // rompemos el loop
                                break;
                            }
                        }
                    }

                    if (model.IdRespuestaAuditor != null)
                    {
                        var respuestaauditorindb = respAudit.FirstOrDefault(ra => ra.IdAnswerAudit == model.IdRespuestaAuditor);
                        if (respuestaauditorindb != null)
                        {
                            respuestaauditorindb.IdRespuesta = model.IdRespuesta;
                            respuestaauditorindb.Respuesta = model.Respuesta;
                            respuestaauditorindb.NumerodeCaja = numerocaja;
                            respuestas.Add(respuestaauditorindb);
                        }
                    }

                    else
                    {
                        if (model.IdRespuesta != 0 || !string.IsNullOrEmpty(model.Respuesta))
                        {
                            // creamos un respuesta auditor con la información de la respuesta
                            RespuestaAuditor answer = new RespuestaAuditor
                            {
                                IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                                IdRespuesta = model.IdRespuesta,
                                IdRuta = App.CurrentRoute.IdRuta,
                                IdSeccion = model.IdSeccion,
                                IdUbicacion = null,
                                NumerodeCaja = numerocaja,
                                Respuesta = model.Respuesta,
                                IdProducto = null,
                                Foto = model.Foto,
                                FotoNombre = model.FotoNombre
                            };
                            // guardamos la respuesta en el array previo
                            respuestas.Add(answer);
                        }
                    }
                }
                else if (model.PreguntaPadre.IdControl == 2) // la pregunta es una tabla con switch
                {
                    foreach (var item in model.Respuestas)
                    {
                        if (model.PreguntaPadre.SeValida.HasValue && model.PreguntaPadre.SeValida.Value)
                        {
                            var r = model.Respuestas.FirstOrDefault(s => s.Valor == 1);
                            if (r == null)
                            {
                                OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                                {
                                    IsSuccess = false,
                                    Message = "Ingresa una respuesta en la pregunta " + (_models.IndexOf(model) + 1) + " " + model.PreguntaPadre.Descripcion
                                });
                                notcomplete = true;
                                if (btnsave != null)
                                {
                                    btnsave.IsEnabled = true;
                                }
                                break;
                            }
                        }


                        var respuestaauditorindb = respAudit.FirstOrDefault(ra => ra.IdAnswerAudit == item.IdRespuestaAuditor);
                        if (respuestaauditorindb != null)
                        {
                            respuestaauditorindb.IdRespuesta = item.IdRespuesta;
                            respuestaauditorindb.NumerodeCaja = numerocaja;
                            respuestaauditorindb.Respuesta = item.Valor.HasValue && item.Valor.Value == 1 ? "1" : "0";
                            respuestas.Add(respuestaauditorindb);
                        }
                        else
                        {
                            RespuestaAuditor answer = new RespuestaAuditor
                            {
                                IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                                IdRespuesta = item.IdRespuesta,
                                IdRuta = App.CurrentRoute.IdRuta,
                                IdSeccion = model.IdSeccion,
                                IdUbicacion = null, // falta este elemtno
                                NumerodeCaja = numerocaja, // falta ese elemento
                                Respuesta = item.Valor.HasValue && item.Valor.Value == 1 ? "1" : "0",
                                IdProducto = null
                            };
                            respuestas.Add(answer);
                        }
                    }
                }
                else if (model.PreguntaPadre.IdControl == 6) // la pregunta es un boton de foto
                {
                    if (model.PreguntaPadre.SeValida.HasValue && model.PreguntaPadre.SeValida.Value)
                    {
                        // se debe verificar que la respuesta exista
                        // verificamos que haya una foto
                        //if (String.IsNullOrEmpty(model.FileName)) // no  hay foto entonces
                        //{
                        if (model.File == null && String.IsNullOrEmpty(model.FileName)) // no  hay foto entonces
                        {
                            // mandamos estatus false para decir que no se lleno la sección
                            OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                            {
                                IsSuccess = false,
                                Message = "Toma la foto para la pregunta " + model.PreguntaPadre.Descripcion
                            });
                            // seccion incompleta
                            btnsave.IsEnabled = true;
                            notcomplete = true;
                            break;
                        }
                    }
                    if (model.IdRespuestaAuditor != null)
                    {
                        var respuestaauditorindb = respAudit.FirstOrDefault(ra => ra.IdAnswerAudit == model.IdRespuestaAuditor);
                        if (respuestaauditorindb != null)
                        {
                            if (string.IsNullOrEmpty(model.FotoNombre))
                                respuestaauditorindb.FotoNombre = model.FileName;
                            else
                                respuestaauditorindb.FotoNombre = model.FotoNombre;


                            if (model.Foto == null)
                                respuestaauditorindb.Foto = model.FotoEdit;
                            else
                                respuestaauditorindb.Foto = model.Foto;

                            respuestaauditorindb.NumerodeCaja = numerocaja;
                            //respuestaauditorindb.Respuesta = model.Respuesta;
                            respuestas.Add(respuestaauditorindb);
                        }
                    }
                    else
                    {
                        // guardamos el valor de la respuesta
                        RespuestaAuditor answer = new RespuestaAuditor
                        {
                            IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                            IdRespuesta = model.IdRespuesta,
                            IdRuta = App.CurrentRoute.IdRuta,
                            IdSeccion = model.IdSeccion,
                            IdUbicacion = null,
                            NumerodeCaja = numerocaja,
                            Foto = model.Foto,
                            FotoNombre = model.FotoNombre,
                            IdProducto = null
                        };
                        respuestas.Add(answer);
                    }
                }
            }
            // la sección se completo?
            if (!notcomplete)
            {
                // lanzamos el evento de que se ha completado, con las preguntas y el valor de si se debe hacer un update...
                OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                {
                    IsSuccess = true,
                    Edit = EditAnswers,
                    Respuestas = respuestas
                });
            }
        }

        /// <summary>
        /// Evento para cachar las respuestas procesadas
        /// </summary>
        private event EventHandler<RespuestaAuditorComplete> OnRespuestaAuditorComplete;

        private void OnRespuestaAuditorCompleted(RespuestaAuditorComplete answer)
        {
            if (OnRespuestaAuditorComplete != null)
            {
                OnRespuestaAuditorComplete(this, answer);
            }
        }

        #endregion

        #region Init
        int numerocaja = 0;
        private async void InitSection()
        {
            App.CurrentLocation.IdUbicacion = 0;
            int IdRuta = App.CurrentRoute.IdRuta;
            int IdSeccion = App.CurrentSection.IdSeccion;

            if (App.CurrentSection == null) return;
            // obtenemos todas las preguntas de esta seccion [_seccion]
            _models = PreguntaHelper.GetPreguntas();
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoASeccion, "InitSection", "QuestionPage", $"Ingresó a la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} ");
            //Bitacora.Insert("Init", "QuestionPage", _models);
            // si la sección no tiene preguntas, regresamos a la pagina anterior
            if (_models.Count == 0)
            {
                await DisplayAlert("PSM 360", "No hay preguntas para esta sección", "Aceptar");
                await Navigation.PopAsync();
                return;
            }

            // obtenemos el máximo para el número de caja
            int? max = App.DB.RespuestaAuditor.Where(e => e.IdSeccion == App.CurrentSection.IdSeccion && e.IdRuta == App.CurrentRoute.IdRuta).Max(e => e.NumerodeCaja);
            numerocaja = max.HasValue ? max.Value + 1 : 0;
            App.CycleNumber = numerocaja;

            //Verificamos si las preguntas tienen respuestas previamente contestadas dado que si es tipo anquel editable necesitamos traerlas
            if (App.CurrentSection.SeEdita == true)
            {
                var presentacionpregunta = new List<PreguntaModel>();
                var respuestaAuditor = App.DB.RespuestaAuditor.Where(s => s.Sincronizado == false && s.IdSeccion == IdSeccion && s.IdRuta == IdRuta).ToList();

                if (respuestaAuditor.Count() != 0)
                    EditAnswers = true;

                //Bandera que designa si el cuestionario es para productos o no
                //typepage = 0 No es tipo producto, typepage = 1 Es tipo producto.
                bool typepage = false;
                int indice = 1;
                PreguntaHelper.GetRespuestasPregunta(_models, respuestaAuditor, presentacionpregunta, null, typepage, indice);

                _models = presentacionpregunta;
            }

            // asignamos las preguntas
            //await SetPreguntas(_models);
            await Default(_models);
        }

        private async Task Default(List<PreguntaModel> models)
        {
            try
            {
                int contador = models.Count();
                var modelsstatic = models.To<List<PreguntaModel>>();
                int index = 1;

                for (int i = 0; i < contador; i++)
                {
                    //var model = models[i];
                    var modelstatic = modelsstatic[i];

                    var modelselect = models.FirstOrDefault(d => d.IdPregunta == modelstatic.IdPregunta);
                    {
                        var child = new PreguntaModelSub(modelstatic, this);
                        RootLayout.Children.Add(child);
                    }

                    index++;
                }
                //foreach (var model in models)
                //{
                //    var child = new PreguntaModelSub(model, this);
                //    RootLayout.Children.Add(child);
                //}
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
            }
        }

        private async Task SetPreguntas(List<PreguntaModel> models)
        {
            // Limpiamos la página
            RootLayout.Children.Clear();
            try
            {
                // iteramos las preguntas
                foreach (var model in models)
                {
                    // Obtenemos la pregunta
                    var pregunta = model.PreguntaPadre;
                    Label label = null;

                    if (pregunta.IdControl == 8)
                    {
                        label = new Label
                        {
                            Text = pregunta.Descripcion,
                            FontAttributes = FontAttributes.Bold,
                            FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                            HorizontalOptions = LayoutOptions.CenterAndExpand
                        };
                    }
                    else if (pregunta.IdControl == 9)
                    {
                        label = new Label
                        {
                            Text = pregunta.Descripcion,
                            FontAttributes = FontAttributes.Bold,
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                        };
                    }
                    else
                    {
                        label = new Label
                        {
                            Text = pregunta.Descripcion,
                            FontAttributes = FontAttributes.Bold
                        };
                    }

                    switch (pregunta.IdControl)
                    {
                        case 1:
                        case 5:
                            // creamos el campo de edición
                            Entry entry = new Entry
                            {
                                Placeholder = "Ingresa tu respuesta"
                            };
                            // si el campo es numerico
                            if (pregunta.IdControl == 5)
                            {
                                // asignamos
                                entry.Keyboard = Keyboard.Numeric;
                            }
                            // asignamos el id para el elemento de entrada
                            entry.ClassId = model.GetHashCode().ToString();
                            // evento para obtener la respuesta en tiempo de ejecución
                            entry.TextChanged += Entry_TextChanged;
                            entry.Unfocused += Entry_Unfocused;
                            // si al renderear el modelo tiene una respuesta
                            if (!string.IsNullOrEmpty(model.Respuesta))
                            {
                                //esta respuesta es asignada
                                entry.Text = model.Respuesta;
                            }
                            // asignamos la vista al model
                            model.View = entry;
                            // Agrgamos el label al rootlayou y el entry
                            RootLayout.Children.Add(label);
                            RootLayout.Children.Add(entry);

                            break;

                        case 7:
                            // el entry es de tipo moneda
                            EntryCurrency entrycurrency = new EntryCurrency();
                            // asignamos el id para la view del model
                            entrycurrency.ClassId = model.GetHashCode().ToString();
                            // evento de edición
                            entrycurrency.EntryCurrencyTextChanged += Entrycurrency_EntryCurrencyTextChanged;
                            // si tiene una respuesta el modelo
                            if (!string.IsNullOrEmpty(model.Respuesta))
                            {
                                // asignamos la respuesta
                                entrycurrency.Text = model.Respuesta.TextToMoney();
                            }

                            // asignamos la vista al model
                            model.View = entrycurrency;
                            // asignamos los elementos a la view
                            RootLayout.Children.Add(label);
                            RootLayout.Children.Add(entrycurrency);

                            break;

                        case 2:
                            var respuestascheck = App.DB.Respuesta.Where(e => e.IdPregunta == model.IdPregunta).ToList();
                            TableRoot root = new TableRoot();
                            TableView table = new TableView(root) { Intent = TableIntent.Form };
                            table.HeightRequest = respuestascheck.Count * 55;
                            TableSection section = new TableSection();
                            if (model.Respuestas == null)
                            {
                                model.Respuestas = new List<Respuesta>();
                            }
                            foreach (var respuesta in respuestascheck)
                            {
                                int respuestaint = 0;
                                bool respuestabool = false;
                                if (model.Respuestas != null)
                                {
                                    // buscamos la respuesta previamente realizada
                                    var respuestafound = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                                    // si la respuesta existe
                                    if (respuestafound != null)
                                    {
                                        // parseamos a boolean
                                        if (respuestafound.Valor.HasValue)
                                        {
                                            respuestaint = (int)respuestafound.Valor.Value;
                                        }
                                        respuestabool = respuestaint == 1;
                                    }
                                }
                                SwitchCell sview = new SwitchCell
                                {
                                    Text = respuesta.TextoOpcion,
                                    On = respuestabool,
                                    ClassId = respuesta.IdRespuesta.ToString(),
                                    StyleId = model.Id.ToString()
                                };
                                sview.OnChanged += Sview_OnChanged;
                                section.Add(sview);
                                if (model.Respuestas != null)
                                {
                                    var respuestafound = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                                    if (respuestafound != null)
                                    {
                                        //respuestafound.Valor = respuesta.Valor;
                                        //model.Respuestas.Remove(respuestafound);
                                    }
                                    //respuesta.Valor = respuestaint;
                                    //model.Respuestas.Add(respuesta);
                                }
                            }
                            root.Add(section);
                            RootLayout.Children.Add(label);
                            RootLayout.Children.Add(table);
                            break;

                        case 3:
                            // sacamos las respuestas del si/no
                            var respuestascombo = App.DB.Respuesta.Where(e => e.IdPregunta == pregunta.IdPregunta).ToList();
                            // creamos el picker
                            Picker picker = new Picker();
                            // asignamos el picker al modelo
                            model.View = picker;
                            // asignamos un id unico a la vista
                            picker.ClassId = model.GetHashCode().ToString();
                            // buscammos alguna respuesta por defecto o por default (valordefault) en las respuestas del combo
                            var answerdefault = respuestascombo.FirstOrDefault(e => e.ValorDefault.HasValue && e.ValorDefault.Value);
                            if (answerdefault != null)
                            {
                                // existe una respuesta por default
                            }
                            else
                            {
                                // no existe respuesta por default entonces agregamos una opcion por default
                                picker.Items.Add("-- Selecciona una opción --");
                            }

                            // el valor por default en index = 0, si encontramos un valor por default, cambiamos el indice al número correspondiente
                            int indexdefault = 0;
                            Respuesta respuestadefault = null;
                            // iteramos las respuestas del combo en busqueda del valor indexdefault
                            for (var o = 0; o < respuestascombo.Count; o++)
                            {
                                // obtenemos la respuesta
                                var respuesta = respuestascombo[o];
                                // si hay una respuesta por default, podemos comparar el idrespuesta del item que se asignará al picker, para así obtener el indice de ese valor por default
                                if (answerdefault != null)
                                {
                                    // si el idrespuesta es igual, este item es el correcto para el indice
                                    if (respuesta.IdRespuesta == answerdefault.IdRespuesta)
                                    {
                                        // asignamos el valor de o = indice del picker
                                        indexdefault = o;
                                        respuestadefault = respuesta;
                                    }
                                }
                                // asignamos el valor de la respuesta al picker
                                picker.Items.Add(respuesta.TextoOpcion);
                            }

                            // obtenemos el index de la respuesta actual
                            var selected = picker.Items.IndexOf(model.Respuesta);
                            // si el picker tiene más de un elemento, asignamos el index al primer elemento
                            if (picker.Items.Count > 0) { picker.SelectedIndex = 0; }
                            // si existe un index para la respuesta actual [post-render]
                            if (selected > -1)
                            {
                                // asignamos la respuesta previa del usuario al picker
                                picker.SelectedIndex = selected;
                            }
                            else
                            {
                                // en caso contrario, de no existir un index de respuesta anterior, verificamos
                                // si existe un default para la respuesta
                                if (answerdefault != null)
                                {
                                    // asignamos el index por default para la respuesta
                                    picker.SelectedIndex = indexdefault;
                                    model.Respuesta = picker.Items[indexdefault];
                                    if (respuestadefault != null) model.IdRespuesta = respuestadefault.IdRespuesta;
                                }
                            }
                            // evento lanzado cuando el picker cambie de indice
                            picker.SelectedIndexChanged += Picker_SelectedIndexChanged;
                            // asignamos la vista y los detalles a la vista
                            RootLayout.Children.Add(label);
                            RootLayout.Children.Add(picker);
                            break;

                        case 4:
                            // creamos el picker para fecha
                            DatePicker datepicker = new DatePicker();
                            // asignamos el id unico a la visa
                            datepicker.ClassId = model.GetHashCode().ToString();
                            // asignamos una fecha por default
                            datepicker.Date = new DateTime(DateTime.Now.Year, 1, 1);
                            // evento a lanzar cuando la fecha cambie
                            datepicker.DateSelected += Datepicker_DateSelected;
                            // si el modelo tiene una respuesta previa
                            if (!string.IsNullOrEmpty(model.Respuesta))
                            {
                                // asignamos la respuesta
                                datepicker.Date = DateTime.ParseExact(model.Respuesta, "yyyy-MM-dd", CultureInfo.CurrentCulture);
                            }
                            // asignamos la vista al modelo
                            model.View = datepicker;
                            // asignamos la pregunta y el datepicker a la vista
                            RootLayout.Children.Add(label);
                            RootLayout.Children.Add(datepicker);
                            break;

                        // foto
                        case 6:
                            // boton para tomar foto
                            Button btnfoto = new Button
                            {
                                Text = "Tomar foto"
                            };
                            // asignamos el id unico a la vista
                            btnfoto.ClassId = model.GetHashCode().ToString();
                            // asignamos el evento de click al boton
                            btnfoto.Clicked += Btnfoto_Clicked;
                            // si el botón tiene una respuesta previa, la asignamos
                            if (!string.IsNullOrEmpty(model.Respuesta))
                            {
                                btnfoto.BackgroundColor = Color.Accent;
                            }
                            // asignamos la pregunta y el botno a la vista
                            RootLayout.Children.Add(label);
                            RootLayout.Children.Add(btnfoto);
                            break;
                        case 8:
                        case 9:
                            RootLayout.Children.Add(label);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
            }
        }

        private void Entry_Unfocused(object sender, FocusEventArgs e)
        {
            // convertimos el sender en un campo
            var entry = sender as Entry;
            // obtenmos el hashcode
            var hashcode = int.Parse(entry.ClassId);
            // buscamos el modelo con base al hashcode
            var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
            // asignamos al modelo la respuesta del campo de texto
            model.Respuesta = entry.Text;
            // cambiamos el color del campo
            entry.BackgroundColor = Color.Accent;
        }

        private void Entrycurrency_EntryCurrencyTextChanged(object sender, EntryCurrencyTextChanged e)
        {
            // obtenemos el objeto entrycurrency
            var currency = sender as EntryCurrency;
            // convertimos el id unico a un int
            int hascode = int.Parse(currency.ClassId);
            // comparamos por hashcode
            var model = _models.FirstOrDefault(m => m.GetHashCode() == hascode);
            // si encontramos el modelo
            if (model != null)
            {
                // asignamos la respuesta al modelo
                model.Respuesta = e.Currency;
            }
        }

        private async void Btntakepicture_Clicked(object sender, EventArgs e)
        {
            // convertimos el sender en un boton
            var btntakepicture = sender as Button;
            // obtenemos el hashcode
            int hashcode = int.Parse(btntakepicture.ClassId);
            // obtengo el modelo asociado a este boton
            var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
            // si existe un modelo
            if (model != null)
            {
                // creamos el nombre de la foto
                var filename = App.CurrentUser.Alias + "-" + (App.CurrentRoute.IdProyectoTienda == 0 ? "0" : App.CurrentRoute.IdProyectoTienda.ToString()) + "-" + model.IdSeccion + "-" + model.IdPregunta + "-" + (App.CycleNumber == 0 ? "0" : App.CycleNumber.ToString()) + "-" + (App.CurrentPackage != null ? App.CurrentPackage.IdPresentacionEmpaque.ToString() : "0") + "-" + (App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion.ToString() : "0") + "-" + App.CurrentRoute.IdRuta + ".jpg";
                // creamos un cameracapturetask para 
                CameraCaptureTask task = new CameraCaptureTask
                {
                    FileName = filename,
                    FolderName = "PSM360Photos"
                };
                // photo result = null para 
                PhotoResult result = null;
                try
                {
                    // obtenemos la foto
                    result = await task.TakePhoto();
                }
                catch
                {

                }

                // si el resultado existe
                if (result != null)
                {
                    // si se tomo una foto
                    if (result.Success)
                    {
                        // validamos el archivo de la foto
                        if (result.Photo != null)
                        {
                            // asignamos el valor resultante de la foto
                            model.File = result;
                            // cambiamos el color del boton
                            btntakepicture.BackgroundColor = Color.Accent;
                        }
                        else
                        {
                            // en caso de que no exista el archivo mandamos un mensaje
                            await DisplayAlert("PSM 360", result.Message, "Aceptar");
                        }
                    }
                }
            }
        }

        private void Sview_OnChanged(object sender, ToggledEventArgs e)
        {
            // obtenemos el switch
            var switchcell = sender as SwitchCell;
            // convertimos el id unico en un idrespuesta
            int idrespuesta = int.Parse(switchcell.ClassId);
            // buscamos la respuesta en la base de datos a traves del idrespuesta
            var respuesta = App.DB.Respuesta.FirstOrDefault(r => r.IdRespuesta == idrespuesta);
            // buscamos el modelo a traves del idpregunta de la respuesta
            var model = _models.FirstOrDefault(m => m.IdPregunta == respuesta.IdPregunta);
            // asignamos el valor a la respuesta
            respuesta.Valor = e.Value ? 1 : 0;
            // si el modelo tiene respuestas
            if (model.Respuestas != null)
            {
                // buscamos la respuesta en el modelo
                var respuestaenmodel = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                // si existe la respuesta
                if (respuestaenmodel != null)
                {
                    // quitamos la respuesta para evitar duplicados
                    model.Respuestas.Remove(respuestaenmodel);
                }
                // agregamos la respuesta
                model.Respuestas.Add(respuesta);
            }
            else
            {
                // agregamos la respuesta
                model.Respuestas = new List<Respuesta> { respuesta };
            }
        }

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            // convertimos el sender en un campo
            var entry = sender as Entry;
            // cambiamos el color del campo
            entry.BackgroundColor = Color.Accent;
        }

        private void Datepicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            // convertimos el campo en un datepicker
            var datepicker = sender as DatePicker;
            // obtenemos el hashcode
            var hashcode = int.Parse(datepicker.ClassId);
            // obtenemos el modelo con base al hashcode
            var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
            // si el modelo existe
            if (model != null)
            {
                // asignamos la respuesta al modelo
                model.Respuesta = datepicker.Date.ToString("yyyy-MM-dd");
            }
            // cambiamos un background al datepicker
            datepicker.BackgroundColor = Color.Accent;
        }

        private async void Btnfoto_Clicked(object sender, EventArgs e)
        {
            var btnfoto = sender as Button;
            int hashcode = int.Parse(btnfoto.ClassId);
            var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
            if (model != null)
            {
                // nombre de la foto
                var filename = App.CurrentUser.Alias + "-" + (App.CurrentRoute.IdProyectoTienda == 0 ? "0" : App.CurrentRoute.IdProyectoTienda.ToString()) + "-" + model.IdSeccion + "-" + model.IdPregunta + "-" + (App.CycleNumber == 0 ? "0" : App.CycleNumber.ToString()) + "-" + (App.CurrentPackage != null ? App.CurrentPackage.IdPresentacionEmpaque.ToString() : "0") + "-" + (App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion.ToString() : "0") + "-" + App.CurrentRoute.IdRuta + ".jpg";
                // task para capturar foto
                CameraCaptureTask task = new CameraCaptureTask
                {
                    FileName = filename,
                    FolderName = "PSM360Photos"
                };

                // obtenemos la foto
                PhotoResult result = null;
                //if (Device.RuntimePlatform == Device.UWP)
                //{
                //    result = await task.PickPhoto();
                //}
                //else
                result = await task.TakePhoto();
                // si existe la foto
                if (result != null)
                {
                    // si falló la captura
                    if (!result.Success)
                    {
                        // mostramos el error generado
                        await DisplayAlert("PSM 360", result.Message, "Aceptar");
                    }
                    else
                    {
                        // guardamos el path en el modelo
                        model.Respuesta = result.Photo.Path;
                        // guardamos el archivo en el modelo
                        model.File = result;
                        // asignamos un color al botón
                        btnfoto.BackgroundColor = Color.Accent;
                    }
                }
            }
        }

        // picker si/no
        private async void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            double time = 2.00;
            // sender convertido a picker
            var picker = sender as Picker;
            // si hay un id en picker
            if (!string.IsNullOrEmpty(picker.ClassId))
            {

                // converitmos el id en hashcode
                int hashcode = int.Parse(picker.ClassId);
                // obtenemos el modelo
                var preguntamodel = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                // si el modelo no existe, terminamos la ejecución
                if (preguntamodel == null) return;
                // obtenemos la respuesta del picker
                var respuesta = picker.Items[picker.SelectedIndex];
                // si el modelo existe, asignamos la respuesta
                preguntamodel.Respuesta = respuesta;
                // obtenemos el indice del modelo con base al modelo
                var indexofmodel = _models.IndexOf(preguntamodel);
                // buscamos en la base de datos las respuestas
                var respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.TextoOpcion == respuesta && r.IdPregunta == preguntamodel.IdPregunta);
                // si hay una respuesta en la base de datos
                if (respuestaindb != null)
                {
                    // asignamos la respuesta seleccionada al modelo
                    preguntamodel.IdRespuesta = respuestaindb.IdRespuesta;
                    // si el indice del modelo es mayor a -1 quiere decir que existe en los modelos
                    if (indexofmodel > -1)
                    {
                        // preguntamos si la respuesta desencadena preguntas
                        if (respuestaindb.Desencadena)
                        {
                            // si la pregunta tiene preguntas hijas, podemos desencadenar
                            if (preguntamodel.PreguntaPadre.TieneHijos)
                            {
                                // asignamos al modelo que se ha descenadenado la respuesta | pregunta
                                preguntamodel.Desencadenada = true;
                                // insertamos en el rango, posterior al indice del modelo, las preguntas hijas
                                _models.InsertRange(indexofmodel + 1, preguntamodel.PreguntasHijas);
                                // volvemos a renderear la vista
                                await SetPreguntas(_models);
                            }
                        }
                        else if (preguntamodel.Desencadenada) // en caos de que la respuesta no desencadene, revisamos si previamente se ha descenadenado
                        {
                            // cambiamos el valor de desencadenada 
                            preguntamodel.Desencadenada = false;
                            // iteramos las preguntas hijas del modelo
                            foreach (var preguntahija in preguntamodel.PreguntasHijas)
                            {
                                // preguntamos si la pregunta hija se ha desencadenado
                                if (preguntahija.Desencadenada)
                                {
                                    // si se ha desencadenado, obtenemos la vista de la pregunta hija  [picker]
                                    var pickerhija = preguntahija.View as Picker;
                                    // y la asignamos a 0 para que lanze este evento, y borre las preguntas hijas que tiene del modelo
                                    pickerhija.SelectedIndex = 0;
                                }
                            }
                            // quitamos el rango de preguntas hijas asignadas despues del indice del modelo
                            _models.RemoveRange(indexofmodel + 1, preguntamodel.PreguntasHijas.Count);
                            // volvemos a renderear la vista
                            await SetPreguntas(_models);
                        }
                    }
                }
                else // no hay respuesta en la base de datos
                {
                    // preguntamos si se ha desencadenado el modelo
                    if (preguntamodel.Desencadenada)
                    {
                        // cambiamos el valor del desencadenamiento
                        preguntamodel.Desencadenada = false;
                        // iteramos las preguntas hijas en busqueda de pickers que desencadenen más preguntas
                        foreach (var preguntahija in preguntamodel.PreguntasHijas)
                        {
                            // preguntamos si la pregunta hija se ha desencadenado
                            if (preguntahija.Desencadenada)
                            {
                                // si se ha desencadenado, obtenemos la vista de la pregunta hija  [picker]
                                var pickerhija = preguntahija.View as Picker;
                                // y la asignamos a 0 para que lanze este evento, y borre las preguntas hijas que tiene del modelo
                                pickerhija.SelectedIndex = 0;
                            }
                        }
                        // quitamos el rango de preguntas hijas asignadas despues del indice del modelo
                        _models.RemoveRange(indexofmodel + 1, preguntamodel.PreguntasHijas.Count);
                        // volvemos a renderear la vista
                        await SetPreguntas(_models);
                    }
                }
            }
        }

        #endregion

        private void BtnSaveAnswers_Clicked(object sender, EventArgs e)
        {
            btnsave = sender as Button;
            btnsave.IsEnabled = false;
            // quitamos al evento el metodo questionpage_on...
            OnRespuestaAuditorComplete -= QuestionPage_OnRespuestaAuditorComplete;
            // asignamos el metodo al evento
            OnRespuestaAuditorComplete += QuestionPage_OnRespuestaAuditorComplete;
            // procesamos las respuestas para poder guardarlas
            RespuestaAuditorProcess();
        }
    }
}
