﻿using Plugin.Geolocator.Abstractions;
using PSM.Helpers;
using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM
{
    public partial class Tienda : ContentPage
    {
        private string dia = "";
        private bool child = false;
        private double longitud, latitud;
        private DateTimeOffset? FechaGps { get; set; }
        public ObservableCollection<Seccion> Modulos { get; set; }
        public Position Position { get; set; }
        private bool Sincronized { get; set; }
        private bool Extra { get; set; }
        #region eventos de xaml
        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dia = dias.Items[dias.SelectedIndex];
            List<Informacion> rutas = GetInformacion(dia);
            ListaTiendas.ItemsSource = rutas;
        }

        public bool TiendaClick { get; set; }
        private async void OnTiendaClick(Informacion inf)
        {
            ListaTiendas.IsEnabled = false;
            TiendaClick = true;
            App.IdTienda = inf.IdTienda;
            await Navigation.PushAsync(new UbicacionPage() { Title = inf.NombreTienda });
        }

        private void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            ListaTiendas.SelectedItem = null;
        }

        private async void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            ValidateDevice.Validate("Tienda_Handle_ItemTapped", "TiendaPage");
            if (TiendaClick)
            {
                TiendaClick = false;
            }
            else
            {
                var inf = e.Item as Informacion;
                if (App.DB.Asistencia.Any(a => a.IdRuta == inf.IdRuta && a.descripcion == App.MODULO_SALIDA))
                {

                    await DisplayAlert("PSM 360", "La tienda ya se ha cerrado", "Aceptar");
                    ListaTiendas.IsEnabled = true;
                }
                else
                {
                    App.CurrentRoute = App.DB.Ruta.FirstOrDefault(r => r.IdRuta == inf.IdRuta);
                    await Navigation.PushAsync(new ModulosPage());
                }
            }
        }


        #endregion
        //public static HomePage Instance { get; set; }

        public Tienda()
        {
            InitializeComponent();
            App.CurrentPage = this;
            foreach (var item in GetDias())
            {
                dias.Items.Add(item.Dia);
            }
            int nodia = (int)DateTime.Now.DayOfWeek;
            string day = GetDias().FirstOrDefault(a => a.IdDia == nodia).Dia;
            int idday = GetDias().FirstOrDefault(a => a.IdDia == nodia).IdDia;
            dias.SelectedIndex = idday;
            List<Informacion> rutas = GetInformacion(dia);
            ListaTiendas.ItemsSource = rutas;
        }

        protected override void OnAppearing()
        {
            ListaTiendas.IsEnabled = true;
            //DisplayAlert("Hola", "Vine a esta pantalla", "Aceptar");
            if (child)
            {
                List<Informacion> rutas = GetInformacion(dia);
                ListaTiendas.ItemsSource = rutas;
                child = false;
            }
        }

        public List<Informacion> GetInformacion(string dia)
        {
            string fecha = DateTime.Now.ToString();
            DateTime inputDate = DateTime.Parse(fecha.Trim());
            // formatea la fecha de acuerdo a la zona del computador
            var d = inputDate;

            CultureInfo cul = CultureInfo.CurrentCulture;
            // Usa la fecha formateada y calcula el número de la semana
            int NumeroSemana = cul.Calendar.GetWeekOfYear(
                 d,
                 CalendarWeekRule.FirstDay,
                 DayOfWeek.Sunday);

            var rutas = App.DB.Ruta.Where(r => r.NombreDelDia.Equals(dia) && r.NumeroSemana == NumeroSemana).ToList();
            var informacion = App.DB.InfoTienda.ToList();

            var rutainfotiendalist = (from r in rutas
                                      join i in informacion on r.IdTienda equals i.IdTienda
                                      where r.Sincronizado == false
                                      select new Informacion
                                      {
                                          Direccion = i.Direccion,
                                          IdRuta = r.IdRuta,
                                          IdTienda = i.IdTienda,
                                          NombreTienda = i.NombreTienda,
                                          IdRutaDinamica = r.IdRutaDinamica,
                                          OnTiendaClick = new Command<Informacion>(OnTiendaClick),
                                          OnTiendaOpenClick = new Command<Informacion>(OnTiendaOpenClick)
                                      }).ToList();
            return rutainfotiendalist;
        }


        private async void OnTiendaOpenClick(Informacion informacion)
        {
            ListaTiendas.IsEnabled = false;
            if (TiendaClick)
            {
                ListaTiendas.IsEnabled = true;
                TiendaClick = false;
            }
            else
            {
                var info = App.DB.InfoTienda.FirstOrDefault(g => g.IdTienda == informacion.IdTienda);


                if (info.EstatusCheck != null)
                {
                    if ((bool)info.EstatusCheck)
                    {
                        var idTiempo = App.DB.Ruta.FirstOrDefault(s => s.IdRuta == informacion.IdRuta).IdTiempo;

                        var asistenciaEntrada = App.DB.Asistencia.Where(e => e.descripcion == App.MODULO_ENTRADAGLOB).ToList();
                        var ruta = App.DB.Ruta.Where(s => s.IdTiempo == idTiempo).ToList();

                        var asistenciadiaEntrada = (from a in asistenciaEntrada
                                                    join r in ruta on a.IdRuta equals r.IdRuta
                                                    select a).ToList();

                        var asistenciaSalida = App.DB.Asistencia.Where(e => e.descripcion == App.MODULO_SALIDAGLOB).ToList();
                        var asistenciadiaSalida = (from a in asistenciaSalida
                                                   join r in ruta on a.IdRuta equals r.IdRuta
                                                   select a).ToList();

                        //valida que para realizar la entrada de la visita final debe cerrar la visita inicial
                        if (asistenciadiaEntrada.Count() == 0 && asistenciadiaSalida.Count() == 0)
                        {
                            await DisplayAlert("PSM 360", "Debe realizar primero la 'Actividad de inicio'", "Aceptar");
                            ListaTiendas.IsEnabled = true;
                        }
                        //valida que para realizar la entrada de la visita final debe cerrar la visita inicial
                        else if (asistenciadiaEntrada.Count() != 0 && asistenciadiaSalida.Count() == 0)
                        {
                            await DisplayAlert("PSM 360", "Debe cerrar primero la 'Actividad de inicio'", "Aceptar");
                            ListaTiendas.IsEnabled = true;
                        }
                        else if (App.DB.Asistencia.Any(a => a.IdRuta == informacion.IdRuta && (a.descripcion == App.MODULO_SALIDA || a.descripcion == App.MODULO_SALIDAGLOB)))
                        {
                            await DisplayAlert("PSM 360", "La tienda ya se ha cerrado", "Aceptar");
                            ListaTiendas.IsEnabled = true;
                        }
                        else
                        {
                            App.CurrentRoute = App.DB.Ruta.FirstOrDefault(r => r.IdRuta == informacion.IdRuta);
                            await Navigation.PushAsync(new ModulosPage());
                            child = true;
                        }
                    }
                    else
                    {
                        if (App.DB.Asistencia.Any(a => (a.IdRuta == informacion.IdRuta || a.IdRuta == informacion.IdRutaDinamica) && (a.descripcion == App.MODULO_SALIDA || a.descripcion == App.MODULO_SALIDAGLOB)))
                        {
                            await DisplayAlert("PSM 360", "La tienda ya se ha cerrado", "Aceptar");
                            ListaTiendas.IsEnabled = true;
                        }
                        else
                        {
                            App.CurrentRoute = App.DB.Ruta.FirstOrDefault(r => r.IdRuta == informacion.IdRuta);
                            await Navigation.PushAsync(new ModulosPage());
                            child = true;
                        }
                    }
                }
                else
                {
                    if (App.DB.Asistencia.Any(a => (a.IdRuta == informacion.IdRuta || a.IdRuta == informacion.IdRutaDinamica) && (a.descripcion == App.MODULO_SALIDA || a.descripcion == App.MODULO_SALIDAGLOB)))
                    {
                        await DisplayAlert("PSM 360", "La tienda ya se ha cerrado", "Aceptar");
                        ListaTiendas.IsEnabled = true;
                    }
                    else
                    {
                        App.CurrentRoute = App.DB.Ruta.FirstOrDefault(r => r.IdRuta == informacion.IdRuta);
                        await Navigation.PushAsync(new ModulosPage());
                        child = true;
                    }
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {
            var navigationpage = new NavigationPage(new MainPage());
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            HomePage.Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
            return true;
        }

        //private async Task createRoute(TiendasCercanasRuta tienda)
        //{
        //    DateTime fecha = DateTime.Now;
        //    DateTimeFormatInfo dfy = DateTimeFormatInfo.CurrentInfo;
        //    Calendar cal = dfy.Calendar;
        //    int nodia = (int)DateTime.Now.DayOfWeek;
        //    string nombreDia = GetDias().Where(d => d.IdDia == nodia).First().Dia;
        //    //int semanaActual = CultureInfo.CurrentUICulture.Calendar.GetWeekOfYear(fecha, CalendarWeekRule.FirstDay, fecha.DayOfWeek) -1;
        //    int semanaActual = cal.GetWeekOfYear(fecha, dfy.CalendarWeekRule, dfy.FirstDayOfWeek);
        //    Ruta ruta = App.DB.Ruta.Where(r => r.IdTienda == tienda.IdTienda && r.NumeroSemana == semanaActual && r.NombreDelDia == nombreDia).FirstOrDefault();
        //    if (ruta != null)
        //    {
        //        await DisplayAlert("Info", $"La tienda {tienda.TiendaNombre} ya tiene una ruta existente para esta semana", "Aceptar");
        //        return;
        //    }
        //    else
        //    {
        //        var idRuta = App.DB.Ruta.Where(r => r.IdRuta < 0).OrderBy(r => r.IdRuta).FirstOrDefault();
        //        //var idTiempo = App.DB.Ruta.Where(r => r.NumeroSemana == semanaActual && r.NombreDelDia == nombreDia).FirstOrDefault();
        //        var idTiempo = App.DB.Tiempo.Where(r => r.NombreDia.Equals(nombreDia)).FirstOrDefault();
        //        int difSemana = (semanaActual - idTiempo.SemanadelAño) * 7;
        //        int idInfoTienda = 0;

        //        Ruta rutaAlta = new Ruta();
        //        rutaAlta.IdProyecto = App.CurrentUser.IdProyecto;
        //        rutaAlta.IdUsuario = App.CurrentUser.IdUsuario;
        //        rutaAlta.IdTiempo = idTiempo.IdTiempo + difSemana;
        //        rutaAlta.Status = 0;
        //        rutaAlta.IdTienda = tienda.IdTienda;
        //        rutaAlta.IdProyectoTienda = tienda.IdProyectoTienda;
        //        rutaAlta.TiendaNombre = tienda.TiendaNombre;
        //        rutaAlta.NumeroSemana = semanaActual;
        //        rutaAlta.NombreDelDia = nombreDia;
        //        rutaAlta.FechaInicio = null;
        //        rutaAlta.FechaTerminacion = null;
        //        rutaAlta.GpsLatitude = null;
        //        rutaAlta.GpsLongitude = null;
        //        rutaAlta.Sincronizado = false;
        //        rutaAlta.IdRuta = idRuta != null ? (idRuta.IdRuta - 1) : -1;

        //        App.DB.Ruta.Add(rutaAlta);
        //        App.DB.SaveChanges();

        //        // Se comienza a validar si existe InfoTienda
        //        var infoTienda = App.DB.InfoTienda.FirstOrDefault(i => i.IdTienda == tienda.IdTienda);

        //        // Si no existe, se crea el registro de InfoTienda correspondiente a la tienda
        //        if (infoTienda == null)
        //        {
        //            InfoTienda info = new InfoTienda()
        //            {
        //                IdInfoTienda = idInfoTienda,
        //                IdTienda = tienda.IdTienda,
        //                Latitud = tienda.CoordenadaY,
        //                Longitud = tienda.CoordenadaX,
        //                NombreTienda = tienda.TiendaNombre,
        //                Direccion = "",
        //                ImagenUrl = null,
        //                IdCadena = tienda.IdCadena,
        //                Verificada = tienda.Verificada
        //            };

        //            App.DB.InfoTienda.Add(info);
        //            App.DB.SaveChanges();
        //        }
        //        await DisplayAlert("Información", $"Se ha agregado la tienda {tienda.TiendaNombre} para visitarla el día de hoy", "Aceptar");
        //    }
        //}

        #region lista de dias
        private IList<ListaDias> GetDias()
        {
            return new List<ListaDias>
            {
                new ListaDias { IdDia=0, Dia="Domingo" },
                new ListaDias { IdDia=1, Dia="Lunes" },
                new ListaDias { IdDia=2, Dia="Martes" },
                new ListaDias { IdDia=3, Dia="Miercoles" },
                new ListaDias { IdDia=4, Dia="Jueves" },
                new ListaDias { IdDia=5, Dia="Viernes" },
                new ListaDias { IdDia=6, Dia="Sabado" },
            };
        }
        #endregion

        /*
        /// <summary>
        /// [Daniel] una lista observable puede cambiar el estado actual de una listview sin actualizar o hacer cosas raras [MVVM]
        /// Ademas de que permite crear un efecto de transicion que se ve bien OP :D
        /// </summary>
        ObservableCollection<Informacion> infti = new ObservableCollection<Informacion>();

        public Tienda()
        {
            InitializeComponent();
            App.CurrentPage = this;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            foreach (var item in GetDias())
            {
                dias.Items.Add(item.Dia);
            }
            int nodia = (int)DateTime.Now.DayOfWeek;
            string day = GetDias().FirstOrDefault(a => a.IdDia == nodia).Dia;
            int idday = GetDias().FirstOrDefault(a => a.IdDia == nodia).IdDia;
            dias.SelectedIndex = idday;
            ListaTiendas.ItemsSource = infti;
        }

        #region Eventos de XAML
        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var dia = dias.Items[dias.SelectedIndex];
            List<Ruta> ruta = GetRuta(dia);
            List<InfoTienda> info = GetInfo(ruta);
            // limpiamos la lista
            while(infti.Any())
            {
                infti.RemoveAt(0);
            }
            
            foreach (var item in info)
            {
                int idruta = App.DB.Ruta.FirstOrDefault(a => a.IdTienda == item.IdTienda).IdRuta;
                Informacion intienda = new Informacion();
                intienda.NombreTienda = item.NombreTienda;
                intienda.Direccion = item.Direccion;
                intienda.IdRuta = idruta;
                intienda.IdTienda = item.IdTienda;
                intienda.OnTiendaClick = new Command(OnTiendaClick);
                infti.Add(intienda);
            }
        }

        private async void OnTiendaClick(object obj)
        {
            Informacion inf = (Informacion)obj;
            App.IdTienda = inf.IdTienda;
            await Navigation.PushAsync(new UbicacionPage() { Title = "Ubicación de la tienda " + inf.NombreTienda });
        }

        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            ListaTiendas.SelectedItem = null;
        }

        async void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var infor = e.Item as Informacion;
            if (infor != null)
            {
                var ruta = App.DB.Ruta.FirstOrDefault(r => r.IdRuta == infor.IdRuta);
                if (ruta != null)
                {
                    App.IdRuta = ruta.IdRuta;
                    App.CurrentRoute = ruta;
                }
                await Navigation.PushAsync(new ModulosPage() { Title = "Ubicación de la tienda " + infor.NombreTienda });
            }
        }
        
        #endregion

        #region Get's
        public List<Ruta> GetRuta(string dia)
        {
            var rutasdb = App.DB.Ruta.Where(a => a.NombreDelDia == dia);
            List<Ruta> ruta = new List<Ruta>();
            foreach (var item in rutasdb)
            {
                ruta.Add(new Ruta
                {
                    FechaInicio = item.FechaInicio,
                    FechaTerminacion = item.FechaTerminacion,
                    GpsLatitude = item.GpsLatitude,
                    GpsLongitude = item.GpsLongitude,
                    IdProyecto = item.IdProyecto,
                    IdProyectoTienda = item.IdProyectoTienda,
                    IdRuta = item.IdRuta,
                    IdTiempo = item.IdTiempo,
                    IdTienda = item.IdTienda,
                    IdUsuario = item.IdUsuario,
                    NombreDelDia = item.NombreDelDia,
                    NumeroSemana = item.NumeroSemana,
                    Status = item.Status,
                    TiendaNombre = item.TiendaNombre
                });
            }
            return ruta;
        }

        public List<InfoTienda> GetInfo(List<Ruta> ruta)
        {
            List<int> idtiendas = ruta.Select(e => e.IdTienda).ToList();
            var infotiendadb = App.DB.InfoTienda.Where(e => idtiendas.Contains(e.IdTienda));
            List<InfoTienda> infotiendalist = new List<InfoTienda>();
            foreach (var item in infotiendadb)
            {
                infotiendalist.Add(new InfoTienda
                {
                    Direccion = item.Direccion,
                    IdCadena = item.IdCadena,
                    IdInfoTienda = item.IdInfoTienda,
                    IdTienda = item.IdTienda,
                    ImagenUrl = item.ImagenUrl,
                    Latitud = item.Latitud,
                    Longitud = item.Longitud,
                    NombreTienda = item.NombreTienda
                });
            }
            return infotiendalist;
        }
        #endregion

        #region Lista de dias
        private IList<ListaDias> GetDias()
        {
            return new List<ListaDias>
            {
                new ListaDias { IdDia=0, Dia="Domingo" },
                new ListaDias { IdDia=1, Dia="Lunes" },
                new ListaDias { IdDia=2, Dia="Martes" },
                new ListaDias { IdDia=3, Dia="Miercoles" },
                new ListaDias { IdDia=4, Dia="Jueves" },
                new ListaDias { IdDia=5, Dia="Viernes" },
                new ListaDias { IdDia=6, Dia="Sabado" },
            };
        }
        #endregion */
    }
}
