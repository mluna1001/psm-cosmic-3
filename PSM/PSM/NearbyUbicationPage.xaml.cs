﻿using Plugin.Connectivity;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using PSM.Function;
using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NearbyUbicationPage : ContentPage
    {
        double _Latitud;
        double _Longitud;
        public Position Position { get; set; }

        public NearbyUbicationPage()
        {
            InitializeComponent();
            LoadNearbyGPS();
        }

        private void LoadNearbyGPS()
        {
            lvTiendasCercanas.IsPullToRefreshEnabled = true;
            if (App.DB.Tiempo.Any())
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    DisplayAlert("Información", "Debe estar conectado a una conexión de red para mostrar esta pantalla", "Aceptar");
                    return;
                }
                ProgressAction(new AsyncCommand(async () =>
                {
                    await DeterminaUbicacion();
                    lvTiendasCercanas.ItemsSource = await MisSubordinados();
                }));
            }
            else
            {
                DisplayAlert("Info", "Primero descarga tus catálogos.", "Aceptar");
                lvTiendasCercanas.ItemsSource = null;
                return;
            }
        }

        public async Task DeterminaUbicacion()
        {
            if (CrossGeolocator.Current.IsGeolocationAvailable)
            {
                if (CrossGeolocator.Current.IsGeolocationEnabled)
                {
                    try
                    {
                        Position = await CrossGeolocator.Current.GetPositionAsync();
                        var dategps = Position.Timestamp;
                        _Longitud = Position.Longitude;
                        _Latitud = Position.Latitude;
                    }
                    catch { }
                }
                else
                {
                    await DisplayAlert("PSM 360", "No tienes iniciado el GPS, por favor activalo", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoGpsIniciado, "DeterminaUbicacion", "NearbyUbicationPage", "GPS no iniciado");
                }
            }
            else
            {
                await DisplayAlert("PSM 360", "Debes permitir el uso del GPS para poder usar esta app", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SinPermisosGps, "DeterminaUbicacion", "NearbyUbicationPage", "GPS sin permisos");
            }

            if (Position == null)
            {
                if (!CrossGeolocator.Current.IsListening)
                {
                    try
                    {
                        CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
                        if (await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 0))
                        {
                        }
                        else
                        {
                            await DisplayAlert("PSM 360", "No se pudo iniciar el GPS...", "Aceptar");
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoGpsIniciado, "DeterminaUbicacion", "NearbyUbicationPage", "GPS no iniciado");
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void Current_PositionChanged(object sender, PositionEventArgs e)
        {
            Position = e.Position;
            var dategps = e.Position.Timestamp;
            _Longitud = e.Position.Longitude;
            _Latitud = e.Position.Latitude;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RecallUbicacionGps, "Current_PositionChanged", "NearbyUbicationPage", e);
            CrossGeolocator.Current.PositionChanged -= Current_PositionChanged;
        }

        public async void ProgressAction(AsyncCommand command)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                StackProgress.IsVisible = true;
                lvTiendasCercanas.IsVisible = false;
            });

            if (command != null)
            {
                await command.ExecuteAsync(null);
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                StackProgress.IsVisible = false;
                lvTiendasCercanas.IsVisible = true;
            });
        }

        private async Task<List<TiendasCercanasUbicacion>> MisSubordinados()
        {
            var Tiendas = new List<TiendasCercanasUbicacion>();
            var isconnected = false;

            Tiendas = await Task.Run(() => App.API.NearbyUbicationEndPoint.NearbyUbicationStores(App.CurrentUser.IdUsuario, _Latitud, _Longitud, () =>
                isconnected = false));

            return Tiendas;
        }

        private void ListView_Refreshing(object sender, EventArgs e)
        {
            LoadNearbyGPS();
        }

        private void lstTiendasCercanas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lvTiendasCercanas.SelectedItem = null;
        }

        private async void lvTiendasCercanas_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            TiendasCercanasUbicacion tienda = (TiendasCercanasUbicacion)e.Item;
            lvTiendasCercanas.IsEnabled = false;
            await SaveRoute(tienda);
        }

        private async Task SaveRoute(TiendasCercanasUbicacion tienda)
        {
            DateTime fecha = DateTime.Now;
            DateTimeFormatInfo dfy = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfy.Calendar;
            int nodia = (int)DateTime.Now.DayOfWeek;
            string nombreDia = GetDias().Where(d => d.IdDia == nodia).First().Dia;
            //int semanaActual = CultureInfo.CurrentUICulture.Calendar.GetWeekOfYear(fecha, CalendarWeekRule.FirstDay, fecha.DayOfWeek);
            int semanaActual = cal.GetWeekOfYear(fecha, dfy.CalendarWeekRule, dfy.FirstDayOfWeek);
            //Ruta ruta = App.DB.Ruta.Where(r => r.IdTienda == tienda.IdTienda && r.NumeroSemana == semanaActual && r.NombreDelDia == nombreDia).FirstOrDefault();
            //if (ruta != null)
            //{
            //    await DisplayAlert("Info", $"La tienda {tienda.TiendaNombre} ya tiene una ruta existente para esta semana", "Aceptar");
            //    return;
            //}
            //else
            //{
            var idRuta = App.DB.Ruta.Where(r => r.IdRuta < 0).OrderBy(r => r.IdRuta).FirstOrDefault();
            var idTiempo = App.DB.Tiempo.Where(r => r.NombreDia.Equals(nombreDia)).FirstOrDefault();

            if (idTiempo != null)
            {
                int difSemana = (semanaActual - idTiempo.SemanadelAño) * 7;

                Ruta rutaAlta = new Ruta();
                rutaAlta.IdProyecto = App.CurrentUser.IdProyecto;
                rutaAlta.IdUsuario = App.CurrentUser.IdUsuario;
                rutaAlta.IdTiempo = idTiempo.IdTiempo + difSemana;
                rutaAlta.Status = 0;
                rutaAlta.IdTienda = tienda.IdTienda;
                rutaAlta.IdProyectoTienda = tienda.IdProyectoTienda;
                rutaAlta.TiendaNombre = tienda.TiendaNombre;
                rutaAlta.NumeroSemana = semanaActual;
                rutaAlta.NombreDelDia = nombreDia;
                rutaAlta.FechaInicio = null;
                rutaAlta.FechaTerminacion = null;
                rutaAlta.GpsLatitude = null;
                rutaAlta.GpsLongitude = null;
                rutaAlta.Sincronizado = false;
                rutaAlta.IdRuta = idRuta != null ? (idRuta.IdRuta - 1) : -1;

                App.DB.Ruta.Add(rutaAlta);
                App.DB.SaveChanges();

                ExisteInfoTienda(tienda);
                await DisplayAlert("Información", $"Se ha agregado la tienda {tienda.TiendaNombre} para visitarla el día de hoy", "Aceptar");
            }
            else
            {
                await DisplayAlert("Información", $"Es necesario descargar tiendas antes de la creación", "Aceptar");
                return;
            }
            //}
            lvTiendasCercanas.IsEnabled = true;
        }

        private void ExisteInfoTienda(TiendasCercanasUbicacion tienda)
        {
            int idInfoTienda = 0;

            InfoTienda it = App.DB.InfoTienda.FirstOrDefault(t => t.IdTienda == tienda.IdTienda);

            if (it == null)
            {
                InfoTienda info = new InfoTienda()
                {
                    //IdInfoTienda = idInfoTienda,
                    IdTienda = tienda.IdTienda,
                    Latitud = tienda.CoordenadaY,
                    Longitud = tienda.CoordenadaX,
                    IdCadena = tienda.IdCadena,
                    NombreTienda = tienda.TiendaNombre,
                    ImagenUrl = null,
                    Direccion = "",
                    Verificada = tienda.Verificada
                };

                App.DB.InfoTienda.Add(info);
                App.DB.SaveChanges();
            }
        }

        private IList<ListaDias> GetDias()
        {
            return new List<ListaDias>
            {
                new ListaDias { IdDia=0, Dia="Domingo" },
                new ListaDias { IdDia=1, Dia="Lunes" },
                new ListaDias { IdDia=2, Dia="Martes" },
                new ListaDias { IdDia=3, Dia="Miercoles" },
                new ListaDias { IdDia=4, Dia="Jueves" },
                new ListaDias { IdDia=5, Dia="Viernes" },
                new ListaDias { IdDia=6, Dia="Sabado" },
            };
        }

        protected override bool OnBackButtonPressed()
        {
            var navigationpage = new NavigationPage(new MainPage());
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            HomePage.Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
            return true;
        }
    }
}