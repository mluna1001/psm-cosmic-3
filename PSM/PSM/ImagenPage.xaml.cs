﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImagenPage : ContentPage
    {
        #region eventos de xaml
        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            List<Imagen> lstimg = new List<Imagen>();
            lstimg = ListaImagenes().ToList();
            ListaDeImagenes.ItemsSource = lstimg;
        }

        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            ListaDeImagenes.SelectedItem = null;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listaimgen = e.Item as Imagen;
            App.ImagenVisualizar = listaimgen.URL;
            ((NavigationPage)this.Parent).PushAsync(new PintarImagenPage());
        }

        #endregion

        public ImagenPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            App.ImagenVisualizar = "";
            ListaDeImagenes.ItemsSource = ListaImagenes();
        }

        #region Lista Imagen
        private IList<Imagen> ListaImagenes()
        {
            List<Imagen> img = new List<Imagen>();
            img = App.DB.Imagen.Where(a => a.Vigente).ToList();
            return img;
        }
        #endregion
    }
}
