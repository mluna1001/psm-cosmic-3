﻿using PSM.API;
using PSM.Helpers;
using PSM.ORM;
using PSM.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM
{

    public partial class AvisosPage : ContentPage
    {
        #region eventos de xaml
        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            List<Avisos> listaavisos = new List<Avisos>();
            listaavisos = ListaAvisos().ToList();
            ListaDeAvisos.ItemsSource = listaavisos;
        }

        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            ListaDeAvisos.SelectedItem = null;
        }

        async void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listaav = e.Item as Avisos;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "Handle_ItemTapped", "AvisosPage", "Se ha seleccionado el aviso: " + listaav.Mensaje);
            switch (listaav.IdTipoAviso)
            {
                case (int)TypeEnum.TipoAviso.Informativo:
                    await DisplayAlert(listaav.Titulo, listaav.Mensaje, "Aceptar");
                    break;
                case (int)TypeEnum.TipoAviso.EnlaceExterno:
                    await Navigation.PushModalAsync(new NoticeModalPage(listaav.To<Avisos>()));
                    break;
            }
            
        }

        #endregion

        public AvisosPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            ListaDeAvisos.ItemsSource = ListaAvisos();
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "AvisosPage", ListaAvisos());
        }

        protected override bool OnBackButtonPressed()
        {
            var navigationpage = new NavigationPage(new MainPage());
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            HomePage.Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
            return true;
        }

        #region Lista Avisos
        private IList<Avisos> ListaAvisos()
        {
            List<Avisos> avisos = new List<Avisos>();
            avisos = App.DB.Avisos.Where(a => a.Vigente).ToList();
            return avisos;
        }
        #endregion
    }
}
