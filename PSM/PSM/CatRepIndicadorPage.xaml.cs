﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CatRepIndicadorPage : ContentPage
	{
		public CatRepIndicadorPage ()
		{
			InitializeComponent ();
            GetCatalogoIndicador();
		}

        void GetCatalogoIndicador()
        {
            var catalogosIndicador = App.DB.CatalogoIndicador.ToList();
            lvIndicadores.ItemsSource = catalogosIndicador;
        }

        private void LvIndicadores_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lvIndicadores.SelectedItem = null;
        }

        private async void LvIndicadores_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            CatalogoIndicador catalogoIndicador = e.Item as CatalogoIndicador;
            if (catalogoIndicador != null)
            {
                await Navigation.PushAsync(new ReporteIndicadorPage(catalogoIndicador));
            }
        }
    }
}