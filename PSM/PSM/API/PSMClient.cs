﻿using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;
using PSM.Helpers.Services;
using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.API
{

    public class Param
    {

        public object Element { get; set; }

        public string Name { get; set; }

        public string FileName { get; set; }

        public ParamType Type { get; set; }

        public byte[] Bytes
        {
            get
            {
                if (Type == ParamType.File)
                {
                    if (Element is byte[])
                    {
                        return Element as byte[];
                    }
                    else if (Element is Stream)
                    {
                        var streamcontent = Element as Stream;
                        return ReadFully(streamcontent);
                    }
                }
                return null;
            }
        }

        public string ContentType { get; set; }

        public Param(string name, object value, ParamType type = ParamType.String, string filename = "file.jpg", string contenttype = "application/json")
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("El parametro NAME es null o esta vacio");
            }
            Element = value ?? throw new Exception("El parametro VALUE es null");
            Name = name;
            FileName = filename;
            Type = type;
            ContentType = contenttype;
        }

        private byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }

    public enum ParamType
    {
        File, String, Obj
    }

    public class PSMClient
    {
        /// <summary>
        /// Propiedad que permite, hacer peticiones de tipo usuario al servidor
        /// </summary>
        public UserRequest UserEndPoint { get; set; }

        public SeccionRequest SectionEndPoint { get; set; }

        public RutaRequest RouteEndPoint { get; set; }

        public QualiRequest QualiEndPoint { get; set; }

        public CatalogoRequest CatalogoEndPoint { get; set; }

        public UploadRequest UploadEndPoint { get; set; }

        public ChatRequest ChatEndPoint { get; set; }

        public TiendasCercanasRutaRequest NearbyRouteStore { get; set; }

        public TiendasCercanasUbicacionRequest NearbyUbicationEndPoint { get; set; }

        public RutaProspectoRequest ProspectStoreRequest { get; set; }

        private Client _client { get; set; }

        public static String BaseUrl { get; set; }

        public PSMClient()
        {
            // creamos un cliente http
            _client = new Client();

#if DEBUG
            if (Device.RuntimePlatform == Device.UWP)
            {
                //BaseUrl = "http://localhost:45332";
                BaseUrl = "http://api.cosmic.mx";
                //BaseUrl = "http://apistage.cosmic.mx";
                //BaseUrl = "http://desarrollopsm.xyz";
            }
            else
            {
                BaseUrl = "http://api.cosmic.mx";
                //BaseUrl = "http://apistage.cosmic.mx";
                //BaseUrl = "http://desarrollopsm.xyz";
                //BaseUrl = "http://desarrollocosmic.xyz";
                //BaseUrl = "http://209.126.119.40/plesk-site-preview/desarrollocosmic.xyz";
            }
#else
            BaseUrl = "http://api.cosmic.mx";
            //BaseUrl = "http://desarrollopsm.xyz";
            
#endif
            // asignamos el userendpoint [request] 
            UserEndPoint = new UserRequest(_client);
            SectionEndPoint = new SeccionRequest(_client);
            RouteEndPoint = new RutaRequest(_client);
            CatalogoEndPoint = new CatalogoRequest(_client);
            UploadEndPoint = new UploadRequest(_client);
            ChatEndPoint = new ChatRequest(_client);
            QualiEndPoint = new QualiRequest(_client);
            NearbyRouteStore = new TiendasCercanasRutaRequest(_client);
            NearbyUbicationEndPoint = new TiendasCercanasUbicacionRequest(_client);
            ProspectStoreRequest = new RutaProspectoRequest(_client);
        }

        public async Task<Stream> Download(string url, Action noconnected)
        {
            return await _client.DownloadFile(url, noconnected);
        }

        public class Client
        {
            public async Task<T> UploadFile<T>(string url, Stream file, Action noconnected)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    try
                    {
                        var client = new HttpClient();
                        var form = new MultipartFormDataContent
                        {
                            { new StreamContent(file), "fotopsm", "fotopsm" }
                        };
                        var response = await client.PostAsync(url, form);
                        if (response != null && response.IsSuccessStatusCode)
                        {
                            var responsestring = await response.Content.ReadAsStringAsync();
                            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responsestring);
                        }
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex, App.TrackData());
                        System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    }
                }
                else
                {
                    noconnected?.Invoke();
                }
                return default(T);
            }

            /// <summary>
            /// Este método realiza una petición POST
            /// </summary>
            /// <typeparam name="T">Generic, convierte la respuesta json, en un objeto de tipo T</typeparam>
            /// <param name="url">url del recurso</param>
            /// <param name="formdata">datos a enviar via post</param>
            /// <returns>T</returns>
            public async Task<T> Post<T>(string url, Dictionary<string, string> formdata, Action noconnected)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    try
                    {
                        var client = new HttpClient();
                        var form = new FormUrlEncodedContent(formdata);
                        var response = await client.PostAsync(url, form);
                        if (response != null && response.IsSuccessStatusCode)
                        {
                            var responsestring = await response.Content.ReadAsStringAsync();
                            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responsestring);
                        }
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex, App.TrackData());
                    }
                }
                else
                {
                    noconnected?.Invoke();
                }
                return default(T);
            }

            /// <summary>
            /// Este método realiza una petición POST
            /// </summary>
            /// <typeparam name="T">Generic, convierte la respuesta json, en un objeto de tipo T</typeparam>
            /// <param name="url">url del recurso</param>
            /// <param name="formdata">datos a enviar via post</param>
            /// <returns>T</returns>
            public async Task<T> Post<T>(string url, List<Param> formdata, Action noconnected)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    try
                    {
                        var client = new HttpClient();
                        MultipartFormDataContent content = new MultipartFormDataContent();
                        if (formdata != null)
                        {
                            foreach (var item in formdata)
                            {
                                switch (item.Type)
                                {
                                    case ParamType.File:
                                        var stream = new MemoryStream(item.Bytes);
                                        content.Add(new StreamContent(stream), item.Name, item.FileName);
                                        break;
                                    case ParamType.String:
                                        content.Add(new StringContent(item.Element.ToString()), item.Name);
                                        break;
                                    case ParamType.Obj:
                                        var cliente = new HttpClient();
                                        var json = Newtonsoft.Json.JsonConvert.SerializeObject(item.Element);
                                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url)
                                        {
                                            Content = new StringContent(json, Encoding.UTF8, "application/json")
                                        };
                                        content.Add(request.Content, item.Name);
                                        break;
                                }
                            }
                        }
                        var response = await client.PostAsync(url, content);
                        var responsestring = await response.Content.ReadAsStringAsync();
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responsestring);
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex, App.TrackData());
                    }
                }
                else
                {
                    noconnected?.Invoke();
                }
                return default(T);
            }


            public async Task<Stream> DownloadFile(string url, Action noconnected)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    try
                    {
                        var client = new HttpClient();
                        var stream = await client.GetStreamAsync(url);
                        return stream;
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex, App.TrackData());
                    }
                }
                else
                {
                    noconnected?.Invoke();
                }
                return null;
            }

            /// <summary>
            /// Este método realiza una petición GET
            /// </summary>
            /// <typeparam name="T">Generic, convierte la respuesta json, en un objeto de tipo T</typeparam>
            /// <param name="url">url del recurso</param>
            /// <param name="formdata">datos a enviar via GET</param>
            /// <returns>T</returns>
            public async Task<T> Get<T>(string url, Dictionary<string, string> formdata, Action noconnected)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    try
                    {
                        var client = new HttpClient();
                        if (!url.EndsWith("?"))
                        {
                            url += "?";
                        }

                        foreach (var item in formdata)
                        {
                            url += item.Key + "=" + item.Value + "&";
                        }

                        if (url.EndsWith("&"))
                        {
                            url = url.Remove(url.Length - 1, 1);
                        }

                        var response = await client.GetAsync(url);
                        if (response != null && response.IsSuccessStatusCode)
                        {
                            var responsestring = await response.Content.ReadAsStringAsync();
                            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responsestring);
                        }
                    }
                    catch
                    {
                    }
                }
                else
                {
                    noconnected?.Invoke();
                }
                return default(T);
            }

            public async Task<T> Post<T, K>(string url, K auditoria, Action noconnected)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    try
                    {
                        var client = new HttpClient();
                        var json = Newtonsoft.Json.JsonConvert.SerializeObject(auditoria);

                        //var json2 = DependencyService.Get<IFileProcess>().GetTextWriter(auditoria);

                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url)
                        {
                            Content = new StringContent(json, Encoding.UTF8, "application/json")
                        };
                        var response = await client.SendAsync(request);
                        if (response != null && response.IsSuccessStatusCode)
                        {
                            var responsestring = await response.Content.ReadAsStringAsync();
                            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responsestring);
                        }
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex, App.TrackData());
                    }
                }
                else
                {
                    noconnected?.Invoke();
                }
                return default(T);
            }


        }

        #region userrequest
        /// <summary>
        /// Peticion para consumir los metodos del usuario
        /// </summary>
        public class UserRequest
        {
            Client _client;

            public UserRequest(Client client)
            {
                _client = client;
            }

            /// <summary>
            /// Realiza el login del usuario
            /// </summary>
            /// <param name="alias">Alias del usuario</param>
            /// <param name="password">Password del usuario</param>
            /// <returns>UserResponse</returns>
            public async Task<UserResponse> Oauth(string alias, string password, string imei, Action noconnected)
            {
                // usamos el cliente http, realizamos la petición post, y enviamos los parametros correspondientes
                var response = await _client.Post<UserResponse>($"{PSMClient.BaseUrl}/oauth/login", new Dictionary<string, string>
                {
                    { "alias", alias },
                    { "password", password },
                    { "imei", imei },
                    { "plataforma", "1" }
                }, noconnected);
                return response;
            }

            public async Task<User> AddToken(int idusuario, string appcentertoken, Action noconnected)
            {
                var usuario = await _client.Post<User>($"{BaseUrl}/user/addtoken", new Dictionary<string, string>
                {
                    { "appcentertoken", appcentertoken }
                }, noconnected);
                return usuario;
            }

            public async Task<List<usuarioModel>> GetSubordinateUsers(Action noconnected)
            {
                var fecha = DateTime.Now.ToString("yyyy-MM-dd");
                var idUsuario = App.CurrentUser.IdUsuario.ToString();
#if DEBUG
                fecha = "2018-04-10";
                idUsuario = "1545";
#endif
                var response = await _client.Post<List<usuarioModel>>($"{PSMClient.BaseUrl}/User/GetSubordinateUsers", new Dictionary<string, string>
                {
                    { "IdUsuario", idUsuario },
                    { "fecha", fecha },
                }, noconnected);
                return response;
            }
        }

        /// <summary>
        /// Respuesta de la petición Oauth
        /// </summary>
        public class UserResponse
        {
            public bool IsAuthenticated { get; set; }
            public bool IsAnotherIMEI { get; set; }
            public int CifraControl { get; set; }
            public User User { get; set; }
            public List<MenuUser> MenuUser { get; set; }
            public List<SectionModule> SectionModule { get; set; }
            public List<Avisos> Avisos { get; set; }
            public string Message { get; set; }
        }

        /// <summary>
        /// Objeto usuario de la respuesta a la petición Oauth
        /// </summary>
        /// 

        public class SectionModule
        {
            public int IdSeccion { get; set; }
            public int IdProyecto { get; set; }
            public int IdCatalogoSeccion { get; set; }
            public string Descripcion { get; set; }
            public double Porcentaje { get; set; }
            public string CatDescripcion { get; set; }
            public bool EsSeccion { get; set; }
            public bool SeValida { get; set; }
            public bool EsActiva { get; set; }
        }

        public class User
        {
            public int IdUsuario { get; set; }
            public string Alias { get; set; }
            public byte[] Password { get; set; }
            public string Correo { get; set; }
            public string Nombre { get; set; }
            public int IdProyecto { get; set; }
            public int IdRegion { get; set; }
            public string AppCenterToken { get; set; }
            public int BorraBase { get; set; }
            public bool Bloqueado { get; set; }
        }

        public class MenuUser
        {
            public int IdMenuMovil { get; set; }
            public string Descripcion { get; set; }
            public string TargetType { get; set; }
            public string Icon { get; set; }
            public Nullable<int> IdParent { get; set; }
            public int Orden { get; set; }
            public bool Permiso { get; set; }
        }
        #endregion


        public class UploadRequest
        {

            Client _client;

            public UploadRequest(Client client)
            {
                _client = client;
            }


            //Este metodo sube todo de un jalon version 3.3 (metodo viejo).
            public async Task<UploadResponse> UploadAudits(Auditoria auditoria, Action noconnected)
            {
                return await _client.Post<UploadResponse, Auditoria>($"{PSMClient.BaseUrl}/Save/SaveAnswers", auditoria, noconnected);
            }

            //Estos metodos suben por partes la informacion
            //Sube Asistencia
            public async Task<UploadResponse> UploadAuditsAsistencia(Auditoria auditoria, Action noconnected)
            {
                return await _client.Post<UploadResponse, Auditoria>($"{PSMClient.BaseUrl}/Save/SaveAnswersAsistencia", auditoria, noconnected);
            }

            //Sube Respuestas sin fotos
            public async Task<UploadResponse> UploadAuditsRespuesta(Auditoria respuesta, Action noconnected)
            {
                return await _client.Post<UploadResponse, Auditoria>($"{PSMClient.BaseUrl}/Save/SaveAnswersRespuesta", respuesta, noconnected);
            }

            //Sube Abordajes
            public async Task<UploadResponse> UploadAuditsAbordaje(Auditoria respuesta, Action noconnected)
            {
                return await _client.Post<UploadResponse, Auditoria>($"{PSMClient.BaseUrl}/Save/SaveAnswersAbordaje", respuesta, noconnected);
            }

            //Sube Abordajes Multimedia
            public async Task<UploadResponse> UploadAuditsAbordajeMultimedia(Auditoria abordajemultimedia, Action noconnected)
            {
                return await _client.Post<UploadResponse, Auditoria>($"{PSMClient.BaseUrl}/Save/SaveAnswersAbordajeMultimedia", abordajemultimedia, noconnected);
            }


            //Obtener IdRuta Dinamica
            public async Task<UploadResponse> UploadRouteDinamica(Ruta ruta, Action noconnected)
            {
                return await _client.Post<UploadResponse, Ruta>($"{PSMClient.BaseUrl}/Save/ObtenerRutaDinamica", ruta, noconnected);
            }

            //public async Task<UploadResponse> UploadAuditsMult(AuditoriaMult auditoria, Action noconnected)
            //{
            //    return await _client.Post<UploadResponse, AuditoriaMult>($"{PSMClient.BaseUrl}/Save/SaveAnswersMult", auditoria, noconnected);
            //}

            public async Task<UploadResponse> UploadBitacora(List<BitacoraModel> bitacora, Action noconnected)
            {
                return await _client.Post<UploadResponse, List<BitacoraModel>>($"{PSMClient.BaseUrl}/Bitacora/SaveBitacora", bitacora, noconnected);
            }

            public async Task<UploadResponse> UploadEntry(Asistencia asistencia, Action noconnected)
            {
                return await _client.Post<UploadResponse, Asistencia>($"{PSMClient.BaseUrl}/Save/SaveAsistencia", asistencia, noconnected);
            }


            public async Task<UploadResponse> UploadCheckSinc(bool status, Action noconnected)
            {
                return await _client.Post<UploadResponse, bool>($"{PSMClient.BaseUrl}/Save/CheckSincronizacion", status, noconnected);
            }

            //Metodo viejo 3.3 que sube las fotos una por una pero de un jalon
            #region subefoto viejo
            //public async Task<UploadResponse> UploadFoto(RespuestaAuditor item, Action noconnected, int idRutaN = 0)
            //{
            //    List<Param> parameters = new List<Param>
            //    {
            //        new Param("IdRespuesta", item.IdRespuesta, ParamType.String),
            //        new Param("IdProyectoTienda", item.IdProyectoTienda, ParamType.String),
            //        new Param("IdRuta", item.IdRuta, ParamType.String),
            //        new Param("NumerodeCaja", item.NumerodeCaja, ParamType.String),
            //        new Param("FotoNombre", item.FotoNombre, ParamType.String),
            //        new Param("IdSeccion", item.IdSeccion, ParamType.String),
            //        new Param("Fecha", item.Fecha, ParamType.String),
            //        new Param("file", item.Foto, ParamType.Obj),
            //    };

            //    if (idRutaN != 0)
            //    {
            //        parameters.Add(new Param("IdRutaSave", idRutaN, ParamType.String));
            //    }

            //    if (item.IdUbicacion.HasValue)
            //    {
            //        parameters.Add(new Param("IdUbicacion", item.IdUbicacion, ParamType.String));
            //    }

            //    if (!string.IsNullOrEmpty(item.Respuesta))
            //    {
            //        parameters.Add(new Param("Respuesta", item.Respuesta, ParamType.String));
            //    }

            //    if (item.IdProducto.HasValue)
            //    {
            //        parameters.Add(new Param("IdProducto", item.IdProducto, ParamType.String));
            //    }

            //    return await _client.Post<UploadResponse>($"{PSMClient.BaseUrl}/Save/SavePhotoPart", parameters, noconnected);
            //}
            #endregion

            //Metodo que sube las fotos pero en forma de paquetes.
            public async Task<UploadResponse> UploadFoto(RespuestaAuditor item, string fecha, byte[] photosegment, bool ultimafoto, Action noconnected, int idRutaN = 0)
            {
                List<Param> parameters = new List<Param>
                {
                    new Param("IdRespuesta", item.IdRespuesta, ParamType.String),
                    new Param("idAnswerAudit", item.IdAnswerAudit, ParamType.String),
                    new Param("IdProyectoTienda", item.IdProyectoTienda, ParamType.String),
                    new Param("IdRuta", item.IdRuta, ParamType.String),
                    new Param("NumerodeCaja", item.NumerodeCaja, ParamType.String),
                    new Param("FotoNombre", item.FotoNombre, ParamType.String),
                    new Param("IdSeccion", item.IdSeccion, ParamType.String),
                    new Param("Fecha", fecha, ParamType.String),
                    new Param("fileArray", photosegment, ParamType.Obj),
                };

                if (idRutaN != 0)
                {
                    parameters.Add(new Param("IdRutaSave", idRutaN, ParamType.String));
                }

                if (item.IdUbicacion.HasValue)
                {
                    parameters.Add(new Param("IdUbicacion", item.IdUbicacion, ParamType.String));
                }

                if (!string.IsNullOrEmpty(item.Respuesta))
                {
                    parameters.Add(new Param("Respuesta", item.Respuesta, ParamType.String));
                }

                if (item.IdProducto.HasValue)
                {
                    parameters.Add(new Param("IdProducto", item.IdProducto, ParamType.String));
                }

                parameters.Add(new Param("ultimafoto", ultimafoto, ParamType.String));


                return await _client.Post<UploadResponse>($"{PSMClient.BaseUrl}/Save/SavePhotoPart", parameters, noconnected);
            }

            public async Task<UploadResponse> UploadFotoHeader(Ruta ruta, int length, int packages, int idAnswerAudit, string fecha, string fotoNombre, Action noconnected)
            {
                List<Param> parameters = new List<Param>
                {
                    new Param("Ruta", ruta, ParamType.Obj),
                    new Param("length", length, ParamType.String),
                    new Param("packages", packages, ParamType.String),
                    new Param("idAnswerAudit", idAnswerAudit, ParamType.String),
                    new Param("fecha", fecha, ParamType.String),
                    new Param("fotoNombre", fotoNombre, ParamType.String)

                };
                return await _client.Post<UploadResponse>($"{PSMClient.BaseUrl}/Save/SavePhotoHeader", parameters, noconnected);
            }

            public async Task<UploadResponse> UploadRoute(Ruta ruta, Action noconnected)
            {
                return await _client.Post<UploadResponse, Ruta>($"{PSMClient.BaseUrl}/Save/SaveRoute", ruta, noconnected);
            }

            public async Task<UploadResponse> UploadDataBase(RespaldoBDUsuarioDto baseDatosDto, Action noconnected)
            {
                byte[] file = baseDatosDto.BasedeDatos;
                baseDatosDto.BasedeDatos = null;

                List<Param> parameters = new List<Param>
                {
                    new Param("baseDatosStr", baseDatosDto, ParamType.Obj),
                    new Param("file", file, ParamType.File)
                };

                return await _client.Post<UploadResponse>($"{PSMClient.BaseUrl}/Save/SaveDatabase", parameters, noconnected);
            }
        }

        public class Auditoria
        {
            public List<RespuestaAuditor> RespuestaAuditor { get; set; }
            public Ruta Ruta { get; set; }
            public List<Abordaje> Abordaje { get; set; }
            public List<AbordajeMultimedia> AbordajeMultimedia { get; set; }
            public List<Asistencia> Asistencia { get; set; }
        }

        //public class AuditoriaMult
        //{
        //    public List<RespuestaAuditor> RespuestaAuditor { get; set; }
        //    public List<Ruta> Ruta { get; set; }
        //    public List<Abordaje> Abordaje { get; set; }
        //    public List<AbordajeMultimedia> AbordajeMultimedia { get; set; }
        //    public List<Asistencia> Asistencia { get; set; }
        //}

        public class UploadResponse
        {
            public string Message { get; set; }
            public bool Status { get; set; }
            public string Data { get; set; }
            public int Package { get; set; }
            public string IdRuta { get; set; }
            public string IdSeccion { get; set; }
            public bool SendRate { get; set; }
            public Nullable<double> Rate { get; set; }
        }

        #region RutaRequest

        public class RutaRequest
        {
            Client _client;

            public RutaRequest(Client client)
            {
                _client = client;
            }

            /// <summary>
            /// Obtiene todas las rutas de un usuario
            /// </summary>
            /// <param name="idusuario"></param>
            /// <returns></returns>
            public async Task<RutaResponse> UserRoutes(int idusuario, Action noconnected)
            {
                var tiempo = DateTime.Now.ToString("yyyy-MM-dd");
                return await _client.Post<RutaResponse>($"{PSMClient.BaseUrl}/Route/UserRoutes", new Dictionary<string, string>
                {
                    { "idusuario", idusuario.ToString() },
                    { "fecha", tiempo }
                }, noconnected);
            }
        }

        public class RutaResponse
        {
            public List<Ruta> Rutas { get; set; }
            public List<InfoTienda> Informacion { get; set; }
            public List<CatalogoIndicador> CatalogoIndicador { get; set; }
            public List<Indicador> Indicadores { get; set; }
            public List<ReporteIndicador> ReporteIndicador { get; set; }
            public List<Tiempo> Tiempos { get; set; }
        }

        #endregion

        #region SeccionRequest

        public class SeccionRequest
        {
            Client _client;

            public SeccionRequest(Client client)
            {
                _client = client;
            }

            /// <summary>
            /// Obtiene las secciones, preguntas y respuestas de 
            /// </summary>
            /// <param name="idproyecto"></param>
            /// <returns></returns>
            public async Task<SectionResponse> UserSections(int idproyecto, Action noconnected)
            {
                return await _client.Post<SectionResponse>($"{PSMClient.BaseUrl}/Section/UserSections", new Dictionary<string, string>
                {
                    { "idproyecto", idproyecto.ToString() }
                }, noconnected);
            }
        }

        public class SectionResponse
        {
            public List<CatalogoSeccion> ClasificacionSeccion { get; set; }
            public List<Seccion> Secciones { get; set; }
            public List<Pregunta> Preguntas { get; set; }
            public List<Respuesta> Respuestas { get; set; }
            public List<SeccionCadena> SeccionCadena { get; set; }
            public List<SeccionRegion> SeccionRegion { get; set; }
            public List<PreguntaCadena> PreguntaCadena { get; set; }

        }

        #endregion

        #region CatalogoRequest

        public class CatalogoRequest
        {
            Client _client;

            public CatalogoRequest(Client client)
            {
                _client = client;
            }

            public async Task<CatalogoResponse> UserCatalogs(int idusuario, Action noconnected)
            {
                var tiempo = DateTime.Now.ToString("yyyy-MM-dd");
                return await _client.Post<CatalogoResponse>($"{PSMClient.BaseUrl}/Catalogo/UserCatalogs", new Dictionary<string, string>
                {
                    { "idusuario", idusuario.ToString() },
                    { "fecha", tiempo }
                }, noconnected);
            }
        }

        public class CatalogoResponse
        {
            public bool Success { get; set; }
            public string Message { get; set; }
            public List<Compania> Companias { get; set; }
            public List<Ubicacion> Ubicaciones { get; set; }
            public List<Producto> Productos { get; set; }
            public List<CatalogoSeccionMenu> Abordajes { get; set; }
            public List<Multimedia> Archivos { get; set; }
            public List<ProductoCadenaDto> ProductoCadena { get; set; }
            public List<Tiempo> Tiempos { get; set; }
            public List<ProductoClasificacion> ProductoClasificacion { get; set; }
        }

        public class ChatRequest
        {
            Client _client;

            public ChatRequest(Client client)
            {
                _client = client;
            }

            public async Task<List<ORM.ChatGrupo>> Grupos(int idproyecto, int idusuario)
            {
                var chatgrupo = await _client.Get<List<ORM.ChatGrupo>>($"{BaseUrl}/Chat/Grupos", new Dictionary<string, string>
                {
                    { "idproyecto", idproyecto.ToString() },
                    { "idusuario", idusuario.ToString() }
                }, () => { });
                return chatgrupo;
            }

            public async Task<List<ChatMensaje>> Chats(int idchatgrupo)
            {
                return await _client.Get<List<ChatMensaje>>($"{BaseUrl}/Chat/Chats", new Dictionary<string, string>
                {
                    { "idchatgrupo", idchatgrupo.ToString() }
                }, () => { });
            }

            public async Task<ChatMensaje> ReceiveMessage(int idchatgrupo, int idproyecto, int idusuario, string message)
            {
                return await _client.Post<ORM.ChatMensaje>($"{BaseUrl}/Chat/ReceiveMessage", new Dictionary<string, string>
                {
                    { "idchatgrupo", idchatgrupo.ToString() },
                    { "idproyecto", idproyecto.ToString() },
                    { "idusuario", idusuario.ToString() },
                    { "message", message }
                }, () => { });
            }
        }

        #endregion

        #region TiendaCercanaRutaRequest
        public class TiendasCercanasRutaRequest
        {
            Client _client;

            public TiendasCercanasRutaRequest(Client client)
            {
                _client = client;
            }

            /// <summary>
            /// Obtiene todas las rutas de un usuario
            /// </summary>
            /// <param name="idusuario"></param>
            /// <returns></returns>
            public async Task<TiendasCercanasRutaResponse> NearbyRouteStores(int idusuario, Action noconnected)
            {
                var tiempo = DateTime.Now.ToString("yyyy-MM-dd");
                return await _client.Post<TiendasCercanasRutaResponse>($"{PSMClient.BaseUrl}/TiendasCercanasRuta/GetTiendasCercanas", new Dictionary<string, string>
                {
                    { "idusuario", idusuario.ToString() }
                }, noconnected);
            }
        }

        public class TiendasCercanasRutaResponse
        {
            public List<TiendasCercanasRuta> Tiendas { get; set; }
        }
        #endregion

        #region TiendasCercanasUbicacionRequest
        public class TiendasCercanasUbicacionRequest
        {
            Client _client;

            public TiendasCercanasUbicacionRequest(Client client)
            {
                _client = client;
            }

            /// <summary>
            /// Obtiene todas las rutas de un usuario
            /// </summary>
            /// <param name="idusuario"></param>
            /// <returns></returns>
            public async Task<List<TiendasCercanasUbicacion>> NearbyUbicationStores(int idusuario, double latitud, double longitud, Action noconnected)
            {
                var response = await _client.Post<TiendasCercanasUbicacionResponse>($"{PSMClient.BaseUrl}/TiendasCercanasGPS/GetTiendasCercanas", new Dictionary<string, string>
                {
                    { "idusuario", idusuario.ToString() },
                    { "coordenaday", latitud.ToString() },
                    { "coordenadax", longitud.ToString() }
                }, noconnected);

                return response.Tiendas;
            }
        }

        public class TiendasCercanasUbicacionResponse
        {
            public List<TiendasCercanasUbicacion> Tiendas { get; set; }
        }

        #endregion

        #region RutaProspectoRequest
        public class RutaProspectoRequest
        {
            Client _client;

            public RutaProspectoRequest(Client client)
            {
                _client = client;
            }

            /// <summary>
            /// Obtiene todas las rutas de un usuario
            /// </summary>
            /// <param name="idusuario"></param>
            /// <returns></returns>
            public async Task<List<TiendasCercanasUbicacion>> ProspectStores(int idusuario, Action noconnected)
            {
                var response = await _client.Post<TiendasCercanasUbicacionResponse>($"{PSMClient.BaseUrl}/TiendasCercanasGPS/GetTiendaProspecto", new Dictionary<string, string>
                {
                    { "idusuario", idusuario.ToString() }
                }, noconnected);

                return response.Tiendas;
            }
        }

        public class RutaProspectoResponse
        {
            public List<TiendasCercanasUbicacion> Tiendas { get; set; }
        }
        #endregion

        #region CalifiacionRequest

        public class QualiRequest
        {
            Client _client;

            public QualiRequest(Client client)
            {
                _client = client;
            }

            /// <summary>
            /// Obtiene las ultimas calificacione de las rutas de un usuario
            /// </summary>
            /// <param name="idusuario"></param>
            /// <returns></returns>
            public async Task<QualiResponse> QualiRoute(int idusuario, Action noconnected)
            {
                return await _client.Post<QualiResponse>($"{PSMClient.BaseUrl}/Qualification/QualificationRoutes", new Dictionary<string, string>
                {
                    { "idusuario", idusuario.ToString() }

                }, noconnected);
            }
        }


        public class QualiResponse
        {
            public List<Qualification> Qualification { get; set; }
            public List<QualiSection> QualiSections { get; set; }
            public List<QualiPregunta> QualiCuestion { get; set; }
        }
        #endregion

    }
}
