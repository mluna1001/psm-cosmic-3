﻿using PSM.Function;
using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PSM
{
    public partial class PackagePage : ContentPage
    {
        List<Ubicacion> _ubicaciones;
        //List<Compania> _companias;
        //List<Producto> _productos;
        //List<PresentacionEmpaque> _presentaciones;

        public List<ProductoModel> ProductosModel { get; private set; }
        private TipoSeccion _tipo { get; set; }
        Picker UbicacionPicker = new Picker();

        public PackagePage(TipoSeccion tipo)
        {
            InitializeComponent();
            App.CurrentPage = this;
            Title = App.CurrentSection != null ? App.CurrentSection.Descripcion : "";
            _tipo = tipo;
            Init();
        }

        protected override void OnAppearing()
        {
            btnContinuar.IsEnabled = true;
            stackloader.IsVisible = false;
        }

        private async void Init()
        {
            ProductosModel = ProductoHelper.GetProductos();
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoASeccion, "Init", "PackagePage", $"Ingresó a la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} ");
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoASeccion, "Init", "PackagePage", ProductosModel);

            if (ProductosModel.Count == 0 || ProductosModel is null)
            {
                await App.Current.MainPage.DisplayAlert("PSM", "No hay productos disponibles para esta sección. Contacta a tu supervisor.", "Aceptar");
                await Navigation.PopAsync();
            }
            FindLevel(ProductosModel, 1);
            var pickerview = SetProductList(ProductosModel);
        }

        private bool ViewIsSet = false;
        private void SetView()
        {
            if (!ViewIsSet)
            {
                ViewIsSet = true;
                var pickersorder = Pickers.OrderBy(e => e.Key);
                foreach (var picker in pickersorder)
                {
                    var pickerelement = picker.Value;
                    if (pickerelement.LabelProduct != null && pickerelement.PickerProduct != null)
                    {
                        RootLayout.Children.Add(pickerelement.LabelProduct);
                        RootLayout.Children.Add(pickerelement.PickerProduct);
                    }
                }

                Label UbicacionLabel = new Label { Text = "Ubicación" };
                //Picker UbicacionPicker = new Picker();

                _ubicaciones = App.DB.Ubicacion.Where(e => e.IdSeccion == App.CurrentSection.IdSeccion).ToList();
                if (_ubicaciones.Count > 0)
                {
                    foreach (var ubicacion in _ubicaciones)
                    {
                        UbicacionPicker.Items.Add(ubicacion.Nombre);
                    }
                    UbicacionPicker.SelectedIndexChanged += UbicacionPicker_SelectedIndexChanged;
                    UbicacionPicker.SelectedIndex = 0;
                }

                RootLayout.Children.Add(UbicacionLabel);
                RootLayout.Children.Add(UbicacionPicker);
                //Button btncontinuar = new Button { Text = "Continuar" };
                //btncontinuar.Clicked += Btncontinuar_Clicked;
                //RootLayout.Children.Add(btncontinuar);
            }
        }

        private async void Btncontinuar_Clicked(object sender, EventArgs e)
        {
            btnContinuar.IsEnabled = false;
            StartDownload("Cargando Preguntas...");
            await Task.Delay(500);



            if (Pickers.ContainsKey(LevelMax - 1))
            {
                var finalpicker = Pickers[LevelMax - 1];
                var itemselected = finalpicker.Items[finalpicker.PickerProduct.SelectedIndex];
                var ubicacionSelected = UbicacionPicker.Items[UbicacionPicker.SelectedIndex];
                var productosseleccionados = itemselected.ProductosHijos;

                string title = "";
                foreach (var pickeritem in Pickers)
                {
                    if (pickeritem.Value.PickerProduct != null && pickeritem.Value.PickerProduct.Items.Count > 0)
                    {
                        var item = pickeritem.Value.PickerProduct.Items[pickeritem.Value.PickerProduct.SelectedIndex];
                        title += item + " - ";
                    }
                }

                List<Producto> productossection = new List<Producto>();
                foreach (var productoseleccionado in productosseleccionados)
                {
                    var producto = productoseleccionado.ProductoPadre;
                    productossection.Add(new Producto
                    {
                        Categoria = producto.Categoria,
                        CodigoBarras = producto.CodigoBarras,
                        IdCatalogoProducto = producto.IdCatalogoProducto,
                        IdParent = producto.IdParent,
                        IdProducto = producto.IdProducto,
                        IdSeccion = producto.IdSeccion,
                        Nombre = title + producto.Nombre,
                        Precio = producto.Precio,
                        PrecioPromo = producto.PrecioPromo,
                        SKU = producto.SKU,
                        Titulo = producto.Titulo,
                        Vigencia = producto.Vigencia
                    });
                }


                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SeCargaronLosProductos, "BtnContinuar_Clicked", "PackagePage", productosseleccionados);
                switch (_tipo)
                {
                    case TipoSeccion.HallazgoProducto:
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.Navegando, "BtnContinuar_Clicked", "PackagePage", "Navegando a HallazgoProducto");
                        await Navigation.PushAsync(new HallazgoPackagePage(productossection));
                        break;
                    case TipoSeccion.PrecioPromo:
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.Navegando, "BtnContinuar_Clicked", "PackagePage", "Navegando a precio promo");
                        await Navigation.PushAsync(new PreciosPage(productossection, promo: true));
                        break;
                    case TipoSeccion.Precio:
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.Navegando, "BtnContinuar_Clicked", "PackagePage", "Navegando a precio");
                        await Navigation.PushAsync(new PreciosPage(productossection));
                        break;
                    case TipoSeccion.CalificacionProducto:
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.Navegando, "BtnContinuar_Clicked", "PackagePage", "Navegando a CalificacionProducto");
                        await Navigation.PushAsync(new CalificacionProductoPage(productossection));
                        break;
                    case TipoSeccion.CodigoBarra:
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.Navegando, "BtnContinuar_Clicked", "PackagePage", "Navegando a precio");
                        await Navigation.PushAsync(new PreciosPage(productossection));
                        break;
                }
            }
            EndDownload();
        }

        private void StartDownload(string message)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                stackrender.IsVisible = false;
                stackloader.IsVisible = true;
                ProgressDownloadIndicator.IsVisible = true;
                ProgressDownloadIndicator.IsRunning = true;
                ProgressDownloadText.Text = message;
            });


        }

        private void EndDownload()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                stackrender.IsVisible = true;
                ProgressDownloadIndicator.IsVisible = false;
                ProgressDownloadIndicator.IsRunning = false;
                ProgressDownloadText.Text = "";
            });

        }

        private void FindLevel(List<ProductoModel> levellist, int nivel)
        {
            if (levellist.Count > 0)
            {
                LevelMax++;
                FindLevel(levellist[0].ProductosHijos, nivel + 1);
            }
        }

        private int LevelPicker = 0;
        private int LevelMax = 0;
        private Dictionary<int, PickerView> Pickers = new Dictionary<int, PickerView>();

        private PickerView SetProductList(List<ProductoModel> levellist)
        {
            LevelPicker++;
            if (levellist.Count > 0 && LevelPicker < LevelMax)
            {
                string texttolabel = "Nivel " + LevelPicker;
                if (!string.IsNullOrEmpty(levellist[0].ProductoPadre.Titulo))
                {
                    texttolabel = levellist[0].ProductoPadre.Titulo;
                }
                Label level = new Label { Text = texttolabel };
                Picker PickerLevel = new Picker();
                foreach (var item in levellist)
                {
                    PickerLevel.Items.Add(item.ProductoPadre.Nombre);
                    item.ProductoPadre.Titulo = texttolabel;
                }

                var pickerlement = new PickerView { LabelProduct = level, PickerProduct = PickerLevel, Items = levellist };
                Pickers.Add(LevelPicker, pickerlement);

                PickerLevel.ClassId = LevelPicker.ToString();
                PickerLevel.SelectedIndexChanged += PickerLevel_SelectedIndexChanged;
                PickerLevel.SelectedIndex = 0;
                return pickerlement;
            }
            return null;
        }

        private void PickerLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var picker = sender as Picker;
                var pickerlevel = int.Parse(picker.ClassId);
                PickerView pickerview = null;
                if (Pickers.ContainsKey(pickerlevel))
                {
                    pickerview = Pickers[pickerlevel];
                    var selecteditem = pickerview.Items[picker.SelectedIndex];

                    var firstitemslevelid = pickerview.Items.Select(p => p.ProductoPadre.IdProducto);
                    var levellist = ProductosModel.Where(p => firstitemslevelid.Contains(p.ProductoPadre.IdProducto)).ToList();

                    if (levellist.Count > 0)
                    {
                        if (!selecteditem.Desencadenada)
                        {
                            foreach (var sublevelitem in levellist)
                            {
                                sublevelitem.Desencadenada = true;
                                ProductosModel.AddRange(sublevelitem.ProductosHijos);
                            }

                            var pickerchildrens = SetProductList(selecteditem.ProductosHijos);
                            if (Pickers.ContainsKey(LevelMax))
                            {
                                Pickers[LevelMax].Items = selecteditem.ProductosHijos;
                            }
                            else
                            {
                                Pickers.Add(LevelMax, new PickerView { Items = selecteditem.ProductosHijos });
                            }
                            if (pickerchildrens != null)
                            {
                                foreach (var item in levellist)
                                {
                                    item.View = pickerchildrens;
                                }
                            }
                        }
                        else
                        {
                            // buscamos en el siguiente nivel
                            if (Pickers.ContainsKey(pickerlevel + 1))
                            {
                                var nextpicker = Pickers[pickerlevel + 1];
                                nextpicker.Items = selecteditem.ProductosHijos;
                                if (nextpicker.PickerProduct != null && nextpicker.LabelProduct != null)
                                {
                                    nextpicker.PickerProduct.SelectedIndexChanged -= PickerLevel_SelectedIndexChanged;
                                    nextpicker.PickerProduct.Items.Clear();
                                    foreach (var hijo in selecteditem.ProductosHijos)
                                    {
                                        nextpicker.PickerProduct.Items.Add(hijo.ProductoPadre.Nombre);
                                    }
                                    nextpicker.PickerProduct.SelectedIndexChanged += PickerLevel_SelectedIndexChanged;
                                    nextpicker.PickerProduct.SelectedIndex = 0;
                                }
                                else
                                {
                                    // esta iteración es la ultima para ya navegar hacia los productos...
                                }
                            }
                            else
                            {
                                // no hay siguiente nivel, eso quiere decir que estamos en el ultimo picker o mejor dicho, el picker que no se ve
                            }
                        }
                    }
                    else
                    {
                        // la lista no devuelve nada de elementos
                    }
                    SetView();
                }
            });
        }

        Ubicacion _ubicacion;
        private void UbicacionPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var UbicacionPicker = sender as Picker;
            _ubicacion = _ubicaciones[UbicacionPicker.SelectedIndex];
            App.CurrentLocation = _ubicacion;
        }



    }
}
