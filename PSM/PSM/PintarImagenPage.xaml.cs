﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PintarImagenPage : ContentPage
    {
        public PintarImagenPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            VImagen.Source = App.ImagenVisualizar;
        }
    }
}
