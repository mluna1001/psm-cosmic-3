﻿using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Windows.Input;
using PSM.ORM;
using PSM.Pages;

namespace PSM
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            var avisos = ListaAvisos();
            ListaDeAvisos.ItemsSource = avisos;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "MainPage", avisos);
        }

        #region eventos de xaml
        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            List<Avisos> listaavisos = new List<Avisos>();
            listaavisos = ListaAvisos().ToList();
            ListaDeAvisos.ItemsSource = listaavisos;
        }

        void ListaDeAvisos_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            ListaDeAvisos.SelectedItem = null;
        }

        async void ListaDeAvisos_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listaav = e.Item as Avisos;
            switch (listaav.IdTipoAviso)
            {
                case (int)TypeEnum.TipoAviso.Informativo:
                    await DisplayAlert(listaav.Titulo, listaav.Mensaje, "Aceptar");
                    break;
                case (int)TypeEnum.TipoAviso.EnlaceExterno:
                    await Navigation.PushModalAsync(new NoticeModalPage(listaav));
                    break;
            }
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "ListaDeAvisos_ItemTapped", "MainPage", "Se ha seleccionado el aviso: " + listaav.Mensaje);
        }

        #endregion

        #region Lista Avisos
        private IList<Avisos> ListaAvisos()
        {
            List<Avisos> avisos = new List<Avisos>();
            var today = DateTime.Now.Date;
            avisos = App.DB.Avisos.Where(a => a.Vigente).Take(3).ToList();
            return avisos;
        }
        #endregion
    }
}