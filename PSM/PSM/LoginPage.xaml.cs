﻿using DevelopersAzteca.Storage.SQLite;
using PSM.API;
using PSM.Function;
using PSM.ORM;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using Xamarin.Forms;
using static PSM.API.PSMClient;
using ChatApp;
using Plugin.Connectivity;

namespace PSM
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            InitApp();
        }

        public void InitApp()
        {
            // ocultamos la barra de progreso
            Progress.IsVisible = false;
            // ejecutamos codigo para la plataforma de windows 
            Device.OnPlatform(WinPhone: () =>
            {
                BoxUser.TextColor = Color.Black;
                BoxPassword.TextColor = Color.Black;
            });
#if DEBUG
            // [daniel] estas lineas solo se ejecutaran cuando la aplicacion este en modo debug
            BoxUser.Text = "tfal01";
            BoxPassword.Text = "123456";
#endif
        }

        private async void BtnLogin_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Progress.IsRunning = true;
                BoxUser.IsEnabled = false;
                BoxPassword.IsEnabled = false;
            });
            string alias = BoxUser.Text;
            string password = BoxPassword.Text;

            try
            {
                var usercheck = new { alias, password };
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(usercheck);
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoUsuarioContraseña,"BtnLogin_Clicked", "LoginPage", json);
            }
            catch
            {

            }
            
            bool isconnected = true;
            var response = await App.API.UserEndPoint.Oauth(alias, password, App.DeviceIdentifier, () => isconnected = false);

            if (response != null)
            {
                if (response.IsAuthenticated)
                {
                    // el usuario se autentico correctamente por lo que devuelve los datos del usuario mas el menu asignado al usuario.
                    var usuario = new Usuario
                    {
                        Alias = response.User.Alias,
                        Password = response.User.Password,
                        IdUsuario = response.User.IdUsuario,
                        IdProyecto = response.User.IdProyecto
                    };

                    App.CurrentUser = usuario;
                    var userfound = App.DB.Usuario.FirstOrDefault(u => u.Alias.Equals(response.User.Alias));
                    if (userfound != null)
                    {
                        // el usuario ya existe
                        if (userfound.Password != response.User.Password || userfound.IdProyecto != response.User.IdProyecto || userfound.IdUsuario != response.User.IdUsuario) // el password es diferente
                        {
                            // actualizamos el password
                            userfound.Password = response.User.Password;
                            userfound.IdProyecto = response.User.IdProyecto;
                            userfound.IdUsuario = response.User.IdUsuario;
                            App.DB.Entry(userfound).State = DataBase.EntryState.Modify;
                        }

                        SaveAvisos(response.Avisos);
                        //Trae el menu actializado del usuario
                        DownloadSaveMenu(response.MenuUser, response.CifraControl);
                    }
                    else
                    {
                        // el usuario no existe
                        SaveAvisos(response.Avisos);
                        DownloadSaveMenu(response.MenuUser, response.CifraControl);
                        App.DB.Add(usuario);
                        //App.DB.SaveChanges();
                        //GoToMainPage();
                    }
                    App.DB.SaveChanges();
                    //navegamos hacia la pagina principal                    
                    GoToMainPage();
                }
                else
                {
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginIncorrecto,"BtnLogin_Clicked", "LoginPage", "Usuario o contraseña incorrectos, intenta nuevamente.");
                    // el usuario no se autentico correctamente
                    await DisplayAlert("PSM 360", "Usuario o contraseña incorrectos, intenta nuevamente.", "Aceptar");
                }
            }
            else
            {
                //Si no tiene internet realiza el login de manera local 
                Usuario user = GetUserLocal(alias);
                if (user != null)
                {
                    BouncyCastleHashing bhash = new BouncyCastleHashing();
                    //Realiza una validacion local con la libreria BouncyCastle debido a que PasswordHash no es compatible con 
                    //portatiles.
                    bool IsAuthenticated = bhash.ValidatePassword(password, new byte[16], 10000, 20, user.Password);
                    if (IsAuthenticated)
                    {
                        App.CurrentUser = user;
                        GoToMainPage();
                    }
                    else
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginIncorrecto,"BtnLogin_Clicked", "LoginPage", "Usuario o contraseña incorrectos, intenta nuevamente.");
                        await DisplayAlert("PSM 360", "Usuario o contraseña incorrectos, intenta nuevamente.", "Aceptar");
                    }
                }
                else
                {
                    if (!isconnected)
                    {
                        await DisplayAlert("PSM 360", "Revisa tu conexion a internet", "Aceptar");
                    }
                    else
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginSinInternetNoSeLogeo,"BtnLogin_Clicked", "LoginPage", "No tienes conexion a internet, intenta más tarde.");
                        await DisplayAlert("PSM 360", "No tienes conexiona internet, intenta más tarde.", "Aceptar");
                    }
                }
            }

            

            Device.BeginInvokeOnMainThread(() =>
            {
                Progress.IsRunning = false;
                BoxUser.IsEnabled = true;
                BoxPassword.IsEnabled = true;
            });
        }
        
        private void SaveAvisos(List<Avisos> avisos)
        {
            foreach (var aviso in avisos)
            {
                if (!App.DB.Avisos.Exists(e => e.IdAviso == aviso.IdAviso))
                {
                    App.DB.Avisos.Add(aviso);
                }
            }
            App.DB.SaveChanges();
        }

        /// <summary>
        /// Realiza una consulta de  los datos del usuario de maneera local.
        /// </summary>
        /// <param name="Alias"></param>
        /// <returns></returns>
        public Usuario GetUserLocal(string Alias)
        {
            return App.DB.Usuario.FirstOrDefault();
        }

        /// <summary>
        /// Metodo que verifica si han existido cambios en el menu del usuario a partir de una variable que se llama cifra control
        /// esta variable suma los ids del menu asignado al usuario y los compara con la suma de los ids del usuario de manera 
        /// local, si estos son diferentes quiere decir que hay cambios en el menu por lo tanto se eliminaran los registros de 
        /// la tabla MenuUser local y inserta los menus que se obtuvieron del servidor.
        /// </summary>
        /// <param name="menuList"></param>
        /// <param name="cifaControl"></param>
        private void DownloadSaveMenu(List<PSMClient.MenuUser> menuList, int cifaControl)
        {
            int CifraControlLocal = App.DB.MenuUser.Sum(s => s.IdMenuMovil);
            if (cifaControl != CifraControlLocal)
            {
                App.DB.Execute("Delete from MenuUser");
                if (menuList != null && menuList.Count > 0)
                {
                    foreach (var item in menuList)
                    {
                        App.DB.Add(item);
                    }
                }
            }
        }

        public void GoToMainPage()
        {
            if (App.CurrentUser != null)
            {
                try
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(App.CurrentUser);
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginCorrecto,"GoToMainPage", "LoginPage", json);
                }
                catch
                {

                }
            }

            ChatConfig.Init?.Invoke(new ChatClient.Userprofile
            {
                Alias = App.CurrentUser.Alias,
                IdUsuario = App.CurrentUser.IdUsuario,
                Descripcion = "Usuario",
                Nombre = App.CurrentUser.Alias
            }, App.ChatGrupos());
            Application.Current.MainPage = new HomePage();
        }
    }
}
