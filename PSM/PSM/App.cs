﻿using ChatApp;
using DevelopersAzteca.Storage.SQLite;
using Newtonsoft.Json;
using Plugin.Connectivity;
using PSM.API;
using PSM.DeviceSensors;
using PSM.Function;
using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

using Xamarin.Forms;
using Microsoft.AppCenter.Push;
using static PSM.API.PSMClient;
using ChatApp.ORM;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using PSM.Helpers;
using PSM.Helpers.ReadFile;
using System.Threading.Tasks;
using PSM.Helpers.DeviceIdentifier;
using PSM.Helpers.Services;

namespace PSM
{
    public class App : Application
    {
        public static PSMDataBase DB { get; set; }
        public static PSMClient API { get; set; }
        public static Usuario CurrentUser { get; set; }
        public static Page CurrentPage { get; set; }
        public static int CycleNumber { get; set; }
        public static Ruta CurrentRoute { get; set; }
        public static PresentacionEmpaque CurrentPackage { get; set; }
        public static Ubicacion CurrentLocation { get; set; }
        public static Seccion CurrentSection { get; set; }
        public static string ImagenVisualizar { get; set; }
        public static int IdTienda { get; set; }
        public static float ScreenWidth { get; set; }
        public static float ScreenHeight { get; set; }
        public const string MODULO_ENTRADA = "Entrada";
        public const string MODULO_ENTRADAGLOB = "EntradaGlobal";
        public const string MODULO_SALIDA = "Salida";
        public const string MODULO_SALIDAGLOB = "SalidaGlobal";
        private LoginModel model { get; set; }
        private static Guid? AppCenterToken { get; set; }
        public static List<Seccion> AllSections { get; set; }
        public static List<UploadPhoto> CurrentPhoto { get; set; }
        public static List<UploadRespAudit> CurrentRespAudit { get; set; }
        public static List<UploadRespAuditMult> CurrentRespAuditMult { get; set; }
        public static DateTime FechaLogin { get; set; }
        public static bool FechaAutomatica { get; set; }
        public static string DeviceIdentifier { get; set; }
        public static string ListInstallApps { get; set; }
        public static string InfoStorageDevice { get; set; }
        public static string InfoBatteryDevice { get; set; }

        public App()
        {
            App.CurrentRoute = new Ruta();
            App.CycleNumber = 0;
            App.CurrentLocation = new Ubicacion();
            App.CurrentPackage = new PresentacionEmpaque();
            App.AllSections = new List<Seccion>();
            App.CurrentPhoto = new List<UploadPhoto>();
            App.CurrentRespAudit = new List<UploadRespAudit>();

            Keys.DataBaseName = "psmultimate.db3";
            DB = PSMDataBase.Instance;

            API = new PSMClient();

            ChatConfig.BaseUrl = "http://ultimatechat.cosmic.mx";
            ChatConfig.Init?.Invoke(null, ChatGrupos());

            Bitacora.CanInsert = true;

            DeviceIdentifier = DependencyService.Get<IDevice>().GetIdentifier();
            ListInstallApps = DependencyService.Get<IDevice>().GetInstalledApps().ToJson();
            InfoStorageDevice = StorageDevice.InfoStorageDevice();
            InfoBatteryDevice = StorageDevice.InfoBatteryLevel();

            model = new LoginModel
            {
                AppIcon = "icon.png",
                AppName = "PSM",
                BackgroundImage = "walldefault.jpg",
                ButtonOne = "Aceptar",
                EntryOne = "Alias",
                EntryTwo = "Contraseña",
                Message = "Ingresa tu alias y contraseña para empezar.",
                Opacity = "0.2",
                PrimaryColor = "#000000",
#if DEBUG
                EntryOneValue = "jorgenutri",
                EntryTwoValue = "123456",
#endif
                SecondaryColor = "#ffffff"
            };
            model.LoginClick += Model_LoginClick;
            StandarLogin login = new StandarLogin(model) { Title = "PSM" };
            var navigationpage = new NavigationPage(login);
            navigationpage.BarBackgroundColor = Color.Navy;
            navigationpage.BarTextColor = Color.White;
            MainPage = navigationpage;
        }

        public static List<ChatApp.ORM.ChatGrupo> ChatGrupos()
        {
            var grupos = DB.ChatGrupo.ToList();
            List<ChatApp.ORM.ChatGrupo> chatgrupos = new List<ChatApp.ORM.ChatGrupo>();
            foreach (var grupo in grupos)
            {
                chatgrupos.Add(new ChatApp.ORM.ChatGrupo
                {
                    ActualizadoEl = grupo.ActualizadoEl,
                    Baneado = grupo.Baneado,
                    CreadoEl = grupo.CreadoEl,
                    Descripcion = grupo.Descripcion,
                    IdChatGrupo = grupo.IdChatGrupo,
                    IdProyecto = grupo.IdProyecto
                });
            }
            return chatgrupos;
        }

        private void App_LoginComplete(object sender, LoginComplete e)
        {

        }

        public async void UploadBitacora()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var respuesta = await Bitacora.Upload();
            }
            CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged;
        }
        public static async void UploadBitacoras()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var respuesta = await Bitacora.Upload();
            }
        }

        private async void Current_ConnectivityChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            if (e.IsConnected)
            {
                await Bitacora.Upload();
            }
        }

        private void Model_LoginClick(object sender, Dictionary<string, string> e)
        {
            model.LoginClick -= Model_LoginClick;
            var loginmodel = sender as LoginModel;
            if (e != null)
            {
                var alias = e["Alias"];
                var password = e["Contraseña"];
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoUsuarioContraseña, "Model_LoginClick", "App");
                Auth(loginmodel, alias, password);
                model.LoginClick += Model_LoginClick;
                Analytics.TrackEvent("LoginClick");
            }
        }

        public static void Auth(LoginModel loginmodel, string alias, string password)
        {
            (loginmodel.Page as StandarLogin).ProgressAction(new AsyncCommand(async () =>
            {
                try
                {
                    var usercheck = new { alias, password };
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(usercheck);
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoUsuarioContraseña, "Auth", "App", json);
                }
                catch
                {

                }

                // Se busca en la base de datos
                Usuario user = GetUserLocal(alias);
                if (user != null)
                {
                    BouncyCastleHashing bhash = new BouncyCastleHashing();
                    //Realiza una validacion local con la libreria BouncyCastle debido a que PasswordHash no es compatible con 
                    //portatiles.
                    bool IsAuthenticated = bhash.ValidatePassword(password, new byte[16], 10000, 20, user.Password);
                    if (IsAuthenticated)
                    {
                        bool isconnected = true;
                        var response = await App.API.UserEndPoint.Oauth(alias, password, DeviceIdentifier, () => isconnected = false);

                        App.CurrentUser = user;

                        if (isconnected)
                        {
                            App.CurrentUser.BorraBase = response.User.BorraBase;
                            var flagBorraBD = await FlagBorraBase(loginmodel);
                            if (App.CurrentUser.BorraBase == 1)
                            {
                                DownloadSaveMenu(response.MenuUser, response.CifraControl);
                                App.DB.SaveChanges();
                            }
                        }

                        // Borramos los datos sincronizados de la bitácora
                        Bitacora.DeleteBitacora();
                        // Verificamos cuántas veces se creó la BD
                        if (!Bitacora.VerificaRegistroBDVeces())
                        {
                            Bitacora.VerificaCreacionBD();
                        }
                        //Elimina las fotos de fechas atrás
                        BorraFotosAnteriores();
                        if (!response.IsAnotherIMEI)
                        {
                            GoToMainPage();
                        }
                        else
                        {
                            //Si está usando un dispostivo distinto
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IMEIDistinto, $"Se intentó autenticar con este IMEI: { DeviceIdentifier } pero está ligado a otro teléfono.", "App", response.Message);
                            await loginmodel.Page.DisplayAlert("PSM", response.Message, "Aceptar");
                        }
                    }
                    else
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginIncorrecto, "Auth", "App", "Usuario o contraseña incorrectos, intenta nuevamente.");
                        await loginmodel.Page.DisplayAlert("PSM 360", "Usuario o contraseña incorrectos, intenta nuevamente.", "Aceptar");
                    }
                }
                else
                {
                    // No existe en la BD y se busca en linea
                    bool isconnected = true;
                    var response = await App.API.UserEndPoint.Oauth(alias, password, DeviceIdentifier, () => isconnected = false);

                    if (response != null)
                    {
                        if (response.IsAuthenticated)
                        {
                            if (!response.IsAnotherIMEI)
                            {
                                if (!response.User.Bloqueado)
                                {
                                    // el usuario se autentico correctamente por lo que devuelve los datos del usuario mas el menu asignado 
                                    // al usuario.
                                    var usuario = new Usuario
                                    {
                                        Alias = response.User.Alias,
                                        Nombre = response.User.Nombre,
                                        Password = response.User.Password,
                                        IdUsuario = response.User.IdUsuario,
                                        IdRegion = response.User.IdRegion,
                                        IdProyecto = response.User.IdProyecto,
                                        BorraBase = response.User.BorraBase
                                    };

                                    App.CurrentUser = usuario;

                                    var flagBorraBD = await FlagBorraBase(loginmodel);
                                    var userfound = App.DB.Usuario.FirstOrDefault(u => u.Alias.Equals(response.User.Alias));
                                    if (userfound != null)
                                    {
                                        // el usuario ya existe
                                        if (userfound.Password != response.User.Password || userfound.IdProyecto != response.User.IdProyecto || userfound.IdUsuario != response.User.IdUsuario) // el password es diferente
                                        {
                                            // actualizamos el password
                                            userfound.Password = response.User.Password;
                                            userfound.IdProyecto = response.User.IdProyecto;
                                            userfound.IdUsuario = response.User.IdUsuario;
                                            userfound.IdRegion = response.User.IdRegion;
                                            App.DB.Entry(userfound).State = DataBase.EntryState.Modify;
                                        }

                                        SaveAvisos(response.Avisos);
                                        //Trae el menu actializado del usuario
                                        DownloadSaveMenu(response.MenuUser, response.CifraControl);
                                        App.DB.SaveChanges();
                                        // Borramos los datos sincronizados de la bitácora
                                        Bitacora.DeleteBitacora();
                                        // Verificamos cuántas veces se creó la BD
                                        if (!Bitacora.VerificaRegistroBDVeces())
                                        {
                                            Bitacora.VerificaCreacionBD();
                                        }
                                        //Elimina las fotos de fechas atrás
                                        BorraFotosAnteriores();
                                        //navegamos hacia la pagina principal
                                        GoToMainPage();
                                    }
                                    else
                                    {
                                        // el usuario no existe
                                        SaveAvisos(response.Avisos);
                                        DownloadSaveMenu(response.MenuUser, response.CifraControl);
                                        App.DB.Add(usuario);
                                        App.DB.SaveChanges();
                                        // Borramos los datos sincronizados de la bitácora
                                        Bitacora.DeleteBitacora();
                                        // Verificamos cuántas veces se creó la BD
                                        if (!Bitacora.VerificaRegistroBDVeces())
                                        {
                                            Bitacora.VerificaCreacionBD();
                                        }
                                        //Elimina las fotos de fechas atrás
                                        BorraFotosAnteriores();
                                        //navegamos hacia la pagina principal
                                        GoToMainPage();
                                    }
                                }
                                else
                                {
                                    //Si está bloqueado el usuario
                                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginBloqueado, "Auth", "App", "Usuario o contraseña incorrectos, intenta nuevamente.");
                                    // el usuario no se autentico correctamente
                                    await loginmodel.Page.DisplayAlert("PSM", "No puedes ingresar utilizando este usuario porque se encuentra bloqueado. Contacta a Mesa de Control para ingresar a PSM.", "Aceptar");
                                } 
                            }
                            else
                            {
                                //Si está usando un dispostivo distinto
                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IMEIDistinto, $"Se intentó autenticar con este IMEI: { DeviceIdentifier } pero está ligado a otro teléfono.", "App", response.Message);
                                await loginmodel.Page.DisplayAlert("PSM", response.Message, "Aceptar");
                            }
                        }
                        else
                        {
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginIncorrecto, "Auth", "App", "Usuario o contraseña incorrectos, intenta nuevamente.");
                            // el usuario no se autentico correctamente
                            await loginmodel.Page.DisplayAlert("PSM", "Usuario o contraseña incorrectos, intenta nuevamente.", "Aceptar");
                        }
                    }
                    else
                    {
                        if (!isconnected)
                        {
                            await loginmodel.Page.DisplayAlert("PSM", "Revisa tu conexion a internet", "Aceptar");
                        }
                        else
                        {
                            //Bitacora.Insert("BtnLogin_Clicked", "LoginPage", "No tienes conexion a internet, intenta más tarde.");
                            //await loginmodel.Page.DisplayAlert("PSM 360", "No tienes conexion a internet, intenta más tarde.", "Aceptar");
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginIncorrecto, "Auth", "App", "Usuario o contraseña incorrectos, intenta nuevamente.");
                            await loginmodel.Page.DisplayAlert("PSM", "Usuario o contraseña incorrectos, intenta nuevamente.", "Aceptar");
                        }
                    }
                }
            }));
        }

        private static void BorraFotosAnteriores()
        {
            var fecha = DateTime.Now;
            var tiempo = App.DB.Tiempo.OrderByDescending(t => t.IdTiempo).ToList().FirstOrDefault(t => t.Fecha.Date == fecha.Date);
            if (tiempo != null)
            {
                int idtiempoBack = tiempo.IdTiempo - 7;
                // Obtenemos las rutas atrás de 2 semanas sincronizadas
                var idrutas = App.DB.Ruta.Where(r => r.IdTiempo <= idtiempoBack && r.Status == 2 && r.Sincronizado).ToList().Select(r => r.IdRuta).ToList();
                // Borramos datos de RespuestaAuditor de fotos
                App.DB.RespuestaAuditor.Delete(ra => idrutas.Contains(ra.IdRuta) && ra.Foto != null && ra.Sincronizado);
                // Borramos datos de RespuestaAuditor de respuestas
                App.DB.RespuestaAuditor.Delete(ra => idrutas.Contains(ra.IdRuta) && ra.Foto == null);
                // Borramos datos de asistencia
                App.DB.Asistencia.Delete(a => idrutas.Contains(a.IdRuta) && a.Sincronizado);
                // Borramos datos de Abordaje
                App.DB.Abordaje.Delete(ab => idrutas.Contains(ab.IdRuta));
                // Borramos datos de AbordajeMultimedia
                App.DB.AbordajeMultimedia.Delete(ab => idrutas.Contains(ab.IdRuta));
                //Finalmente borramos las rutas
                var rq = App.DB.Ruta.Delete(r => idrutas.Contains(r.IdRuta));
                App.DB.SaveChanges();
                if (rq > 0)
                {
                    App.DB.Execute("VACUUM");
                    App.DB.SaveChanges();
                }
            }
        }

        public static IDictionary<string, string> TrackData(Exception ex = null)
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Usuario", CurrentUser.ToJson());
            if (ex != null)
                dic.Add("Exception", ex.Message);
            return dic;
        }

        private async static Task<bool> FlagBorraBase(LoginModel loginmodel)
        {
            if (CurrentUser.IdUsuario == 0)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.BorradoDBaseAutomatico, "FlagBorraBase", "App", CurrentUser.IdUsuario);
                return false;
            }

            var response = new UploadResponse { Status = false };
            int flag = CurrentUser.BorraBase;
            var bdBack = new RespaldoBDUsuarioDto { IdUsuario = CurrentUser.IdUsuario, Typo = flag };
            if (!CrossConnectivity.Current.IsConnected) return true;
            bool resultado = false;
            switch (flag)
            {
                case 1: /* No acciones */ break;
                case 2:
                    // Revisa pendientes

                    var rutas = DB.Ruta.Where(s => s.Sincronizado == false);
                    List<int> idRutas = new List<int>();
                    foreach (var item in rutas)
                    {
                        idRutas.Add(item.IdRuta);
                    }
                    var resAuditor = DB.RespuestaAuditor.Where(s => idRutas.Contains(s.IdRuta)).ToList();

                    if (resAuditor.Count > 0)
                    {
                        bool answer1 = await loginmodel.Page.DisplayAlert("¡Alerta!", "Tienes respuestas sin subir, ¿Deseas subirlas ahora?", "Si", "No");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.PreguntaBorradoDBaseAutomatico, "FlagBorraBase", "App", "Tienes respuestas sin subir, ¿Deseas subirlas ahora?");

                        if (answer1)
                        {
                            // Intenta subir
                            bool isuploadb = false;
                            foreach (var RutaSelected in DB.Ruta.Where(s => s.Sincronizado == false).ToList())
                            {
                                if (RutaSelected != null)
                                {
                                    if (isuploadb = await UploadData.Upload(RutaSelected, true))
                                    {

                                    }
                                }
                            }
                            if (isuploadb)
                            {
                                await loginmodel.Page.DisplayAlert("Aviso", "Tus respuestas han sido sincronizadas.", "Aceptar");
                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SincronizacionRutaAutomatica, "FlagBorraBase", "App", "Tus respuestas han sido sincronizadas.");
                            }
                        }
                    }
                    // Respalda
                    DB.Close();
                    var respalda = new ReadFileHelper();
                    bdBack = respalda.ReadPsmUltimate();
                    bdBack.IdUsuario = CurrentUser.IdUsuario;
                    bdBack.CurrentPhoto = App.CurrentPhoto;
                    bdBack.CurrentRespAudit = App.CurrentRespAudit;
                    DB = PSMDataBase.Instance;

                    // Borra    Baja    Actualiza user  PSM y Movil
                    resultado = await BorraBajaDBAsync(loginmodel, bdBack);
                    break;
                case 3:
                    // Borra    Baja    Actualiza user  PSM y Movil
                    resultado = await BorraBajaDBAsync(loginmodel, bdBack);
                    break;

                case 4:
                    // Respalda bd en el servidor, tarea oculta para el usuario
                    DB.Close();
                    var respalda2 = new ReadFileHelper();
                    bdBack = respalda2.ReadPsmUltimate();
                    bdBack.IdUsuario = CurrentUser.IdUsuario;
                    bdBack.CurrentPhoto = App.CurrentPhoto;
                    bdBack.CurrentRespAudit = App.CurrentRespAudit;
                    bdBack.Typo = flag;
                    DB = PSMDataBase.Instance;
                    resultado = await RespaldaBDEnServer(loginmodel, bdBack);
                    break;
                case 5:
                    // Borra ambas bases para versión 3.4 productiva en adelante
                    DB.Close();
                    var respalda5 = new ReadFileHelper();
                    bdBack = respalda5.ReadPsmUltimate();
                    bdBack.IdUsuario = CurrentUser.IdUsuario;
                    bdBack.Typo = 5;
                    resultado = await BorraTodaBD(loginmodel, bdBack);
                    break;
            }
            return true;
        }

        private static async Task<bool> BorraTodaBD(LoginModel loginmodel, RespaldoBDUsuarioDto respaldoBDUsuarioDto)
        {
            DB = PSMDataBase.Instance;
            // Borra primero base de datos de PSM
            DB.DropTables();
            // Borra la base de la bitácora
            Bitacora.DropBD();

            // Crea la base de datos
            DB.CreateTables();

            
            await loginmodel.Page.DisplayAlert("Aviso", "Bienvenido a PSM", "Aceptar");
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.BDEliminadaAutomaticamente, "BorraTodaBD", "App", "Se han eliminado la base de datos de PSM y bitácora.");

            respaldoBDUsuarioDto.CurrentPhoto = new List<UploadPhoto>();
            respaldoBDUsuarioDto.CurrentRespAudit = new List<UploadRespAudit>();

            var isconnected = false;
            var response = await Task.Run(() => API.UploadEndPoint.UploadDataBase(respaldoBDUsuarioDto, () => isconnected = false));
            
            // Actualiza usuario local para evitar que vuelva a borrar todo
            if (response.Status)
            {
                App.CurrentUser.BorraBase = 1;
                App.DB.SaveChanges();
            }

            return true;
        }

        private static async Task<bool> BorraBajaDBAsync(LoginModel loginmodel, RespaldoBDUsuarioDto respaldoBDUsuarioDto)
        {
            // Borra BD
            DB.DropTables();
            DB.CreateTables();

            await loginmodel.Page.DisplayAlert("Aviso", "Se ha eliminado la base de datos", "Aceptar");
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.BDEliminadaAutomaticamente, "BorraBajaDBAsync", "App", "Se ha eliminado la base de datos");

            // Baja TODO
            if (await DownloadData.BajaTodo())
                await loginmodel.Page.DisplayAlert("Aviso", "Se ha descargado todo", "Aceptar");

            var isconnected = false;
            var response = await Task.Run(() => API.UploadEndPoint.UploadDataBase(respaldoBDUsuarioDto, () => isconnected = false));
            // Actualiza usuario
            if (response.Status)
            {
                App.CurrentUser.BorraBase = 1;
                App.DB.Add(App.CurrentUser);
                App.DB.SaveChanges();
            }

            return true;
        }

        private static async Task<bool> RespaldaBDEnServer(LoginModel loginmodel, RespaldoBDUsuarioDto respaldoBDUsuarioDto)
        {
            var isconnected = false;
            var response = await Task.Run(() => API.UploadEndPoint.UploadDataBase(respaldoBDUsuarioDto, () => isconnected = false));
            // Actualiza usuario
            if (response.Status)
            {
                App.CurrentUser.BorraBase = 1;
                App.DB.SaveChanges();
            }
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SeRespaldoBD, "RespaldoDB", "Se ha respaldado la base de datos");
            return true;
        }

        private static void SaveAvisos(List<Avisos> avisos)
        {
            foreach (var aviso in avisos)
            {
                if (!App.DB.Avisos.Exists(e => e.IdAviso == aviso.IdAviso))
                {
                    //aviso.Vigente = true;
                    App.DB.Avisos.Add(aviso);
                }
            }
            App.DB.SaveChanges();
        }

        /// <summary>
        /// Metodo que verifica si han existido cambios en el menu del usuario a partir de una variable que se llama cifra control
        /// esta variable suma los ids del menu asignado al usuario y los compara con la suma de los ids del usuario de manera 
        /// local, si estos son diferentes quiere decir que hay cambios en el menu por lo tanto se eliminaran los registros de 
        /// la tabla MenuUser local y inserta los menus que se obtuvieron del servidor.
        /// </summary>
        /// <param name="menuList"></param>
        /// <param name="cifaControl"></param>
        private static void DownloadSaveMenu(List<PSMClient.MenuUser> menuList, int cifaControl)
        {
            int CifraControlLocal = App.DB.MenuUser.Sum(s => s.IdMenuMovil);
            if (cifaControl != CifraControlLocal)
            {
                App.DB.Execute("Delete from MenuUser");
                if (menuList != null && menuList.Count > 0)
                {
                    foreach (var item in menuList)
                    {
                        App.DB.Add(item);
                    }
                }
            }
        }

        public static async void GoToMainPage()
        {
            if (App.CurrentUser != null)
            {
                try
                {
                    var json = JsonConvert.SerializeObject(App.CurrentUser);
                    FechaLogin = DateTime.Now;
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginCorrecto, "GoToMainPage", "App", FechaLogin.ToString());
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.LoginCorrectoConVersion, $"El usuario ingresó con la versión: {new Helpers.Version().NumeroVersion}", "LoginPage", json);
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IMEIDispositivo, $"IMEI del dispositivo: { DeviceIdentifier }", "LoginPage", DeviceIdentifier);
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.AppsInstaladas, $"El usuario tiene las siguientes apps instaladas", "LoginPage", ListInstallApps);
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EspacioAlmacenamientoDisponible, $"Almacenamiento de dispositivo", "LoginPage", InfoStorageDevice);
                    //Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NivelBateria, $"Nivel Batería del dispositivo", "LoginPage", InfoBatteryDevice);
                }
                catch
                {

                }
            }

            ChatConfig.Init?.Invoke(new ChatClient.Userprofile
            {
                Alias = App.CurrentUser.Alias,
                IdUsuario = App.CurrentUser.IdUsuario,
                Descripcion = "Usuario",
                Nombre = App.CurrentUser.Alias
            }, ChatGrupos());

            // Registrar el AppCenterToken al usuario
            if (AppCenterToken.HasValue)
            {
                var result = await API.UserEndPoint.AddToken(App.CurrentUser.IdUsuario, AppCenterToken.Value.ToString(), () => { });
                if (result != null)
                {
                    App.CurrentUser.AppCenterToken = result.AppCenterToken;
                }
            }
            Application.Current.MainPage = new HomePage();
        }

        /// <summary>
        /// Realiza una consulta de  los datos del usuario de maneera local.
        /// </summary>
        /// <param name="Alias"></param>
        /// <returns></returns>
        public static Usuario GetUserLocal(string Alias)
        {
            return App.DB.Usuario.FirstOrDefault(a => a.Alias.ToLower().Equals(Alias.ToLower()));
        }

        protected override async void OnStart()
        {
            Bitacora.CanInsert = true;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IniciaApp, "OnStart", "App");

            if (Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.UWP)
            {
            }
            else
            {
                AppCenter.Start("android=21b96338-772f-4d80-89ae-e8a1aa20553f;" + "uwp=5e17c826-4a9b-40ff-a2b6-346cd06cc35a;" +
                    "ios=49220132-3184-404b-8631-68018268dddf;", typeof(Analytics), typeof(Crashes), typeof(Push));
                AppCenterToken = await AppCenter.GetInstallIdAsync();
                if (AppCenterToken.HasValue)
                {
                }
            }

            ChatConfig.Visibility = VisibilityState.Foreground;

            UploadBitacora();

            var location = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
            switch (location[Permission.Location])
            {
                case PermissionStatus.Denied:
                    await MainPage.DisplayAlert("PSM 360", "Debes permitir el uso del GPS", "Aceptar");
                    break;

                case PermissionStatus.Granted:
                    break;
                case PermissionStatus.Unknown:
                case PermissionStatus.Disabled:
                case PermissionStatus.Restricted:
                default:
                    await MainPage.DisplayAlert("PSM 360", "Debes permitir el uso del GPS", "Aceptar");
                    break;
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.AppOnSleep, "App OnSleep", "App");
            ChatConfig.Visibility = VisibilityState.Background;
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.AppOnResume, "App OnResume", "App");
            ChatConfig.Visibility = VisibilityState.Foreground;
        }

        public static object ToJson()
        {
            string user = "";
            string gps = "{}";
            string route = "";
            string product = "";
            string location = "";
            string section = "";
            string page = "";
            try { user = JsonConvert.SerializeObject(CurrentUser); } catch { }
            try { route = JsonConvert.SerializeObject(CurrentRoute); } catch { }
            try { product = JsonConvert.SerializeObject(CurrentPackage); } catch { }
            try { location = JsonConvert.SerializeObject(CurrentLocation); } catch { }
            try { section = JsonConvert.SerializeObject(CurrentSection); } catch { }
            try { page = "{ " + string.Format(@" ""PageName"": ""{0}""", App.CurrentPage.GetType().FullName) + " }"; } catch { }
            return "{" + string.Format(@" ""user"": {0}, ""gps"": {1}, ""route"": {2}, ""product"": {3}, ""location"": {4}, ""section"": {5}, ""page"": {6} ", user, gps, route, product, location, section, page) + "}";
        }

        public static ToolbarItem GetNotificationToolbarItem()
        {
            var count = App.DB.Avisos.ToList().Count;
            string nombre = "";
            switch (count)
            {
                case 0:
                    nombre = "barnotificaciones_blancoi";
                    break;

                case 1:
                    nombre = "uno";
                    break;

                case 2:
                    nombre = "dos";
                    break;

                case 3:
                    nombre = "tres";
                    break;

                case 4:
                    nombre = "cuatro";
                    break;

                case 5:
                    nombre = "cinco";
                    break;

                case 6:
                    nombre = "seis";
                    break;

                case 7:
                    nombre = "siete";
                    break;

                case 8:
                    nombre = "ocho";
                    break;

                case 9:
                    nombre = "nueve";
                    break;

                default:
                    nombre = "diez";
                    break;
            }

            nombre += ".png";


            ToolbarItem item = new ToolbarItem
            {
                Icon = nombre,
                Command = new Command(() =>
                {
                    HomePage.Instance.SetDetail(new AvisosPage());
                }),
                Priority = 0,
                Order = ToolbarItemOrder.Primary
            };

            return item;
        }
    }
}
