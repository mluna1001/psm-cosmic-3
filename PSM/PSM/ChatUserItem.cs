﻿using ChatApp.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM
{
    public class ChatUserItem
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public List<Chat> Chats { get; set; }
        public int IdUsuario { get; set; }
        public string LastMessage { get; set; }
        public DateTime Fecha { get; set; }
    }
}
