﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IndicadorPage : ContentPage
    {
        public IndicadorPage()
        {
            InitializeComponent();
            GetInfoUser();
            GetIndicatorData();
        }

        public IndicadorPage(int IdTienda)
        {
            InitializeComponent();
            GetInfoUser();
            GetIndicatorData(IdTienda);
        }

        public async void GetInfoUser()
        {
            lblNombre.Text = "Usuario";
            lblAlias.Text = App.CurrentUser.Alias;
            lblNombre.Text = string.IsNullOrEmpty(App.CurrentUser.Nombre) ? "" : App.CurrentUser.Nombre;
        }

        public async void GetIndicatorData(int IdTienda = 0)
        {
            List<Indicador> lstIndicador = new List<Indicador>();

            if (IdTienda == 0)
               lstIndicador = App.DB.Indicador.Where(i => i.IdUsuario == App.CurrentUser.IdUsuario).ToList(); 
            else
                lstIndicador = App.DB.Indicador.Where(i => i.IdUsuario == App.CurrentUser.IdUsuario && i.IdTienda == IdTienda).ToList();

            if (lstIndicador != null || lstIndicador.Count > 0)
            {
                lvIndicadores.ItemsSource = lstIndicador;
            }
            else
            {
                if (IdTienda == 0)
                {
                    await DisplayAlert("PSM", "Por favor primero descargue su información.", "Aceptar");
                }
                else
                {
                    await DisplayAlert("PSM", "No hay indicador aplicable para esta tienda.", "Aceptar");
                }
            }
        }

        private void LvIndicadores_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lvIndicadores.SelectedItem = null;
        }

        private async void LvIndicadores_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var detalleIndicador = (Indicador)e.Item;

            DetalleIndicadorPage detalle = new DetalleIndicadorPage(detalleIndicador);
            detalle.Title = detalleIndicador.Descripcion;
            await Navigation.PushAsync(detalle);
        }
    }
}