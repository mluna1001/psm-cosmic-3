﻿using ChatApp;
using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConversationGroupPage : ContentPage
    {

        ChatGrupo _grupo;

        public ConversationGroupPage(ChatGrupo grupo)
        {
            InitializeComponent();
            _grupo = grupo;
            ChatConfig.ConversationGroup = new ChatApp.ORM.ChatGrupo { IdProyecto = grupo.IdProyecto, IdChatGrupo = grupo.IdChatGrupo, Descripcion = grupo.Descripcion, CreadoEl = grupo.CreadoEl, Baneado = grupo.Baneado, ActualizadoEl = grupo .ActualizadoEl };
            ChatConfig.InConversationGroup = true;
            ChatConfig.GroupMessageReceive -= ChatConfig_GroupMessageReceive;
            ChatConfig.GroupMessageReceive += ChatConfig_GroupMessageReceive;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "ConversationPage");
        }

        private void ChatConfig_GroupMessageReceive(object sender, ChatApp.ORM.ChatMensaje e)
        {
            if(e != null)
            {
                var chatmessage = new PSM.ORM.ChatMensaje
                {
                    Fecha = e.Fecha,
                    IdChatGrupo = e.IdChatGrupo,
                    IdChatMensaje = e.IdChatMensaje,
                    IdProyecto = e.IdProyecto,
                    IdUsuario = e.IdUsuario,
                    Mensaje = e.Mensaje
                };
                RenderChats(chatmessage);
                ScrollToLastBubbleRender();
            }
        }

        private ChatBubbleView LastBubbleRender { get; set; }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            List<ChatMensaje> chats = await App.API.ChatEndPoint.Chats(_grupo.IdChatGrupo);
            if(chats != null)
            {
                RenderChats(chats.OrderBy(e => e.Fecha.Value).ToArray());
                ScrollToLastBubbleRender();
            }
        }

        private void RenderChats(params ChatMensaje[] chats)
        {
            var lastindex = chats.Length - 1;
            for (var i = 0; i < chats.Length; i++)
            {
                var chat = chats[i];
                ChatBubbleView render = MakeBubble(chat);
                Device.BeginInvokeOnMainThread(() =>
                {
                    StackChats.Children.Add(render);
                });
                if (lastindex == i)
                {
                    LastBubbleRender = render;
                }
            }
        }

        public void ScrollToLastBubbleRender()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (LastBubbleRender != null) await ScrollChats.ScrollToAsync(LastBubbleRender, ScrollToPosition.End, true);
            });
        }
        
        private void BtnEnviar_Clicked(object sender, EventArgs e)
        {
            var message = BoxMessage.Text ?? "";
            if (!string.IsNullOrEmpty(message))
            {
                ChatConfig.SendToGroup?.Invoke(new ChatApp.ORM.ChatMensaje
                {
                    Fecha = DateTime.Now,
                    IdChatGrupo = _grupo.IdChatGrupo,
                    IdProyecto = App.CurrentUser.IdProyecto,
                    IdUsuario = App.CurrentUser.IdUsuario,
                    Mensaje = message
                });

                /*
                var chatsend = await App.API.ChatEndPoint.ReceiveMessage(_grupo.IdChatGrupo, App.CurrentUser.IdProyecto, App.CurrentUser.IdUsuario, message);
                if(chatsend != null)
                {
                    RenderChats(new List<ChatMensaje> { chatsend });
                    ScrollToLastBubbleRender();
                }
                */
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction,"BtnEnviar_Clicked", "ConversationGroupPage", message);
                BoxMessage.Text = "";
            }
        }

        public ChatBubbleView MakeBubble(ChatMensaje chat)
        {
            ChatBubbleView bubble;
            if (chat.IdUsuario.Value == App.CurrentUser.IdUsuario)
            {
                bubble = new ChatBubbleView(ChatBubbleViewType.Me);
            }
            else
            {
                bubble = new ChatBubbleView(ChatBubbleViewType.He);
            }
            bubble.Message = chat.Mensaje;
            bubble.SendDate = chat.Fecha.Value.ToString("yyyy-MM-dd hh:mm:ss");
            return bubble.Render();
        }
    }
}
