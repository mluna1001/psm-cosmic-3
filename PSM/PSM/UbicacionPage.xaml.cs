﻿using PSM.ORM;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TK.CustomMap.Api.Google;
using TK.CustomMap.Overlays;
using Plugin.Geolocator;
using PSM.Helpers;

namespace PSM
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UbicacionPage : ContentPage
    {
        #region variables de pagina
        double longitud, latitud;
        bool hasposition = false;
        #endregion

        public UbicacionPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            InfoTienda inftienda = new InfoTienda();
            inftienda = App.DB.InfoTienda.FirstOrDefault(b => b.IdTienda == App.IdTienda);
            if (inftienda != null)
            {
                var map = new Map()
                {
                    IsShowingUser = true,
                    HeightRequest = 100,
                    WidthRequest = 960,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };

                SetMap(inftienda, map);

                var stack = new StackLayout { Spacing = 0 };
                stack.Children.Add(map);
                Content = stack;
                //hasposition = true;
                //longitud = double.Parse(inftienda.Longitud.ToString());
                //latitud = double.Parse(inftienda.Latitud.ToString());
                //var pin = new TK.CustomMap.TKCustomMapPin
                //{
                //    Position = new Xamarin.Forms.Maps.Position(latitud, longitud),
                //    Title = inftienda.NombreTienda,
                //    Subtitle = inftienda.Direccion,
                //    ID = inftienda.IdTienda.ToString()
                //};
                //List<TK.CustomMap.TKCustomMapPin> pins = new List<TK.CustomMap.TKCustomMapPin>
                //{
                //    pin
                //};
                //CustomMap.CustomPins = pins;
                //CustomMap.IsShowingUser = true;
                //CustomMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitud, longitud), Distance.FromKilometers(0.5)));
                //// https://maps.googleapis.com/maps/api/directions/json?origin=19.480056,%20-98.834496&destination=19.509114,%20-98.883326&key=AIzaSyBBHuPnEjdRz-A3fzwm3aHPKqOezX342GI
            }
        }

        private void SetMap(InfoTienda inftienda, Map map)
        {
            var latitudes = new List<double>();
            var longitudes = new List<double>();

            double.TryParse(inftienda.Latitud.ToString(), out latitud);
            double.TryParse(inftienda.Longitud.ToString(), out longitud);

            map.Pins.Add(new Pin
            {
                Type = PinType.Place,
                Position = new Position(latitud, longitud),
                Label = inftienda.NombreTienda,
            });

            latitudes.Add(latitud);
            longitudes.Add(longitud);

            double lowestLat = latitudes.Min();
            double highestLat = latitudes.Max();
            double lowestLong = longitudes.Min();
            double highestLong = longitudes.Max();
            double finalLat = (lowestLat + highestLat) / 2;
            double finalLong = (lowestLong + highestLong) / 2;
            double distance = DistanceCalculation.GeoCodeCalc.CalcDistance(lowestLat, lowestLong, highestLat, highestLong, DistanceCalculation.GeoCodeCalcMeasurement.Kilometers);

            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(finalLat, finalLong), Distance.FromKilometers(0.5)));
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            GmsDirection.Init("AIzaSyBBHuPnEjdRz-A3fzwm3aHPKqOezX342GI");
            //if (hasposition)
            //{
            //    var currentpositionofuser = await CrossGeolocator.Current.GetPositionAsync();
            //    if (currentpositionofuser != null)
            //    {
            //        var directions = await GmsDirection.Instance.CalculateRoute(new Position(latitud, longitud), new Position(currentpositionofuser.Latitude, currentpositionofuser.Latitude), GmsDirectionTravelMode.Driving);
            //        if (directions != null)
            //        {
            //            if (directions.Status == GmsDirectionResultStatus.Ok)
            //            {
            //                var routes = new ObservableCollection<TKRoute>();
            //                CustomMap.Routes = routes;
            //                if (directions.Routes.Count() > 0)
            //                {
            //                    var direction = directions.Routes.ElementAt(0);
            //                    if (direction.Legs.Count() > 0)
            //                    {
            //                        var leg = direction.Legs.ElementAt(0);
            //                        var tkroute = new TKRoute { LineWidth = 1, Selectable = false, TravelMode = TKRouteTravelMode.Any, Color = Color.FromHex("#ABC0AB") };
            //                        List<TKRouteStep> steps = new List<TKRouteStep>();
            //                        foreach (var step in leg.Steps)
            //                        {
            //                            TKRouteStep tkstep = new TKRouteStep
            //                            {
            //                                BindingContext = step
            //                            };
            //                            steps.Add(tkstep);
            //                        }
            //                        routes.Add(tkroute);
            //                    }
            //                    else
            //                    {

            //                    }
            //                }
            //                else
            //                {

            //                }
            //            }
            //            else
            //            {
            //                //await DisplayAlert("PSM 360", directions.Status.ToString(), "Aceptar");
            //            }
            //        }
            //        else
            //        {

            //        }
            //    }
            //    else
            //    {

            //    }
            //}
            //else
            //{
            //}
        }
    }
}
