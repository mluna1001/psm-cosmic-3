﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PSM
{
    public partial class InformacionPage : ContentPage
    {
        public InformacionPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            Version.Text = "Versión " + new Helpers.Version().NumeroVersion;
            Usuario.Text = App.CurrentUser.Alias;
            Fecha.Text = App.FechaLogin.ToString();
        }

        protected override bool OnBackButtonPressed()
        {
            var navigationpage = new NavigationPage(new MainPage());
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            HomePage.Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
            return true;
        }
    }

}
