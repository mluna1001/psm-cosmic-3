﻿using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NearbyRoutePage : ContentPage
    {
        public NearbyRoutePage()
        {
            InitializeComponent();
            LlenaTiendasCercanasRuta();
            lstTiendaCercana.ItemTapped += LstTiendaCercana_ItemTapped;
            lstTiendaCercana.ItemSelected += LstTiendaCercana_ItemSelected;
        }

        private void LstTiendaCercana_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lstTiendaCercana.SelectedItem = null;
        }

        private async Task SaveRoute(TiendasCercanasRuta tienda)
        {
            DateTime fecha = DateTime.Now;
            DateTimeFormatInfo dfy = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfy.Calendar;
            int nodia = (int)DateTime.Now.DayOfWeek;
            string nombreDia = GetDias().Where(d => d.IdDia == nodia).First().Dia;
            //int semanaActual = CultureInfo.CurrentUICulture.Calendar.GetWeekOfYear(fecha, CalendarWeekRule.FirstDay, fecha.DayOfWeek) -1;
            int semanaActual = cal.GetWeekOfYear(fecha, dfy.CalendarWeekRule, dfy.FirstDayOfWeek);
            //Ruta ruta = App.DB.Ruta.Where(r => r.IdTienda == tienda.IdTienda && r.NumeroSemana == semanaActual && r.NombreDelDia == nombreDia).FirstOrDefault();
            //if (ruta != null)
            //{
            //    await DisplayAlert("Info", $"La tienda {tienda.TiendaNombre} ya tiene una ruta existente para esta semana", "Aceptar");
            //    return;
            //}
            //else
            //{
            var idRuta = App.DB.Ruta.Where(r => r.IdRuta < 0).OrderBy(r => r.IdRuta).FirstOrDefault();
            //var idTiempo = App.DB.Ruta.Where(r => r.NumeroSemana == semanaActual && r.NombreDelDia == nombreDia).FirstOrDefault();
            var idTiempo = App.DB.Tiempo.Where(r => r.NombreDia.Equals(nombreDia)).FirstOrDefault();
            int difSemana = (semanaActual - idTiempo.SemanadelAño) * 7;
            int idInfoTienda = 0;

            Ruta rutaAlta = new Ruta();
            rutaAlta.IdProyecto = App.CurrentUser.IdProyecto;
            rutaAlta.IdUsuario = App.CurrentUser.IdUsuario;
            rutaAlta.IdTiempo = idTiempo.IdTiempo + difSemana;
            rutaAlta.Status = 0;
            rutaAlta.IdTienda = tienda.IdTienda;
            rutaAlta.IdProyectoTienda = tienda.IdProyectoTienda;
            rutaAlta.TiendaNombre = tienda.TiendaNombre;
            rutaAlta.NumeroSemana = semanaActual;
            rutaAlta.NombreDelDia = nombreDia;
            rutaAlta.FechaInicio = null;
            rutaAlta.FechaTerminacion = null;
            rutaAlta.GpsLatitude = null;
            rutaAlta.GpsLongitude = null;
            rutaAlta.Sincronizado = false;
            rutaAlta.IdRuta = idRuta != null ? (idRuta.IdRuta - 1) : -1;

            App.DB.Ruta.Add(rutaAlta);
            App.DB.SaveChanges();

            // Se comienza a validar si existe InfoTienda
            var infoTienda = App.DB.InfoTienda.FirstOrDefault(i => i.IdTienda == tienda.IdTienda);

            // Si no existe, se crea el registro de InfoTienda correspondiente a la tienda
            if (infoTienda == null)
            {
                InfoTienda info = new InfoTienda()
                {
                    IdInfoTienda = idInfoTienda,
                    IdTienda = tienda.IdTienda,
                    Latitud = tienda.CoordenadaY,
                    Longitud = tienda.CoordenadaX,
                    NombreTienda = tienda.TiendaNombre,
                    Direccion = "",
                    ImagenUrl = null,
                    IdCadena = tienda.IdCadena,
                    Verificada = tienda.Verificada
                };

                App.DB.InfoTienda.Add(info);
                App.DB.SaveChanges();
            }
            await DisplayAlert("Información", $"Se ha agregado la tienda {tienda.TiendaNombre} para visitarla el día de hoy", "Aceptar");
            //}
            lstTiendaCercana.IsEnabled = true;
        }

        private IList<ListaDias> GetDias()
        {
            return new List<ListaDias>
            {
                new ListaDias { IdDia=0, Dia="Domingo" },
                new ListaDias { IdDia=1, Dia="Lunes" },
                new ListaDias { IdDia=2, Dia="Martes" },
                new ListaDias { IdDia=3, Dia="Miercoles" },
                new ListaDias { IdDia=4, Dia="Jueves" },
                new ListaDias { IdDia=5, Dia="Viernes" },
                new ListaDias { IdDia=6, Dia="Sabado" },
            };
        }

        private void LlenaTiendasCercanasRuta()
        {
            List<TiendasCercanasRuta> tiendas = new List<TiendasCercanasRuta>();
            tiendas = App.DB.TiendasCercanasRuta.ToList();

            if (tiendas == null || tiendas.Count == 0)
            {
                DisplayAlert("Info", "No hay tiendas disponibles para crear rutas.", "Aceptar");
                return;
            }
            lstTiendaCercana.ItemsSource = tiendas;
        }

        private async void LstTiendaCercana_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            TiendasCercanasRuta tienda = (TiendasCercanasRuta)e.Item;
            lstTiendaCercana.IsEnabled = false;
            await SaveRoute(tienda);
        }

        protected override bool OnBackButtonPressed()
        {
            var navigationpage = new NavigationPage(new MainPage());
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            HomePage.Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
            return true;
        }
    }
}