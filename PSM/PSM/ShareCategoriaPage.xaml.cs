﻿using PSM.ORM;
using PSM.Function;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShareCategoriaPage : CarouselPage
    {
        private List<Producto> _presentaciones;

        public ShareCategoriaPage(List<Producto> presentaciones)
        {
            InitializeComponent();
            App.CurrentPage = this;
            Init();
        }

        private void Init()
        {
            PreciosPage.SavePage += PreciosPage_SavePage;
            for (int i = 0; i < _presentaciones.Count; i++)
            {
                string backtext = "Anterior";
                string nexttext = "Siguiente";
                if ((i + 1) == _presentaciones.Count)
                {
                    nexttext = "Terminar sección";
                }
                var model = new HallazgoPackageModel
                {
                    Index = i,
                    BtnBackText = backtext,
                    BtnNextText = nexttext,
                    BtnBackCommand = new Command(BtnBack),
                    BtnNextCommand = new Command(BtnNext)
                };

                var producto = _presentaciones[i];
                QuestionSharePage package = new QuestionSharePage(new List<Producto> { producto }, model);
                this.Children.Add(package);
            }
        }

        private Dictionary<int, QuestionSharePage> preciospagetoremove = new Dictionary<int, QuestionSharePage>();

        private void BtnNext(object obj)
        {
            var model = obj as HallazgoPackageModel;
            if (model != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "BtnNext", "ShareCategoriaPage", "Siguiente");
                var preciospage = model.Page as QuestionSharePage;
                if (preciospage != null)
                {
                    if (preciospagetoremove.ContainsKey(model.Index))
                    {
                        preciospagetoremove[model.Index] = preciospage;
                    }
                    else
                    {
                        preciospagetoremove.Add(model.Index, preciospage);
                    }
                    NextPage();
                }

                if (model.BtnNextText.Equals("Terminar sección"))
                {
                    SaveSecction();
                }
            }
        }

        private void NextPage()
        {
            this.ShowNext();
        }

        private void BtnBack(object obj)
        {
            var model = obj as HallazgoPackageModel;
            if (model != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "BtnBack", "ShareCategoriaPage", "Anterior");
                var preciospage = model.Page as QuestionSharePage;
                if (preciospage != null)
                {
                    if (preciospagetoremove.ContainsKey(model.Index))
                    {
                        preciospagetoremove[model.Index] = preciospage;
                    }
                    else
                    {
                        preciospagetoremove.Add(model.Index, preciospage);
                    }
                    PreviusPage();
                }
                if (model.BtnNextText.Equals("Terminar sección"))
                {
                    SaveSecction();
                }
            }
        }

        private void PreviusPage()
        {
            this.ShowPrevious();
        }

        private void SaveSecction()
        {
            foreach (var pageitem in preciospagetoremove)
            {
                pageitem.Value.SaveSection();
            }
        }

        private async void PreciosPage_SavePage(object sender, SavePage e)
        {
            if (e.Success)
            {
                Children.Remove(e.Page);
                if (Children.Count == 0)
                {
                    await DisplayAlert("PSM 360", "Se han guardado las respuestas correctamente", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.GuardoRespuestas, "PreciosPage_SavePage", "ShareCategoriaPage", "Se han guardado las respuestas correctamente");
                    await Navigation.PopAsync();
                }
            }
            else
            {
                await DisplayAlert("PSM 360", "Te hace falta rellenar algunos campos", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.FaltaLlenarCampos, "PreciosPage_SavePage", "ShareCategoriaPage", "Te hace falta rellenar algunos campos");
            }
        }
    }
}