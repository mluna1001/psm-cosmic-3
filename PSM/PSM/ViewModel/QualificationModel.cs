﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.ViewModel
{
    public class QualificationModel
    {
        public int IdQualification { get; set; }
        public int IdRuta { get; set; }
        public int IdTienda { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreTienda { get; set; }
        public double Calificacion { get; set; }

        //
        public int IdQualiSection { get; set; }
        public int IdSeccion { get; set; }
        public string Seccion { get; set; }
        public double Quali { get; set; }
        public double Total { get; set; }

        //

        public int IdQualiPregunta { get; set; }
        public int IdPregunta { get; set; }
        public string Pregunta { get; set; }
        public string Respuesta { get; set; }
        public double QualiPregunta { get; set; }

        public int Nivel { get; set; }
        //public List<QualificationModel> qualificationModel { get; set; }
        //public List<QualiSectionModel> qualiSection { get; set; }
    }

    public class QualiSectionModel
    {
        public int IdQualiSection { get; set; }
        public int IdSeccion { get; set; }
        public string Seccion { get; set; }
        public double Quali { get; set; }
        public List<QualiPreguntaModel> qualiPregunta { get; set; }
    }

    public class QualiPreguntaModel
    {
        public int IdQualiPregunta { get; set; }
        public int IdPregunta { get; set; }
        public int IdQualiSection { get; set; }
        public string Pregunta { get; set; }
        public double Quali { get; set; }
    }
}
