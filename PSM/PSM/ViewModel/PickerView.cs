﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.ViewModel
{
    public class PickerView
    {
        /// <summary>
        /// Picker actual
        /// </summary>
        public Picker PickerProduct { get; set; }
        /// <summary>
        /// Label del picker actual
        /// </summary>
        public Label LabelProduct { get; set; }
        /// <summary>
        /// Lista de productos actuales para el picker
        /// </summary>
        public List<ProductoModel> Items { get; internal set; }
    }
}
