﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.ViewModel
{
    public class AbordajeModel : CatalogoSeccionMenu
    {

        public ImageSource Image
        {
            get
            {
                if (string.IsNullOrEmpty(Icono))
                {
                    return "";
                }
                else
                {
                    if (Icono.Contains("http"))
                    {
                        return Icono;
                    }
                    else
                    {
                        return Xamarin.Forms.ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(Icono)));
                    }
                }
            }
        }

    }
}
