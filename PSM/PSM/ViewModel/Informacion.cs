﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.ViewModel
{
    public class Informacion
    {
        public int IdRuta { get; set; }
        public int IdRutaDinamica { get; set; }
        public int IdTienda { get; set; }
        public string NombreTienda { get; set; }
        public string Direccion { get; set; }
        public Command OnTiendaClick { get; set; }
        public Command<Informacion> OnTiendaOpenClick { get; internal set; }
    }
}
