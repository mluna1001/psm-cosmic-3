﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.ViewModel
{
    public class ProductoModel
    { 
        public Producto ProductoPadre { get; set; }
        public List<ProductoModel> ProductosHijos { get; set; }
        public int IdSeccion { get; internal set; }
        public bool Desencadenada { get; set; }
        public int Id { get; internal set; }
        public PickerView View { get; set; }
        public ProductoModel()
        {
            ProductosHijos = new List<ProductoModel>();
        }
    }
}
