﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ViewModel
{
    public class MenuModel
    {

        public string Title { get; set; }
        public string Icon { get; set; }
        public Type TargetType { get; set; }
        public string Target { get; internal set; }
    }
}
