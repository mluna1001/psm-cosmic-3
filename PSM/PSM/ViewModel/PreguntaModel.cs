﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Media.Abstractions;
using PSM.DeviceSensors;
using PSM.Function;

namespace PSM.ViewModel
{
    public class PreguntaModel
    {
        public int Indice { get; set; }
        public int IdRespuesta { get; set; }
        public string Respuesta { get; set; }
        public Pregunta PreguntaPadre { get; set; }
        public List<PreguntaModel> PreguntasHijas { get; set; }
        public View View { get; set; }
        public int IdPregunta { get; set; }
        public int IdSeccion { get; set; }
        public bool Desencadenada { get; set; }
        public List<Respuesta> Respuestas { get; set; }
        public PhotoResult File { get; set; }
        public byte[] FotoEdit { get; set; }
        public string FileName { get; set; }
        public int Id { get; set; }
        public int IdControl { get; set; }

        public byte[] Foto
        {
            get
            {
                if (File != null)
                {
                    try { return File.Photo.GetStream().ToByteArray(); } catch { }
                }
                return null;
            }
        }

        public string FotoNombre
        {
            get
            {
                if (File != null)
                {
                    return File.Name;
                }
                return null;
            }
        }

        public int? IdRespuestaAuditor { get; set; }

        public PreguntaModel()
        {
            PreguntasHijas = new List<PreguntaModel>();
        }
    }
}
