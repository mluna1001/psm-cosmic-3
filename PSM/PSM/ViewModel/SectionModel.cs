﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ViewModel
{
    public class SectionModel
    {
        public int IdSeccion { get; set; }
        public int IdProyecto { get; set; }
        public int IdCatalogoSeccion { get; set; }
        public Nullable<int> Orden { get; set; }
        public string Icono { get; set; }
        public string Descripcion { get; set; }
        public double Porcentaje { get; set; }
        public Type TargetType { get; set; }
        public bool EsSeccion { get; set; }
        public bool SeValida { get; set; }
        public bool EsActiva { get; set; }
    }
}
