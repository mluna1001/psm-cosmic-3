﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ViewModel
{
    public class ListaDias
    {
        public int IdDia { get; set; }
        public string Dia { get; set; }
    }
}
