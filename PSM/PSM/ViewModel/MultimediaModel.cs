﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ViewModel
{
    public class MultimediaModel : Multimedia
    {
        public object Icon { get; set; }
        public string Source { get; set; }
        public Abordaje Abordaje { get; set; }
    }
}
