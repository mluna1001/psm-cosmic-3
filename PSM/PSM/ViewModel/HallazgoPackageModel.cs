﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.ViewModel
{
    public class HallazgoPackageModel
    {

        public Page Page { get; set; }
        public Command BtnBackCommand { get; set; }
        public Command BtnNextCommand { get; set; }
        public string BtnBackText { get; set; }
        public string BtnNextText { get; set; }
        public int Index { get; internal set; }
    }
}
