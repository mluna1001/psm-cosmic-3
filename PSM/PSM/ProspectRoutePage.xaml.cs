﻿using PSM.Function;
using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProspectRoutePage : ContentPage
    {
        public ProspectRoutePage()
        {
            InitializeComponent();
            LoadNearbyGPS();
        }

        private void LoadNearbyGPS()
        {
            lvTiendasCercanas.IsPullToRefreshEnabled = true;
            if (App.DB.Tiempo.Any())
            {
                ProgressAction(new AsyncCommand(async () =>
                {
                    lvTiendasCercanas.ItemsSource = await MisSubordinados();
                }));
            }
            else
            {
                DisplayAlert("Info", "Primero descarga tus catálogos.", "Aceptar");
                lvTiendasCercanas.ItemsSource = null;
                return;
            }
        }

        public async void ProgressAction(AsyncCommand command)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                StackProgress.IsVisible = true;
                lvTiendasCercanas.IsVisible = false;
            });

            if (command != null)
            {
                await command.ExecuteAsync(null);
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                StackProgress.IsVisible = false;
                lvTiendasCercanas.IsVisible = true;
            });
        }

        private async Task<List<TiendasCercanasUbicacion>> MisSubordinados()
        {
            var Tiendas = new List<TiendasCercanasUbicacion>();
            var isconnected = false;

            Tiendas = await Task.Run(() => App.API.ProspectStoreRequest.ProspectStores(App.CurrentUser.IdUsuario, () =>
                isconnected = false));

            return Tiendas;
        }

        private void ListView_Refreshing(object sender, EventArgs e)
        {
            LoadNearbyGPS();
        }

        private void lstTiendasCercanas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lvTiendasCercanas.SelectedItem = null;
        }

        private async void lvTiendasCercanas_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            TiendasCercanasUbicacion tienda = (TiendasCercanasUbicacion)e.Item;
            lvTiendasCercanas.IsEnabled = false;
            await SaveRoute(tienda);
        }

        private async Task SaveRoute(TiendasCercanasUbicacion tienda)
        {
            DateTime fecha = DateTime.Now;
            DateTimeFormatInfo dfy = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfy.Calendar;
            int nodia = (int)DateTime.Now.DayOfWeek;
            string nombreDia = GetDias().Where(d => d.IdDia == nodia).First().Dia;
            //int semanaActual = CultureInfo.CurrentUICulture.Calendar.GetWeekOfYear(fecha, CalendarWeekRule.FirstDay, fecha.DayOfWeek);
            int semanaActual = cal.GetWeekOfYear(fecha, dfy.CalendarWeekRule, dfy.FirstDayOfWeek);
            var idRuta = App.DB.Ruta.Where(r => r.IdRuta < 0).OrderBy(r => r.IdRuta).FirstOrDefault();
            //var idTiempo = App.DB.Ruta.Where(r => r.NumeroSemana == semanaActual && r.NombreDelDia == nombreDia).FirstOrDefault();
            var idTiempo = App.DB.Tiempo.Where(r => r.NombreDia.Equals(nombreDia)).FirstOrDefault();

            if (idTiempo != null)
            {
                int difSemana = (semanaActual - idTiempo.SemanadelAño) * 7;

                Ruta rutaAlta = new Ruta();
                rutaAlta.IdProyecto = App.CurrentUser.IdProyecto;
                rutaAlta.IdUsuario = App.CurrentUser.IdUsuario;
                rutaAlta.IdTiempo = idTiempo.IdTiempo + difSemana;
                rutaAlta.Status = 0;
                rutaAlta.IdTienda = tienda.IdTienda;
                rutaAlta.IdProyectoTienda = tienda.IdProyectoTienda;
                rutaAlta.TiendaNombre = tienda.TiendaNombre + " " + Math.Abs(idRuta != null ? (idRuta.IdRuta - 1) : -1);
                rutaAlta.NumeroSemana = semanaActual;
                rutaAlta.NombreDelDia = nombreDia;
                rutaAlta.FechaInicio = null;
                rutaAlta.FechaTerminacion = null;
                rutaAlta.GpsLatitude = null;
                rutaAlta.GpsLongitude = null;
                rutaAlta.Sincronizado = false;
                rutaAlta.IdRuta = idRuta != null ? (idRuta.IdRuta - 1) : -1;

                App.DB.Ruta.Add(rutaAlta);
                App.DB.SaveChanges();

                ExisteInfoTienda(tienda);
                await DisplayAlert("Información", $"Se ha agregado la tienda {tienda.TiendaNombre} para visitarla el día de hoy", "Aceptar");
            }
            else
            {
                await DisplayAlert("Información", $"Es necesario descargar tiendas antes de la creación", "Aceptar");
                return;
            }
            lvTiendasCercanas.IsEnabled = true;
        }

        private void ExisteInfoTienda(TiendasCercanasUbicacion tienda)
        {
            int idInfoTienda = 0;

            InfoTienda it = App.DB.InfoTienda.FirstOrDefault(t => t.IdTienda == tienda.IdTienda);

            if (it == null)
            {
                InfoTienda info = new InfoTienda()
                {
                    //IdInfoTienda = idInfoTienda,
                    IdTienda = tienda.IdTienda,
                    Latitud = tienda.CoordenadaY,
                    Longitud = tienda.CoordenadaX,
                    IdCadena = tienda.IdCadena,
                    NombreTienda = tienda.TiendaNombre,
                    ImagenUrl = null,
                    Direccion = "",
                    Verificada = tienda.Verificada
                };

                App.DB.InfoTienda.Add(info);
                App.DB.SaveChanges();
            }
        }

        private IList<ListaDias> GetDias()
        {
            return new List<ListaDias>
            {
                new ListaDias { IdDia=0, Dia="Domingo" },
                new ListaDias { IdDia=1, Dia="Lunes" },
                new ListaDias { IdDia=2, Dia="Martes" },
                new ListaDias { IdDia=3, Dia="Miercoles" },
                new ListaDias { IdDia=4, Dia="Jueves" },
                new ListaDias { IdDia=5, Dia="Viernes" },
                new ListaDias { IdDia=6, Dia="Sabado" },
            };
        }

        protected override bool OnBackButtonPressed()
        {
            var navigationpage = new NavigationPage(new MainPage());
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            HomePage.Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
            return true;
        }

    }
}