﻿using Plugin.Connectivity;
using PSM.Function;
using PSM.Pages;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;

namespace PSM
{
    public partial class SemaforoPage : ContentPage
    {     
        public SemaforoPage()
        {
            InitializeComponent();
            Default();
        }

        private void Default()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                LVSubordinadoList.IsPullToRefreshEnabled = true;
                ProgressAction(new AsyncCommand(async () =>
                {
                    LVSubordinadoList.ItemsSource = await MisSubordinados();
                })); 
            }
            else
            {
                StackProgress.IsVisible = false;
                DisplayAlert("Advertencia", "No tienes conexión a internet para ver el semáforo", "Aceptar");
            }
        }

        protected override void OnAppearing()
        {
            LVSubordinadoList.IsEnabled = true;
        }

        private async Task<List<usuarioModel>> MisSubordinados()
        {
            var response = new List<usuarioModel>();
            var isconnected = false;

            response = await Task.Run(() => App.API.UserEndPoint.GetSubordinateUsers(() => isconnected = false));

            return response;
        }

        public async void ProgressAction(AsyncCommand command)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                StackProgress.IsVisible = true;
                LVSubordinadoList.IsVisible = false;
            });

            if (command != null)
            {
                await command.ExecuteAsync(null);
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                StackProgress.IsVisible = false;
                LVSubordinadoList.IsVisible = true;
            });
        }

        private async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            usuarioModel um = (usuarioModel)e.Item;
            LVSubordinadoList.IsEnabled = false;
            if (!um.Asistencia.Any())
                await DisplayAlert("Semaforo", "Este colaborador, no tiene entradas registradas.", "Aceptar");

            await Navigation.PushAsync(new SemaforoDetailPage(um));
        }

        private void Handle_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            LVSubordinadoList.SelectedItem = null;
        }

        protected void ListItems_Refreshing(object sender, EventArgs e)
        {
            Default();
            LVSubordinadoList.EndRefresh();
        }

        protected override bool OnBackButtonPressed()
        {
            var navigationpage = new NavigationPage(new MainPage());
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            HomePage.Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
            return true;
        }
    }
}