﻿using DevelopersAzteca.Storage.SQLite;
using PSM.API;
using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSM.Function;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static PSM.API.PSMClient;
using Microsoft.AppCenter.Crashes;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UploadPage : ContentPage
    {
        private List<Ruta> Rutas;
        private Ruta RutaSelected;
        private int SelectedIndex { get; set; }
        private List<InfoTienda> InfoTienda;

        public UploadPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            Rutas = App.DB.Ruta.Where(e => e.Status == 3 || e.Status == 2 && e.Sincronizado == false).ToList();
            PickerTienda.Items.Add("-- Selecciona una tienda --");

            var idtiendas = Rutas.Select(s => s.IdTienda).ToList();
            InfoTienda = App.DB.InfoTienda.Where(s => idtiendas.Contains(s.IdTienda)).ToList();
            var asistencia = App.DB.Asistencia;


            var bit = InfoTienda.Where(s => s.EstatusCheck != null).ToList();
            if (bit.Count() != 0)
            {
                var asistenciaSalida = App.DB.Asistencia.Where(e => e.descripcion == App.MODULO_SALIDAGLOB).ToList();
                var asistenciadiaSalida = (from a in asistenciaSalida
                                           join r in Rutas on a.IdRuta equals r.IdRuta
                                           select r).ToList();

                foreach (var ruta in Rutas)
                {
                    var vasi = asistenciadiaSalida.FirstOrDefault(s => s.IdTiempo == ruta.IdTiempo);
                    var rufin = InfoTienda.FirstOrDefault(s => s.IdTienda == ruta.IdTienda);
                    if (rufin.EstatusCheck != null)
                    {
                        if ((bool)rufin.EstatusCheck)
                        {
                            PickerTienda.Items.Add(ruta.IdRuta + " - " + ruta.NombreDelDia + " - " + ruta.TiendaNombre);
                        }
                    }
                    if (vasi == null)
                    {
                        PickerTienda.Items.Add(ruta.IdRuta + " - " + ruta.NombreDelDia + " - " + ruta.TiendaNombre);
                    }

                }
            }
            else
            {
                foreach (var ruta in Rutas)
                {
                    PickerTienda.Items.Add(ruta.IdRuta + " - " + ruta.NombreDelDia + " - " + ruta.TiendaNombre);
                }
            }

            PickerTienda.SelectedIndex = 0;
            PickerTienda.SelectedIndexChanged += PickerTienda_SelectedIndexChanged;
        }

        private void PickerTienda_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedIndex = PickerTienda.SelectedIndex;
            if (PickerTienda.SelectedIndex > 0)
            {
                var item = PickerTienda.Items[PickerTienda.SelectedIndex];
                item = item.Replace(" ", string.Empty);
                var itemsplit = item.Split('-');
                var idruta = string.IsNullOrEmpty(itemsplit[0]) ? "-" + itemsplit[1] : itemsplit[0];
                var dia = string.IsNullOrEmpty(itemsplit[0]) ? itemsplit[2] : itemsplit[1];
                var tienda = string.IsNullOrEmpty(itemsplit[0]) ? itemsplit[3] : itemsplit[2];

                RutaSelected = Rutas.FirstOrDefault(r => r.IdRuta == int.Parse(idruta));

                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TiendaSeleccionadaUpload, "PickerTienda_SelectedIndexChanged", "UploadPage", item);
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TiendaSeleccionadaUpload, "PickerTienda_SelectedIndexChanged", "UploadPage", RutaSelected);
            }
            else
            {
                RutaSelected = null;
            }
        }

        private async void BtnUpload_Clicked(object sender, EventArgs e)
        {
            BtnUpload.IsEnabled = false;
            HomePage.Instance.IsGestureEnabled = false;
            HomePage.Instance.Detail.IsEnabled = false;




            if (RutaSelected != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    StackForm.IsVisible = false;
                    StackProgress.IsVisible = true;
                });
                
                List<Ruta> RutaSelectedList = new List<Ruta>();
                var info = InfoTienda.FirstOrDefault(s => s.IdTienda == RutaSelected.IdTienda);
                //Si la tienda para subir informacion es del tipo salida global se manda todas las tiendas para subir, sino se subira de la manera normal (una tienda)
                if (info != null)
                {
                    if (info.EstatusCheck != null)
                    {
                        if ((bool)info.EstatusCheck)
                        {
                            RutaSelectedList = App.DB.Ruta.Where(re => re.Status == 3 || re.Status == 2 && re.Sincronizado == false && re.IdTiempo == RutaSelected.IdTiempo).ToList();
                        }
                    }
                    else
                    {
                        RutaSelectedList.Add(RutaSelected);
                    } 
                }
                else
                {
                    RutaSelectedList.Add(RutaSelected);
                }

                ///     //
                ///   / //
                ///     //
                ///     //
                ///  //////// 
                //Aqui se suben las rutas es el primer metodo donde entra al seleccionar una tienda pendiente por subir
                foreach (var item in RutaSelectedList)
                {
                    if (await UploadData.Upload(item, false))
                    {
                        try
                        {
                            if (SelectedIndex >= 0)
                            {
                                if (info != null)
                                {
                                    if (info.EstatusCheck != null)
                                    {
                                        if ((bool)info.EstatusCheck)
                                        {
                                            if (RutaSelectedList.IndexOf(item) == RutaSelectedList.Count - 1)
                                            {
                                                PickerTienda.Items.RemoveAt(SelectedIndex);
                                                RutaSelected = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PickerTienda.Items.RemoveAt(SelectedIndex);
                                        RutaSelected = null;
                                    } 
                                }
                                else
                                {
                                    PickerTienda.Items.RemoveAt(SelectedIndex);
                                    RutaSelected = null;
                                }
                            }
                            EliminaFotosSincronizadas(item);
                            BtnUpload.IsEnabled = true;
                        }
                        catch (Exception ex)
                        {
                            Crashes.TrackError(ex, App.TrackData());
                        }

                    }
                    else
                    {
                        //Rollback(RutaSelectedList);
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioTienda, "BtnUpload_Clicked", "UploadPage", $"No fue posible subir la información de la ruta: { item.IdRuta } - { item.TiendaNombre}");
                        break;
                    }
                    var r = App.DB.Ruta;
                    if (r.Any())
                    {
                        bool be;
                        bool sendRate = bool.TryParse(r.FirstOrDefault().SendQual.ToString(), out be);
                        if (sendRate)
                        {
                            if (be)
                            {
                                bool c = await BajarCalificacion();
                            }
                        }
                    }
                }



                Device.BeginInvokeOnMainThread(() =>
                {
                    StackForm.IsVisible = true;
                    StackProgress.IsVisible = false;
                });

                if (PickerTienda.Items.Count == 1)
                {
                    await Navigation.PopAsync();
                }
            }

            HomePage.Instance.IsGestureEnabled = true;
            BtnUpload.IsEnabled = true;
            HomePage.Instance.IsGestureEnabled = true;
            HomePage.Instance.Detail.IsEnabled = true;

        }

        private void Rollback(List<Ruta> RutaSelectedList)
        {
            foreach (var item2 in RutaSelectedList)
            {
                item2.Sincronizado = false;
                App.DB.Entry(item2).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                App.DB.SaveChanges();
            }


            List<int> idsRuta = RutaSelectedList.Select(s => s.IdRuta).ToList();

            List<RespuestaAuditor> respuestas = App.DB.RespuestaAuditor.Where(it => idsRuta.Contains(it.IdRuta)).ToList();
            if (respuestas.Any())
            {
                foreach (var item2 in respuestas)
                {
                    item2.Sincronizado = false;
                    App.DB.Entry(item2).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                    App.DB.SaveChanges();
                }

            }

            List<Abordaje> abordajes = App.DB.Abordaje.Where(it => idsRuta.Contains(it.IdRuta)).ToList();
            if (abordajes.Any())
            {
                foreach (var item2 in abordajes)
                {
                    item2.Sincronizado = false;
                    App.DB.Entry(item2).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                    App.DB.SaveChanges();
                }


            }

            List<AbordajeMultimedia> abordajesmultimedia = App.DB.AbordajeMultimedia.Where(it => idsRuta.Contains(it.IdRuta)).ToList();
            if (abordajesmultimedia.Any())
            {
                foreach (var item2 in abordajesmultimedia)
                {
                    item2.Sincronizado = false;
                    App.DB.Entry(item2).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                    App.DB.SaveChanges();
                }

            }

            List<Asistencia> asistencias = App.DB.Asistencia.Where(it => idsRuta.Contains(it.IdRuta)).ToList();
            if (asistencias.Any())
            {
                foreach (var item2 in asistencias)
                {
                    item2.Sincronizado = false;
                    App.DB.Entry(item2).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                    App.DB.SaveChanges();
                }


            }

            List<Foto> fotos = App.DB.Foto.Where(it => idsRuta.Contains(it.IdRuta)).ToList();
            if (fotos.Any())
            {
                foreach (var item2 in fotos)
                {
                    item2.Sincronizado = false;
                    App.DB.Entry(item2).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                    App.DB.SaveChanges();
                }
            }
        }

        public async Task<bool> BajarCalificacion()
        {
            bool status = false;
            bool isconnected = true;


            var qualiResponse = await App.API.QualiEndPoint.QualiRoute(App.CurrentUser.IdUsuario, async () =>
            {
                isconnected = false;
            });

            if (qualiResponse != null)
            {
                if (qualiResponse.Qualification != null && qualiResponse.Qualification.Count > 0)
                {
                    foreach (var item in qualiResponse.Qualification)
                    {
                        if (!App.DB.Qualification.Exists(a => a.IdRuta == item.IdRuta))
                        {
                            App.DB.Qualification.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }
                if (qualiResponse.QualiSections != null && qualiResponse.QualiSections.Count > 0)
                {
                    foreach (var item in qualiResponse.QualiSections)
                    {
                        if (!App.DB.QualiSection.Exists(a => a.IdRuta == item.IdRuta))
                        {
                            App.DB.QualiSection.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }
                if (qualiResponse.QualiCuestion != null && qualiResponse.QualiCuestion.Count > 0)
                {
                    App.DB.Execute("DELETE FROM QualiPregunta");

                    foreach (var item in qualiResponse.QualiCuestion)
                    {
                        //if (!App.DB.QualiPregunta.Exists(a => a.IdRuta == item.IdRuta))
                        //{
                        App.DB.QualiPregunta.Add(item);
                        //}
                    }
                    App.DB.SaveChanges();
                }

                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarCalificacion", "UploadPage", "Se han descargado las calificaciones");

                status = true;
            }
            else
            {
                if (isconnected)
                {
                    await Application.Current.MainPage.DisplayAlert("PSM 360", "No tienes conexión a internet, intenta más tarde", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarCalificacion", "UploadPage", "Ocurrio un error al descargar calificacion, intenta mas tarde");
                }
            }

            return status;
        }

        protected void EliminaFotosSincronizadas(Ruta ruta)
        {
            //Eliminamos las respuestas de las rutas que se suben en estos momentos
            App.DB.RespuestaAuditor.Delete(f => f.IdRuta == ruta.IdRuta && f.Foto != null && f.Sincronizado);
            //Eliminamos si en algún momento llegan a existir fotos sincronizadas de otras rutas antes subidas
            //App.DB.RespuestaAuditor.Delete(f => f.Sincronizado);
            App.DB.SaveChanges();
        }

        NavigationPage navigationpage;

        protected override bool OnBackButtonPressed()
        {
            navigationpage = new NavigationPage(new MainPage());
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            HomePage.Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
            return true;
        }
    }
}