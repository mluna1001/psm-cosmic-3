﻿using PSM.Controls;
using PSM.DeviceSensors;
using PSM.Function;
using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.Helpers.Subcontrols
{
    public class PreguntaModelSub : StackLayout
    {
        //public int IdRespuesta { get; set; }
        //public string Respuesta { get; set; }
        //public Pregunta PreguntaPadre { get; set; }
        //public List<PreguntaModel> PreguntasHijas { get; set; }
        public View View { get; set; }
        //public int IdPregunta { get; set; }
        //public int IdSeccion { get; set; }
        //public bool Desencadenada { get; set; }
        //public List<Respuesta> Respuestas { get; set; }
        //public PhotoResult File { get; set; }
        //public string FileName { get; set; }
        //public int Id { get; set; }
        //public int IdControl { get; set; }

        public PreguntaModel model { get; set; }
        public QuestionPage question { get; set; }

        /// <summary>
        /// Lista de presentaciones o empaques
        /// </summary>
        private List<Producto> _presentaciones;
        public PreciosPage precios { get; set; }
        private bool _promo { get; set; }

        public PreguntaModelSub(PreguntaModel _model, QuestionPage _question)
        {
            this.model = _model;
            this.question = _question;
            //Patriarca = new StackLayout();
            RenderSigleLayout(model, this);
        }

        public PreguntaModelSub(PreguntaModel _model, PreciosPage _question, bool promo, List<Producto> presentaciones)
        {
            this.model = _model;
            this.precios = _question;

            _promo = promo;
            _presentaciones = presentaciones;
            RenderSigleLayout(model, this);
        }

        protected void RenderSigleLayout(PreguntaModel pregunta, StackLayout stack, int indice = -1)
        {
            model = pregunta;
            var stackHijo = new StackLayout();
            Label label = null;
            Producto presentacion = new Producto();
            if (pregunta.IdControl == 8)
            {
                label = new Label
                {
                    Text = pregunta.PreguntaPadre.Descripcion,
                    FontAttributes = FontAttributes.Bold,
                    FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                    HorizontalOptions = LayoutOptions.CenterAndExpand
                };
            }
            else if (pregunta.IdControl == 9)
            {
                label = new Label
                {
                    Text = pregunta.PreguntaPadre.Descripcion,
                    FontAttributes = FontAttributes.Bold,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                };
            }
            else
            {
                if (this.precios != null)
                {
                    presentacion = _presentaciones.FirstOrDefault(p => p.IdProducto == model.Id);

                    Label labelProducto = new Label
                    {
                        Text = presentacion.Nombre
                    };
                    stackHijo.Children.Add(labelProducto);

                    if (indice == -1)
                    {
                        label = new Label
                        {
                            Text = pregunta.Indice + ".- " + pregunta.PreguntaPadre.Descripcion,
                            TextColor = Color.Black
                        };
                    }
                    else
                    {
                        indice = indice + 1;
                        label = new Label
                        {

                            Text = pregunta.Indice + "." + indice + ".- " + pregunta.PreguntaPadre.Descripcion,
                            TextColor = Color.Black
                        };
                    }
                }
                else
                {
                    label = new Label
                    {
                        Text = pregunta.PreguntaPadre.Descripcion,
                        FontAttributes = FontAttributes.Bold
                    };
                }
            }

            stackHijo.Children.Add(label);



            switch (pregunta.PreguntaPadre.IdControl)
            {
                case 1:
                case 5:
                    // creamos el campo de edición
                    Entry entry = new Entry
                    {
                        Placeholder = "Ingresa tu respuesta"
                    };
                    // si el campo es numerico
                    if (pregunta.PreguntaPadre.IdControl == 5)
                    {
                        // asignamos
                        entry.Keyboard = Keyboard.Numeric;
                    }
                    // asignamos el id para el elemento de entrada
                    entry.ClassId = this.model.IdPregunta.ToString();

                    // asignamos el id para el elemento de entrada
                    entry.StyleId = this.model.Id.ToString();

                    // evento para obtener la respuesta en tiempo de ejecución
                    entry.TextChanged += Entry_TextChanged;
                    // si al renderear el preguntao tiene una respuesta
                    if (!string.IsNullOrEmpty(pregunta.Respuesta))
                    {
                        //esta respuesta es asignada
                        entry.Text = pregunta.Respuesta;
                    }
                    // asignamos la vista al pregunta
                    View = entry;
                    // Agrgamos el label al rootlayou y el entry
                    //stack.Children.Add(label);
                    stackHijo.Children.Add(entry);

                    break;

                case 7:

                    // el entry es de tipo moneda
                    EntryCurrency entrycurrency = new EntryCurrency();
                    // asignamos el id para la view del pregunta
                    entrycurrency.ClassId = this.model.IdPregunta.ToString();

                    entrycurrency.StyleId = this.model.Id.ToString();
                    // evento de edición
                    entrycurrency.EntryCurrencyTextChanged += Entrycurrency_EntryCurrencyTextChanged;
                    
                    // si tiene una respuesta el preguntao
                    if (!string.IsNullOrEmpty(pregunta.Respuesta))
                    {
                        // asignamos la respuesta
                        entrycurrency.Text = pregunta.Respuesta.TextToMoney();
                    }
                    else
                    {
                        if (_promo)
                        {
                            if (presentacion.Precio.HasValue)
                            {
                                var textmoney = string.Format(CultureInfo.CurrentCulture, "{0:F2}", presentacion.Precio.Value);
                                entrycurrency.Text = textmoney.TextToMoney();
                                model.Respuesta = entrycurrency.Text.MoneyToText();
                            }
                        }
                    }
                    // asignamos la vista al pregunta
                    View = entrycurrency;
                    // asignamos los elementos a la view
                    //stack.Children.Add(label);
                    stackHijo.Children.Add(entrycurrency);

                    break;

                case 10:

                    EntryCurrency currencypromo = new EntryCurrency();
                    currencypromo.ClassId = this.model.IdPregunta.ToString();
                    currencypromo.StyleId = this.model.Id.ToString();
                    currencypromo.EntryCurrencyTextChanged += Entrycurrency_EntryCurrencyTextChanged;
                    if (!string.IsNullOrEmpty(model.Respuesta))
                    {
                        currencypromo.Text = model.Respuesta.TextToMoney();
                    }
                    else
                    {
                        if (_promo)
                        {
                            if (presentacion.PrecioPromo.HasValue)
                            {
                                var textmoney = string.Format(CultureInfo.CurrentCulture, "{0:F2}", presentacion.PrecioPromo.Value);
                                currencypromo.Text = textmoney.TextToMoney();
                                model.Respuesta = currencypromo.Text.MoneyToText();
                            }
                        }
                    }
                    // asignamos la vista al pregunta
                    View = currencypromo;
                    // asignamos los elementos a la view
                    //stack.Children.Add(label);
                    stackHijo.Children.Add(currencypromo);
                    break;

                case 2:
                    // buscamos las respuestas para el check
                    var respuestascheck = App.DB.Respuesta.Where(e => e.IdPregunta == pregunta.IdPregunta).ToList();
                    // creamos una tabla
                    TableRoot root = new TableRoot();
                    // asignamos la tabla a una vista
                    TableView table = new TableView(root) { Intent = TableIntent.Form };
                    // asignamos el alto de la tabla
                    table.HeightRequest = respuestascheck.Count * 55;
                    // asignamos una seccion a la tabla
                    TableSection section = new TableSection();
                    // iteramos las respuestas que hay en la base de datos
                    foreach (var respuesta in respuestascheck)
                    {
                        // valor por defecto a la respuesta
                        int respuestaint = 0;
                        // valor boolean por defecto a la respuesta
                        bool respuestabool = false;
                        // si el preguntao respuestas es diferente de null, 
                        if (pregunta.Respuestas != null)
                        {
                            // buscamos la respuesta previamente realizada
                            var respuestafound = pregunta.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                            // si la respuesta existe
                            if (respuestafound != null)
                            {
                                // parseamos a boolean
                                if (respuestafound.Valor.HasValue)
                                {
                                    // asignamos el valor a la respuesta
                                    respuestaint = (int)respuestafound.Valor.Value;
                                }
                                // obtenemos el valor a asignar en respuestabool
                                respuestabool = respuestaint == 1;
                            }
                        }
                        // creamos el switch que mostrará la respuesta

                        string style = "";
                        if (this.precios != null)
                        {
                            style = model.Id.ToString();
                        }

                        SwitchCell sview = new SwitchCell
                        {
                            // texto a mostrar en el switch
                            Text = respuesta.TextoOpcion,
                            // valor asignado
                            On = respuestabool,
                            // id para obtener el valor de la respuesta
                            ClassId = respuesta.IdRespuesta.ToString()

                        };
                        // evento que se lanza al cambiar el valor del switch
                        sview.OnChanged += Sview_OnChanged;
                        // asignamos el switch a la section
                        section.Add(sview);
                        // hay respuestas en el preguntao
                        if (pregunta.Respuestas != null)
                        {
                            // buscamos la respuesta iterada en las respuestas previamente contestadas
                            var respuestafound = pregunta.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                            // si hay una respuesta en los preguntaos
                            if (respuestafound != null)
                            {
                                // la quitamos para evitar duplicados
                                pregunta.Respuestas.Remove(respuestafound);
                            }
                            // asignamos el valor por default o post render a la respuesta
                            respuesta.Valor = respuestaint;
                            // agregamos la respuesta a la lista
                            pregunta.Respuestas.Add(respuesta);
                        }
                    }
                    // asignamos la seccion a la tabla
                    root.Add(section);
                    // asignamos el titulo de la pregunta y la tabla a la página
                    //stack.Children.Add(label);
                    stackHijo.Children.Add(table);
                    break;

                case 3:
                    // sacamos las respuestas del si/no
                    var respuestascombo = App.DB.Respuesta.Where(e => e.IdPregunta == pregunta.PreguntaPadre.IdPregunta).ToList();
                    // creamos el picker
                    Picker picker = new Picker();
                    // asignamos el picker al preguntao
                    View = picker;
                    // asignamos un id unico a la vista
                    picker.ClassId = this.model.IdPregunta.ToString();
                    picker.StyleId = this.model.Id.ToString();

                    // buscammos alguna respuesta por defecto o por default (valordefault) en las respuestas del combo
                    var answerdefault = respuestascombo.FirstOrDefault(e => e.ValorDefault.HasValue && e.ValorDefault.Value);
                    if (answerdefault != null)
                    {
                        // existe una respuesta por default
                    }
                    else
                    {
                        // no existe respuesta por default entonces agregamos una opcion por default
                        picker.Items.Add("-- Selecciona una opción --");
                    }

                    // el valor por default en index = 0, si encontramos un valor por default, cambiamos el indice al número correspondiente
                    int indexdefault = 0;
                    Respuesta respuestadefault = null;
                    // iteramos las respuestas del combo en busqueda del valor indexdefault
                    for (var o = 0; o < respuestascombo.Count; o++)
                    {
                        // obtenemos la respuesta
                        var respuesta = respuestascombo[o];
                        // si hay una respuesta por default, podemos comparar el idrespuesta del item que se asignará al picker, para así obtener el indice de ese valor por default
                        if (answerdefault != null)
                        {
                            // si el idrespuesta es igual, este item es el correcto para el indice
                            if (respuesta.IdRespuesta == answerdefault.IdRespuesta)
                            {
                                // asignamos el valor de o = indice del picker
                                indexdefault = o;
                                respuestadefault = respuesta;
                            }
                        }
                        // asignamos el valor de la respuesta al picker
                        picker.Items.Add(respuesta.TextoOpcion);
                    }

                    // obtenemos el index de la respuesta actual
                    var selected = picker.Items.IndexOf(pregunta.Respuesta);
                    // si el picker tiene más de un elemento, asignamos el index al primer elemento
                    if (picker.Items.Count > 0) { picker.SelectedIndex = 0; }
                    // si existe un index para la respuesta actual [post-render]

                    // evento lanzado cuando el picker cambie de indice
                    picker.SelectedIndexChanged += Picker_SelectedIndexChanged;
                    // asignamos la vista y los detalles a la vista
                    //stack.Children.Add(label);
                    stackHijo.Children.Add(picker);

                    if (selected > -1)
                    {
                        // asignamos la respuesta previa del usuario al picker
                        picker.SelectedIndex = selected;
                    }
                    else
                    {
                        // en caso contrario, de no existir un index de respuesta anterior, verificamos
                        // si existe un default para la respuesta
                        if (answerdefault != null)
                        {
                            // asignamos el index por default para la respuesta
                            picker.SelectedIndex = indexdefault;
                            pregunta.Respuesta = picker.Items[indexdefault];
                            if (respuestadefault != null) pregunta.IdRespuesta = respuestadefault.IdRespuesta;
                        }
                    }

                    break;

                case 4:
                    // creamos el picker para fecha
                    DatePicker datepicker = new DatePicker();
                    // asignamos el id unico a la visa
                    datepicker.ClassId = this.model.IdPregunta.ToString();

                    datepicker.StyleId = this.model.Id.ToString();
                    // asignamos una fecha por default
                    datepicker.Date = new DateTime(DateTime.Now.Year, 1, 1);
                    // evento a lanzar cuando la fecha cambie
                    datepicker.DateSelected += Datepicker_DateSelected;
                    // si el preguntao tiene una respuesta previa
                    if (!string.IsNullOrEmpty(pregunta.Respuesta))
                    {
                        // asignamos la respuesta
                        datepicker.Date = DateTime.ParseExact(pregunta.Respuesta, "yyyy-MM-dd", CultureInfo.CurrentCulture);
                    }
                    // asignamos la vista al preguntao
                    View = datepicker;
                    // asignamos la pregunta y el datepicker a la vista
                    //stack.Children.Add(label);
                    stackHijo.Children.Add(datepicker);
                    break;

                // foto
                case 6:
                    // boton para tomar foto
                    Button btnfoto = new Button
                    {
                        Text = "Tomar foto"
                    };
                    // asignamos el id unico a la vista
                    if (this.precios != null)
                    {
                        btnfoto.ClassId = this.model.IdPregunta.ToString();
                        btnfoto.StyleId = this.model.Id.ToString();
                    }
                    else
                    {
                        btnfoto.ClassId = this.model.IdPregunta.ToString();
                    }
                    // asignamos el evento de click al boton
                    btnfoto.Clicked += Btnfoto_Clicked;
                    // si el botón tiene una respuesta previa, la asignamos
                    if (!string.IsNullOrEmpty(pregunta.Respuesta))
                    {
                        btnfoto.BackgroundColor = Color.Accent;
                    }
                    else if (!string.IsNullOrEmpty(model.FileName))
                    {
                        btnfoto.BackgroundColor = Color.Accent;
                    }
                    // asignamos la pregunta y el botno a la vista
                    //stack.Children.Add(label);
                    stackHijo.Children.Add(btnfoto);
                    break;
            }
            stack.Children.Add(stackHijo);
        }

        private void Tgr_Tapped(object sender, EventArgs es)
        {
            var stack = (StackLayout)sender;

            var pregunta = model.PreguntaPadre;


        }

        #region Eventos

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool esPrecio = false;
            if (this.precios != null)
                esPrecio = true;

            // convertimos el sender en un campo
            var entry = sender as Entry;
            // obtenmos el hashcode
            var hashcode = int.Parse(entry.ClassId);
            var id = int.Parse(entry.StyleId);
            // buscamos el modelo con base al hashcode
            //var modelEntry = this.question._models.FirstOrDefault(m => m.IdPregunta == hashcode);

            PreguntaModel modelEntry = new PreguntaModel();
            if (esPrecio)
            {
                modelEntry = findPreguntaModel(this.precios._models, hashcode, esPrecio, id);
            }
            else
            {
                modelEntry = findPreguntaModel(this.question._models, hashcode, esPrecio, id);
            }
            // asignamos al modelo la respuesta del campo de texto
            modelEntry.Respuesta = entry.Text;
            // cambiamos el color del campo
            entry.BackgroundColor = Color.Accent;
        }

        private void Entrycurrency_EntryCurrencyTextChanged(object sender, EntryCurrencyTextChanged e)
        {
            bool esPrecio = false;
            if (this.precios != null)
                esPrecio = true;

            // obtenemos el objeto entrycurrency
            var currency = sender as EntryCurrency;
            // convertimos el id unico a un int
            int hashcode = int.Parse(currency.ClassId);
            var id = int.Parse(currency.StyleId);
            // comparamos por hashcode
            //var model = question._models.FirstOrDefault(m => m.IdPregunta == hascode);
            PreguntaModel model = new PreguntaModel();
            if (esPrecio)
            {
                model = findPreguntaModel(this.precios._models, hashcode, esPrecio, id);
            }
            else
            {
                model = findPreguntaModel(this.question._models, hashcode, esPrecio, id);
            }
            // si encontramos el modelo
            if (model != null)
            {
                // asignamos la respuesta al modelo
                model.Respuesta = e.Currency;
            }
        }

        private void Datepicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            bool esPrecio = false;
            if (this.precios != null)
                esPrecio = true;


            // convertimos el campo en un datepicker
            var datepicker = sender as DatePicker;
            // obtenemos el hashcode
            var hashcode = int.Parse(datepicker.ClassId);

            var id = int.Parse(datepicker.StyleId);
            // obtenemos el modelo con base al hashcode

            //var model = question._models.FirstOrDefault(m => m.IdPregunta == hashcode);
            PreguntaModel model = new PreguntaModel();
            if (esPrecio)
            {
                model = findPreguntaModel(this.precios._models, hashcode, esPrecio, id);
            }
            else
            {
                model = findPreguntaModel(this.question._models, hashcode, esPrecio, id);
            }
            // si el modelo existe
            if (model != null)
            {
                // asignamos la respuesta al modelo
                model.Respuesta = datepicker.Date.ToString("yyyy-MM-dd");
            }
            // cambiamos un background al datepicker
            datepicker.BackgroundColor = Color.Accent;
        }

        private async void Btnfoto_Clicked(object sender, EventArgs e)
        {

            bool esPrecio = false;
            if (this.precios != null)
                esPrecio = true;


            var btnfoto = sender as Button;
            int hashcode = int.Parse(btnfoto.ClassId);
            int id = 0;

            PreguntaModel model = new PreguntaModel();
            if (esPrecio)
            {
                id = int.Parse(btnfoto.StyleId);
                model = findPreguntaModel(this.precios._models, hashcode, esPrecio, id);
            }
            else
            {
                model = findPreguntaModel(this.question._models, hashcode, esPrecio, id);
            }
            //var model = question._models.FirstOrDefault(m => m.IdPregunta == hashcode);
            if (model != null)
            {
                try
                {
                    var idproducto = model.Id;

                    string filename = "";
                    if (esPrecio)
                    {
                        // nombre de la foto
                        filename = App.CurrentUser.Alias + "-" + (App.CurrentRoute.IdProyectoTienda == 0 ? "0" : App.CurrentRoute.IdProyectoTienda.ToString()) + "-" + model.IdSeccion + "-" + model.IdPregunta + "-" + idproducto + "-" + (App.CycleNumber == 0 ? "0" : App.CycleNumber.ToString()) + "-" + (App.CurrentPackage != null ? App.CurrentPackage.IdPresentacionEmpaque.ToString() : "0") + "-" + (App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion.ToString() : "0") + "-" + App.CurrentRoute.IdRuta + ".jpg";
                    }
                    else
                    {
                        filename = App.CurrentUser.Alias + "-" + (App.CurrentRoute.IdProyectoTienda == 0 ? "0" : App.CurrentRoute.IdProyectoTienda.ToString()) + "-" + model.IdSeccion + "-" + model.IdPregunta + "-" + (App.CycleNumber == 0 ? "0" : App.CycleNumber.ToString()) + "-" + (App.CurrentPackage != null ? App.CurrentPackage.IdPresentacionEmpaque.ToString() : "0") + "-" + (App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion.ToString() : "0") + "-" + App.CurrentRoute.IdRuta + ".jpg";
                    }



                    // task para capturar foto
                    CameraCaptureTask task = new CameraCaptureTask
                    {
                        FileName = filename,
                        FolderName = "PSM360Photos"
                    };

                    // obtenemos la foto
                    PhotoResult result = null;
                    //if (Device.RuntimePlatform == Device.UWP)
                    //{
                    //    result = await task.PickPhoto();
                    //}
                    //else
                    result = await task.TakePhoto();
                    // si existe la foto
                    if (result != null)
                    {
                        // si falló la captura
                        if (!result.Success)
                        {
                            // mostramos el error generado
                            await Application.Current.MainPage.DisplayAlert("PSM 360", result.Message, "Aceptar");
                        }
                        else
                        {
                            // guardamos el path en el modelo
                            //model.Respuesta = result.Photo.Path;
                            // guardamos el archivo en el modelo
                            model.File = result;
                            // asignamos un color al botón
                            btnfoto.BackgroundColor = Color.Accent;
                        }
                    }
                }
                catch (Exception d)
                {
                }
            }
        }

        // picker si/no
        private async void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            double time = 2.00;
            //sender convertido a picker
            var picker = sender as Picker;

            bool esPrecio = false;
            if (this.precios != null)
                esPrecio = true;

            //si hay un id en picker
            if (!string.IsNullOrEmpty(picker.ClassId))
            {
                //converitmos el id en hashcode
                int hashcode = int.Parse(picker.ClassId);
                //obtenemos el modelo
                //var preguntamodel = this.question._models.FirstOrDefault(m => m.IdPregunta == hashcode);

                int id = int.Parse(picker.StyleId);
                picker.BackgroundColor = Color.Accent;

                PreguntaModel preguntamodel = new PreguntaModel();
                if (esPrecio)
                {
                    preguntamodel = findPreguntaModel(this.precios._models, hashcode, esPrecio, id);
                }
                else
                {
                    preguntamodel = findPreguntaModel(this.question._models, hashcode, esPrecio, id);
                }


                //si el modelo no existe, terminamos la ejecución
                if (preguntamodel == null) return;
                //obtenemos la respuesta del picker
                var respuesta = picker.Items[picker.SelectedIndex];
                //si el modelo existe, asignamos la respuesta
                preguntamodel.Respuesta = respuesta;
                //obtenemos el indice del modelo con base al modelo
                int indexofmodel = 0;
                if (esPrecio)
                {
                    indexofmodel = precios._models.IndexOf(preguntamodel);
                }
                else
                {
                    indexofmodel = question._models.IndexOf(preguntamodel);
                }
                //buscamos en la base de datos las respuestas
                var respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.TextoOpcion == respuesta && r.IdPregunta == preguntamodel.IdPregunta);
                //si hay una respuesta en la base de datos
                if (respuestaindb != null)
                {
                    //asignamos la respuesta seleccionada al modelo
                    preguntamodel.IdRespuesta = respuestaindb.IdRespuesta;
                    //si el indice del modelo es mayor a -1 quiere decir que existe en los modelos
                    if (indexofmodel > -1)
                    {
                        //preguntamos si la respuesta desencadena preguntas
                        if (respuestaindb.Desencadena)
                        {
                            //si la pregunta tiene preguntas hijas, podemos desencadenar
                            if (preguntamodel.PreguntaPadre.TieneHijos)
                            {
                                //asignamos al modelo que se ha descenadenado la respuesta | pregunta
                                preguntamodel.Desencadenada = true;


                                //insertamos en el rango, posterior al indice del modelo, las preguntas hijas
                                if (esPrecio)
                                {
                                    precios._models.InsertRange(indexofmodel + 1, preguntamodel.PreguntasHijas);
                                }
                                else
                                {
                                    question._models.InsertRange(indexofmodel + 1, preguntamodel.PreguntasHijas);
                                }

                                var stackPadre = picker.Parent as StackLayout;
                                var stackChild = new StackLayout();

                                int indexchild = 0;
                                foreach (var modelHija in preguntamodel.PreguntasHijas)
                                {
                                    modelHija.Indice = preguntamodel.Indice;
                                    RenderSigleLayout(modelHija, stackChild, indexchild);
                                    indexchild++;
                                }

                                stackPadre.Children.Add(stackChild);

                                //volvemos a renderear la vista
                                //await SetPreguntas(question._models);
                            }
                            else if (preguntamodel.Desencadenada) // en caos de que la respuesta no desencadene, revisamos si previamente se ha descenadenado
                            {
                                //cambiamos el valor de desencadenada
                                preguntamodel.Desencadenada = false;
                                //iteramos las preguntas hijas del modelo
                                foreach (var preguntahija in preguntamodel.PreguntasHijas)
                                {
                                    //preguntamos si la pregunta hija se ha desencadenado
                                    if (preguntahija.Desencadenada)
                                    {
                                        //si se ha desencadenado, obtenemos la vista de la pregunta hija[picker]
                                        var pickerhija = preguntahija.View as Picker;
                                        //y la asignamos a 0 para que lanze este evento, y borre las preguntas hijas que tiene del modelo
                                        pickerhija.SelectedIndex = 0;
                                    }
                                }
                                //quitamos el rango de preguntas hijas asignadas despues del indice del modelo
                                if (esPrecio)
                                {
                                    precios._models.RemoveRange(indexofmodel + 1, preguntamodel.PreguntasHijas.Count);
                                }
                                else
                                {
                                    question._models.RemoveRange(indexofmodel + 1, preguntamodel.PreguntasHijas.Count);
                                }
                                //volvemos a renderear la vista

                                var variable = picker.Parent as StackLayout;

                                var sKill = variable.Children.FirstOrDefault(x => x.GetType() == typeof(StackLayout));
                                variable.Children.Remove(sKill);
                            }
                        }
                        else
                        {
                            //volvemos a renderear la vista quitando las hijas
                            var preguntasHijas = preguntamodel.PreguntasHijas.Select(h => h.IdPregunta);
                            var productosHijas = preguntamodel.PreguntasHijas.Select(h => h.Id);

                            PreguntaModel result = new PreguntaModel();
                            if (esPrecio)
                            {
                                result = precios._models.FirstOrDefault(q => preguntasHijas.Contains(q.IdPregunta) && productosHijas.Contains(q.Id));
                            }
                            else
                            {
                                result = question._models.FirstOrDefault(q => preguntasHijas.Contains(q.IdPregunta));
                            }
                            if (result != null)
                            {
                                //Eliminar toda la rama de  la pregunta padre.
                                int postpreguntapadre = preguntaRamaModel(preguntamodel);

                                //quitamos el rango de preguntas hijas asignadas despues del indice del modelo
                                if (esPrecio)
                                {
                                    precios._models.RemoveRange(indexofmodel + 1, postpreguntapadre);
                                }
                                else
                                {
                                    question._models.RemoveRange(indexofmodel + 1, postpreguntapadre);
                                }
                                //volvemos a renderear la vista
                                var variable = picker.Parent as StackLayout;

                                var sKill = variable.Children.FirstOrDefault(x => x.GetType() == typeof(StackLayout));
                                if (sKill != null)
                                {
                                    variable.Children.Remove(sKill);
                                }
                            }
                        }
                    }
                    else // no hay respuesta en la base de datos
                    {
                        //preguntamos si se ha desencadenado el modelo
                        if (preguntamodel.Desencadenada)
                        {
                            //cambiamos el valor del desencadenamiento
                            preguntamodel.Desencadenada = false;
                            //iteramos las preguntas hijas en busqueda de pickers que desencadenen más preguntas
                            foreach (var preguntahija in preguntamodel.PreguntasHijas)
                            {
                                //preguntamos si la pregunta hija se ha desencadenado
                                if (preguntahija.Desencadenada)
                                {
                                    //si se ha desencadenado, obtenemos la vista de la pregunta hija[picker]
                                    var pickerhija = preguntahija.View as Picker;
                                    //y la asignamos a 0 para que lanze este evento, y borre las preguntas hijas que tiene del modelo
                                    pickerhija.SelectedIndex = 0;
                                }
                            }
                            //quitamos el rango de preguntas hijas asignadas despues del indice del modelo
                            if (esPrecio)
                            {
                                precios._models.RemoveRange(indexofmodel + 1, preguntamodel.PreguntasHijas.Count);
                            }
                            else
                            {
                                question._models.RemoveRange(indexofmodel + 1, preguntamodel.PreguntasHijas.Count);
                            }
                            //volvemos a renderear la vista
                            var variable = picker.Parent as StackLayout;

                            var sKill = variable.Children.FirstOrDefault(x => x.GetType() == typeof(StackLayout));
                            variable.Children.Remove(sKill);
                        }
                    }
                }
                else // no hay respuesta en la base de datos
                {
                    //preguntamos si se ha desencadenado el modelo
                    if (preguntamodel.Desencadenada)
                    {
                        //cambiamos el valor del desencadenamiento
                        preguntamodel.Desencadenada = false;
                        //iteramos las preguntas hijas en busqueda de pickers que desencadenen más preguntas
                        foreach (var preguntahija in preguntamodel.PreguntasHijas)
                        {
                            //preguntamos si la pregunta hija se ha desencadenado
                            if (preguntahija.Desencadenada)
                            {
                                //si se ha desencadenado, obtenemos la vista de la pregunta hija[picker]
                                var pickerhija = preguntahija.View as Picker;
                                //y la asignamos a 0 para que lanze este evento, y borre las preguntas hijas que tiene del modelo
                                if (pickerhija != null)
                                {
                                    pickerhija.SelectedIndex = 0;
                                }
                            }
                        }

                        var preguntasHijas = preguntamodel.PreguntasHijas.Select(h => h.IdPregunta);

                        PreguntaModel result = new PreguntaModel();
                        if (esPrecio)
                        {
                            result = precios._models.FirstOrDefault(q => preguntasHijas.Contains(q.IdPregunta));
                        }
                        else
                        {
                            result = question._models.FirstOrDefault(q => preguntasHijas.Contains(q.IdPregunta));
                        }

                        if (result != null)
                        {
                            //quitamos el rango de preguntas hijas asignadas despues del indice del modelo
                            if (esPrecio)
                            {
                                precios._models.RemoveRange(indexofmodel + 1, preguntamodel.PreguntasHijas.Count);
                            }
                            else
                            {
                                question._models.RemoveRange(indexofmodel + 1, preguntamodel.PreguntasHijas.Count);
                            }

                            //volvemos a renderear la vista
                            var variable = picker.Parent as StackLayout;

                            var sKill = variable.Children.FirstOrDefault(x => x.GetType() == typeof(StackLayout));
                            variable.Children.Remove(sKill);
                        }
                    }
                }
            }
        }

        private void Sview_OnChanged(object sender, ToggledEventArgs e)
        {
            // obtenemos el switch
            var switchcell = sender as SwitchCell;
            // convertimos el id unico en un idrespuesta
            int idrespuesta = int.Parse(switchcell.ClassId);
            // buscamos la respuesta en la base de datos a traves del idrespuesta
            var respuesta = App.DB.Respuesta.FirstOrDefault(r => r.IdRespuesta == idrespuesta);
            // buscamos el modelo a traves del idpregunta de la respuesta

            //var model = question._models.FirstOrDefault(m => m.IdPregunta == respuesta.IdPregunta);
            if (this.precios != null)
            {
                model = findPreguntaModel(this.precios._models, respuesta.IdPregunta, true, 0);
            }
            else
            {
                model = findPreguntaModel(this.question._models, respuesta.IdPregunta, false, 0);
            }

            // asignamos el valor a la respuesta
            respuesta.Valor = e.Value ? 1 : 0;
            // si el modelo tiene respuestas
            if (model.Respuestas != null)
            {
                // buscamos la respuesta en el modelo
                var respuestaenmodel = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                // si existe la respuesta
                if (respuestaenmodel != null)
                {
                    // quitamos la respuesta para evitar duplicados
                    model.Respuestas.Remove(respuestaenmodel);
                }
                // agregamos la respuesta
                model.Respuestas.Add(respuesta);
            }
            else
            {
                // agregamos la respuesta
                model.Respuestas = new List<Respuesta> { respuesta };
            }
        }

        #endregion

        #region Busquedas

        public static PreguntaModel findPreguntaModel(List<PreguntaModel> preguntaModel, int hashcode, bool esprecio, int id)
        {
            PreguntaModel model = new PreguntaModel();
            foreach (var item in preguntaModel)
            {
                if (esprecio)
                {
                    if (item.IdPregunta == hashcode && item.Id == id)
                    {
                        model = item;
                        break;
                    }
                    else
                    {
                        model = findPreguntaHijaModel(item, hashcode, esprecio, id);
                        if (model.IdRespuesta != 0)
                            break;
                    }
                }
                else
                {
                    if (item.IdPregunta == hashcode)
                    {
                        model = item;
                        break;
                    }
                    else
                    {
                        model = findPreguntaHijaModel(item, hashcode, esprecio, id);
                        if (model.IdRespuesta != 0)
                            break;
                    }
                }
            }

            return model;
        }

        public static PreguntaModel findPreguntaHijaModel(PreguntaModel preguntaModel, int hashcode, bool esprecio, int id)
        {
            PreguntaModel model = new PreguntaModel();
            foreach (var item in preguntaModel.PreguntasHijas)
            {
                if (esprecio)
                {
                    if (item.IdPregunta == hashcode && item.Id == id)
                    {
                        model = item;
                        break;
                    }
                    else
                    {
                        model = findPreguntaHijaModel(item, hashcode, esprecio, id);
                        if (model.IdRespuesta != 0)
                            break;
                    }
                }
                else
                {
                    if (item.IdPregunta == hashcode)
                    {
                        model = item;
                        break;
                    }
                    else
                    {
                        model = findPreguntaHijaModel(item, hashcode, esprecio, id);
                        if (model.IdRespuesta != 0)
                            break;
                    }
                }
            }
            return model;
        }


        //Obtiene el numero de preguntas de la rama de una pregunta padre
        public static int preguntaRamaModel(PreguntaModel preguntaModel)
        {
            int numeropregunta = preguntaModel.PreguntasHijas.Count();

            foreach (var item in preguntaModel.PreguntasHijas)
            {
                var respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.TextoOpcion == item.Respuesta && r.IdPregunta == item.IdPregunta);
                if (respuestaindb != null)
                {
                    if (respuestaindb.Desencadena)
                        numeropregunta += preguntaRamaModel(item);
                }
            }
            return numeropregunta;
        }

        #endregion

        public byte[] Foto
        {
            get
            {
                if (model.File != null)
                {
                    try { return model.File.Photo.GetStream().ToByteArray(); } catch { }
                }
                return null;
            }
        }

        public string FotoNombre
        {
            get
            {
                if (model.File != null)
                {
                    return model.File.Name;
                }
                return null;
            }
        }

        public int? IdRespuestaAuditor { get; set; }

    }
}
