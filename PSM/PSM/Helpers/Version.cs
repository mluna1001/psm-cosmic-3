﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Helpers
{
    public class Version
    {
        private const string _NumeroVersion = "3.6.1";

        public string NumeroVersion
        {
            get { return _NumeroVersion; }
        }
    }
}
