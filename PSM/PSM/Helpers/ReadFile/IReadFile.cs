﻿using PSM.ORM;

namespace PSM.Helpers.ReadFile
{
    public interface IReadFile
    {
        RespaldoBDUsuarioDto ReadFile(string nombreArchivo);
    }
}