﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.Helpers.Services
{
    public class CheckAutomaticTime
    {
        public static bool IsAutomaticTime()
        {
            var automatic = DependencyService.Get<ICheckAutomaticTime>();
            bool bandera = automatic.IsAutomaticTime();
            return bandera;
        }

        public static bool IsAutomaticZone()
        {
            var automatic = DependencyService.Get<ICheckAutomaticTime>();
            bool bandera = automatic.IsAutomaticZone();
            return bandera;
        }
    }
}
