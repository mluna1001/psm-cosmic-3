﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.Helpers.Services
{
    public class StorageDevice
    {
        public static string InfoStorageDevice()
        {
            var storage = DependencyService.Get<IStorageDevice>();
            return storage.GetFullStorageDevive();
        }

        public static string InfoBatteryLevel()
        {
            var battery = DependencyService.Get<IStorageDevice>();
            return battery.GetBatteryLevel();
        }
    }
}
