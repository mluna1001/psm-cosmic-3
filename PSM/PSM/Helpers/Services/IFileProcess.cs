﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Helpers.Services
{
    public interface IFileProcess
    {
        JsonSerializer GetTextWriter(object objeto);
    }
}
