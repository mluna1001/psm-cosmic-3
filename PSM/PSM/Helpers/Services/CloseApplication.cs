﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.Helpers.Services
{
    public class CloseApplication
    {
        public void CloseApp()
        {
            var close = DependencyService.Get<ICloseApplication>();
            close?.CloseApp();
        }
    }
}
