﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Helpers.Services
{
    public interface ICheckAutomaticTime
    {
        bool IsAutomaticTime();

        bool IsAutomaticZone();
    }
}
