﻿namespace PSM.Helpers.Services
{
    public interface IStorageDevice
    {
        string GetFullStorageDevive();

        string GetBatteryLevel();
    }
}
