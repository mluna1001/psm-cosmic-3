﻿using Microsoft.AppCenter.Crashes;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using PSM.DeviceSensors;
using PSM.Function;
using PSM.Helpers;
using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM
{
    public partial class ModulosPage : ContentPage
    {
        #region variables de pagina

        private double longitud, latitud;
        private DateTimeOffset? FechaGps { get; set; }
        public ObservableCollection<Seccion> Modulos { get; set; }
        public Position Position { get; set; }
        private bool Sincronized { get; set; }
        private bool Extra { get; set; }
        #endregion variables de pagina

        public ModulosPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            ListaDeModulo.IsEnabled = true;
            Modulos = GetModuloList();
            ListaDeModulo.ItemsSource = Modulos;

#if DEBUG
            //StackList.IsVisible = true;
            //StackProgress.IsVisible = false;

            StackList.IsVisible = false;
            StackProgress.IsVisible = true;
#else
            if (Modulos.Any(m => m.IdCatalogoSeccion == 8))
            {
                StackList.IsVisible = false;
                StackProgress.IsVisible = true;
            }
            else
            {
                StackList.IsVisible = true;
                StackProgress.IsVisible = false;
            }
#endif
            Localizacion();

        }

        private async void Localizacion()
        {
            var location = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
            switch (location[Permission.Location])
            {
                case PermissionStatus.Denied:
                    await DisplayAlert("PSM 360", "Debes permitir el uso del GPS", "Aceptar");
                    break;

                case PermissionStatus.Granted:
                    break;

                case PermissionStatus.Unknown:
                case PermissionStatus.Disabled:
                case PermissionStatus.Restricted:
                default:
                    await DisplayAlert("PSM 360", "Debes permitir el uso del GPS", "Aceptar");
                    break;
            }

            if (CrossGeolocator.Current.IsGeolocationAvailable)
            {
                if (CrossGeolocator.Current.IsGeolocationEnabled)
                {
                    try
                    {
                        Position = await CrossGeolocator.Current.GetPositionAsync();
                        var dategps = Position.Timestamp;
                        FechaGps = dategps.Year < DateTime.Now.Year ? DateTime.Now : dategps;
                        longitud = Position.Longitude;
                        latitud = Position.Latitude;
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            StackList.IsVisible = true;
                            StackProgress.IsVisible = false;
                        });
                    }
                    catch { }
                }
                else
                {
                    await DisplayAlert("PSM 360", "No tienes iniciado el GPS, por favor activalo", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoGpsIniciado, "Constructor", "ModulosPage", "GPS no iniciado");
                }
            }
            else
            {
                await DisplayAlert("PSM 360", "Debes permitir el uso del GPS para poder usar esta app", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SinPermisosGps, "Constructor", "ModulosPage", "GPS sin permisos");
            }

            if (Position == null)
            {
                if (!CrossGeolocator.Current.IsListening)
                {
                    try
                    {
                        CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
                        if (await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 0))
                        {
                        }
                        else
                        {
                            await DisplayAlert("PSM 360", "No se pudo iniciar el GPS...", "Aceptar");
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoGpsIniciado, "Constructor", "ModulosPage", "GPS no iniciado");
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void Current_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        {
            Position = e.Position;
            Device.BeginInvokeOnMainThread(() =>
            {
                ListaDeModulo.IsEnabled = true;
                StackList.IsVisible = true;
                StackProgress.IsVisible = false;
            });
            var dategps = e.Position.Timestamp;
            FechaGps = dategps.Year < DateTime.Now.Year ? DateTime.Now : dategps;
            longitud = e.Position.Longitude;
            latitud = e.Position.Latitude;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RecallUbicacionGps, "GPS_PositionChange", "ModulosPage", e);
            CrossGeolocator.Current.PositionChanged -= Current_PositionChanged;
        }

        #region eventos de xaml

        private void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            ListaDeModulo.SelectedItem = null;
        }

        private async void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            ValidateDevice.Validate("Handle_ItemTapped", "ModulosPage");
            var rutaM = App.DB.Ruta.Where(x => x.IdRuta == App.CurrentRoute.IdRuta && x.Sincronizado == false).FirstOrDefault();
            var tienda = App.DB.InfoTienda.FirstOrDefault(it => it.IdTienda == App.CurrentRoute.IdTienda);
            var findProyecto = App.DB.InfoTienda.FirstOrDefault(es => es.EstatusCheck != null);
            bool typeProyecto = false;
            if (findProyecto != null)
                typeProyecto = true;

            var item = e.Item as Seccion;
            string message = "";
            if (item != null)
            {
                ListaDeModulo.IsEnabled = false;
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "Handle_ItemTapped", "ModulosPage", item);

                if (item.IdCatalogoSeccion == 8) // entrada
                {
                    bool answ = false;
                    if (!tienda.Verificada)
                    {
                        answ = await DisplayAlert("PSM 360 ¡Atención!", "Esta seguro de realizar la entrada para la tienda: " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Aceptar", "Cancelar");
                    }

                    if (tienda.Verificada)
                        answ = true;

                    if (answ)
                    {
                        //Et Bandera para verificar si la entrada es del tipo normal o global
                        bool et = false;
                        //Verifica si el proyecto es del tipo entrada global/salida globar
                        if (tienda.EstatusCheck != null)
                        {
                            et = true;
                            if (typeProyecto && (bool)tienda.EstatusCheck)
                            {
                                if (!verificaSalidaGlobal())
                                {
                                    await DisplayAlert("PSM 360 ¡Atencion!", "Debes cerrar la actividad inicial", "Aceptar");
                                    ListaDeModulo.IsEnabled = true;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (typeProyecto)
                            {
                                if (!verificaSalidaGlobal())
                                {
                                    await DisplayAlert("PSM 360 ¡Atencion!", "Debes cerrar la actividad inicial", "Aceptar");
                                    ListaDeModulo.IsEnabled = true;
                                    return;
                                }
                            }
                        }

                        //Realiza la entrada true Entrada Global, false Entrada normal por tienda
                        //et = Entrada(et).Result;
                        et = await Entrada(et);

                        if (et)
                        {
                            message = this.Sincronized ? "Sus datos han sido sincronizados." : (this.Extra) ? " Esta es una ruta externa. Al terminar recuerda subir toda la información." : "";
                            await DisplayAlert("PSM 360", "Se ha realizado una entrada. " + message, "Aceptar");
                            Modulos.Remove(item);
                        }
                        else
                        {
                            await DisplayAlert("PSM 360", "No te encuentras cerca de la tienda para realizar entrada. " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Aceptar");
                            ListaDeModulo.IsEnabled = true;
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaLejosTienda, "Entrada", "ModulosPage", $"Intentó realizar entrada en la tienda: { rutaM.TiendaNombre} pero se encuentra lejos de ella");
                            return;
                        }
                        ListaDeModulo.IsEnabled = true;
                    }
                    ListaDeModulo.IsEnabled = true;
                }
                else if (item.IdCatalogoSeccion == 14) // salida
                {
                    bool answ = await DisplayAlert("PSM 360 ¡Atención!", "Esta seguro de realizar una salida para la tienda: " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Aceptar", "Cancelar");
                    //Se verifica primero que exista la entrada en la tienda
                    if (answ)
                    {
                        if (ExisteEntrada())
                        {
                            if (latitud != 0 || longitud != 0)
                            {
                                // Se verifica que la distancia donde se realizará la salida está entre los 500 mts a la redonda
                                bool cp = CumpleDistancia();
                                bool typeSalida = false;
                                //Si la cadena es tipo actividades no vamos a verificar distancia ya que es un checkin checkout global por dia
                                if (tienda.EstatusCheck != null)
                                {
                                    typeSalida = true;
                                    cp = true;
                                }

                                if (cp)
                                {
                                    if (await Salida(typeSalida))
                                    {
                                        message = this.Sincronized ? "Sus datos han sido sincronizados." : this.Extra ? "Recuerda subir la información de la ruta." : "";
                                        //await DisplayAlert("PSM 360", "Se ha realizado una salida." + message, "Aceptar");
                                        Modulos.Remove(item);
                                        // Validamos que existan solamente el módulo de entrada y salida.
                                        if (ValidaModuloEntradaSalida())
                                        {
                                            //Si existe, se actualiza la ruta terminada
                                            var uploadRouteConnected = App.DB.Ruta.Where(x => x.IdRuta == App.CurrentRoute.IdRuta).FirstOrDefault();
                                            // Se verifica que haya internet, si lo hay sube la tienda directo al server
                                            if (!await UploadData.Upload(uploadRouteConnected, false))
                                            {
                                                message = "No tienes conexión a internet. Se ha guardado tu salida en este dispositivo";
                                            }
                                        }
                                        await DisplayAlert("PSM 360", "Se ha realizado una salida." + message, "Aceptar");
                                        try { await Navigation.PopAsync(); } catch { }
                                    }
                                }
                                else
                                {
                                    if (tienda.Verificada)
                                    {
                                        await DisplayAlert("PSM 360", "No se puede realizar la salida debido a que estás lejos de la tienda." + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Aceptar");
                                    }
                                    else
                                    {
                                        await DisplayAlert("PSM 360", "No se puede realizar la salida debido a que estás lejos de tu entrada." + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Aceptar");
                                    }
                                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaLejosTienda, "Salida", "ModulosPage", $"Intentó realizar salida en la tienda: { rutaM.TiendaNombre} pero se encuentra lejos de ella");
                                }
                            }
                            else
                            {
                                await DisplayAlert("PSM 360", "No hemos encontrado aún tu ubicación.", "Aceptar");
#if DEBUG
                                StackList.IsVisible = true;
                                StackProgress.IsVisible = false;

                                latitud = 0.000001;
                                longitud = 0.000001;

#else
                                StackList.IsVisible = false;
                                StackProgress.IsVisible = true;
#endif
                                Localizacion();
                            }
                        }
                        else
                        {
                            await DisplayAlert("PSM 360", "No se ha realizado una entrada en esta tienda.", "Aceptar");
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaSinEntrada, "Salida", "ModulosPage", $"Intentó realizar salida en la tienda: { rutaM.TiendaNombre} pero no hay una entrada previa");
                        }
                    }
                    ListaDeModulo.IsEnabled = true;
                }
                else // otro modulo
                {
                    Asistencia currentasistencia;
                    bool et = false;
                    if (tienda.EstatusCheck != null)
                        et = true;

                    if (et)
                    {
                        //Verifica si tiene asistencia de entrada para la ruta seleccionada
                        currentasistencia = App.DB.Asistencia.FirstOrDefault(j => j.IdRuta == App.CurrentRoute.IdRuta && j.descripcion == App.MODULO_ENTRADAGLOB);
                    }
                    else
                    {
                        //Verifica si tiene asistencia de entrada para la ruta seleccionada
                        currentasistencia = App.DB.Asistencia.FirstOrDefault(j => j.IdRuta == App.CurrentRoute.IdRuta && j.descripcion == App.MODULO_ENTRADA);
                    }

                    //verifica que la tienda seleccionada no este verificada
                    tienda = App.DB.InfoTienda.FirstOrDefault(it => it.IdTienda == App.CurrentRoute.IdTienda);
                    bool answ = false;

                    if (currentasistencia == null && !tienda.Verificada)
                    {
                        answ = await DisplayAlert("PSM 360 ¡Atención!", "Esta seguro de realizar la entrada para la tienda: " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Aceptar", "Cancelar");
                    }

                    if (tienda.Verificada || currentasistencia != null)
                        answ = true;

                    if (answ)
                    {
                        //Verifica si el proyecto es del tipo entrada global/salida global
                        if (tienda.EstatusCheck != null)
                        {
                            et = true;
                            if (typeProyecto && (bool)tienda.EstatusCheck)
                            {
                                if (!verificaSalidaGlobal())
                                {
                                    await DisplayAlert("PSM 360 ¡Atencion!", "Debes cerrar la actividad inicial", "Aceptar");
                                    ListaDeModulo.IsEnabled = true;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (typeProyecto)
                            {
                                if (!verificaSalidaGlobal())
                                {
                                    await DisplayAlert("PSM 360 ¡Atencion!", "Debes cerrar la actividad inicial", "Aceptar");
                                    ListaDeModulo.IsEnabled = true;
                                    return;
                                }
                            }
                        }


                        //Realiza la entrada true Entrada Global, false Entrada normal por tienda
                        et = await Entrada(et);

                        //et = Entrada(et).Result;

                        if (et)
                        {
                            var entrada = Modulos.FirstOrDefault(m => m.IdCatalogoSeccion == 8);
                            Modulos.Remove(entrada);
                        }
                        else
                        {
                            //await DisplayAlert("PSM 360", "No te encuentras cerca de la tienda para realizar entrada.", "Aceptar");
                            if (tienda.Verificada)
                            {
                                await DisplayAlert("PSM 360", "No te encuentras cerca de la tienda para realizar entrada. " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Aceptar");
                            }
                            else
                            {
                                await DisplayAlert("PSM 360", "No te encuentras cerca de la tienda para realizar entrada. " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Aceptar");
                            }
                            ListaDeModulo.IsEnabled = true;
                            return;
                        }

                        Type page = Type.GetType($"PSM.{item.TargetType},PSM");
                        if (item.TargetType == "SeccionesPage")
                        {
                            try
                            {
                                await Navigation.PushAsync((Page)Activator.CreateInstance(page, false), true);
                            }
                            catch
                            {
                                await DisplayAlert("PSM 360", "No podemos mostrar esta seccion, contacta con tu supervisor", "Aceptar");

                            }
                        }
                        else if (item.TargetType.Equals("IndicadorPage"))
                        {
                            try
                            {
                                IndicadorPage pageInd = new IndicadorPage(App.CurrentRoute.IdTienda);
                                await Navigation.PushAsync(pageInd);
                            }
                            catch (Exception ex)
                            {
                                await DisplayAlert("PSM 360", "No podemos mostrar esta seccion, contacta con tu supervisor", "Aceptar");
                            }
                        }
                        else
                        {
                            if (item.IdCatalogoSeccion == 17 || item.IdCatalogoSeccion == 11)
                            {
                                App.CurrentSection = item;
                            }

                            var question = (Page)Activator.CreateInstance(page);
                            if (item.IdCatalogoSeccion == 11)
                            {
                                question.Title = App.CurrentSection.Descripcion;
                                var questionpage = question as QuestionPage;
                                if (questionpage != null)
                                {
                                    questionpage.InicidenciaCompleta += Questionpage_InicidenciaCompleta;
                                }
                            }
                            try
                            {
                                await Navigation.PushAsync(question, true);
                            }
                            catch
                            {
                                await DisplayAlert("PSM 360", "No podemos mostrar esta seccion, contacta con tu supervisor", "Aceptar");

                            }
                        }

                    }
                    ListaDeModulo.IsEnabled = true;

                }
                var resp = await Bitacora.Upload();
            }
        }

        private bool CumpleDistancia(double value = 350)
        {
            double dist = 0, latitudAsis = 0, longitudAsis = 0;

            // Se obtienen los datos de la ruta actual para obtener información de la coordenada
            var RutaAct = App.DB.Ruta.Where(x => x.IdRuta == App.CurrentRoute.IdRuta).FirstOrDefault();
            var asistencia = App.DB.Asistencia.FirstOrDefault(a => a.IdRuta == RutaAct.IdRuta && a.estatus == true);

            if (asistencia != null)
            {
                latitudAsis = (double)asistencia.Latitud;
                longitudAsis = (double)asistencia.Longitud;
            }
            else
            {
                InfoTienda infoTienda = App.DB.InfoTienda.FirstOrDefault(t => t.IdTienda == App.CurrentRoute.IdTienda);
                if (infoTienda != null)
                {
                    latitudAsis = (double)infoTienda.Latitud;
                    longitudAsis = (double)infoTienda.Longitud;
                }
            }

            // Se calcula las variables de distancia
            decimal theta = (decimal)longitud - (decimal)longitudAsis;
            double distancia = (Math.Sin(DistanceHelper.Radianes(latitud)) * Math.Sin(DistanceHelper.Radianes(latitudAsis)))
                    + Math.Cos(DistanceHelper.Radianes(latitud)) * Math.Cos(DistanceHelper.Radianes(latitudAsis)) * Math.Cos(DistanceHelper.Radianes((double)theta));

            //var a = Math.Sin(difLatitud / 2).AlCuadrado() + Math.Cos(posOrigen.Latitud.EnRadianes()) * Math.Cos(posDestino.Latitud.EnRadianes()) * Math.Sin(difLongitud / 2).AlCuadrado();

            // se obtiene la distancia en KM
            dist = (DistanceHelper.Grados(Math.Acos(distancia))) * 60 * 1.1515 * 1.609344;

            // se convierte la distancia obtenida a metros
            dist = dist * 1000;

            if (dist <= value)
            {
                return true;
            }

            return false;
        }

        private bool ExisteEntrada()
        {
            var RutaAct = App.DB.Ruta.Where(x => x.IdRuta == App.CurrentRoute.IdRuta).FirstOrDefault();
            var asistencia = App.DB.Asistencia.FirstOrDefault(a => a.IdRuta == RutaAct.IdRuta && a.estatus == true);

            if (asistencia != null)
            {
                return true;
            }
            return false;
        }

        private bool ValidaModuloEntradaSalida()
        {
            var modulos = App.DB.Seccion.Where(x => x.EsSeccion == false).ToList();

            var entradaexists = modulos.Exists(x => x.IdCatalogoSeccion == 8);
            var salidaexists = modulos.Exists(x => x.IdCatalogoSeccion == 14);

            if ((entradaexists && salidaexists) && modulos.Count == 2)
            {
                return true;
            }

            return false;
        }

        private async void Questionpage_InicidenciaCompleta(object sender, EventArgs e)
        {
            if (await Salida(false, true))
            {
                await DisplayAlert("PSM 360", "Se ha realizado una salida", "Aceptar");
                try { await Navigation.PopAsync(); } catch { }
            }
        }

        #endregion eventos de xaml

        #region Funciones de cada modulo

        private bool verificaSalidaGlobal()
        {
            var idTiempo = App.CurrentRoute.IdTiempo;

            var asistenciaEntrada = App.DB.Asistencia.Where(e => e.descripcion == App.MODULO_ENTRADAGLOB).ToList();
            var ruta = App.DB.Ruta.Where(s => s.IdTiempo == idTiempo).ToList();

            var asistenciadiaEntrada = (from a in asistenciaEntrada
                                        join r in ruta on a.IdRuta equals r.IdRuta
                                        select a).ToList();

            var asistenciaSalida = App.DB.Asistencia.Where(e => e.descripcion == App.MODULO_SALIDAGLOB).ToList();
            var asistenciadiaSalida = (from a in asistenciaSalida
                                       join r in ruta on a.IdRuta equals r.IdRuta
                                       select a).ToList();

            //Valida que se realizo entrada y salida del check por dia
            if (asistenciadiaEntrada.Count() != 0 && asistenciadiaSalida.Count() != 0)
            {
                return true;
            }

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.VerificaEntradaSalidaGlobal, "EntradaSalidaGlobal", "ModulosPage");

            return false;
        }

        public async Task<bool> Entrada(bool isTypeEntrada)
        {
            bool isconnected = true;
            Asistencia currentasistencia;
            string descripcion = "";

            int IdRuta = App.CurrentRoute.IdRuta;

            if (isTypeEntrada)
            {
                currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == IdRuta && e.descripcion == App.MODULO_ENTRADAGLOB);
                descripcion = App.MODULO_ENTRADAGLOB;
            }
            else
            {
                currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == IdRuta && e.descripcion == App.MODULO_ENTRADA);
                descripcion = App.MODULO_ENTRADA;
            }

            if (currentasistencia != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoEntrada, "Entrada", "ModulosPage", currentasistencia);
                return true;
            }
            else
            {
                var tienda = App.DB.InfoTienda.FirstOrDefault(it => it.IdTienda == App.CurrentRoute.IdTienda);

                if (!isTypeEntrada)
                {
                    if (tienda.Verificada)
                    {
                        if (!CumpleDistancia(350))
                        {
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaLejosTienda, "Entrada", "ModulosPage", "No se encuentra cerca de 350 mts. de la tienda");
                            return false;
                        }
                    }
                }


                Asistencia asistencia = new Asistencia()
                {
                    IdRuta = IdRuta,
                    IdUsuario = App.CurrentUser.IdUsuario,
                    Fecha = FechaGps != null ? DateTime.Parse(FechaGps.ToString()) : DateTime.Now,
                    Longitud = (decimal)longitud,
                    Latitud = (decimal)latitud,
                    estatus = true,
                    descripcion = descripcion
                };
                App.DB.Asistencia.Add(asistencia);
                App.DB.SaveChanges();

                var currentroute = App.DB.Ruta.FirstOrDefault(e => e.IdRuta == IdRuta);
                currentroute.FechaInicio = DateTime.Now;
                currentroute.Status = 1;
                currentroute.GpsLatitude = (decimal)latitud;
                currentroute.GpsLongitude = (decimal)longitud;

                App.DB.Entry(currentroute).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                App.DB.SaveChanges();

                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoEntrada, "Entrada", "ModulosPage", asistencia);

                if (App.CurrentRoute.IdRuta > 0)
                {
                    //verifica que va a iniciar la sincronizacion

                    var response = await App.API.UploadEndPoint.UploadEntry(asistencia, () =>
                        {
                            isconnected = false;
                        });

                    if (response != null)
                    {
                        if (response.Status)
                        {
                            if (isconnected)
                            {
                                var bitacoraSinc = App.DB.BitacoraSincronizacion.FirstOrDefault(a => a.IdRuta == IdRuta);

                                if (bitacoraSinc == null)
                                {
                                    bitacoraSinc = new BitacoraSincronizacion();
                                    bitacoraSinc.IdRuta = IdRuta;
                                    App.DB.Add(bitacoraSinc);
                                }

                                bitacoraSinc.Entrada = true;

                                App.DB.Entry(bitacoraSinc).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;

                                asistencia.Sincronizado = true;
                                App.DB.Entry(asistencia).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                                App.DB.SaveChanges();
                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaEnServidor, "Entrada", "ModulosPage", "La entrada ha sido subida al servidor");
                            }
                        }
                    }

                    if (!isconnected)
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaGuardadaLocalmente, "Entrada", "ModulosPage", "La entrada no se pudo subir al servidor, se guardó en base de datos");
                        this.Sincronized = false;
                        this.Extra = false;
                    }
                    else
                    {
                        this.Sincronized = true;
                        this.Extra = false;
                    }
                }
                else
                {
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaExternaGuardadaLocalmente, "Entrada", "ModulosPage", "La entrada se guardó en base de datos, es externa");
                    this.Sincronized = false;
                    this.Extra = true;
                }

                App.AllSections = new List<Seccion>();
            }

            return true;
        }

        public async Task<bool> Salida(bool isTypeSalida, bool salidaporinicidencia = false)
        {
            CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
            bool isconnected = true;
            int IdRuta = App.CurrentRoute.IdRuta;

            Asistencia currentasistencia;
            string descripcion = "";

            if (isTypeSalida)
            {
                currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == IdRuta && e.descripcion == App.MODULO_SALIDAGLOB);
                descripcion = App.MODULO_SALIDAGLOB;
            }
            else
            {
                currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == IdRuta && e.descripcion == App.MODULO_SALIDA);
                descripcion = App.MODULO_SALIDA;
            }


            if (currentasistencia != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoSalida, "Salida", "ModulosPage", currentasistencia);
                return true;
            }

            if (salidaporinicidencia)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoSalidaIncidencia, "Salida", "ModulosPage", "Salida con incidencia");
            }
            else
            {
                var secciones = App.DB.Seccion.Where(e => e.EsActiva && e.EsSeccion).ToList();

                var lstCad = LlenaSeccionCadena(secciones);
                var lstAllSection = LlenaSeccionRegion(lstCad);

                //var seccionesid = secciones.Where(e => e.SeValida).Select(e => e.IdSeccion).ToList();

                var seccionesid = lstAllSection.Where(e => e.SeValida).Select(e => e.IdSeccion).ToList();

                foreach (var id in seccionesid)
                {
                    if (!App.DB.RespuestaAuditor.Exists(ra => ra.IdSeccion == id && ra.IdRuta == IdRuta))
                    {
                        await DisplayAlert("PSM 360", "Te hacen falta secciones por llenar", "Aceptar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaFaltanSecciones, "Salida", "ModulosPage", "Salida sin incidencia, te hacen falta secciones por llenar");
                        return false;
                    }
                }
            }

            Asistencia asistencia = new Asistencia()
            {
                IdRuta = App.CurrentRoute.IdRuta,
                IdUsuario = App.CurrentUser.IdUsuario,
                //Fecha = FechaGps != null ? DateTime.Parse(FechaGps.ToString()) : DateTime.Now,
                Fecha = DateTime.Now,
                Longitud = (decimal)longitud,
                Latitud = (decimal)latitud,
                estatus = false,
                descripcion = descripcion
            };

            App.DB.Asistencia.Add(asistencia);
            App.DB.SaveChanges();

            var currentroute = App.DB.Ruta.FirstOrDefault(e => e.IdRuta == IdRuta);
            currentroute.FechaTerminacion = DateTime.Now;
            if (salidaporinicidencia)
            {
                currentroute.Status = 3;
            }
            else
            {
                currentroute.Status = 2;
            }

            App.DB.Entry(currentroute).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
            App.DB.SaveChanges();
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoSalida, "Salida", "ModulosPage", asistencia);

            if (App.CurrentRoute.IdRuta > 0)
            {
                var response = await App.API.UploadEndPoint.UploadEntry(asistencia, () =>
                    {
                        isconnected = false;
                    });

                if (response != null)
                {
                    if (response.Status)
                    {
                        if (isconnected)
                        {
                            var bitacoraSinc = App.DB.BitacoraSincronizacion.FirstOrDefault(a => a.IdRuta == IdRuta);

                            if (bitacoraSinc == null)
                            {
                                bitacoraSinc = new BitacoraSincronizacion();
                                bitacoraSinc.IdRuta = IdRuta;
                                App.DB.Add(bitacoraSinc);

                            }

                            bitacoraSinc.Salida = true;
                            App.DB.Entry(bitacoraSinc).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                            App.DB.SaveChanges();

                            asistencia.Sincronizado = true;
                            App.DB.Entry(asistencia).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                            App.DB.SaveChanges();
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaEnServidor, "Salida", "ModulosPage", "La salida ha sido subida al servidor");
                        }
                    }
                }

                if (!isconnected)
                {
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaGuardadaLocalmenta, "Salida", "ModulosPage", "La salida no se pudo subir al servidor, se guardó en base de datos. Sube esta tienda cuando tengas conexión a internet");
                    this.Sincronized = false;
                }
                else
                {
                    this.Sincronized = true;
                }
            }
            else
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaExternaGuadadaLocalmente, "Salida", "ModulosPage", "La salida se guardó en base de datos, es externa");
                this.Sincronized = false;
                this.Extra = true;
            }

            App.AllSections.Clear();

            return true;
        }

        private List<Seccion> LlenaSeccionCadena(List<Seccion> items)
        {
            //App.AllSections.Clear();
            List<Seccion> seccCads = new List<Seccion>();

            try
            {
                foreach (var seccion in items)
                {
                    var secCad = App.DB.SeccionCadena.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion);
                    if (secCad == null)
                    {
                        seccCads.Add(seccion);
                    }
                    else
                    {
                        Ruta r = App.CurrentRoute;
                        InfoTienda tienda = App.DB.InfoTienda.Where(x => x.IdTienda == r.IdTienda).FirstOrDefault();

                        if (secCad.IdCadena == tienda.IdCadena)
                        {
                            seccCads.Add(seccion);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
                seccCads = items;
            }

            return seccCads;


        }

        public List<Seccion> LlenaSeccionRegion(List<Seccion> items)
        {
            List<Seccion> seccRegs = new List<Seccion>();

            try
            {
                foreach (var seccion in items)
                {
                    var secReg = App.DB.SeccionRegion.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion);
                    if (secReg == null)
                    {
                        seccRegs.Add(seccion);
                    }
                    else
                    {
                        Ruta r = App.CurrentRoute;
                        Usuario usuario = App.CurrentUser;

                        if (secReg.IdRegion == usuario.IdRegion)
                        {
                            seccRegs.Add(seccion);
                        }
                    }
                }

                //App.AllSections = temp;
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
                seccRegs = items;
            }

            return seccRegs;
        }

        #endregion Funciones de cada modulo

        #region Lista modulos

        private ObservableCollection<Seccion> GetModuloList()
        {
            ObservableCollection<Seccion> listsecciones = new ObservableCollection<Seccion>();
            var secciones = App.DB.Seccion.ToList();
            List<Seccion> moduloList = secciones.Where(s => !s.EsSeccion && s.EsActiva).OrderBy(s => s.Orden).ToList();
            foreach (var item in moduloList)
            {
                if (item.IdCatalogoSeccion == 8)
                {
                    var currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == App.CurrentRoute.IdRuta && (e.descripcion == App.MODULO_ENTRADA || e.descripcion == App.MODULO_ENTRADAGLOB));
                    if (currentasistencia != null) continue;
                }
                else if (item.IdCatalogoSeccion == 14)
                {
                    var currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == App.CurrentRoute.IdRuta && (e.descripcion == App.MODULO_SALIDA || e.descripcion == App.MODULO_SALIDAGLOB));
                    if (currentasistencia != null) continue;
                }
                else if (item.IdCatalogoSeccion == 12)
                {
                    continue;
                }
                else
                {
                }
                listsecciones.Add(item);
            }
            return listsecciones;
        }

        #endregion Lista modulos

        #region get

        public Asistencia Getentrada(int IdRuta)
        {
            Asistencia asist = new Asistencia();
            asist = App.DB.Asistencia.FirstOrDefault(a => a.IdRuta == IdRuta && a.estatus == true);
            return asist;
        }

        public Asistencia Getsalida(int IdRuta)
        {
            Asistencia asistsal = new Asistencia();
            asistsal = App.DB.Asistencia.FirstOrDefault(a => a.IdRuta == IdRuta && a.estatus == false);
            return asistsal;
        }

        #endregion get
    }
}