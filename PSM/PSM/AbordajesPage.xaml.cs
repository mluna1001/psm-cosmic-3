﻿using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PSM
{
    public partial class AbordajesPage : ContentPage
    {
        public AbordajesPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            var seccionmenu = App.DB.CatalogoSeccionMenu.Where(e => e.IdProyecto == App.CurrentUser.IdProyecto).ToList();
            List<AbordajeModel> modellist = new List<AbordajeModel>();
            foreach (var item in seccionmenu)
            {
                modellist.Add(new AbordajeModel
                {
                    Descripcion = item.Descripcion,
                    Icono = item.Icono,
                    IdProyecto = item.IdProyecto,
                    IdSeccionMenu = item.IdSeccionMenu
                });
            }
            ListAbordajes.ItemsSource = modellist;
            ListAbordajes.ItemSelected += ListAbordajes_ItemSelected;
            ListAbordajes.ItemTapped += ListAbordajes_ItemTapped;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "AbordajesPage");
        }

        protected override void OnAppearing()
        {
            ListAbordajes.IsEnabled = true;
        }

        private async void ListAbordajes_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var seccionmenu = e.Item as AbordajeModel;
            ListAbordajes.IsEnabled = false;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "Se ha seleccionado un abordaje", "AbordajesPage", seccionmenu);
            var multimedialist = App.DB.Multimedia.Where(m => m.IdSeccionMenu == seccionmenu.IdSeccionMenu).ToList();
            await Navigation.PushAsync(new MultimediaList(multimedialist, seccionmenu.IdSeccionMenu));
        }

        private void ListAbordajes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ListAbordajes.SelectedItem = null;
        }
    }
}
