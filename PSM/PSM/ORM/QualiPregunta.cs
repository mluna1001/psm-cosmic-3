﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class QualiPregunta
    {
        [AutoIncrement, PrimaryKey]
        public int IdQualiPregunta { get; set; }
        public int IdRuta { get; set; }
        public int IdPregunta { get; set; }
        public int IdQualiSection { get; set; }
        public int IdSeccion { get; set; }
        public string Pregunta { get; set; }
        public string Respuesta { get; set; }
        public double Quali { get; set; }
    }
}
