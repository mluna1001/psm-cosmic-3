
using SQLite;

namespace PSM.ORM
{
	public class Compania
	{
		[PrimaryKey]
		public int IdCompania { get; set; }

		public int IdSeccion { get; set; }

		public string Nombre { get; set; }

		public int IdCatalogoCompania { get; set; }

		public bool Vigencia { get; set; }
	}
}

