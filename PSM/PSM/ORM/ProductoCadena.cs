﻿using SQLite;

namespace PSM.ORM
{
	public class ProductoCadenaDto
	{
		[PrimaryKey]
		public int IdProductoCadena { get; set; }

		public int IdCadena { get; set; }

		public int IdProducto { get; set; }

		public int IdSeccion { get; set; }

		public int IdProyecto { get; set; }

		public decimal? Precio { get; set; }

		public bool bPrecio { get; set; }

		public string Promocion { get; set; }

		public decimal? PrecioPromo { get; set; }

		public bool bPrecioPromo { get; set; }
	}
}
