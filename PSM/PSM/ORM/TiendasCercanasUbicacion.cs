﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class TiendasCercanasUbicacion
    {
        [JsonProperty("IdTienda")]
        public int IdTienda { get; set; }
        [JsonProperty("IdProyectoTienda")]
        public int IdProyectoTienda { get; set; }
        [JsonProperty("TiendaNombre")]
        public string TiendaNombre { get; set; }
        [JsonProperty("Direccion")]
        public string Direccion { get; set; }
        [JsonProperty("IdCadena")]
        public int IdCadena { get; set; }
        [JsonProperty("CoordenadaX")]
        public Nullable<decimal> CoordenadaX { get; set; }
        [JsonProperty("CoordenadaY")]
        public Nullable<decimal> CoordenadaY { get; set; }
        [JsonProperty("Verificada")]
        public bool Verificada { get; set; }
    }
}
