﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class ProductoClasificacion
    {
        public int IdProductoClasificacion { get; set; }
        public int IdProducto { get; set; }
        public int IdCodigoTienda { get; set; }
        public string Clasificacion { get; set; }
    }
}
