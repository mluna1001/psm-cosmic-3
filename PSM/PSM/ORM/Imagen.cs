﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace PSM.ORM
{
    public class Imagen
    {
        [PrimaryKey]
        public int IdImagen { get; set; }
        public int? IdCadena { get; set; }
        public int IdSeccion { get; set; }
        public string Descripcion { get; set; }
        public string URL { get; set; }
        public string NombreImagen { get; set; }
        public bool Vigente { get; set; }
    }
}
