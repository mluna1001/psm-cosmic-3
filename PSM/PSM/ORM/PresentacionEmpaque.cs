﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class PresentacionEmpaque
    {
        [PrimaryKey]
        public int IdPresentacionEmpaque { get; set; }
        public int IdProducto { get; set; }
        public string Nombre { get; set; }
        public int IdCatalogoPresentacionEmpaque { get; set; }
        public bool Vigencia { get; set; }
        public string CodigoBarras { get; set; }
        public string mg { get; set; }
        public int? Categoria { get; set; }
        public double? RangoPrecio { get; set; }
    }
}
