﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class QualiSection
    {
        [AutoIncrement, PrimaryKey]
        public int IdQualiSection { get; set; }
        public int IdRuta { get; set; }
        public int IdSeccion { get; set; }
        public string Seccion { get; set; }
        public double Quali { get; set; }
        public Nullable<double> Total { get; set; }
    }
}
