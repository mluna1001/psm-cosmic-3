﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class UploadRespAudit
    {
        public bool Key { get; set; }
        public string Value { get; set; }

        public int IdRuta { get; set; }
        public int IdSeccion { get; set; }
    }

    public class UploadRespAuditMult
    {
        public bool Key { get; set; }
        public string Value { get; set; }

        public List<int> IdRuta { get; set; }
        public int IdSeccion { get; set; }
    }
}
