﻿using DevelopersAzteca.Storage.SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public interface IPSMDataBase
    {

        CosmicResponse GetDataBase();

    }
}
