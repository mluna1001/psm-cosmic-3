﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class UploadPhoto
    {
        public bool Key { get; set; }
        public string Value { get; set; }
        public string IdRuta { get; set; }
        public string IdSeccion { get; set; }
    }

}
