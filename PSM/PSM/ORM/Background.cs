﻿using DevelopersAzteca.Storage.SQLite;
using System;
using Xamarin.Forms;

namespace PSM.ORM
{
    public class Background : DataBase
    {
        public Table<Bitacora> Bitacora { get; set; }

        public Background(string databasePath, bool storeDateTimeAsTicks = true) : base(databasePath, storeDateTimeAsTicks)
        {
            Bitacora = DBSet<Bitacora>();
        }

        public void DropTables()
        {
            DropTable<Bitacora>();
        }

        public static Background Instance
        {
            get
            {
                var service = DependencyService.Get<IBackground>();
                if (service == null) throw new NullReferenceException("La dependencia de servicio es null");
                return service.GetDataBase();
            }
        }
    }
}