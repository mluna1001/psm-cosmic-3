﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class Multimedia
    {
        [PrimaryKey]
        public int IdMultimedia { get; set; }
        public string Nombre { get; set; }
        public int IdSeccionMenu { get; set; }
        public bool Vigencia { get; set; }
        public int Categoria { get; set; }
        public string url { get; set; }

        public string GetIcon()
        {
            var separatename = Nombre.Split('.');
            var extension = separatename[separatename.Length - 1];
            switch (extension)
            {
                case "jpg":
                    return "pngicon.png";

                case "pdf":
                    return "pdficon.png";

                case "mp4":
                    return "mpgicon.png";

                default:
                    return "fileunknownicon.png";
            }
        }
    }
}
