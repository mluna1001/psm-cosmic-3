﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class SeccionRegion
    {
        public int IdSeccionRegion { get; set; }
        public int IdSeccion { get; set; }
        public int IdRegion { get; set; }
        public int IdProyecto { get; set; }
    }
}
