﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class TiendasCercanasRuta
    {
        [PrimaryKey]
        public int IdTienda { get; set; }
        public int IdProyectoTienda { get; set; }
        public int IdCadena { get; set; }
        public string TiendaNombre { get; set; }
        public string Direccion { get; set; }
        public Nullable<decimal> CoordenadaX { get; set; }
        public Nullable<decimal> CoordenadaY { get; set; }
        public bool Verificada { get; set; }
    }
}
