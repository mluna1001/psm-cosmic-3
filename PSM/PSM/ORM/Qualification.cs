﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class Qualification
    {
        [AutoIncrement, PrimaryKey]
        public int IdQualification { get; set; }
        public int IdRuta { get; set; }
        public int IdTienda { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreTienda { get; set; }
        public double Calificacion { get; set; }
    }
}
