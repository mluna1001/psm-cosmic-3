﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{

    public class Ruta
    {

        [PrimaryKey]
        public int IdRuta { get; set; }
        public int IdUsuario { get; set; }
        public int IdProyectoTienda { get; set; }
        public int IdTiempo { get; set; }
        public int IdProyecto { get; set; }
        public int IdTienda { get; set; }
        public string TiendaNombre { get; set; }
        public int NumeroSemana { get; set; }
        public string NombreDelDia { get; set; }
        public int IdRutaDinamica { get; set; }

        public int Status { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaTerminacion { get; set; }
        public decimal? GpsLatitude { get; set; }
        public decimal? GpsLongitude { get; set; }
        public bool Sincronizado { get; set; }
        public Nullable<double> Calificacion { get; set; }
        public Nullable<bool> SendQual { get; set; }
    }

}
