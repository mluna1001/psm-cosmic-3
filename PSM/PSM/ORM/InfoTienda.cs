﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class InfoTienda
    {
        [PrimaryKey, AutoIncrement]
        public int IdInfoTienda { get; set; }
        public int IdTienda { get; set; }
        public string NombreTienda { get; set; }
        public string Direccion { get; set; }
        public decimal? Longitud { get; set; }
        public decimal? Latitud { get; set; }
        public string ImagenUrl { get; set; }
        public int IdCadena { get; set; }
        public int IdFormato { get; set; }
        public bool Verificada { get; set; }
        public Nullable<bool> EstatusCheck { get; set; }
        public string Clasificacion { get; set; }
        public int? IdCodigoTienda { get; set; }
    }
}
