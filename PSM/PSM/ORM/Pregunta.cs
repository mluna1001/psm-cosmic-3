﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class Pregunta
    {
        [PrimaryKey]
        public int IdPregunta { get; set; }
        public int IdSeccion { get; set; }
        public int IdControl { get; set; }
        public int Numero { get; set; }
        public string Descripcion { get; set; }
        public bool Vigencia { get; set; }
        public int? IdParent { get; set; }
        public bool TieneHijos { get; set; }
        public bool? SeValida { get; set; }
    }
}
