﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class SeccionRuta
    {

        [PrimaryKey, AutoIncrement]
        public int IdSeccionRuta { get; set; }
        public int IdRuta { get; set; }
        public int IdSeccion { get; set; }

    }
}
