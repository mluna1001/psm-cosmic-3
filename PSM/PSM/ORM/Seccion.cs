﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class Seccion
    {
        [PrimaryKey]
        public int IdSeccion { get; set; }
        public int IdProyecto { get; set; }
        public int IdCatalogoSeccion { get; set; }
        public int? Orden { get; set; }
        public string Icono { get; set; }
        public string Descripcion { get; set; }
        public double Porcentaje { get; set; }
        public string TargetType { get; set; }
        public bool EsSeccion { get; set; }
        public bool SeValida { get; set; }
        public bool EsActiva { get; set; }
        public Nullable<bool> SeEdita { get; set; }
    }
}
