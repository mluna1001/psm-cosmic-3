﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class CatalogoSeccionMenu
    {

        [PrimaryKey]
        public int IdSeccionMenu { get; set; }
        public string Descripcion { get; set; }
        public int IdProyecto { get; set; }
        public string Icono { get; set; }

    }
}
