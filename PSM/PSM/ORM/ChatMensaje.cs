﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class ChatMensaje
    {
        public long IdChatMensaje { get; set; }
        public string Mensaje { get; set; }
        public DateTime? Fecha { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdProyecto { get; set; }
        public int? IdChatGrupo { get; set; }
    }
}
