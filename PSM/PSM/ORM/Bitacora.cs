﻿using Microsoft.AppCenter.Crashes;
using PSM.Helpers;
using PSM.ViewModel;
using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class Bitacora : IDisposable
    {
        [PrimaryKey, AutoIncrement]
        public int IdBitacora { get; set; }
        public int IdCatalogoBitacoraMovil { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public DateTime Fecha { get; set; }
        public static bool CanInsert { get; set; }
        public string Parameters { get; set; }
        public bool Sincronizado { get; set; }
        public int? IdRuta { get; set; }
        public int? IdUsuario { get; set; }

        public static bool Insert(int IdCatalogoBitacora, string action, string controller, string parameters = "")
        {
            if (Bitacora.CanInsert)
            {
                try
                {
                    var ruta = App.CurrentRoute?.IdRuta;
                    var db = Background.Instance;
                    db.Add(new Bitacora
                    {
                        IdCatalogoBitacoraMovil = IdCatalogoBitacora,
                        Action = action,
                        Controller = controller,
                        Fecha = DateTime.Now,
                        Parameters = (parameters.Length >= 150 && IdCatalogoBitacora != (int)TypeEnum.IdCatalogoBitacora.AppsInstaladas) 
                            ? parameters.Substring(0, 150) 
                            : parameters,
                        Sincronizado = false,
                        IdRuta = ruta,
                        IdUsuario = App.CurrentUser?.IdUsuario
                    });
                    return db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData(ex));
                }
                return false;
            }
            return false;
        }

        public static bool DropBD()
        {
            try
            {
                var db = Background.Instance;
                db.DropTables();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool Insert<T>(int IdCatalogoBitacora, string action, string controller, T data = default(T))
        {
            if (Bitacora.CanInsert)
            {
                string json = "";
                try
                {
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                    Debug.WriteLine("Se ha convertido el objeto a json", "Bitacora");
                }
                catch(Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData());
                }

                try
                {
                    var db = Background.Instance;
                    db.Add(new Bitacora
                    {
                        IdCatalogoBitacoraMovil = IdCatalogoBitacora,
                        Action = action,
                        Controller = controller,
                        Fecha = DateTime.Now,
                        Parameters = (json.Length >= 150 && IdCatalogoBitacora != (int)TypeEnum.IdCatalogoBitacora.AppsInstaladas)
                            ? json.Substring(0, 150)
                            : json,
                        Sincronizado = false,
                        IdRuta = App.CurrentRoute?.IdRuta,
                        IdUsuario = App.CurrentUser?.IdUsuario
                    });
                    return db.SaveChanges();
                }
                catch(Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData());
                }
                return false;
            }
            return false;
        }

        public static bool DeleteBitacora()
        {
            var db = Background.Instance;
            var listFotosB = db.Bitacora.Where(b => b.Parameters.Contains("\"Foto\":\"/"))//.Where(b => b.Sincronizado)
                .ToList().Select(b => b.IdBitacora);

            db.Bitacora.Delete(b => listFotosB.Contains(b.IdBitacora));
            db.Bitacora.Delete(b => b.Sincronizado);
            db.Execute("VACUUM");
            return db.SaveChanges();
        }

        public static List<Bitacora> GetNoSincronizado()
        {
            var db = Background.Instance;
            List<Bitacora> bitacora = new List<Bitacora>();
            var bitacoralist = db.Bitacora.Where(b => b.Sincronizado == false).ToList();
            foreach (var item in bitacoralist)
            {
                var idruta = item.IdRuta == null ? null : item.IdRuta == 0 ? null : item.IdRuta;
                item.IdRuta = idruta;
                bitacora.Add(item);
            }
            return bitacora;
        }

        public static bool UpdateBitacora(Bitacora bitacora)
        {
            var db = Background.Instance;
            var bitacoraindb = db.Bitacora.FirstOrDefault(b => b.IdBitacora == bitacora.IdBitacora);
            if (bitacoraindb != null)
            {
                bitacoraindb.IdCatalogoBitacoraMovil = bitacora.IdCatalogoBitacoraMovil;
                bitacoraindb.Action = bitacora.Action;
                bitacoraindb.Controller = bitacora.Controller;
                bitacoraindb.Fecha = bitacora.Fecha;
                bitacoraindb.Parameters = bitacora.Parameters;
                bitacoraindb.Sincronizado = bitacora.Sincronizado;
                db.Update(bitacoraindb);
                return true;
            }
            return false;
        }

        public static void VerificaCreacionBD()
        {
            var db = Background.Instance;
            int contadorVeces = 0;
            var bitacoraindb = db.Bitacora.FirstOrDefault(b => b.Action.Contains("Veces que se creó la BD: "));
            if (bitacoraindb != null)
            {
                var cont = bitacoraindb.Action.Split(':')[1];
                int.TryParse(cont, out contadorVeces);
                if (contadorVeces >= 1)
                {
                    bitacoraindb.Action = "Veces que se creó la BD: " + (contadorVeces + 1);

                    if (App.CurrentUser != null)
                    {
                        bitacoraindb.IdUsuario = App.CurrentUser.IdUsuario;
                    }
                    db.Update(bitacoraindb);
                    db.SaveChanges();
                }
            }
            else
            {
                contadorVeces = 1;
                var ruta = App.CurrentRoute?.IdRuta;
                db.Add(new Bitacora
                {
                    IdCatalogoBitacoraMovil = (int)TypeEnum.IdCatalogoBitacora.SeCreoBD,
                    Action = "Veces que se creó la BD: " + contadorVeces,
                    Controller = "LoginPage",
                    Fecha = DateTime.Now,
                    Parameters = "BD Creación",
                    Sincronizado = false,
                    IdRuta = ruta == null ? null : ruta == 0 ? null : ruta,
                    IdUsuario = App.CurrentUser?.IdUsuario
                });
                db.SaveChanges();
            }
        }

        internal static bool VerificaRegistroBDVeces()
        {
            var db = Background.Instance;
            var bitacoraindb = db.Bitacora.Exists(b => b.IdUsuario == App.CurrentUser.IdUsuario && b.Action.Contains("Veces que se creó la BD: "));
            return bitacoraindb;
        }

        public static async Task<string> Upload()
        {
            var bitacoras = Bitacora.GetNoSincronizado().Take(100);
            var abitacora = bitacoras.To<List<BitacoraModel>>();
            var isconnectedtoserver = true;
            var bitacorauploadresult = await App.API.UploadEndPoint.UploadBitacora(abitacora, () =>
            {
                isconnectedtoserver = false;
            });

            if (isconnectedtoserver)
            {
                if (bitacorauploadresult != null)
                {
                    if (bitacorauploadresult.Status)
                    {
                        foreach (var bitacora in bitacoras)
                        {
                            if (!bitacora.Action.Contains("Veces que se creó la BD: "))
                            {
                                bitacora.Sincronizado = true;
                                Bitacora.UpdateBitacora(bitacora); 
                            }
                        }
                        return "Se subio la bitacora";
                    }
                    else
                    {
                        return "No se subio la bitacora";
                    }
                }
                else
                {
                    return "No se pudo subir la bitacora";
                }
            }
            else
            {
                return "No hay conexion a internet";
            }
        }

        public void Dispose()
        {
            var db = Background.Instance;
            db.Close();
            db.Dispose();
        }
    }
}
