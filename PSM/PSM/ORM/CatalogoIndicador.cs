﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class CatalogoIndicador
    {
        [PrimaryKey]
        public int IdCatalogoIndicador { get; set; }
        public int? IdTienda { get; set; }
        public string Descripcion { get; set; }
        public int? ValorMinimo { get; set; }
        public int? ValorMedio { get; set; }
        public int? ValorAlto { get; set; }
        public string SemaforoMinimo { get; set; }
        public string SemaforoMedio { get; set; }
        public string SemaforoAlto { get; set; }
        public string ColorMinimo { get; set; }
        public string ColorMedio { get; set; }
        public string ColorAlto { get; set; }
        public int? IdIconoMinimo { get; set; }
        public int? IdIconoMedio { get; set; }
        public int? IdIconoAlto { get; set; }
    }
}
