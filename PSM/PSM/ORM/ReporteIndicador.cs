﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class ReporteIndicador
    {
        [PrimaryKey]
        public int IdIndicador { get; set; }
        public int IdCatalogoIndicador { get; set; }
        public string Alias { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int NumeroIndicador { get; set; }
        public string TextoIndicador { get; set; }
        public DateTime FechaAlta { get; set; }

    }
}
