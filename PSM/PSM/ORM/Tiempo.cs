﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class Tiempo
    {
        public int IdTiempo { get; set; }
        public DateTime Fecha { get; set; }
        public int DiaSemana { get; set; }
        public string NombreDia { get; set; }
        public int DiaMes { get; set; }
        public int DiaAño { get; set; }
        public int SemanadelAño { get; set; }
        public int MesdelAño { get; set; }
        public string NombreMes { get; set; }
        public int Bimestre { get; set; }
        public int Trimestre { get; set; }
        public int Cuatrimestre { get; set; }
        public int Semestre { get; set; }
        public int Año { get; set; }
    }
}
