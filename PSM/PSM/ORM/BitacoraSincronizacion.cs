﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.ORM
{
    public class BitacoraSincronizacion
    {
        [PrimaryKey, AutoIncrement]
        public int IdBitacoraSincronizacion { get; set; }
        public int IdRuta { get; set; }
        public bool Entrada { get; set; }
        public bool Salida { get; set; }
        public bool Respuestas { get; set; }
        public bool Abordaje { get; set; }
        public bool AbordajeMultimedia { get; set; }
        public bool Ruta { get; set; }
        public bool Foto { get; set; }
    }
}
