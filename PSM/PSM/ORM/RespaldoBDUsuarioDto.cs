﻿using System.Collections.Generic;

namespace PSM.ORM
{
    public class RespaldoBDUsuarioDto
    {
        public int IdUsuario { get; set; }
        public byte[] BasedeDatos { get; set; }
        public string Nombre { get; set; }
        public int Typo { get; set; }
        public List<UploadPhoto> CurrentPhoto { get; set; }
        public List<UploadRespAudit> CurrentRespAudit { get; set; }
    }
}