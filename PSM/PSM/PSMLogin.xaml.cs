﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PSMLogin : ContentPage
    {

        public string BaseUrl { get; set; }

        public PSMLogin(string project, string redirecturl)
        {
            InitializeComponent();
            BaseUrl = $"http://login.cosmic.mx/Login?project={project}&redirecturl={redirecturl}";
            WebPSMLogin.Source = BaseUrl;
            WebPSMLogin.Navigated += WebPSMLogin_Navigated;
            WebPSMLogin.Navigating += WebPSMLogin_Navigating;
        }

        private void WebPSMLogin_Navigating(object sender, WebNavigatingEventArgs e)
        {
            
        }

        private void WebPSMLogin_Navigated(object sender, WebNavigatedEventArgs e)
        {
            if (e.Url.Contains("idusuario"))
            {
                var url = e.Url.Replace(BaseUrl, string.Empty);
                Dictionary<string, string> data = new Dictionary<string, string>();
                var split = url.Split('?');
                if(split.Length > 1)
                {
                    var parameters = split[1];
                    var keyvalues = parameters.Split('&');
                    foreach (var keyvalue in keyvalues)
                    {
                        var keyvaluesplit = keyvalue.Split('=');
                        data.Add(keyvaluesplit[0], keyvaluesplit[1]);
                    }
                }
                string jsonresult = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                System.Diagnostics.Debug.WriteLine(jsonresult);
                OnLoginComplete(null);
            }
        }

        public event EventHandler<LoginComplete> LoginComplete;

        private void OnLoginComplete(LoginComplete lc)
        {
            LoginComplete?.Invoke(this, lc);
        }
    }

    public class LoginComplete
    {
        
    }
}