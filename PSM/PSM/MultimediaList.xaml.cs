﻿using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PSM
{
    public partial class MultimediaList : ContentPage
    {
        public List<MultimediaModel> _multimedia { get; set; }

        public static Abordaje Abordaje {get;set;}
        
        public MultimediaList(List<Multimedia> multimedia, int idseccionmenu)
        {
            InitializeComponent();
            App.CurrentPage = this;
            Abordaje = new Abordaje
            {
                IdSeccionMenu = idseccionmenu,
                Inicio = DateTime.Now,
                IdRuta = App.CurrentRoute.IdRuta,
                Number = App.DB.Abordaje.Count(e => e.IdRuta == App.CurrentRoute.IdRuta)
            };
            _multimedia = new List<MultimediaModel>();
            foreach (var item in multimedia)
            {
                var iconsource = item.GetIcon();
                _multimedia.Add(new MultimediaModel
                {
                    Categoria = item.Categoria,
                    IdMultimedia = item.IdMultimedia,
                    IdSeccionMenu = item.IdSeccionMenu,
                    Nombre = item.Nombre,
                    url = item.url,
                    Vigencia = item.Vigencia,
                    Source = $"localpath/{item.Nombre}",
                    Icon = iconsource
                });
            }
            ListMultimedia.ItemSelected += ListMultimedia_ItemSelected;
            ListMultimedia.ItemTapped += ListMultimedia_ItemTapped;
            ListMultimedia.ItemsSource = _multimedia;
        }

        protected override void OnAppearing()
        {
            ListMultimedia.IsEnabled = true;
        }

        protected override bool OnBackButtonPressed()
        {
            if (MultimediaList.Abordaje != null)
            {
                MultimediaList.Abordaje.Fin = DateTime.Now;
                App.DB.Add(MultimediaList.Abordaje);
                App.DB.SaveChanges();
                MultimediaList.Abordaje = null;
            }
            return base.OnBackButtonPressed();
        }

        private async void ListMultimedia_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var model = e.Item as MultimediaModel;
            ListMultimedia.IsEnabled = false;
            if (model != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "ListMultimedia_ItemTapped", "MultimediaList");
                MultimediaPage visualizer = new MultimediaPage(model, Abordaje);
                if (visualizer.Render())
                {
                    await Navigation.PushAsync(visualizer);
                }
                else
                {
                    await DisplayAlert("PSM 360", "No se pudo mostrar el archivo, contacta con tu supervisor", "Aceptar");
                }
            }
        }

        private void ListMultimedia_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ListMultimedia.SelectedItem = null;
        }
    }
}
