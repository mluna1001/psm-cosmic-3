﻿using Microcharts;
using PSM.Helpers;
using PSM.ORM;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReporteIndicadorPage : ContentPage
	{
        CatalogoIndicador Catalogo;

		public ReporteIndicadorPage ()
		{
			InitializeComponent ();
		}

        public ReporteIndicadorPage(CatalogoIndicador catalogo)
        {
            InitializeComponent();
            Catalogo = catalogo;
            lblTitulo.Text = catalogo.Descripcion;
            GetReporteIndicador();
        }

        void GetReporteIndicador()
        {
            var reportesIndicador = App.DB.ReporteIndicador.Where(r => r.IdCatalogoIndicador == Catalogo.IdCatalogoIndicador).ToList();
            PopulateChart(reportesIndicador);
        }

        void PopulateChart(List<ReporteIndicador> reporteIndicadors)
        {
            var entries = new List<Microcharts.Entry>();
            //var entries = new Microcharts.Entry[reporteIndicadors.Count];

            foreach (var item in reporteIndicadors)
            {
                Random rnd = new Random();
                Color randomColor = Color.FromRgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                string color = randomColor.ToHex();
                var entry = new Microcharts.Entry(item.NumeroIndicador)
                {
                    Color = SKColor.Parse(color),
                    ValueLabel = item.NumeroIndicador.ToString(),
                    Label = item.Alias                    
                };

                entries.Add(entry);
            }

            bcChart.Chart = new BarChart
            {
                Entries = entries,
                LabelOrientation = Orientation.Default,
            };
        }
	}
}