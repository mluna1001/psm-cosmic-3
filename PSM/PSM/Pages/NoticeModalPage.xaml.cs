﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoticeModalPage : ContentPage
    {
        public NoticeModalPage()
        {
            InitializeComponent();
        }

        public NoticeModalPage(Avisos aviso)
        {
            InitializeComponent();
            Print(aviso);
        }

        private void Print(Avisos notification)
        {
            lblTitulo.Text = notification.Titulo;
            spanMessage.Text = notification.Mensaje;
            if (!string.IsNullOrEmpty(notification.URL))
            {
                spanLink.Text = notification.URL;
            }
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(spanLink.Text));
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}