﻿using PSM.Helpers;
using PSM.ViewModel;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace PSM.Pages
{
    public partial class SemaforoDetailPage : ContentPage
    {
        private usuarioModel usuariomodel;

        public SemaforoDetailPage(usuarioModel item)
        {
            this.usuariomodel = item;
            InitializeComponent();

            Default();
        }

        private void Default()
        {
            var asistencias = usuariomodel.Asistencia.Where(s => s.Entrada);
            if (!asistencias.Any())
            {
                DisplayAlert("Colaborador", "El colaborador aun no tiene entradas registradas.", "Cerrar");
                return;
            }

            var map = new Map()
            {
                IsShowingUser = true,
                HeightRequest = 100,
                WidthRequest = 960,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            SetMap(asistencias.ToList(), map);

            var stack = new StackLayout { Spacing = 0 };
            stack.Children.Add(map);
            Content = stack;
        }

        //private void Defaults()
        //{
        //    var pins = new List<TKCustomMapPin>();
        //    var asistencias = usuariomodel.Asistencia.Where(s => s.Entrada);
        //    if (!asistencias.Any())
        //    {
        //        DisplayAlert("Colaborador", "El colaborador aun no tiene entradas registradas.", "Cerrar");
        //        return;
        //    }

        //    foreach (var asiste in asistencias)
        //    {
        //        pins.Add(new TKCustomMapPin
        //        {
        //            Position = new Position(double.Parse(asiste.Latitud.ToString()), double.Parse(asiste.Longitud.ToString())),
        //            Title = asiste.TiendaNombre,
        //            Subtitle = asiste.IdUsuarioCosmic.ToString(),
        //            ID = asiste.IdRuta.ToString(),
        //        });
        //    }
        //    CustomMap.CustomPins = pins;
        //    CustomMap.IsShowingUser = true;
        //    //CustomMap.MoveToRegion(MapSpan.FromCenterAndRadius(pins.FirstOrDefault().Position, Distance.FromKilometers(0.5)));
        //    CustomMap.MapCenter = pins.FirstOrDefault().Position;
        //    CustomMap.PinSelected += CustomMap_PinSelected;

        //    CustomMap.MapRegion = MapSpan.FromCenterAndRadius(pins.FirstOrDefault().Position, Distance.FromKilometers(0.5));
        //    CustomMap.HasZoomEnabled = true;
        //    CustomMap.IsRegionChangeAnimated = true;
        //}

        public void SetMap(List<AsistenciaUsuarioDto> asistencias, Map map)
        {
            var latitudes = new List<double>();
            var longitudes = new List<double>();

            foreach (var asiste in asistencias)
            {
                var latitud = double.Parse(asiste.Latitud.ToString());
                var longitud = double.Parse(asiste.Longitud.ToString());
                var latitudSalida = double.Parse(asiste.LatitudSalida.ToString());
                var longitudSalida = double.Parse(asiste.LongitudSalida.ToString());

                map.Pins.Add(new Pin
                {
                    Type = PinType.Place,
                    Position = new Position(latitud, longitud),
                    Label = asiste.TiendaNombre.Trim() + " · Programada: " + asiste.DiaProgramado,
                    Address = "Entrada: " + asiste.FechaEntrada.ToString()
                });

                if (asiste.LatitudSalida != 0)
                {
                    map.Pins.Add(new Pin
                    {
                        Type = PinType.Place,
                        Position = new Position(latitudSalida, longitudSalida),
                        Label = asiste.TiendaNombre.Trim() + " · Programada: " + asiste.DiaProgramado,
                        Address = "Salida: " + asiste.FechaSalida.ToString(),
                    });
                }

                latitudes.Add(latitud);
                longitudes.Add(longitud);
            }

            double lowestLat = latitudes.Min();
            double highestLat = latitudes.Max();
            double lowestLong = longitudes.Min();
            double highestLong = longitudes.Max();
            double finalLat = (lowestLat + highestLat) / 2;
            double finalLong = (lowestLong + highestLong) / 2;
            double distance = DistanceCalculation.GeoCodeCalc.CalcDistance(lowestLat, lowestLong, highestLat, highestLong, DistanceCalculation.GeoCodeCalcMeasurement.Kilometers);

            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(finalLat, finalLong), Distance.FromKilometers(distance)));
        }
    }
}