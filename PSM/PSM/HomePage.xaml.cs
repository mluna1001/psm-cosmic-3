﻿using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PSM
{
    [Xamarin.Forms.Xaml.XamlCompilation(Xamarin.Forms.Xaml.XamlCompilationOptions.Compile)]
    public partial class HomePage : MasterDetailPage
    {

        public static HomePage Instance { get; set; }

        public HomePage()
        {
            InitializeComponent();
            this.MasterBehavior = MasterBehavior.Popover;
            App.CurrentPage = this;
            Version.Text = "PSM " + new Helpers.Version().NumeroVersion;
            Bienvenido.Text = App.CurrentUser.Alias;
            lblNombre.Text = (string.IsNullOrEmpty(App.CurrentUser.Nombre)) ? "" : App.CurrentUser.Nombre;
            Instance = this;
            //Lista del menu generado de forma dinamica a partir de la base de datos que se encuentra en el servidor
            List<MenuUser> menuUsuario = App.DB.MenuUser.OrderBy(e => e.Orden).ToList();
            if (!menuUsuario.Any())
            {
                //Si no tengo permisos asignados al menu lanza el mensaje
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "HomePage", "No hay permisos para la aplicación");
                DisplayAlert("PSM 360", "¡No tiene permisos en la app!. Comuniquese con el administrador del sistema.", "Aceptar");
                return;
            }
            else
            {
                var menuaviso = menuUsuario.FirstOrDefault(e => e.IdMenuMovil == 16);
                if (menuaviso != null)
                {
                    var avisos = App.DB.Avisos.Where(a => a.Vigente).ToList();
                    var message = "";
                    if (avisos.Count == 1)
                    {
                        message = "1 aviso";
                    }
                    else
                    {
                        message = $"{avisos.Count} avisos";
                    }
                    menuaviso.Descripcion = message;
                }
            }
            // asignamos el menu a la lista
            navigationDrawerList.ItemsSource = menuUsuario;
            // instanciamos la pagina detail
            SetDetail((Page)Activator.CreateInstance(typeof(MainPage)));
            // Agregamos un elemento a la tooblar
        }

        public void SetDetail(Page page)
        {
            var navigationpage = new NavigationPage(page);
            navigationpage.BarTextColor = Color.White;
            navigationpage.BarBackgroundColor = Color.FromHex("#306c9f");
            navigationpage.ToolbarItems.Add(App.GetNotificationToolbarItem());
            Instance.Detail = navigationpage;
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Icon = new FileImageSource { File = "icon.png" };
            }
        }

        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            navigationDrawerList.SelectedItem = null;
        }

        private async void OnMenuItemTapped(object sender, ItemTappedEventArgs e)
        {
            var item = e.Item as MenuUser;
            if (item == null) return;
            // Borra base   Borra DB
            if (item.IdMenuMovil == 10)
            {
                var answer = await DisplayAlert("¡Alerta!", "¿Estas seguro de eliminar la base de datos?", "Si", "No");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "HomePage", "¿Estas seguro de eliminar la base de datos?");
                Debug.WriteLine("Answer: " + answer);
                if (answer)
                {
                    var answer2 = await DisplayAlert("¡Alerta!", "¿Al eliminar la base se elimina toda tu información, ¿Deseas eliminarla?", "Si", "No");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "HomePage", "¿Al eliminar la base se elimina toda tu información, ¿Deseas eliminarla?");
                    Debug.WriteLine("Answer: " + answer2);
                    if (answer2)
                    {
                        LoginModel model = new LoginModel
                        {
                            AppIcon = "icon.png",
                            AppName = "PSM 360",
                            BackgroundImage = "walldefault.jpg",
                            ButtonOne = "Aceptar",
                            EntryOne = "Alias",
                            EntryTwo = "Contraseña",
                            Message = "Ingresa tu alias y contraseña para empezar.",
                            Opacity = "0.2",
                            PrimaryColor = "#000000",
#if DEBUG
                            EntryOneValue = "jorgenutri",
                            EntryTwoValue = "123456",
#endif
                            SecondaryColor = "#ffffff"
                        };
                        model.LoginClick += Model_LoginClick;
                        if (App.DB.Respuesta.ToList().Count > 0)
                        {
                            var answer3 = await DisplayAlert("¡Alerta!", "Tienes respuestas sin subir, con esta acción se eliminaran, ¿Estas seguro de eliminarla?", "Si", "No");
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "HomePage", "Tienes respuestas sin subir, con esta acción se eliminaran, ¿Estas seguro de eliminarla?");
                            Debug.WriteLine("Answer: " + answer3);
                            if (answer3)
                            {
                                Bitacora.DeleteBitacora();
                                App.DB.DropTables();
                                App.DB.CreateTables();
                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "HomePage", answer3);
                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "Se ha eliminado la base de datos");
                                Application.Current.MainPage = new StandarLogin(model);
                            }
                            else
                            {
                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "No se elimino la base de datos");
                            }
                        }
                        else
                        {
                            Bitacora.DeleteBitacora();
                            App.DB.DropTables();
                            App.DB.CreateTables();
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "Se ha eliminado la base de datos");
                            Application.Current.MainPage = new StandarLogin(model);
                        }
                    }
                }
            }
            else
            {
                Type page = Type.GetType("PSM." + item.TargetType + ",PSM");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "OnMenuItemSelected", "HomePage", $"Navegando al menu {page}");
                Page pageinstance = null;

                try
                {
                    pageinstance = (Page)Activator.CreateInstance(page);
                }
                catch
                {
                    await DisplayAlert("PSM 360", "No podemos abrir este menu, contacta a tu supervisor", "Aceptar");
                }

                if (pageinstance != null)
                {
                    SetDetail(pageinstance);
                    if (Device.RuntimePlatform != "Windows")
                    {
                        IsPresented = false;
                    }
                }
            }
        }

        private void Model_LoginClick(object sender, Dictionary<string, string> e)
        {
            var loginmodel = sender as LoginModel;
            if (e != null)
            {
                var alias = e["Alias"];
                var password = e["Contraseña"];
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoUsuarioContraseña, "LoginModel_Click", "HomePage", e);
                App.Auth(loginmodel, alias, password);
            }
        }
    }
}
