﻿using System;
using Newtonsoft.Json;

using System.Collections.Generic;
using System.Linq;

using System.Globalization;
using ChatApp.ORM;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChatApp
{
	public class ChatClient
	{

		public async Task<UsersResult> GetUsers(string alias)
		{
			try
			{
                HttpClient client = new HttpClient();
                var response = await client.PostAsync($"{ChatConfig.BaseUrl}/api/getusers", new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "alias", alias }
                }));

                //var client = new RestClient($"{ChatConfig.BaseUrl}/api/getusers");
				// var request = new RestRequest(Method.POST);
				// request.AddParameter("alias", alias);
				// var response = client.Execute(request);
				if (response != null)
				{
					var result = JsonConvert.DeserializeObject<UsersResult>(await response.Content.ReadAsStringAsync());
					if (result != null)
					{
						result.Alias = alias;
					}
					return result;
				}
			}
			catch
			{

			}
			return default(UsersResult);
		}

		public async Task<NotifyReceivedResponse> NotifyReceived(string idchat)
		{
			try
			{
                HttpClient client = new HttpClient();
                var response = await client.PostAsync($"{ChatConfig.BaseUrl}/api/notifyreceived", new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "idchat", idchat }
                }));
                /*
                var client = new RestClient();
				var request = new RestRequest(Method.POST);
				request.AddParameter("idchat", idchat);
				var response = client.Execute(request);
                */
				if (response != null)
				{
					return JsonConvert.DeserializeObject<NotifyReceivedResponse>(await response.Content.ReadAsStringAsync());
				}
			}
			catch
			{

			}
			return null;
		}

		public class Userprofile
		{
			public int IdUsuario { get; set; }
			public string Nombre { get; set; }
			public string Alias { get; set; }
			public int? IdParent { get; set; }
			public string Descripcion { get; set; }

			public static Userprofile FromUserChat(UserChat user)
			{
				if (user == null) return null;
				return new Userprofile
				{
					Alias = user.Alias,
					Descripcion = user.Descripcion,
					IdParent = user.IdParent,
					IdUsuario = user.IdUsuario,
					Nombre = user.Nombre
				};
			}
		}

		public class UsersResult
		{
			public string Alias { get; set; }
			public bool IsSuccess { get; set; }
			public List<Userprofile> Users { get; set; }

			public List<ChatBubbleView> GetUserAsChats()
			{
				List<ChatBubbleView> bubbles = new List<ChatBubbleView>();
				var usuarioigual = Users.FirstOrDefault(e => e.Alias.Equals(Alias));
				if (usuarioigual != null)
				{
					Users.Remove(usuarioigual);
				}
				foreach (var user in Users)
				{
                    ChatDataBase db = ChatDataBase.Instance;
					var unreadlist = db.GetChatsUnread(user.IdUsuario);
					string details = "";
					if (unreadlist.Count > 0)
					{
						details = unreadlist.Count.ToString();
					}
					bubbles.Add(new ChatBubbleView(0) { Id = user.IdUsuario, Message = user.Descripcion, SendDate = "", User = user.Nombre });
				}
				return bubbles;
			}

			public List<UserChat> GetUsers()
			{
				List<UserChat> listuserchat = new List<UserChat>();
				foreach (var user in Users)
				{
					listuserchat.Add(new UserChat
					{
						Alias = user.Alias,
						Descripcion = user.Descripcion,
						IdParent = user.IdParent,
						IdUsuario = user.IdUsuario,
						Nombre = user.Nombre
					});
				}
				return listuserchat;
			}
		}

		public async Task<ChatPendientesResponse> ChatsPendientes(int idusuario)
		{
			try
			{
                HttpClient client = new HttpClient();
                var response = await client.PostAsync($"{ChatConfig.BaseUrl}/api/ChatsPendientes", new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "idusuario", idusuario.ToString() }
                }));

                /*
                var client = new RestClient();
				var request = new RestRequest(Method.POST);
				request.AddParameter("idusuario", idusuario);
				var response = client.Execute(request);
                */
				if (response != null)
				{
					return JsonConvert.DeserializeObject<ChatPendientesResponse>(await response.Content.ReadAsStringAsync());
				}
			}
			catch
			{

			}
			return null;
		}

		public class ChatPendientesResponse
		{
			public bool IsSuccess { get; set; }
			public List<ChatResponse> Chats { get; set; }

			public List<ORM.Chat> GetAsChat()
			{
				List<ORM.Chat> chats = new List<ORM.Chat>();
				foreach (var chat in Chats)
				{
                    DateTime fecha = DateTime.Now;
                    if (chat.FechaDeEnvio != null)
                    {
                        fecha = DateTime.ParseExact(chat.FechaDeEnvio, "yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                    }
					
					chats.Add(new ORM.Chat
					{
						Entregado = chat.Entregado,
						Fecha = fecha,
						IdChat = chat.IdChat,
						IdUsuarioDestino = chat.IdUsuarioDestino,
						IdUsuarioOrigen = chat.IdUsuarioOrigen,
						Mensaje = chat.Mensaje
					});
				}
				return chats;
			}
		}

		public class ChatResponse
		{
			public bool Entregado { get; set; }

			public int IdChat { get; set; }

			public int IdUsuarioDestino { get; set; }

			public int IdUsuarioOrigen { get; set; }

			public string Mensaje { get; set; }

			public string FechaDeEnvio { get; set; }
		}

		public class NotifyReceivedResponse
		{
			public string IdChat
			{
				get;
				set;
			}

			public bool TryUpdate
			{
				get;
				set;
			}

			public bool Updated
			{
				get;
				set;
			}
		}

	}
}
