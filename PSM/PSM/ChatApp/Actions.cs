
namespace ChatApp
{
    public enum Actions
    {
        ReceiveMessage, UpdateComplete, ReceiveMessageFromGroup
    }
}