﻿using System;
using Xamarin.Forms;

namespace ChatApp
{
    /*
	public class ChatBubble
	{
        public int Id { get; set; }

        public string Title
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}

		public string SendDate
		{
			get;
			set;
		}

		public Xamarin.Forms.Color UserColor
		{
			get;
			set;
		}

        public ChatBubbleViewType Type { get; set; }

        public ChatBubble(ChatBubbleViewType type)
		{
            Type = type;
            if (type == ChatBubbleViewType.Me)
			{
				UserColor = Xamarin.Forms.Color.LightBlue;
			}
			else if(type == ChatBubbleViewType.He)
			{
				UserColor = Xamarin.Forms.Color.LightGray;
			}
		}

        public StackLayout Render()
        {
            StackLayout layout = new StackLayout();
            // Label titlelabel = new Label { Text = Title };
            Label messagelabel = new Label { Text = Message };

            switch (Type)
            {
                case ChatBubbleViewType.Me:
                    // titlelabel.HorizontalTextAlignment = TextAlignment.End;
                    messagelabel.HorizontalTextAlignment = TextAlignment.End;
                    messagelabel.TextColor = Color.Navy;
                    break;
                case ChatBubbleViewType.He:
                    // titlelabel.HorizontalTextAlignment = TextAlignment.Start;
                    messagelabel.HorizontalTextAlignment = TextAlignment.Start;
                    messagelabel.TextColor = Color.Black;
                    break;
            }
            // layout.Children.Add(titlelabel);
            layout.Children.Add(messagelabel);
            return layout;
        }
    }
    */

    public enum ChatBubbleViewType
    {
        Me, He
    }
}
