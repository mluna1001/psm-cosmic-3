﻿using System;
using SQLite;
using System.Linq;
using System.Collections.Generic;
using YunoDBCore;
using DevelopersAzteca.Storage.SQLite;
using ChatApp.ORM;

namespace ChatApp
{
	public class ChatDataBase
	{

        private YunoDBCore.YunoConnection _connection { get; set; }
        public DocumentQuery<ORM.Chat> Chat { get; set; }
        public DocumentQuery<ORM.UserChat> UserChat { get; set; }

        public event EventHandler<Chat> ChatInserted;

        private void OnChatInserted(ORM.Chat chat)
        {
            ChatInserted?.Invoke(this, chat);
        }

        private ChatDataBase()
        {
            YunoDBCore.Debugger.Info = true;
            _connection = YunoDBCore.YunoConnection.Instance;
            Chat = _connection.Query<ORM.Chat>();
            UserChat = _connection.Query<ORM.UserChat>();
        }

        private static ChatDataBase _instance { get; set; }

        public static ChatDataBase Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ChatDataBase();
                }
                return _instance;
            }
        }
       
        public bool InsertChat(ORM.Chat chat)
        {
            try
            {
                Chat.Add(chat);
                Chat.SaveChanges();
                OnChatInserted(chat);
                return true;
            }
            catch (Exception ex)
            {
                Debugger.WriteLine(ex.StackTrace);
            }
            return false;
        }

		public List<ORM.Chat> GetChats(int idlocaluser, int idcurrentuser)
		{
            return Chat.Where(e => (e.IdUsuarioDestino == idlocaluser && e.IdUsuarioOrigen == idcurrentuser) || (e.IdUsuarioOrigen == idlocaluser && e.IdUsuarioDestino == idcurrentuser)).OrderBy(e => e.Fecha).ToList();
		}

        public void InsertUsers(List<ORM.UserChat> users)
        {
            var listusers = UserChat.ToList();
            try
            {
                foreach (var user in users)
                {
                    var usuarioencontrado = listusers.FirstOrDefault(e => e.IdUsuario == user.IdUsuario);
                    if (usuarioencontrado == null)
                    {
                        UserChat.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                Debugger.WriteLine(ex.StackTrace);
            }
            UserChat.SaveChanges();
        }
        
        public List<ORM.UserChat> GetUsuarios()
        {
            return UserChat.ToList();
        }

        public ORM.UserChat GetUser(string idusuario)
        {
            var userfound = UserChat.FirstOrDefault(e => e.IdUsuario.ToString().Equals(idusuario));
            return userfound;
        }

		public List<ORM.Chat> GetChatsUnread(int idusuario)
		{
			return Chat.Where(e => !e.Entregado && e.IdUsuarioOrigen == idusuario).ToList();
		}

        public bool UpdateChat(ORM.Chat item)
        {
            var currentchat = Chat.FirstOrDefault(e => e.IdChat == item.IdChat);
            if (currentchat != null)
            {
                currentchat.Entregado = item.Entregado;
                currentchat.Fecha = item.Fecha;
                currentchat.IdChat = item.IdChat;
                currentchat.IdUsuarioDestino = item.IdUsuarioDestino;
                currentchat.IdUsuarioOrigen = item.IdUsuarioOrigen;
                currentchat.Mensaje = item.Mensaje;
                Chat.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
