﻿using System;

namespace ChatApp.ORM
{
    public class UserChat
    {
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Alias { get; set; }
        public int? IdParent { get; set; }
        public string Descripcion { get; set; }
    }

    public class Chat
    {
        public int IdChat
        {
            get;
            set;
        }

        public int IdUsuarioOrigen
        {
            get;
            set;
        }

        public int IdUsuarioDestino
        {
            get;
            set;
        }

        public string Mensaje
        {
            get;
            set;
        }

        public DateTime Fecha
        {
            get;
            set;
        }

        public bool Entregado
        {
            get;
            set;
        }
    }

    public class ChatMensaje
    {
        public long IdChatMensaje { get; set; }
        public string Mensaje { get; set; }
        public DateTime? Fecha { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdProyecto { get; set; }
        public int? IdChatGrupo { get; set; }
    }

    public class ChatGrupo
    {
        public int IdChatGrupo { get; set; }
        public int IdUsuario { get; set; }
        public string Descripcion { get; set; }
        public int IdProyecto { get; set; }
        public bool Baneado { get; set; }
        public DateTime CreadoEl { get; set; }
        public DateTime? ActualizadoEl { get; set; }
    }
}
