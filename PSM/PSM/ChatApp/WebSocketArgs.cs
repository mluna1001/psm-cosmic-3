using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChatApp
{
    public class WebSocketArgs
    {
        public Actions Action
        {
            get;
            set;
        }

        public dynamic Data
        {
            get;
            set;
        }
    }
}