﻿using System;
using ChatApp.ORM;
using PSM;
using System.Collections.Generic;

namespace ChatApp
{
    public class ChatConfig
    {
        public static Action<ChatClient.Userprofile, List<ChatGrupo>> Init { get; set; }
        public static string BaseUrl { get; set; }
        public static VisibilityState Visibility { get; set; }
        public static ChatUserItem Conversation { get; set; }
        public static ChatGrupo ConversationGroup { get;  set; }
        public static bool InConversation { get; set; }
        public static bool InChat { get; internal set; }
        public static Action<Chat> Send { get; set; }
        public static int? ChatOfUser { get; set; }
        public static Action<ChatMensaje> SendToGroup { get; set; }
        public static bool InConversationGroup { get; set; }
        public static Action<List<ChatGrupo>> SetGrupos { get; set; }
        public static void Vibrate()
        {
            if (Plugin.Vibrate.CrossVibrate.IsSupported)
            {
                if (Plugin.Vibrate.CrossVibrate.Current.CanVibrate)
                {
                    Plugin.Vibrate.CrossVibrate.Current.Vibration();
                }
            }
        }

        public static event EventHandler<ChatMensaje> GroupMessageReceive;

        public static void OnGroupMessageReceive(ChatMensaje chatMensaje)
        {
            GroupMessageReceive?.Invoke(null, chatMensaje);
        }
    }
}
