﻿using ChatApp;
using Plugin.Media;
using Plugin.Media.Abstractions;
using PSM.DeviceSensors;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using PSM.ORM;


namespace PSM
{
    public partial class ChatPage : ContentPage
    {
        public ChatPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "ChatPage");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Source = new ObservableCollection<PSM.ORM.ChatGrupo>();
            ChatUserList.ItemsSource = Source;
            var listchats = App.DB.ChatGrupo.ToList();

            foreach (var item in listchats)
            {
                Source.Add(item);
            }
            ChatConfig.SetGrupos?.Invoke(App.ChatGrupos());
        }

        public ObservableCollection<PSM.ORM.ChatGrupo> Source { get; set; }

        private void ChatUserList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ChatUserList.SelectedItem = null;
        }

        private async void ChatUserList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var chatgrupo = e.Item as PSM.ORM.ChatGrupo;
            await Navigation.PushAsync(new ConversationGroupPage(chatgrupo));
        }
    }
}
