﻿using ChatApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatBubbleView : ContentView
    {
        private ChatBubbleViewType _type { get; set; }
        public ChatBubbleView(ChatBubbleViewType type)
        {
            InitializeComponent();
            BindingContext = this;
            _type = type;
        }

        public readonly BindableProperty UserProperty = BindableProperty.Create("User", typeof(string), typeof(ChatBubbleView), string.Empty);
        public string User { get { return (string)GetValue(UserProperty); } set { SetValue(UserProperty, value); } }
        public readonly BindableProperty MessageProperty = BindableProperty.Create("Message", typeof(string), typeof(ChatBubbleView), string.Empty);
        public string Message { get { return (string)GetValue(MessageProperty); } set { SetValue(MessageProperty, value); } }
        public readonly BindableProperty SendDateProperty = BindableProperty.Create("SendDate", typeof(string), typeof(ChatBubbleView), string.Empty);
        public string SendDate { get { return (string)GetValue(SendDateProperty); } set { SetValue(SendDateProperty, value); } }
        public int Id { get; set; }

        public ChatBubbleView Render()
        {
            switch (_type)
            {
                case ChatBubbleViewType.Me:
                    ChatMe.IsVisible = true;
                    break;
                case ChatBubbleViewType.He:
                    ChatHe.IsVisible = true;
                    break;
            }
            return this;
        }
    }
}