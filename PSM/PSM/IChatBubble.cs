﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM
{
    public interface IChatBubble
    {

        string User { get; set; }
        string Message { get; set; }
        string SendDate { get; set; }

    }
}
