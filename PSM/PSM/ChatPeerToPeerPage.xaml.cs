﻿using ChatApp;
using Plugin.Media;
using Plugin.Media.Abstractions;
using PSM.DeviceSensors;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using ChatApp.ORM;
using PSM.ORM;

namespace PSM
{
    public partial class ChatPeerToPeerPage : ContentPage
    {
        public ChatPeerToPeerPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "ChatPeerToPeerPage");
            InitChat();
        }

        private void Instance_ChatInserted(object sender, Chat e)
        {
            if (Source != null)
            {
                // el mensaje lo envie yo
                ChatUserItem chatuseritem = null;
                if (e.IdUsuarioOrigen == App.CurrentUser.IdUsuario)
                {
                    chatuseritem = Source.FirstOrDefault(s => s.IdUsuario == e.IdUsuarioDestino);
                }
                else
                {
                    chatuseritem = Source.FirstOrDefault(s => s.IdUsuario == e.IdUsuarioOrigen);
                }

                if (chatuseritem != null)
                {
                    Source.Remove(chatuseritem);
                    chatuseritem.Chats.Add(e);
                    chatuseritem.Count = chatuseritem.Chats.Count;
                    var sizemessage = 20;
                    var mensaje = e.Mensaje.Length > sizemessage ? e.Mensaje.Substring(0, sizemessage) : e.Mensaje;
                    if (e.IdUsuarioOrigen == App.CurrentUser.IdUsuario)
                    {
                        mensaje = $"Has dicho {mensaje}...";
                    }
                    else
                    {
                        mensaje = $"Te ha dicho {mensaje}...";
                    }

                    chatuseritem.LastMessage = mensaje;
                    Source.Insert(0, chatuseritem);
                }
            }
        }

        protected override void OnAppearing()
        {
            ChatConfig.InConversation = false;
            ChatConfig.Conversation = null;
            base.OnAppearing();
        }

        public ObservableCollection<ChatUserItem> Source { get; set; }

        public void InitChat()
        {
            Source = new ObservableCollection<ChatUserItem>();
            ChatUserList.ItemsSource = Source;
            var db = ChatDataBase.Instance;
            var usuarios = db.UserChat.ToList();
            List<ChatUserItem> listofchats = new List<ChatUserItem>();
            foreach (var usuario in usuarios)
            {
                if (usuario.IdUsuario == App.CurrentUser.IdUsuario) continue;
                var chats = db.Chat.Where(e => (e.IdUsuarioOrigen == usuario.IdUsuario && e.IdUsuarioDestino == App.CurrentUser.IdUsuario) || (e.IdUsuarioOrigen == App.CurrentUser.IdUsuario && e.IdUsuarioDestino == usuario.IdUsuario)).OrderBy(e => e.Fecha).ToList();
                var lastchat = chats.LastOrDefault();
                var lastmessage = "";
                int sizemessage = 20;
                DateTime lastdate = DateTime.Now;
                if (lastchat != null)
                {
                    var userchat = db.UserChat.FirstOrDefault(e => e.IdUsuario == lastchat.IdUsuarioOrigen);
                    if (userchat != null)
                    {
                        if (userchat.IdUsuario == App.CurrentUser.IdUsuario)
                        {
                            var mensaje = lastchat.Mensaje.Length > sizemessage ? lastchat.Mensaje.Substring(0, sizemessage) : lastchat.Mensaje;
                            lastmessage = $"Has dicho {mensaje}...";
                        }
                        else
                        {
                            var mensaje = lastchat.Mensaje.Length > sizemessage ? lastchat.Mensaje.Substring(0, sizemessage) : lastchat.Mensaje;
                            lastmessage = $"Te ha dicho {mensaje}...";
                        }
                    }
                    else
                    {
                        if (lastchat.IdUsuarioOrigen == App.CurrentUser.IdUsuario)
                        {
                            var mensaje = lastchat.Mensaje.Length > sizemessage ? lastchat.Mensaje.Substring(0, sizemessage) : lastchat.Mensaje;
                            lastmessage = $"Has dicho {mensaje}...";
                        }
                        else
                        {
                            var mensaje = lastchat.Mensaje.Length > sizemessage ? lastchat.Mensaje.Substring(0, sizemessage) : lastchat.Mensaje;
                            lastmessage = $"Te ha dicho {mensaje}...";
                        }
                    }
                    lastdate = lastchat.Fecha;
                }
                ChatUserItem item = new ChatUserItem
                {
                    Name = usuario.Nombre,
                    IdUsuario = usuario.IdUsuario,
                    Count = chats.ToList().Count,
                    Chats = chats,
                    LastMessage = lastmessage,
                    Fecha = lastdate
                };
                listofchats.Add(item);
            }

            foreach (var item in listofchats.OrderBy(e => e.Fecha).ToList())
            {
                Source.Add(item);
            }

            ChatConfig.InChat = true;
            ChatConfig.ChatOfUser = null;
            ChatDataBase.Instance.ChatInserted += Instance_ChatInserted;
        }

        private void ChatUserList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ChatUserList.SelectedItem = null;
        }

        private async void ChatUserList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var item = e.Item as ChatUserItem;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "ChatUserList_ItemTapped", "ChatPeerToPeerPage", "Chat Seleccionado");
            await Navigation.PushAsync(new ConversationPage(item));
        }
    }
}
