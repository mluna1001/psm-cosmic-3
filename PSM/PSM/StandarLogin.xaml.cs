﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using PSM.Function;
using PSM.ViewModel;
using PSM.ORM;
using DevelopersAzteca.Storage.SQLite;
using PSM.API;
using ChatApp;
using PSM.Helpers.Services;
using PSM.Helpers;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StandarLogin : ContentPage
    {
        private LoginModel _model;
        public StandarLogin(LoginModel model)
        {
            InitializeComponent();
            App.CurrentPage = this;
            BindingContext = model;
            model.Page = this;
            _model = model;
#if DEBUG
            // [daniel] estas lineas solo se ejecutaran cuando la aplicacion este en modo debug
            EntryOne.Text = "asesorleno01";
            EntryTwo.Text = "123456";
#endif
            ValidateDevice.Validate("Init","LoginPage");
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            _model.OnLoginClick(new Dictionary<string, string>
            {
                { _model.EntryOne, EntryOne.Text ?? "" },
                { _model.EntryTwo, EntryTwo.Text ?? "" }
            });
        }
        
        public async void ProgressAction(AsyncCommand command)
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
            {
                StackProgress.IsVisible = true;
                GridContent.IsVisible = false;
            });

            if (command != null)
            {
                await command.ExecuteAsync(null);
            }

            Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
            {
                StackProgress.IsVisible = false;
                GridContent.IsVisible = true;
            });
        }
    }
}
