﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetalleIndicadorPage : ContentPage
	{
        private Indicador _Indicador;

        public DetalleIndicadorPage()
        {
            InitializeComponent();
            _Indicador = new Indicador();
            GetData(_Indicador);
        }

		public DetalleIndicadorPage (Indicador indicador)
		{
			InitializeComponent ();
            this._Indicador = indicador;
            GetData(indicador);
 		}

        public void GetData(Indicador indicador)
        {
            lblSemaforo.Text = indicador.TextoIndicador == null ? indicador.Semaforo: indicador.TextoIndicador;
            clSemaforo.Color = Color.FromHex(indicador.Color == null ? "#000000" : indicador.Color);
            lblCalificacion.Text = indicador.NumeroIndicador.ToString();
            lblFecha.Text = indicador.Fecha.ToString("dd-MM-yyyy");
            var catInd = App.DB.CatalogoIndicador.FirstOrDefault(ci => ci.IdCatalogoIndicador == indicador.IdCatalogoIndicador);

            if (catInd.ValorMinimo.HasValue)
            {
                slSemaforoMinimo.IsVisible = true;
                bvSemaforoMinimo.BackgroundColor = Color.FromHex(catInd.ColorMinimo == null ? "#000000" : catInd.ColorMinimo);
                lblValorMinimo.Text = catInd.ValorMinimo.Value.ToString();
                lblSemaforoMinimo.Text = catInd.SemaforoMinimo;
            }
            if (catInd.ValorMedio.HasValue)
            {
                slSemaforoMedio.IsVisible = true;
                bvSemaforoMedio.BackgroundColor = Color.FromHex(catInd.ColorMedio == null ? "#000000" : catInd.ColorMedio);
                lblValorMedio.Text = catInd.ValorMedio.Value.ToString();
                lblSemaforoMedio.Text = catInd.SemaforoMedio;
            }
            if (catInd.ValorAlto.HasValue)
            {
                slSemaforoAlto.IsVisible = true;
                bvSemaforoAlto.BackgroundColor = Color.FromHex(catInd.ColorAlto == null ? "#000000" : catInd.ColorAlto);
                lblValorAlto.Text = catInd.ValorAlto.Value.ToString();
                lblSemaforoAlto.Text = catInd.SemaforoAlto;
            }
        }
	}
}