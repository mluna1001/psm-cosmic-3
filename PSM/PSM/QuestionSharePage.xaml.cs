﻿using Newtonsoft.Json;
using PSM.Controls;
using PSM.DeviceSensors;
using PSM.Function;
using PSM.Helpers;
using PSM.ORM;
using PSM.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuestionSharePage : ContentPage
    {
        public static event EventHandler<SavePage> SavePage;
        public bool EditAnswers { get; set; }
        Button btnsave { get; set; }
        public static void OnSavePage(SavePage page)
        {
            if (SavePage != null)
            {
                SavePage.Invoke(null, page);
            }
        }

        /// <summary>
        /// Lista de presentaciones o empaques
        /// </summary>
        private List<Producto> _presentaciones;

        /// <summary>
        /// Lista de preguntas
        /// </summary>
        private List<PreguntaModel> _models;

        public bool _hallazgo { get; set; }
        private bool _promo { get; set; }

        public QuestionSharePage(List<Producto> presentaciones, HallazgoPackageModel hallazomodel = null, bool promo = false)
        {
            InitializeComponent();
            App.CurrentPage = this;
            _presentaciones = presentaciones;
            _hallazgo = hallazomodel != null;
            _promo = promo;

            if (_hallazgo)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    GridHallazgo.IsVisible = true;
                });
                hallazomodel.Page = this;
                BindingContext = hallazomodel;
                SaveAnswers.IsVisible = false;
            }
            else
            {
                // StartToolBar();
            }

            Init();
        }

        private void StartToolBar()
        {
            ToolbarItem salvarseccion = new ToolbarItem
            {
                Icon = "diskette.png",
                Priority = 0,
                Order = ToolbarItemOrder.Primary,
                Text = "Salvar respuestas"
            };
            salvarseccion.Clicked += Salvarseccion_Clicked;
            ToolbarItems.Add(salvarseccion);
        }

        private void Salvarseccion_Clicked(object sender, EventArgs e)
        {
            btnsave = sender as Button;
            btnsave.IsEnabled = false;
            ValidateDevice.Validate("SaveSection_Clicked", "QuestionSharePage");
            SaveSection();
        }

        public void SaveSection()
        {
            OnRespuestaAuditorComplete -= PreciosPage_OnRespuestaAuditorComplete;
            OnRespuestaAuditorComplete += PreciosPage_OnRespuestaAuditorComplete;
            RespuestasAuditorProcess();
        }

        private async void Init()
        {
            // verificamos que la seccion actual no sea null
            if (App.CurrentSection == null) return;
            // obtenemos todas las preguntas de esta seccion [_seccion]
            _models = PreguntaHelper.GetPreguntas();
            // si la sección no tiene preguntas, regresamos a la pagina anterior
            if (_models.Count == 0)
            {
                await DisplayAlert("PSM 360", "No hay preguntas para esta sección", "Aceptar");
                if (!_hallazgo)
                {
                    await Navigation.PopAsync();
                }
                return;
            }
            // verificamos que las presentaciones no sean nullas
            if (_presentaciones != null)
            {
                var presentacionpregunta = new List<PreguntaModel>();
                foreach (var presentacion in _presentaciones)
                {
                    foreach (var model in _models)
                    {
                        var preguntamodel = new PreguntaModel
                        {
                            Id = presentacion.IdProducto,
                            IdPregunta = model.IdPregunta,
                            PreguntaPadre = model.PreguntaPadre,
                            PreguntasHijas = model.PreguntasHijas,
                            IdSeccion = model.IdSeccion,
                            IdRespuesta = model.IdRespuesta,
                            Desencadenada = model.Desencadenada,
                            File = model.File,
                            FileName = model.FileName,
                            Respuesta = model.Respuesta,
                            Respuestas = model.Respuestas,
                            View = model.View
                        };

                        var respuestaauditor = App.DB.RespuestaAuditor.FirstOrDefault(ra => ra.IdRuta == App.CurrentRoute.IdRuta && ra.IdSeccion == App.CurrentSection.IdSeccion && ra.IdRespuesta == preguntamodel.IdRespuesta && ra.IdProducto == preguntamodel.Id);
                        if (respuestaauditor != null)
                        {
                            EditAnswers = true;
                            preguntamodel.Respuesta = respuestaauditor.Respuesta;
                            preguntamodel.IdRespuesta = respuestaauditor.IdRespuesta;
                            preguntamodel.IdRespuestaAuditor = respuestaauditor.IdAnswerAudit;
                        }
                        presentacionpregunta.Add(preguntamodel);
                    }
                }
                _models = presentacionpregunta;
                // asignamos las preguntas a los empaques
                SetPreguntas(_models);
            }
            else
            {
                await DisplayAlert("PSM 360", "No hay presentaciones para esta sección", "Aceptar");
                if (!_hallazgo)
                {
                    await Navigation.PopAsync();
                }
            }
        }

        private async void PreciosPage_OnRespuestaAuditorComplete(object sender, RespuestaAuditorComplete e)
        {
            if (!e.IsSuccess)
            {
                await DisplayAlert("Info", e.Message, "Aceptar");
            }
            else
            {
                bool respuestas = false;
                try
                {
                    var dateofregister = DateTime.Now;
                    if (e.Edit)
                    {
                        foreach (var respuesta in e.Respuestas)
                        {
                            respuesta.Fecha = dateofregister;
                            App.DB.Entry(respuesta).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                            App.DB.SaveChanges();
                        }
                        respuestas = true;
                    }
                    else
                    {
                        foreach (var respuesta in e.Respuestas)
                        {
                            respuesta.Fecha = dateofregister;
                            App.DB.RespuestaAuditor.Add(respuesta);
                        }
                        respuestas = App.DB.SaveChanges();
                    }

                    if (respuestas)
                    {
                        if (!_hallazgo)
                        {
                            await DisplayAlert("PSM 360", "Se han guardado las respuestas", "Aceptar");
                            await Navigation.PopAsync();
                        }
                        else
                        {
                            OnSavePage(new SavePage
                            {
                                Page = this,
                                Success = true
                            });
                        }
                    }
                    else
                    {
                        if (!_hallazgo)
                        {
                            await DisplayAlert("PSM 360", "Ocurrio un error al guardar las respuestas, intenta más tarde", "Aceptar");
                            await Navigation.PopAsync();
                        }
                        else
                        {
                            OnSavePage(new SavePage
                            {
                                Page = this,
                                Success = false
                            });
                        }
                    }
                }
                catch
                {
                    await DisplayAlert("PSM 360", "Ocurrio un error al guardar las respuestas, intenta más tarde", "Aceptar");
                    if (!_hallazgo)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        OnSavePage(new SavePage
                        {
                            Page = this,
                            Success = false
                        });
                    }
                }
            }
        }

        private void RespuestasAuditorProcess()
        {
            List<RespuestaAuditor> respuestas = new List<RespuestaAuditor>();
            List<Foto> photos = new List<Foto>();
            bool notcomplete = false;
            int location = App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion : 0;
            foreach (var model in _models)
            {
                if (model.PreguntaPadre.IdControl == 1 || model.PreguntaPadre.IdControl == 4 || model.PreguntaPadre.IdControl == 5 || model.PreguntaPadre.IdControl == 7 || model.PreguntaPadre.IdControl == 3)
                {
                    if (model.PreguntaPadre.SeValida.HasValue && model.PreguntaPadre.SeValida.Value)
                    {
                        if (string.IsNullOrEmpty(model.Respuesta))
                        {
                            OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                            {
                                IsSuccess = false,
                                Message = "Ingresa una respuesta en la pregunta " + model.PreguntaPadre.Descripcion
                            });
                            notcomplete = true;
                            btnsave.IsEnabled = true;
                            break;
                        }
                    }

                    if (model.IdRespuestaAuditor != null)
                    {
                        var respuestaauditorindb = App.DB.RespuestaAuditor.FirstOrDefault(ra => ra.IdAnswerAudit == model.IdRespuestaAuditor);
                        if (respuestaauditorindb != null)
                        {
                            respuestaauditorindb.Respuesta = model.Respuesta;
                            respuestas.Add(respuestaauditorindb);
                        }
                    }
                    else
                    {
                        RespuestaAuditor answer = new RespuestaAuditor
                        {
                            IdProducto = model.Id,
                            IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                            IdRespuesta = model.IdRespuesta,
                            IdRuta = App.CurrentRoute.IdRuta,
                            IdSeccion = model.IdSeccion,
                            IdUbicacion = location,
                            NumerodeCaja = 0, // falta ese elemento
                            Respuesta = model.Respuesta,
                            Foto = model.Foto,
                            FotoNombre = model.FotoNombre
                        };
                        respuestas.Add(answer);
                    }
                }
                else if (model.PreguntaPadre.IdControl == 2)
                {
                    foreach (var item in model.Respuestas)
                    {
                        if (model.IdRespuestaAuditor != null)
                        {
                            var respuestaauditorindb = App.DB.RespuestaAuditor.FirstOrDefault(ra => ra.IdAnswerAudit == model.IdRespuestaAuditor);
                            if (respuestaauditorindb != null)
                            {
                                respuestaauditorindb.Respuesta = item.Valor.HasValue && item.Valor.Value == 1 ? "1" : "0";
                                respuestas.Add(respuestaauditorindb);
                            }
                        }
                        else
                        {
                            RespuestaAuditor answer = new RespuestaAuditor
                            {
                                IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                                IdRespuesta = item.IdRespuesta,
                                IdRuta = App.CurrentRoute.IdRuta,
                                IdSeccion = model.IdSeccion,
                                IdUbicacion = location, // falta este elemtno
                                NumerodeCaja = 0, // falta ese elemento
                                Respuesta = item.Valor.HasValue && item.Valor.Value == 1 ? "1" : "0",
                                IdProducto = model.Id
                            };
                            respuestas.Add(answer);
                        }
                    }
                }
                else if (model.PreguntaPadre.IdControl == 6)
                {
                    if (model.PreguntaPadre.SeValida.HasValue && model.PreguntaPadre.SeValida.Value)
                    {
                        if (model.File == null)
                        {
                            OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                            {
                                IsSuccess = false,
                                Message = "Toma la foto para la pregunta " + model.PreguntaPadre.Descripcion
                            });
                            notcomplete = true;
                            btnsave.IsEnabled = true;
                            break;
                        }
                    }

                    if (model.IdRespuestaAuditor != null)
                    {
                        var respuestaauditorindb = App.DB.RespuestaAuditor.FirstOrDefault(ra => ra.IdAnswerAudit == model.IdRespuestaAuditor);
                        if (respuestaauditorindb != null)
                        {
                            respuestaauditorindb.Respuesta = model.Respuesta;
                            respuestas.Add(respuestaauditorindb);
                        }
                    }
                    else
                    {
                        RespuestaAuditor respuestaAuditor = new RespuestaAuditor
                        {
                            IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                            IdRespuesta = model.IdRespuesta,
                            IdRuta = App.CurrentRoute.IdRuta,
                            IdSeccion = model.IdSeccion,
                            IdUbicacion = location,
                            NumerodeCaja = 0, // falta ese elemento
                            Respuesta = model.Respuesta,
                            Foto = model.Foto,
                            FotoNombre = model.FotoNombre,
                            IdProducto = model.Id
                        };
                        respuestas.Add(respuestaAuditor);
                    }
                }
            }

            if (!notcomplete)
            {
                OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                {
                    IsSuccess = true,
                    Edit = EditAnswers,
                    Respuestas = respuestas
                });
            }
        }

        private event EventHandler<RespuestaAuditorComplete> OnRespuestaAuditorComplete;

        private void OnRespuestaAuditorCompleted(RespuestaAuditorComplete answer)
        {
            if (OnRespuestaAuditorComplete != null)
            {
                OnRespuestaAuditorComplete(this, answer);
            }
        }

        private void SetPreguntas(List<PreguntaModel> models)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                RootLayout.Children.Clear();
                foreach (var model in _models)
                {
                    var presentacion = _presentaciones.FirstOrDefault(p => p.IdProducto == model.Id);
                    if (presentacion != null)
                    {
                        Label label = new Label
                        {
                            Text = presentacion.Nombre
                        };
                        RootLayout.Children.Add(label);

                        var pregunta = model.PreguntaPadre;
                        Label labelpregunta = new Label
                        {
                            Text = pregunta.Descripcion,
                            TextColor = Color.Black
                        };

                        RootLayout.Children.Add(labelpregunta);
                        switch (pregunta.IdControl)
                        {
                            case 1:
                            case 5:
                                Entry entry = new Entry
                                {
                                    Placeholder = "Ingresa tu respuesta"
                                };
                                if (pregunta.IdControl != 1)
                                {
                                    entry.Keyboard = Keyboard.Numeric;
                                }
                                entry.ClassId = model.GetHashCode().ToString();
                                entry.TextChanged += Entry_TextChanged;
                                model.View = entry;

                                // Asignamos los datos de respuesta realizados antes de cualquier cambio en la UI
                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    entry.Text = model.Respuesta;
                                }
                                RootLayout.Children.Add(label);
                                RootLayout.Children.Add(entry);

                                break;

                            case 7:
                                EntryCurrency currency = new EntryCurrency();
                                currency.ClassId = model.GetHashCode().ToString();
                                currency.EntryCurrencyTextChanged += Currency_EntryCurrencyTextChanged;
                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    currency.Text = model.Respuesta.TextToMoney();
                                }
                                else
                                {
                                    if (_promo)
                                    {
                                        if (presentacion.Precio.HasValue)
                                        {
                                            var textmoney = string.Format(CultureInfo.CurrentCulture, "{0:F2}", presentacion.Precio.Value);
                                            currency.Text = textmoney.TextToMoney();
                                            model.Respuesta = currency.Text.MoneyToText();
                                        }
                                    }
                                }
                                model.View = currency;
                                RootLayout.Children.Add(currency);
                                break;

                            case 9:
                                EntryCurrency currencypromo = new EntryCurrency();
                                currencypromo.ClassId = model.GetHashCode().ToString();
                                currencypromo.EntryCurrencyTextChanged += Currency_EntryCurrencyTextChanged;
                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    currencypromo.Text = model.Respuesta.TextToMoney();
                                }
                                else
                                {
                                    if (_promo)
                                    {
                                        if (presentacion.PrecioPromo.HasValue)
                                        {
                                            var textmoney = string.Format(CultureInfo.CurrentCulture, "{0:F2}", presentacion.PrecioPromo.Value);
                                            currencypromo.Text = textmoney.TextToMoney();
                                            model.Respuesta = currencypromo.Text.MoneyToText();
                                        }
                                    }
                                }
                                model.View = currencypromo;
                                RootLayout.Children.Add(currencypromo);
                                break;

                            case 2:
                                var respuestascheck = App.DB.Respuesta.Where(e => e.IdPregunta == model.IdPregunta).ToList();
                                TableRoot root = new TableRoot();
                                TableView table = new TableView(root) { Intent = TableIntent.Form };
                                table.HeightRequest = respuestascheck.Count * 55;
                                TableSection section = new TableSection();
                                if (model.Respuestas == null)
                                {
                                    model.Respuestas = new List<Respuesta>();
                                }
                                foreach (var respuesta in respuestascheck)
                                {
                                    int respuestaint = 0;
                                    bool respuestabool = false;
                                    if (model.Respuestas != null)
                                    {
                                        // buscamos la respuesta previamente realizada
                                        var respuestafound = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                                        // si la respuesta existe
                                        if (respuestafound != null)
                                        {
                                            // parseamos a boolean
                                            if (respuestafound.Valor.HasValue)
                                            {
                                                respuestaint = (int)respuestafound.Valor.Value;
                                            }
                                            respuestabool = respuestaint == 1;
                                        }
                                    }
                                    SwitchCell sview = new SwitchCell
                                    {
                                        Text = respuesta.TextoOpcion,
                                        On = respuestabool,
                                        ClassId = respuesta.IdRespuesta.ToString()
                                    };
                                    sview.OnChanged += Sview_OnChanged;
                                    section.Add(sview);
                                    if (model.Respuestas != null)
                                    {
                                        var respuestafound = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                                        if (respuestafound != null)
                                        {
                                            model.Respuestas.Remove(respuestafound);
                                        }
                                        respuesta.Valor = respuestaint;
                                        model.Respuestas.Add(respuesta);
                                    }
                                }
                                root.Add(section);
                                RootLayout.Children.Add(label);
                                RootLayout.Children.Add(table);
                                break;

                            case 3:
                                var respuestascombo = App.DB.Respuesta.Where(e => e.IdPregunta == pregunta.IdPregunta).ToList();
                                Picker picker = new Picker();
                                picker.ClassId = model.GetHashCode().ToString();

                                var answerdefault = respuestascombo.FirstOrDefault(e => e.ValorDefault.HasValue && e.ValorDefault.Value);
                                if (answerdefault != null)
                                {

                                }
                                else
                                {
                                    picker.Items.Add("-- Selecciona una opción --");
                                }

                                int indexdefault = 0;
                                for (var o = 0; o < respuestascombo.Count; o++)
                                {
                                    var respuesta = respuestascombo[o];
                                    if (answerdefault != null)
                                    {
                                        if (respuesta.IdRespuesta == answerdefault.IdRespuesta)
                                        {
                                            indexdefault = o;
                                        }
                                    }
                                    picker.Items.Add(respuesta.TextoOpcion);
                                }

                                // obtenemos el index de la respuesta actual
                                var selected = picker.Items.IndexOf(model.Respuesta);
                                // si el picker tiene más de un elemento, asignamos el index al primer elemento
                                if (picker.Items.Count > 0) { picker.SelectedIndex = 0; }
                                // si existe un index para la respuesta actual [post-render]
                                if (selected > -1)
                                {
                                    // asignamos la respuesta previa del usuario al picker
                                    picker.SelectedIndex = selected;
                                }
                                else
                                {
                                    // en caso contrario, de no existir un index de respuesta anterior, verificamos
                                    // si existe un default para la respuesta
                                    if (answerdefault != null)
                                    {
                                        // asignamos el index por default para la respuesta
                                        picker.SelectedIndex = indexdefault;
                                    }
                                }
                                picker.SelectedIndexChanged += Picker_SelectedIndexChanged;
                                model.View = picker;
                                RootLayout.Children.Add(label);
                                RootLayout.Children.Add(picker);
                                break;

                            case 4:
                                DatePicker datepicker = new DatePicker();
                                datepicker.ClassId = model.GetHashCode().ToString();

                                datepicker.Date = new DateTime(DateTime.Now.Year, 1, 1);
                                datepicker.DateSelected += Datepicker_DateSelected;
                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    datepicker.Date = DateTime.ParseExact(model.Respuesta, "yyyy-MM-dd", CultureInfo.CurrentCulture);
                                }
                                model.View = datepicker;
                                RootLayout.Children.Add(label);
                                RootLayout.Children.Add(datepicker);
                                break;

                            // foto
                            case 6:
                                Button btnfoto = new Button
                                {
                                    Text = "Tomar foto"
                                };
                                btnfoto.ClassId = model.GetHashCode().ToString();

                                btnfoto.Clicked += Btnfoto_Clicked;
                                RootLayout.Children.Add(label);
                                RootLayout.Children.Add(btnfoto);
                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    btnfoto.BackgroundColor = Color.Accent;
                                }
                                break;
                        }

                        /*
                        Button button = TakePicture(model);
                        if (button != null)
                        {
                            RootLayout.Children.Add(button);
                        }
                        */
                    }
                }
            });
        }

        private async void Btnfoto_Clicked(object sender, EventArgs e)
        {
            var btntakepicture = sender as Button;
            int hashcode = int.Parse(btntakepicture.ClassId);
            var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
            if (model != null)
            {
                var idpregunta = model.PreguntaPadre.IdPregunta;
                var filename = App.CurrentUser.Alias + "-" + (App.CurrentRoute.IdProyectoTienda == 0 ? "0" : App.CurrentRoute.IdProyectoTienda.ToString()) + "-" + model.IdSeccion + "-" + idpregunta + "-" + (App.CycleNumber == 0 ? "0" : App.CycleNumber.ToString()) + "-" + (App.CurrentPackage != null ? App.CurrentPackage.IdPresentacionEmpaque.ToString() : "0") + "-" + (App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion.ToString() : "0") + "-" + App.CurrentRoute.IdRuta + ".jpg";
                CameraCaptureTask task = new CameraCaptureTask
                {
                    FileName = filename,
                    FolderName = "PSM360Photos"
                };
                var result = await task.TakePhoto();
                if (!result.Success)
                {
                    await DisplayAlert("PSM 360", result.Message, "Aceptar");
                }
                else if (result.Photo != null)
                {
                    model.File = result;
                    btntakepicture.BackgroundColor = Color.Accent;
                }
            }
        }

        private void Datepicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            if (sender != null)
            {
                var obj = sender as DatePicker;
                int hashcode = int.Parse(obj.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (model != null)
                {
                    model.Respuesta = obj.Date.ToString("yyyy-MM-dd");
                    obj.BackgroundColor = Color.Accent;
                }
            }
        }

        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = sender as Picker;
            /*
            if (!string.IsNullOrEmpty(picker.ClassId))
            {
                int hashcode = int.Parse(picker.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                var respuesta = picker.Items[picker.SelectedIndex];
                if (model == null) return;
                model.Respuesta = respuesta;
                var respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.TextoOpcion == respuesta);
                if (respuestaindb == null) return;
                model.IdRespuesta = respuestaindb.IdRespuesta;
            }
            */

            if (!string.IsNullOrEmpty(picker.ClassId))
            {
                int hashcode = int.Parse(picker.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                var respuesta = picker.Items[picker.SelectedIndex];
                model.Respuesta = respuesta;
                var indexofmodel = _models.IndexOf(model);
                var respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.TextoOpcion == respuesta && r.IdPregunta == model.IdPregunta);
                if (respuestaindb != null)
                {
                    model.IdRespuesta = respuestaindb.IdRespuesta;
                    if (indexofmodel > -1)
                    {
                        if (respuestaindb.Desencadena)
                        {
                            if (model.PreguntaPadre.TieneHijos)
                            {
                                foreach (var hijo in model.PreguntasHijas)
                                {
                                    hijo.Id = model.Id;
                                }
                                model.Desencadenada = true;
                                _models.InsertRange(indexofmodel + 1, model.PreguntasHijas);
                                SetPreguntas(_models);
                            }
                        }
                        else if (model.Desencadenada)
                        {
                            model.Desencadenada = false;
                            foreach (var preguntahija in model.PreguntasHijas)
                            {
                                if (preguntahija.Desencadenada)
                                {
                                    var pickerhija = preguntahija.View as Picker;
                                    pickerhija.SelectedIndex = 0;
                                }
                            }
                            _models.RemoveRange(indexofmodel + 1, model.PreguntasHijas.Count);
                            SetPreguntas(_models);
                        }
                    }
                }
                else
                {
                    if (model.Desencadenada)
                    {
                        model.Desencadenada = false;
                        foreach (var preguntahija in model.PreguntasHijas)
                        {
                            if (preguntahija.Desencadenada)
                            {
                                var pickerhija = preguntahija.View as Picker;
                                pickerhija.SelectedIndex = 0;
                            }
                        }
                        _models.RemoveRange(indexofmodel + 1, model.PreguntasHijas.Count);
                        SetPreguntas(_models);
                    }
                }
            }
        }

        private void Sview_OnChanged(object sender, ToggledEventArgs e)
        {
            var switchcell = sender as SwitchCell;
            int idrespuesta = int.Parse(switchcell.ClassId);
            var respuesta = App.DB.Respuesta.FirstOrDefault(r => r.IdRespuesta == idrespuesta);
            var model = _models.FirstOrDefault(m => m.IdPregunta == respuesta.IdPregunta);
            respuesta.Valor = e.Value ? 1 : 0;
            if (model.Respuestas != null)
            {
                var respuestaenmodel = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                if (respuestaenmodel != null)
                {
                    model.Respuestas.Remove(respuestaenmodel);
                }
                model.Respuestas.Add(respuesta);
            }
            else
            {
                model.Respuestas = new List<Respuesta> { respuesta };
            }
        }

        private async void Btntakepicture_Clicked(object sender, EventArgs e)
        {
            var btntakepicture = sender as Button;
            var modelatsender = JsonConvert.DeserializeObject<PreguntaModel>(btntakepicture.ClassId);
            var model = _models.FirstOrDefault(m => m.Equals(modelatsender));
            if (model != null)
            {
                var idpregunta = model.PreguntaPadre.IdPregunta;
                var filename = App.CurrentUser.Alias + "-" + (App.CurrentRoute.IdProyectoTienda == 0 ? "0" : App.CurrentRoute.IdProyectoTienda.ToString()) + "-" + model.IdSeccion + "-" + idpregunta + "-" + (App.CycleNumber == 0 ? "0" : App.CycleNumber.ToString()) + "-" + (App.CurrentPackage != null ? App.CurrentPackage.IdPresentacionEmpaque.ToString() : "0") + "-" + (App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion.ToString() : "0") + "-" + App.CurrentRoute.IdRuta + ".jpg";
                CameraCaptureTask task = new CameraCaptureTask
                {
                    FileName = filename,
                    FolderName = "PSM360Photos"
                };
                var result = await task.TakePhoto();
                if (!result.Success)
                {
                    await DisplayAlert("PSM 360", result.Message, "Aceptar");
                }
                else if (result.Photo != null)
                {
                    model.File = result;
                    btntakepicture.BackgroundColor = Color.Accent;
                }
            }
        }

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                var obj = sender as Entry;
                int hashcode = int.Parse(obj.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (model != null)
                {
                    model.Respuesta = obj.Text;
                    obj.BackgroundColor = Color.Accent;
                }
            }
        }

        private void Currency_EntryCurrencyTextChanged(object sender, EntryCurrencyTextChanged e)
        {
            if (sender != null)
            {
                var obj = sender as EntryCurrency;
                int hashcode = int.Parse(obj.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (model != null)
                {
                    model.Respuesta = e.Currency;
                    obj.BackgroundColor = Color.Accent;
                }
            }
        }

        private void BtnSaveAnswers_Clicked(object sender, EventArgs e)
        {
            btnsave = sender as Button;
            btnsave.IsEnabled = false;
            SaveSection();
        }
    }
}