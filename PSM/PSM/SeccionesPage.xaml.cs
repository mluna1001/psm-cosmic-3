﻿using Microsoft.AppCenter.Crashes;
using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM
{
    public partial class SeccionesPage : ContentPage
    {

        public ObservableCollection<Seccion> Secciones { get; set; }
        public bool _faltante { get; set; }
        public SeccionesPage(bool faltante = false)
        {
            _faltante = faltante;
            App.CurrentPage = this;
            InitializeComponent();
            ListSecciones.ItemSelected += ListSecciones_ItemSelected;
            ListSecciones.ItemTapped += ListSecciones_ItemTapped;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ListSecciones.IsEnabled = true;
            stackloader.IsVisible = false;
            stackrender.IsVisible = true;
            InitPage();
        }

        private void InitPage()
        {
            Secciones = new ObservableCollection<Seccion>();
            ListSecciones.ItemsSource = Secciones;
            ListSeccionesFaltantes.ItemsSource = Secciones;
            if (_faltante)
            {
                //var seccionesavalidar = App.DB.Seccion.Where(e => e.EsSeccion && e.EsActiva && e.SeValida).OrderBy(e => e.Orden).ToList();
                var seccionesavalidar = App.AllSections.Where(e => e.EsSeccion && e.EsActiva && e.SeValida).OrderBy(e => e.Orden).ToList();
                var idseccionesfaltantes = seccionesavalidar.Select(e => e.IdSeccion).ToList();
                var seccionescontestadas = App.DB.RespuestaAuditor.Where(e => e.IdRuta == App.CurrentRoute.IdRuta).GroupBy(e => e.IdSeccion).Select(e => e.First()).Select(e => e.IdSeccion).ToList();
                var seccionesfaltantes = idseccionesfaltantes.Where(e => !seccionescontestadas.Contains(e));
                var items = seccionesavalidar.Where(e => seccionesfaltantes.Contains(e.IdSeccion)).ToList();
                ListSeccionesFaltantes.IsVisible = true;
                ListSecciones.IsVisible = false;
                PopulateSecciones(items);
                ImagenFondo.Source = ImageSource.FromFile("fondonaranja.jpg");
                StackFaltantes.IsVisible = true;
            }
            else
            {
                ListSeccionesFaltantes.IsVisible = false;
                ListSecciones.IsVisible = true;
                var items = App.DB.Seccion.Where(e => e.EsSeccion && e.EsActiva).OrderBy(e => e.Orden).ToList();
                Usuario user = App.CurrentUser;
                List<Seccion> filterSections = new List<Seccion>();

                filterSections = LlenaSeccionCadena(items);
                filterSections = LlenaSeccionRegion(filterSections);

                App.AllSections = filterSections;

                PopulateSecciones(filterSections);
            }
            ToolBar();
        }

        private List<Seccion> LlenaSeccionCadena(List<Seccion> items)
        {
            List<Seccion> seccCads = new List<Seccion>();

            try
            {
                foreach (var seccion in items)
                {
                    var secCad = App.DB.SeccionCadena.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion);
                    if (secCad == null)
                    {
                        seccCads.Add(seccion);
                    }
                    else
                    {
                        Ruta r = App.CurrentRoute;
                        InfoTienda tienda = App.DB.InfoTienda.Where(x => x.IdTienda == r.IdTienda).FirstOrDefault();

                        var secCad2 = App.DB.SeccionCadena.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion & s.IdCadena == tienda.IdCadena);

                        if (secCad2 != null)
                        {
                            if (secCad2.IdCadena == tienda.IdCadena)
                            {
                                seccCads.Add(seccion);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
                seccCads = items;
            }

            return seccCads;
        }

        public List<Seccion> LlenaSeccionRegion(List<Seccion> items)
        {
            List<Seccion> seccRegs = new List<Seccion>();

            try
            {
                foreach (var seccion in items)
                {
                    var secReg = App.DB.SeccionRegion.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion);
                    if (secReg == null)
                    {
                        seccRegs.Add(seccion);
                    }
                    else
                    {
                        Ruta r = App.CurrentRoute;
                        Usuario usuario = App.CurrentUser;

                        var secReg2 = App.DB.SeccionRegion.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion && s.IdRegion == usuario.IdRegion);

                        if (secReg2 != null)
                        {
                            if (secReg2.IdRegion == usuario.IdRegion)
                            {
                                seccRegs.Add(seccion);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
                seccRegs = items;
            }

            return seccRegs;
        }

        private void PopulateSecciones(List<Seccion> items)
        {
            if (items == null) return;
            foreach (var item in items)
            {
                if (item.IdCatalogoSeccion == 1)
                {
                    bool seEdita = false;

                    if (item.SeEdita != null)
                        seEdita = (bool)item.SeEdita;

                    if (!seEdita)
                    {
                        var fuecontestadalaseccion = App.DB.RespuestaAuditor.Count(ra => ra.IdSeccion == item.IdSeccion && ra.IdRuta == App.CurrentRoute.IdRuta) > 0;
                        if (fuecontestadalaseccion)
                        {
                            continue;
                        }
                    }
                }
                else if (item.IdCatalogoSeccion == 11)
                {
                    continue;
                }
                Secciones.Add(item);
            }
        }

        private void ToolBar()
        {
            if (ToolbarItems.Count == 0)
            {
                // El icono de la tabbar se programo acá debido a que en XAML no hay compatibilidad [VS2015]
                var toolbaritem = new ToolbarItem
                {
                    Text = "Secciones faltantes",
                    Order = ToolbarItemOrder.Primary
                };
                toolbaritem.Clicked += SeccionesFaltantes_Clicked;
                ToolbarItems.Add(toolbaritem);
            }
        }

        private async void SeccionesFaltantes_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SeccionesPage(true));
        }

        private async void ListSecciones_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            StartDownload("Cargando Información...");
            await Task.Delay(500);
            Seccion seccion = e.Item as Seccion;
            ListSecciones.IsEnabled = false;
            if (seccion != null)
            {
                var catalogoseccion = App.DB.CatalogoSeccion.ToList();
                var tipodeseccion = catalogoseccion.FirstOrDefault(c => c.IdCatalogoSeccion == seccion.IdCatalogoSeccion);
                if (tipodeseccion != null)
                {
                    App.CurrentSection = seccion;
                    switch (tipodeseccion.Tipo())
                    {
                        case TipoSeccion.HallazgoProducto:
                        case TipoSeccion.PrecioPromo:
                        case TipoSeccion.Precio:
                            await Navigation.PushAsync(new PackagePage(tipodeseccion.Tipo()) { Title = seccion.Descripcion });
                            break;
                        case TipoSeccion.CalificacionProducto:
                            await Navigation.PushAsync(new PackagePage(tipodeseccion.Tipo()) { Title = seccion.Descripcion });
                            break;
                        case TipoSeccion.CodigoBarra:
                            await Navigation.PushAsync(new BarcodePage() { Title = seccion.Descripcion });
                            break;
                        default:
                            try
                            {
                                Type typepage = Type.GetType($"PSM.{seccion.TargetType},PSM");
                                var page = (Page)Activator.CreateInstance(typepage);
                                page.Title = seccion.Descripcion;
                                await Navigation.PushAsync(page);
                            }
                            catch (Exception ex)
                            {
                                await DisplayAlert("PSM 360", "No podemos abrir esta seccion, contacta con tu supervisor.", "Aceptar");
                            }
                            break;
                    }
                }
                else
                {
                    await DisplayAlert("PSM 360", "La seccion no tiene un catalogo asignado", "Aceptar");
                }
            }
            else
            {
                await DisplayAlert("PSM 360", "No podemos abrir esta seccion, contacta con tu supervisor.", "Aceptar");
            }
        }

        private void ListSecciones_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ListSecciones.SelectedItem = null;
        }

        private void StartDownload(string message)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                stackrender.IsVisible = false;
                stackloader.IsVisible = true;
                ProgressDownloadIndicator.IsVisible = true;
                ProgressDownloadIndicator.IsRunning = true;
                ProgressDownloadText.Text = message;
            });


        }

        private void EndDownload()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                stackrender.IsVisible = true;
                ProgressDownloadIndicator.IsVisible = false;
                ProgressDownloadIndicator.IsRunning = false;
                ProgressDownloadText.Text = "";
            });

        }
    }
}
