﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ChatApp;
using ChatApp.ORM;
using Xamarin.Forms;
using static ChatApp.ChatClient;
using Microsoft.AspNet.SignalR.Client;
using UIKit;

namespace ChatApp.iOS
{
    public class ChatInit
    {

        public static Userprofile CurrentUser { get; set; }
        public static ChatDataBase DB { get; set; }
        public static List<ChatBubbleView> UserChats { get; set; }
        public static ChatClient Client { get; set; }
        public static Userprofile UserSelectedForChat { get; set; }
        public string IdChat { get; set; }
        public static List<ChatGrupo> Grupos { get; set; }

        public static void SetGrupos(List<ChatGrupo> grupos)
        {
            Grupos = grupos;
        }

        public static void Init(Userprofile userprofile = null, List<ChatGrupo> grupos = null)
        {
            // iniciamos la base de datos
            if (DB == null) DB = ChatDataBase.Instance;
            // Inuiciamos el cliente del chat
            if (Client == null) Client = new ChatClient();
            // si el usuario es null cargamos al usuario local
            if (userprofile == null)
            {
                LoadLocalUser();
            }
            else
            {
                CurrentUser = userprofile;
            }

            if (grupos != null) Grupos = grupos;

            if (CurrentUser != null)
            {
                GetChats();
                ChatsPendientes();
            }
        }

        private static async void GetChats()
        {
            // obtenemos los chats
            var usersresult = await Client.GetUsers(CurrentUser.Alias);
            if (usersresult != null)
            {
                // insertamos los usuarios en la base de datos
                DB.InsertUsers(usersresult.GetUsers());
                // cargamos la lista
                UserChats = usersresult.GetUserAsChats();
            }
        }

        public static void LoadLocalUser()
        {
            // nos conectamos a la base de datos local para obtener al usuario actual
            var connection = new PSM.iOS.LiteConnection.Connection();
            // obtenemos la base de datos local
            var psmdb = connection.GetDataBase();
            // buscamos al usuario local
            var currentuser = psmdb.Usuario.FirstOrDefault();
            // si el usuario existe ....
            if (currentuser != null)
            {
                // asignamos al usuario a una variable global
                CurrentUser = new Userprofile
                {
                    Alias = currentuser.Alias,
                    IdUsuario = currentuser.IdUsuario,
                    Descripcion = "Usuario",
                    Nombre = currentuser.Alias
                };
            }
            else
            {
                CurrentUser = null;
            }
        }

        private static async void ChatsPendientes()
        {
            /*
            Task.Run(async () =>
            { */
            // creamos el cliente del chat
            ChatClient client = new ChatClient();
            // descargamos los chats pendientes
            var chats = await client.ChatsPendientes(CurrentUser.IdUsuario);
            // verificamos que los chats sean diferentes de null
            if (chats != null)
            {
                // iteramos los chats como un valor de la base de datos
                foreach (var item in chats.GetAsChat())
                {
                    try
                    {
                        // verificamos que el chat sea de un miembro del proyecto
                        var userfound = ChatInit.DB.GetUser(item.IdUsuarioOrigen.ToString());
                        // el chat es de un miembro del proyecto
                        if (userfound != null)
                        {
                            // si el usuario de destino soy yo, entonces ....
                            if (item.IdUsuarioDestino == CurrentUser.IdUsuario)
                            {
                                // insertamos el chat en la base de datos
                                DB.InsertChat(item);
                                // notificamos de recivido
                                var receivedchat = await client.NotifyReceived(item.IdChat.ToString());
                                // si se recivio entonces mandamos la notificación
                                if (receivedchat != null)
                                {
                                    // si se actualizo mandamos la notificación
                                    if (receivedchat.Updated)
                                    {
                                        // modificamos el elemento diciendo que si se ha recivido
                                        item.Entregado = true;
                                        // actualizamos en la base de datos
                                        DB.UpdateChat(item);
                                        // enviamos la notificación
                                        SendNotification(item.Mensaje, item.IdChat.ToString(), item.IdUsuarioDestino.ToString(), userfound);
                                    }
                                }
                            }
                            else
                            {
                                // el chat no es para mi
                            }
                        }
                        else
                        {
                            // el chat no es de un usuario del proyecto
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    }
                }
            }
            // });
        }

        public static void SendNotificationGroup(ChatMensaje chatmensaje)
        {
            var notification = new UILocalNotification();
            notification.AlertAction = "View Alert";
            notification.AlertTitle = "Tienes un nuevo mensaje";
            notification.AlertBody = chatmensaje.Mensaje;
            var actualnumber = notification.ApplicationIconBadgeNumber;
            notification.ApplicationIconBadgeNumber = actualnumber++;
            notification.SoundName = UILocalNotification.DefaultSoundName;
            UIApplication.SharedApplication.ScheduleLocalNotification(notification);
        }

        public static void SendNotification(string message, string idchat, string localidchat, UserChat userfound)
        {
            var notification = new UILocalNotification();
            notification.AlertAction = "View Alert";
            notification.AlertTitle = userfound.Nombre;
            notification.AlertBody = message;
            var actualnumber = notification.ApplicationIconBadgeNumber;
            notification.ApplicationIconBadgeNumber = actualnumber++;
            notification.SoundName = UILocalNotification.DefaultSoundName;
            UIApplication.SharedApplication.ScheduleLocalNotification(notification);
        }

        public static void Send(ORM.Chat chat)
        {
            VerifyHubConnection();
            if (HubConnection == null)
            {
                StartHubConnection();
            }

            if (HubProxy != null)
            {
                // obtnemos la base de datos
                ChatDataBase db = ChatDataBase.Instance;
                // si el chat ya existe en la base local, quiere decir que ya se ha recibido
                // if (db.Chat.Exists(e => e.IdChat == int.Parse(idchat))) return;
                // insertamos el chat a enviar
                db.InsertChat(chat);
                // enviamos el chat al usuario
                HubProxy.Invoke("Send", chat.Mensaje, chat.IdUsuarioOrigen, chat.IdUsuarioDestino);
            }
        }


        public static void SendToGroup(ChatMensaje chatmensaje)
        {
            VerifyHubConnection();
            if (HubConnection != null)
            {
                StartHubConnection();
            }

            if (HubProxy != null)
            {
                HubProxy.Invoke("SendToGroup", chatmensaje.IdChatGrupo, chatmensaje.IdProyecto, chatmensaje.IdUsuario, chatmensaje.Mensaje);
            }
        }

        public static HubConnection HubConnection
        {
            get; set;
        }

        public static IHubProxy HubProxy { get; set; }

        private static VisibilityState _appstate = VisibilityState.Background;
        public static VisibilityState AppState
        {
            get
            {
                return _appstate;
            }

            set
            {
                _appstate = value;
            }
        }

        public static void StartHubConnection()
        {
            if (HubConnection != null)
            {
            }
            else
            {
                HubConnection = new HubConnection($"{ChatConfig.BaseUrl}");
                HubProxy = HubConnection.CreateHubProxy("ChatHub");
                HubProxy.On<string, string, string, string>("ReceiveMessage", ReceiveMessage);
                HubProxy.On<string, string, string, string, string, string>("ReceiveMessageFromGroup", ReceiveMessageFromGroup);
                try
                {
                    HubConnection.Start();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
        }

        private static void ReceiveMessageFromGroup(string arg1, string arg2, string arg3, string arg4, string arg5, string arg6)
        {
            OnWebSocketReceiver(new { idchatmessage = arg1, idgrupo = arg2, idproyecto = arg3, idusuario = arg4, message = arg5, now = arg6 }, Actions.ReceiveMessageFromGroup);
        }

        public static void VerifyHubConnection()
        {
            if (HubConnection.State == ConnectionState.Disconnected)
            {
                try
                {
                    HubConnection.Stop();
                    HubConnection.Start();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public static event EventHandler<WebSocketArgs> WebSocketReceiver;

        public static void OnWebSocketReceiver(dynamic receivedata, Actions actions)
        {
            if (WebSocketReceiver != null)
            {
                WebSocketReceiver(null, new WebSocketArgs { Data = receivedata, Action = actions });
            }
        }

        // 
        // idchat 		[Chat en database]
        // localidchat  [Id del chatlocal]
        // username 	[Nombre de quien esta enviando el mensaje]
        // message		[Mensaje]
        public static void ReceiveMessage(string idchat, string localidchat, string username, string message)
        {
            OnWebSocketReceiver(new { idchat, localidchat, username, message }, Actions.ReceiveMessage);
        }
    }
}