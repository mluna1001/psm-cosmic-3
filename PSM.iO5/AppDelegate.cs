﻿using Foundation;
using UIKit;
using ChatApp;
using ChatApp.iOS;
using TK.CustomMap.iOSUnified;
using Xamarin.Forms.Platform.iOS;

namespace PSM.iO5
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register ("AppDelegate")]
	public class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            app.StatusBarStyle = UIStatusBarStyle.LightContent;
            Xamarin.FormsMaps.Init();
            TKCustomMapRenderer.InitMapRenderer();
            App.ScreenWidth = (float)UIScreen.MainScreen.Bounds.Width;
            App.ScreenHeight = (float)UIScreen.MainScreen.Bounds.Height;

            //--> Inicia el scanner
            global::ZXing.Net.Mobile.Forms.iOS.Platform.Init();

            #region Notificaciones
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null
                );
                app.RegisterUserNotificationSettings(notificationSettings);
            }

            if (options != null)
            {
                if (options.ContainsKey(UIApplication.LaunchOptionsLocalNotificationKey))
                {
                    var localNotification = options[UIApplication.LaunchOptionsLocalNotificationKey] as UILocalNotification;
                    if (localNotification != null)
                    {
                        UIAlertController okayAlertController = UIAlertController.Create(localNotification.AlertAction, localNotification.AlertBody, UIAlertControllerStyle.Alert);
                        okayAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                        Window.RootViewController.PresentViewController(okayAlertController, true, null);
                        UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
                    }
                }
            }
            #endregion

            LoadApplication(new App());

            ChatConfig.Init = ChatInit.Init;
            ChatConfig.Send = ChatInit.Send;
            ChatConfig.SendToGroup = ChatInit.SendToGroup;
            ChatConfig.SetGrupos = ChatInit.SetGrupos;

            return base.FinishedLaunching(app, options);
        }

        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
            UIAlertController okayAlertController = UIAlertController.Create(notification.AlertAction, notification.AlertBody, UIAlertControllerStyle.Alert);
            okayAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
            Window.RootViewController.PresentViewController(okayAlertController, true, null);
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
        }
    }
}