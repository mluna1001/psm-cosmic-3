using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using System.Threading.Tasks;
using PCLStorage;
using System.IO;
using System.Net.Http;
using Xamarin.Forms.Platform.iOS;
using MediaPlayer;
using UIKit;
using Foundation;
using AVFoundation;

[assembly: ExportRenderer(typeof(PSM.Controls.MediaElement), typeof(PSM.iOS.Controls.MediaElement))]
namespace PSM.iOS.Controls
{
    public class MediaElement : ViewRenderer<PSM.Controls.MediaElement, UIView>
    {
        

        protected override async void OnElementChanged(ElementChangedEventArgs<PSM.Controls.MediaElement> e)
        {
            base.OnElementChanged(e);
            var element = Element as PSM.Controls.MediaElement;
            if (element != null && e.OldElement == null)
            {
                var file = await SaveFile(element);
                var native = CreateNativeControl(Control, element, file);
                SetNativeControl(native);
            }
        }

        private async Task<IFile> SaveFile(PSM.Controls.MediaElement element)
        {
            IFile file = null;
            if (element.Source.IsFile)
            {
                var path = element.Source.FilePath;
                var pathsplit = path.Split('/');
                var filename = pathsplit[pathsplit.Length - 1];
                try
                {
                    file = await PCLStorage.FileSystem.Current.LocalStorage.GetFileAsync(filename);
                }
                catch
                {

                }

                if (file != null)
                {
                    return file;
                }
                else
                {
                    file = await IFileFromUri(new Uri(path, UriKind.RelativeOrAbsolute));
                    return file;
                }
            }
            else if (element.Source.IsUri)
            {
                var path = element.Source.Uri.ToString();
                var pathsplit = path.Split('/');
                var filename = pathsplit[pathsplit.Length - 1];
                file = await IFileFromUri(element.Source.Uri);
                return file;
            }
            else
            {
                if (element.Source.Stream != null)
                {
                    if (await FileSystem.Current.LocalStorage.CheckExistsAsync(element.Source.TempFileName) == ExistenceCheckResult.NotFound)
                    {
                        file = await FileSystem.Current.LocalStorage.CreateFileAsync(element.Source.TempFileName, CreationCollisionOption.OpenIfExists);
                        using (var stream = await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite))
                        {
                            await element.Source.Stream.CopyToAsync(stream);
                            element.Source.Stream.Dispose();
                        }
                    }
                    else
                    {
                        file = await FileSystem.Current.LocalStorage.GetFileAsync(element.Source.TempFileName);
                    }
                }
            }

            return file;
        }

        private async Task<IFile> IFileFromUri(Uri uri)
        {
            var path = uri.ToString();
            var pathsplit = path.Split('/');
            var filename = pathsplit[pathsplit.Length - 1];
            IFile file = null;

            if (path.Contains("http"))
            {
                HttpClient client = new HttpClient();
                var data = await client.GetByteArrayAsync(path);
                if (await FileSystem.Current.LocalStorage.CheckExistsAsync(filename) == ExistenceCheckResult.NotFound)
                {
                    try
                    {
                        file = await PCLStorage.FileSystem.Current.LocalStorage.CreateFileAsync(filename, PCLStorage.CreationCollisionOption.OpenIfExists);
                        using(var stream = new MemoryStream(data))
                        {
                            await stream.CopyToAsync(await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite));
                        }
                    }
                    catch (Exception ex)
                    {
                        file = null;
                    }
                }
                else
                {
                    file = await FileSystem.Current.LocalStorage.GetFileAsync(path);
                }
            }
            else
            {
                if (System.IO.File.Exists(path))
                {
                    if (await FileSystem.Current.LocalStorage.CheckExistsAsync(filename) == ExistenceCheckResult.NotFound)
                    {
                        try
                        {
                            file = await PCLStorage.FileSystem.Current.LocalStorage.CreateFileAsync(filename, PCLStorage.CreationCollisionOption.OpenIfExists);
                            using (var tempfile = File.Open(path, FileMode.Open))
                            {
                                await tempfile.CopyToAsync(await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite));
                            }
                        }
                        catch
                        {
                            file = null;
                        }
                    }
                    else
                    {
                        file = await FileSystem.Current.LocalStorage.GetFileAsync(path);
                    }
                }
                else
                {
                    throw new Exception("No podemos obtener el archivo");
                }
            }
            return file;
        }

        
        private UIView CreateNativeControl(UIView view, PSM.Controls.MediaElement element, IFile file)
        {
            MPMoviePlayerController player = new MPMoviePlayerController(NSUrl.FromFilename(file.Path));
            view.AddSubview(player.View);
            return view;
        }
    }
}