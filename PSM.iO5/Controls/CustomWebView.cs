using Foundation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(PSM.iOS.Controls.CustomWebView), typeof(PSM.Controls.PdfElement))]
namespace PSM.iOS.Controls
{
    public class CustomWebView : ViewRenderer<PSM.Controls.PdfElement, UIWebView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<PSM.Controls.PdfElement> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                SetNativeControl(new UIWebView());
            }

            if (e.OldElement != null)
            {
                // Cleanup
            }

            if (e.NewElement != null)
            {
                var customWebView = Element as PSM.Controls.PdfElement;
                string fileName = Path.Combine(NSBundle.MainBundle.BundlePath, customWebView.Uri);
                Control.LoadRequest(new NSUrlRequest(new NSUrl(fileName, false)));
                Control.ScalesPageToFit = true;
            }
        }
    }
}