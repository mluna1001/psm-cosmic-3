﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

using Foundation;
using PSM.Helpers.ReadFile;
using PSM.ORM;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iO5.Controls.ReadFile))]
namespace PSM.iO5.Controls
{
    public class ReadFile : IReadFile
    {
        RespaldoBDUsuarioDto IReadFile.ReadFile(string nombreArchivo)
        {
            var result = new RespaldoBDUsuarioDto();

            string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, "psmultimate.db3");

            result.BasedeDatos = File.ReadAllBytes(path);
            result.Nombre = "psmultimateBack";

            return result;
        }

    }
}
