﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using PSM.Helpers.Services;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iO5.Services.StorageDevice))]
namespace PSM.iO5.Services
{
    public class StorageDevice : IStorageDevice
    {
        public string GetBatteryLevel()
        {
            return "";
        }

        public string GetFullStorageDevive()
        {
            return $"Almacenamiento: { NSFileManager.DefaultManager.GetFileSystemAttributes(Environment.GetFolderPath(Environment.SpecialFolder.Personal)).FreeSize } ";
        }
    }
}