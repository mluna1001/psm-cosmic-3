﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using PSM.Helpers.Services;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iO5.Services.CheckAutomaticTime))]
namespace PSM.iO5.Services
{
    public class CheckAutomaticTime : ICheckAutomaticTime
    {
        public bool IsAutomaticTime()
        {
            return true;
        }

        public bool IsAutomaticZone()
        {
            return true;
        }
    }
}