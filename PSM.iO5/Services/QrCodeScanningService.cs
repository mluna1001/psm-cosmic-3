﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foundation;
using PSM.Helpers.Services;
using UIKit;
using ZXing.Mobile;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iO5.Services.QrCodeScanningService))]
namespace PSM.iO5.Services
{
    public class QrCodeScanningService : IQrCodeScanningService
    {
        public async Task<string> ScanAsync()
        {
            var scanner = new MobileBarcodeScanner();
            var scanResults = await scanner.Scan();
            return (scanResults != null) ? scanResults.Text : string.Empty;
        }
    }
}