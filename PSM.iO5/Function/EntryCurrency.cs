
using Xamarin.Forms;
using PSM.Controls;
using Xamarin.Forms.Platform.iOS;
using UIKit;

[assembly: ExportRenderer(typeof(PSM.Controls.EntryCurrency),
                          typeof(PSM.Droid.Function.EntryCurrency))]
namespace PSM.Droid.Function
{
    public class EntryCurrency : ViewRenderer
    {

        UITextField Box;
        PSM.Controls.EntryCurrency Currency;

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.View> e)
        {
            base.OnElementChanged(e);
            Currency = Element as PSM.Controls.EntryCurrency;
            if (Currency != null && e.OldElement == null)
            {
                Box = new UITextField();
                Box.Layer.BorderColor = UIColor.LightGray.CGColor;
                Box.Layer.BorderWidth = 1f;
                if (!string.IsNullOrEmpty(Currency.Text))
                {
                    Box.Text = Currency.Text;
                }
                Box.Placeholder = "$0.00";
                Box.KeyboardType = UIKeyboardType.NumberPad;
                Box.EditingChanged += Box_ValueChanged;
                SetNativeControl(Box);
            }
        }

        private void Box_ValueChanged(object sender, System.EventArgs e)
        {
            Box.EditingChanged -= Box_ValueChanged;
            if (Box != null)
            {
                var text = Box.Text;
                if (!string.IsNullOrEmpty(text))
                {
                    var formatted = text.TextToMoney();
                    var currency = formatted.MoneyToText();
                    Currency.Text = formatted;
                    Currency.OnEntryCurrencyTextChanged(formatted, currency);
                    Box.Text = formatted;
                    //selection
                }
            }
            Box.EditingChanged += Box_ValueChanged;
        }

    }
}