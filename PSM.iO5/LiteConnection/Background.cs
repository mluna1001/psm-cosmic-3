﻿using PSM.ORM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;

[assembly: Dependency(typeof(PSM.iOS.LiteConnection.Background))]
namespace PSM.iOS.LiteConnection
{
    public class Background : IBackground
    {
        public ORM.Background GetDataBase()
        {
            string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, "iossyscheck.db3");
            return new ORM.Background(path);
        }
    }
}