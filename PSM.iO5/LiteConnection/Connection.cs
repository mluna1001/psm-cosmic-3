﻿
using System;
using System.IO;
using PSM.ORM;
using DevelopersAzteca.Storage.SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iOS.LiteConnection.Connection))]
namespace PSM.iOS.LiteConnection
{
    public class Connection : IPSMDataBase
    {
        public CosmicResponse GetDataBase()
        {
            string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, Keys.DataBaseName);

            CosmicResponse cosmicResponse = new CosmicResponse
            {
                Status = false,
                Message = "Existe"
            };

            if (File.Exists(path))
            {
                var psmdatabase = new PSMDataBase(path);
                cosmicResponse.Objeto = psmdatabase;
                cosmicResponse.Usuario = psmdatabase.Usuario;
                cosmicResponse.Status = true;
            }
            else
            {
                var psmdatabase = new PSMDataBase(path);
                cosmicResponse.Objeto = psmdatabase;
            }

            return cosmicResponse;
        }
    }
}
